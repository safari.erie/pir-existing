<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE , ANY');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization , accept');
header('Access-Control-Allow-Credentials: true');
Route::post('/registrasi', function() {
	$email = Request::input('email');
	$registrasi = Request::input('nama');
	Mail::to($email)->send(new App\Mail\RegistrasiEmail($registrasi));
	return 'Registrasi berhasil';
  });
Route::get('/asa', function () {
  return view('registrasi');
});


Route::get('/pir/{lang}', 'HomeController@home');
Route::get('/api/key', 'ApiController@listDemografi');
Route::get('/map-home', 'HomeController@map');
Route::get('/map-daerah', 'HomeController@mapdaerah');
Route::get('/map-infra', 'HomeController@mapinfra');
Route::get('/map-infranasional', 'HomeController@mapinfranasional');
Route::get('/key/analis/{token}', 'HomeController@keyanalis');
Route::get('/all-agent', 'HomeController@agents');
Route::get('/listing', 'HomeController@listing');
Route::get('/list-view', 'HomeController@list_properties');
Route::get('agent/properties/{id}/{name}', 'AgentsController@listing');
Route::get('/maplisting/{lang}', 'HomeController@mapListing');
Route::get('/chartinvestasi/{tahun}/{lang}', 'HomeController@realisasiinvestasi');
Route::get('/chartinvestasinasional/{id}/{tahun}', 'HomeController@realisasiinvestasinasional');
Route::get('/chartsector/{tahun}', 'HomeController@realisasisector');
Route::get('/chartsectordaerah/{daerah}/{tahun}/{lang}', 'HomeController@RealisasisectorDaerah');
Route::get('/chartkomoditidaerah/{daerah}/{tahun}/{lang}', 'HomeController@komoditiDaerah');
Route::get('/chartpdrbdaerah/{daerah}/{tahun}', 'HomeController@pdrbDaerah');
Route::get('/peluang/{lang}', 'HomeController@peluang');
Route::get('/dev_peluang/{lang}', 'HomeController@dev_peluang');
Route::get('/peluangdaerah/{lang}', 'HomeController@peluangdaerah');
Route::get('/property/{id}/{name}', 'HomeController@detaildaerah');
Route::get('/propertypro/{id}/{name}/{lang}', 'HomeController@detailprioritas');
Route::get('/dev_propertypro/{id}/{name}/{lang}', 'HomeController@dev_detailprioritas');
Route::get('/dev_map_propertypro/{id}/{name}/{lang}', 'HomeController@dev_map_detailprioritas');
Route::get('/propertyprofull/{id}/{name}', 'HomeController@detailprioritasfull');
Route::post('/send_request', 'HomeController@send_request');
Route::get('/getkota/{id}', 'HomeController@getKotaByProvinsi');
Route::get('/infraAPI/{id}', 'HomeController@infraAPI');
Route::get('/live_search', 'LiveSearch@index');
Route::get('/about', 'HomeController@abouts');
Route::get('/key', 'HomeController@key');
Route::get('/keyApi', 'HomeController@Apifunction');
Route::get('/holiday/{lang}', 'HomeController@holiday');
Route::get('/databandara', 'HomeController@databandara');
Route::get('/datapelabuhan', 'HomeController@datapelabuhan');
Route::get('/datapendidikan', 'HomeController@datapendidikan');
Route::get('/datarumahsakit', 'HomeController@datarumahsakit');
Route::get('/datahotel', 'HomeController@datahotel');
Route::get('/kirimemail', 'HomeController@kirimemail');
Route::get('/master/{lang}', 'HomeController@master');
Route::get('/profil/{lang}', 'HomeController@profil');
Route::get('/profil/detail/{id}/{lang}', 'HomeController@profildetail');
Route::get('/profil/detaildaerah/{id}/{lang}', 'HomeController@profildetaildaerah');
Route::get('/invest/detail/{id}/{lang}', 'HomeController@investdetail');
Route::get('/oppo/detail/{id}/{lang}', 'HomeController@oppodetail');
Route::get('/industri/detail/{id}/{lang}', 'HomeController@industridetail');
Route::get('/industrinasional/{lang}', 'HomeController@industrinasional');
Route::get('/potensial/detail/{id}/{lang}', 'HomeController@potensialdetail');
Route::get('/potensial/detail/{id}/{lang}', 'HomeController@potensialdetail');
Route::get('/infra/detail/{id}/{lang}', 'HomeController@infrastrukturdetail');
Route::get('/infranasional/{lang}', 'HomeController@infrastrukturnasional');
Route::get('/chart/{text}/{id}/{lang}', 'HomeController@chart');
Route::get('/chartkawasan/{text}/{id}', 'HomeController@chartkawasan');
Route::get('/chartkawasannasional/{text}/{id}', 'HomeController@chartkawasannasional');
Route::get('/allow/{lang}', 'HomeController@allow');
Route::get('/super/{lang}', 'HomeController@super');
Route::get('/contact', 'HomeController@contactz');
Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.action');
route::get('/product','admin\OrderController@vieworder')->name('vieworder');
route::get('/order','HomeController@mapListing');
Route::get('/cari', 'HomeController@contact');
Route::get('/contactprovinsi/{id}/{lang}', 'HomeController@contactprovinsi');
Route::get('/home', 'HomeController@home');
Route::get('/gallery', 'HomeController@gallery');
Route::get('/listingdaerah', 'HomeController@listingdaerah');
Route::get('/listingprioritas/{lang}', 'HomeController@listingprioritas');
Route::get('/dev_listingprioritas/{lang}', 'HomeController@dev_listingprioritas');
Route::get('localization/{locale}', 'LocalizationController@index');
route::get('/order/search/','HomeController@contact');
/// Social Login 
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');
//email
Route::get('/email', function () {
    return view('send_email');
});
Route::get('/emailz/{id}', 'HomeController@emailz');
Route::post('/sendEmail/{id}', 'HomeController@sendEmail');



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Home Controller */
Route::get('/', 'HomeController@index');
Route::get('download_admin/{id}/{file_name}', 'HomeController@download_admin');
Route::get('setLang/{lang}', 'HomeController@setLang');
Route::post('post_rate', 'HomeController@rate_post');
Route::get('download_guide/{id}', 'HomeController@download_guide');
Route::get('refresh_captcha', 'HomeController@refresh_captcha');

Route::get('gen_nswi/{tahun}', 'NswiController@genData');
Route::get('test', function() {
	echo "TEST";
});

Route::get('download/kawasan/{id_kawasan}/{file_name}', 'HomeController@download_kawasan');
Route::get('view_pdf/{id_kawasan}/{file_name}', 'HomeController@download_kawasan');


Route::auth();

Route::group(['middlewareGroups' => 'web'], function () {


		Route::get('/admin', 'DashboardController@index');
      
      
        Route::get('/verify_email', 'AgentsController@verifyEmail');
        Route::get('register/verify/{confirmationCode}', 'AgentsController@confirm');
        Route::post('agents/update_profile', 'AgentsController@updateProfile');
        Route::post('agents/password_reset/', 'AgentsController@passwordReset');
		
        Route::post('/postPayment', 'PropertyController@postPayment');
        Route::get('/listing/featured/{id}', 'PropertyController@featured');

        
        Route::get('/payments', 'PaymentController@index');
        Route::post('/paypalCallback', 'PaymentController@paypalCallback');
		
       
        Route::post('/newsletter_subscription', 'HomeController@newsletter_subscription');
		
		
		///
		
			Route::get('/menus', 'MenuController@index');
		Route::group(['prefix' => 'admin'], function () {
			Route::get('all-newsletter', 'NewsletterController@index');
			 Route::post('/property/fileupload', 'PropertyController@fileUpload');
       
			Route::get('/dashboard', 'DashboardController@index');
			Route::get('/customer-requests', 'DashboardController@customerRequets');
			Route::get('/profile', 'HomeController@profile');
		   

			Route::get('/agents', 'AgentsController@index');
			Route::post('/agents/save', 'AgentsController@save');
			Route::post('/agents/get', 'AgentsController@get');
			Route::post('/agents/delete', 'AgentsController@delete');
			

			Route::get('/properties', 'PropertyController@index');
							
			Route::get('/property/add', 'PropertyController@form');
			Route::get('/listing/delete/{id}', 'PropertyController@delete');
			Route::get('/listing/edit/{id}', 'PropertyController@edit');
			Route::get('/listing/image_delete/{id}', 'PropertyController@imageDelete');
			Route::post('/property/save', 'PropertyController@save');
			Route::post('property/addTofeatured', 'PropertyController@addToFeatured');
			Route::post('property/addToArchive', 'PropertyController@addToArchive');

			Route::get('/settings', 'SettingController@index');
			Route::post('/setting/save', 'SettingController@save');
			
			

			Route::get('/features', 'FeatureController@index');
			Route::post('/feature/save', 'FeatureController@save');
			Route::post('/feature/get', 'FeatureController@get');
			Route::post('/feature/delete', 'FeatureController@delete');

			Route::get('/gallery/upload', 'GalleryController@index');
			Route::post('/gallery/fileupload', 'GalleryController@fileUpload');
			Route::post('/gallery/removefile', 'GalleryController@removefile');
			
			/// Pages Controller 
			
			Route::get('/pages', 'PageController@index');
			Route::post('/pages/save', 'PageController@save');
			Route::get('/pages/add', 'PageController@add');
			Route::get('/pages/delete/{id}', 'PageController@delete');
			Route::get('/pages/edit/{id}', 'PageController@edit');

			/// Categories 

			Route::get('/categories', 'CategoryController@index');
			Route::post('/category/save', 'CategoryController@save');
			Route::post('/category/get', 'CategoryController@get');
			Route::post('/category/delete', 'CategoryController@delete');


			//// Slider 
			Route::get("/sliders" , 'SliderController@index');
			Route::post("slider/save" , 'SliderController@save');
			Route::post("slider/get" , 'SliderController@get');
			Route::post("slider/delete" , 'SliderController@delete');
			
			//// Testimonial 
			Route::get("/testimonials" , 'TestimonialController@index');
			Route::post("testimonial/save" , 'TestimonialController@save');
			Route::post("testimonial/get" , 'TestimonialController@get');
			Route::post("testimonial/delete" , 'TestimonialController@delete');
		
		});
    }
);

Route::get('foo', function () {
    return 'Hello World';
});
Route::group(array('prefix'=>'api'),function(){
	Route::resource('comments','HomeController/databandara',array('except'=>array('create','edit')));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
