<section id="maps">
	<div class="container">
		<div class="section-header">
		<h2>Peta</h2>
		</div>

		<div class="row">
			<div class="container demo-1">
				<div class="main">
					<ul id="carousel" class="elastislide-list">
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/gis/apps/MapJournal/index.html?appid=c36298c4ce09435bb8061e8f553480cb&edit=true" target="_blank" alt="Profile Daerah">
								<img class="thumb" src="<?php echo e($_ENV['APP_URL']); ?>/public/assets/img/maps/story_map.jpeg" alt="Profile Daerah" />
								<div id="title_thumb">Profile Daerah</div>
							</a>
						</li>
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/realisasiinvestasinasional" target="_blank" alt="Realisasi Investasi">
								<img class="thumb" src="<?php echo e($_ENV['APP_URL']); ?>/public/assets/img/maps/realisasi_investasi.jpeg" alt="Realisasi Investasi" />
								<div id="title_thumb">Realisasi Investasi</div>
							</a>
						</li>
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/industrinasional" target="_blank" alt="Kawasan Industri">
								<img class="thumb" src="<?php echo e($_ENV['APP_URL']); ?>/public/assets/img/maps/kawasan_industri.jpeg" alt="Kawasan Industri" />
								<div id="title_thumb">Kawasan Industri</div>
							</a>
						</li>
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/peluang" target="_blank" alt="Potensi">
								<img class="thumb" src="<?php echo e($_ENV['APP_URL']); ?>/public/assets/img/maps/peluang.jpeg" alt="Realisasi Investasi" />
								<div id="title_thumb">Peluang Investasi</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>