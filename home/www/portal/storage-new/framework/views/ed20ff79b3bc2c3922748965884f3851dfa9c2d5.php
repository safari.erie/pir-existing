<section id="rating" class="wow fadeInUp">
	<div class="container">
		<div class="section-header">
			<h2>Rating</h2>
		</div>
	</div>
</section>

<section id="rating_detail" class="wow fadeInUp">
	<div class="container">
		<div class="row info">
		<?php if($avgRating): ?>
			<?php $__currentLoopData = $avgRating; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rating): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		
			<div class="<?php echo e($rating['avg']['div_class']); ?>">
				<div class="info">
					<i class="<?php echo e($rating['icon']); ?>"></i>
					<h3><?php echo e($rating['name']); ?></h3>
					<div class='starrr'>
					<?php for($i = 1; $i <= 5; $i++): ?>
						<?php if($i <= $rating['avg']['avg']): ?>
							<span class="fa fa-star"></span>
						<?php else: ?>
							<span class="fa fa-star-o"></span>
						<?php endif; ?>
					<?php endfor; ?>
					</div>
				</div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php endif; ?>
		</div>
	</div>
</section>
<!--
<section id="rating_comment" class="wow fadeInUp">
	<div class="container">	
		<?php if($postRate): ?>
			<?php $__currentLoopData = $postRate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="row comment">
				<div class="col-lg-12">
					<div class="box wow fadeInLeft">
						<h1><?php echo e($pr->name); ?> <span class="date">[ <?php echo e($pr->date_inserted); ?> ]</span></h1>
						<blockquote>
							<div class="row info">
								<?php $__currentLoopData = $postRateDt[$pr->rating_post_id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-md-2">
									<i class="<?php echo e($dt->icon); ?>"></i>
									<h3><?php echo e($dt->rating_name); ?></h3>
									<div class='starrr'>
										<?php for($i = 1; $i <= 5; $i++): ?>
											<?php if($i <= $dt->rate): ?>
												<span class="fa fa-star star"></span>
											<?php else: ?>
												<span class="fa fa-star-o star"></span>
											<?php endif; ?>
										<?php endfor; ?>
									</div>
								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
							<p>" <?php echo e($pr->message); ?> "</p>
						</blockquote>
					</div>
				</div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php endif; ?>
	</div>
</section>
-->
<section id="rating_post">
	<div class="container">
		<?php if($hasRate <= 0): ?>
		<p>Berikan penilaian Anda terhadap Sistem Informasi Potensi Investasi Daerah (<b>SIPID</b>).</p>
		<div class="row">
			<div class="col-lg-6 content">
			<?php if($ratingList): ?>
				<table width="100%">
				<?php $__currentLoopData = $ratingList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td><i class="<?php echo e($rate->icon); ?>"></i><?php echo e($rate->name); ?></td>
						<td>
							<div class='starrr' id='star<?php echo e($rate->rating_id); ?>'></div>
						</td>
					</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</table>
			<?php endif; ?>
			</div>
			<div class="col-lg-6 content">
				<div class="form">
					<?php echo Form::open(['url' => 'post_rate', 'method' => 'post']); ?>

					<?php echo Form::hidden('session_id', ($session_id ? $session_id : 0) ); ?>

					<?php echo Form::hidden('ip', ($client_ip ? $client_ip : '') ); ?>

					
					
					<?php if($ratingList): ?>
						<?php $__currentLoopData = $ratingList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php echo Form::hidden('txt_star'.$rate->rating_id, '', ['id' => 'txt_star'.$rate->rating_id] ); ?>

						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
					
					<div class="form-group">
						<?php echo Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Nama']); ?>

					</div>
					<div class="form-group">
						<?php echo Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email']); ?>

					</div>
					<div class="form-group">
						<?php echo Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'Pesan', 'style' => 'height:100px']); ?>

					</div>
					<div class="form-group<?php echo e($errors->has('captcha') ? ' has-error' : ''); ?>">
						<div class="captcha">
							<span><?php echo captcha_img(); ?></span>
							&nbsp;
							<button type="button" class="btn btn-refresh"><i class="fa fa-refresh" style="padding:0px"></i></button>
						</div>
						<input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">

						<?php if($errors->has('captcha')): ?>
						<span class="help-block">
							<strong><?php echo e($errors->first('captcha')); ?></strong>
						</span>
						<?php endif; ?>
						
					</div>
					<div class="text-left"><button type="submit" class="btn">Kirim Pesan</button></div>
					<?php echo Form::close(); ?>

				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>