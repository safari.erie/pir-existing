<section id="guides" class="wow fadeInUp">
	<div class="container">
		<div class="section-header">
			<h2>Petunjuk</h2>
		</div>

		<div class="row">
			<div class="col-lg-3">
				<div class="box wow fadeInLeft">
					<a href="<?php echo e(Config::get('app.url')); ?>/download_guide/1">
						<i class="fa fa-book"></i>
						<h5 class="title">Profil Daerah</h5>
					</a>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="box wow fadeInLeft">
					<a href="<?php echo e(Config::get('app.url')); ?>/download_guide/2">
						<i class="fa fa-bullhorn"></i>
						<h5 class="title">Kawasan Industri</h5>
					</a>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="box wow fadeInLeft">
					<a href="<?php echo e(Config::get('app.url')); ?>/download_guide/3">
						<i class="fa fa-globe"></i>
						<h5 class="title">Peluang Investasi</h5>
					</a>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="box wow fadeInLeft">
					<a href="<?php echo e(Config::get('app.url')); ?>/download_guide/4">
						<i class="fa fa-bar-chart-o"></i>
						<h5 class="title">Realisasi Investasi</h5>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!--		
		<section id="guides">
		  <div class="container">
			<div class="section-header">
			  <h2>Petunjuk</h2>
			</div>

			<div class="row">

			  <div class="col-lg-6">
				<div class="box wow fadeInLeft">
				  <div class="icon"><i class="fa fa-bar-chart"></i></div>
				  <h4 class="title"><a href="">Profile Daerah</a></h4>
				</div>
			  </div>

			  <div class="col-lg-6">
				<div class="box wow fadeInRight">
				  <div class="icon"><i class="fa fa-picture-o"></i></div>
				  <h4 class="title"><a href="">Realisasi Investasi</a></h4>
				</div>
			  </div>

			  <div class="col-lg-6">
				<div class="box wow fadeInLeft" data-wow-delay="0.2s">
				  <div class="icon"><i class="fa fa-shopping-bag"></i></div>
				  <h4 class="title"><a href="">Kawasan Industri</a></h4>
				</div>
			  </div>

			  <div class="col-lg-6">
				<div class="box wow fadeInRight" data-wow-delay="0.2s">
				  <div class="icon"><i class="fa fa-map"></i></div>
				  <h4 class="title"><a href="">Peluang Investasi</a></h4>
				</div>
			  </div>

			  <div class="col-lg-6">
				<div class="box wow fadeInRight" data-wow-delay="0.2s">
				  <div class="icon"><i class="fa fa-map"></i></div>
				  <h4 class="title"><a href="">Sarana Prasarana</a></h4>
				</div>
			  </div>

			  <div class="col-lg-6">
				<div class="box wow fadeInRight" data-wow-delay="0.2s">
				  <div class="icon"><i class="fa fa-map"></i></div>
				  <h4 class="title"><a href="">Komoditi</a></h4>
				</div>
			  </div>

			</div>

		  </div>
		</section>-->