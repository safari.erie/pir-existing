<!DOCTYPE html>
<html lang="en">
<head>
	<!-- head -->
	<?php echo $__env->make('layouts.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>

<body id="body">
	<div id="preloader"></div>
	<!-- top bar -->
	<?php echo $__env->make('layouts.partials.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<!-- header -->
	<?php echo $__env->make('layouts.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<!-- intro -->
	<?php echo $__env->make('layouts.partials.intro', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<main id="main">
		<!-- maps -->
		<?php echo $__env->make('layouts.partials.map', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
		<!-- news feeder -->
		<?php echo $__env->make('layouts.partials.news_feeder', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		
		<!-- guide -->
		<?php echo $__env->make('layouts.partials.guide', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		
		<!-- rating -->
		<?php echo $__env->make('layouts.partials.rating', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		
    <!--contact-->
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Kontak Kami</h2>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Alamat</h3>
              <address>Jl. Jend. Gatot Subroto No. 44, Kebayoran Baru Jakarta 12190 P.O. Box 3186, Indonesia</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Telpon</h3>
              <p>021 5255284</p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p>sipid@bkpm.go.id</p>
            </div>
          </div>

        </div>
      </div>

    </section><!-- #contact -->
	
	<!--script src="//www.powr.io/powr.js?external-type=html"></script> 
 <div class="powr-rss-feed" id="c2912dbd_1535028924"></div-->

  </main>

	<!-- footer -->
	<?php echo $__env->make('layouts.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- script -->
	<?php echo $__env->make('layouts.partials.script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>
