<?php

return [
	'regionalmap' => 'Regional Industrial Estate Map',
	'profile' => 'Profil',
	'welcome' => 'Welcome to the Investment Coordinating Board',
	'investreali' => 'Investment Realization',
	'RegionalPotential' => 'Regional Potential',
	'investoppo' => 'Invest Opportunity',
	'indusestate' => 'Industrial Estate',
	'infra' => 'Infrastructure',
	'contact' => 'Contact',
	//header PIR
	'home' => 'Home',
	'regionalprof' => 'Regional Profile',
	'projectoppo' => 'Project Opportunity',
	'regionaloppo' => 'Regional Opportunity',
	'infra' => 'Infrastructure',
'submenuinfra' => 'Infrastructure',
'submenuindus' => 'Industrial estate and SEZ',
	'insentif' => 'Investment Incentive',
	'umkm' => 'UMKM',

	// home pir
'tentangpir' => 'About PIR',
'isi' => 'PIR is a geospatial based information system for investment potential and opportunities in 34 Provinces and 514 Regencies / Cities in Indonesia and is part of the website of the Investment Coordinating Board (BKPM). PIR contains information on regional profiles including demographic data, commodities, income and Regional Minimum Wages (UMR), as well as supporting infrastructure..',
'PotensiInvestasi' => 'Investment Opportunity',
	'Jelajahi Potensi ' => 'Explore Potential',
'peluangproyek' => 'Project Opportunity',
	'lihatdetail' => 'See More',
	'realisasiinvestasi' => 'Investment Realization by Sector',
'pilihtahun' => 'Select year',
'ProfilProvinsi' => 'Province Profile',
'Profildaerah' => 'Regional Profile',


	'RealisasiInvestasiP' => 'Investment Realization by Province',
	'LayananInvestasiOnline' => 'Service Online',

	// peluang proyek
'Pencarian_Lanjutan' => 'Advanced Searching',
'pencarian' => 'Searching',
'indonesia' => 'Indonesian',
	'Semua' => 'All',
'Industri' => 'Industry',
	'Infrastruktur' => 'Infrasctruture',
	'Pariwisata' => 'Tourism',
'jasa' => 'Services',
'pangan' => 'Food and Agriculture',

'cari' => 'Search',
'Peta' => 'Map Opportunity',
	'detail' => 'See more',
	'Layer' => 'Infrasctruture',
	'Pelabuhan' => 'Sea Port',
'rumah_sakit' => 'Hospital',
'Pendidikan' => 'Education',
'estimasi' => 'Invest Estimate',

// detail proyek
'sektor' => 'Sector',
'proyek' => 'Project',
'kota' => 'City',
	'luas' => 'Land Area',
'tahun' => 'Year',
	'nilai' => 'Investment',
'investasi' => 'Investment',
	'sumber' => 'Source',
'provinsi' => 'Province',

// detail profil
'peta' => 'Profile Map',
'peluang' => 'Investment Regional',
'sektor' => 'Investment Realization By Sector',
	'komoditi' => 'Regional Commodity',
	'pertumbuhan' => 'Population growth',
'umr' => 'Regional Minimum Wage',
	'bruto' => 'Constant gross price domestic product',
'jumlah' => 'Amount of infrastructure',

//chart series penduduk
'wanita' => 'Female',
'pria' => 'Male',

// infrastruktur
'nama_bandara' => 'Name',
'kelas_bandara' => 'Class',
'lokasi_peta' => 'Location',
'nama_pelabuhan' => 'Name',
'kelas_pelabuhan' => 'Class',
'lokasi_peta_pelabuhan' => 'Location',
'nama_hotel' => 'Name',
'kelas_hotel' => 'Class',
'lokasi_peta_hotel' => 'Location',
'nama_perguruan_tinggi' => 'Name',
'kategori_perguruan_tinggi' => 'Category',
'lokasi_perguruan_tinggi' => 'Location',
'nama_rumah_sakit' => 'Name',
'kategori_rumah_sakit' => 'Category',
'lokasi_rumah_sakit' => 'Location',




'infrastruktur_nasional' => 'National Infrastructure', 
'bandara' => 'Airport',
'pelabuhan' => 'Port',
'perguruan_tinggi' => 'University',
'rumah_sakit' => 'Hospital',

//kawasan industri
'blok_kawasan' => 'Industrial Estate and Blok Site Plan',
'nama_kawasan_industri_blok' => 'Name',
'kategori_blok' => 'Category',
'lihat_peta_blok' => 'Location',
'kawasan_industri' => 'Industrial Estate',
'nama_kawasan_industri' => 'Name',
'kategori_kawasan_industri' => 'Category',
'lihat_peta_kawasan_industri' => 'Location',
'kawasan_ekonomi_khusus' => 'Special Economic Zone',
'nama_kek'=> 'Name',
'kategori_kek' => 'Category',
'lihat_peta_kek' => 'Location',

 

// chartkomoditidaerah
'komoditiregional' => 'Regional Commodity',
'komoditiz' => 'Commodity',
'total' => 'Total Production',

// footer
'1' => 'Project Opportunity',
'2' => 'Regional Opportunity',
'3' => 'infrastructure',
	
'4' => 'Industrial Estate And SEZ',
	
'5' => 'Investment Incentive',
	'6' => 'Online investment services',

// peluang
'prioritas' => 'Priority',

'estimasi' => 'investment estimate',
		'ribu' => 'Thousand',
'juta' => 'Million',

'provinsi' => 'Province',
'asektor' => 'Sector',
'peluanginvestasi' => 'Investment Opportunity',
'semuaprovinsi' => 'All Province',
'semuakotakab' => 'All Districts',
'nilaiinvestasi' => 'Investment Value',
'semuasektor' => 'All Sectors',
'industri' => 'Industry',
'infrastruktur' => 'Infrastructure',
'smelter' => 'Smelter',
'pariwisata' => 'Tourism',
'totalnilaiproyek' => 'Total Cost Projects',
'kabupaten' => 'District',
'miliar' => 'Billion',
'juta' => 'Million',
'triliun' => 'Trillion',
'deskripsipir' => 'PIR is a geospatial based information system for investment potential and opportunities in 34 Provinces and 514 Regencies / Cities in Indonesia and is part of the website of the Investment Coordinating Board (BKPM). PIR contains information on regional profiles including demographic data, commodities, income and Regional Minimum Wages (UMR), as well as supporting infrastructure.',
'helpfullinks' => 'Helpful Links',
'hubungikami' => 'Contact Us',
'phone' => 'Phone',
'pelabuhan' => 'Port',
'rumahsakit' => 'Hospital',
'pendidikan' => 'School',
'overview' => 'Overview',
'gallery' => 'Gallery',
'infopeluang' => 'Information',
'kontak' => 'Contact',
'detaildokumen' => 'Detail Document',
'detaildokumenhint' => 'Please enter your data, download link will be sent via E-Mail. ',
'namalengkap' => 'Full Name',
'telepon' => 'Phone',
'negara' => 'Country',
'kirimemail' => 'Send E-Mail',
'kirimemail' => 'Kirim E-Mail',
'showmore' => 'Show More',
'showless' => 'Show Less',
'luasarea' => 'Total Area',
'lokasi' => 'Location',



];
