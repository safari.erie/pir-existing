<script>

  

@foreach($projecs as $list)
function bandara{{$list->id_bandara}}() {
@if($list->y == '')

@else
var center{{$list->id_bandara}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
@endif

          document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
   map.centerAndZoom(center{{$list->id_bandara}} ,17);
   var content ='<div style="float:left;"  >Kategori Bandara :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               kategori}}</dd> <div style="float:left;"  >Nama Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Kelas Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               kelas}}</dd> <div style="float:left;"  >Jam Operasional :   </div><dd style="margin-left:100px;  "class="">{{$list->jam_operasional}}</dd>   '
         
                  point = esri.geometry.geographicToWebMercator(center{{$list->id_bandara}});
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Transportation/esriDefaultMarker_113_Blue.png", 21, 21);
                  pointInfoTemplate = new esri.InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  bandara = new esri.layers.GraphicsLayer();
                  bandara.add(graphic);
                  map.addLayer(bandara);
                  removeLayer(layer1)
}
@endforeach


@foreach($pelabuhan as $list)
function pelabuhan{{$list->id_pelabuhan}}() {
@if($list->y == '')

@else
var center{{$list->id_pelabuhan}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
@endif

          document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
   map.centerAndZoom(center{{$list->id_pelabuhan}} ,17);
   var content ='<div style="float:left;"  >Name :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Category :   </div><dd style="margin-left:70px;  "class="">{{$list->
               kelas}}</dd> '
         
                  point = esri.geometry.geographicToWebMercator(center{{$list->id_pelabuhan}});
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Marina.png", 21, 21);
                   pointInfoTemplate = new esri.InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  pelabuhan = new esri.layers.GraphicsLayer();
                  pelabuhan.add(graphic);
                  map.addLayer(pelabuhan);
                  removeLayer(layer1)
}
@endforeach


@foreach($hotel as $list)
function hotel{{$list->id_hotel}}() {
@if($list->y == '')

@else
var center{{$list->id_hotel}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
@endif

          document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
   map.centerAndZoom(center{{$list->id_hotel}} ,17);
   var content ='<div style="float:left;"  >Name :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Category :   </div><dd style="margin-left:70px;  "class="">{{$list->
               kelas}}</dd> '
         
                  point = esri.geometry.geographicToWebMercator(center{{$list->id_hotel}});
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/OCHA/Infrastructure/infrastructure_hotel_bluebox.png", 21, 21);
                   pointInfoTemplate = new esri.InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  hotel = new esri.layers.GraphicsLayer();
                  hotel.add(graphic);
                  map.addLayer(hotel);
                  removeLayer(layer1)
}
@endforeach


@foreach($pendidikan as $list)
function pendidikanz{{$list->id_pendidikan}}() {
@if($list->y == '' )

@else
var center{{$list->id_pendidikan}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
@endif

          document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
   map.centerAndZoom(center{{$list->id_pendidikan}} ,17);
   var content =' <div style="float:left;"  >Educational Name :   </div><dd style="margin-left:100px;  "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Educational Category :   </div><dd style="margin-left:100px;  "class="">{{$list->
               kategori}}</dd>'

                  point = esri.geometry.geographicToWebMercator(center{{$list->id_pendidikan}});
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/College-University.png", 21, 21);
                 pointInfoTemplate = new esri.InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphicz = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  pendidikanz = new esri.layers.GraphicsLayer();
                  pendidikanz.add(graphicz);
                  map.addLayer(pendidikanz);
                  
}
@endforeach

@foreach($rumahsakit as $list)
function rumahsakit{{$list->id_rumah_sakit }}() {
@if($list->y == '' )

@else
var center{{$list->id_rumah_sakit}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
@endif

          document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
   map.centerAndZoom(center{{$list->id_rumah_sakit}} ,17);
   var content =' <div style="float:left;"  >Hospital Name :   </div><dd style="margin-left:100px;  "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Hospital Category :   </div><dd style="margin-left:100px;  "class="">{{$list->
               kategori}}</dd>'

                  point = esri.geometry.geographicToWebMercator(center{{$list->id_rumah_sakit}});
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/First-Aid.png", 21, 21);
                pointInfoTemplate = new esri.InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphicz = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  rumahsakit = new esri.layers.GraphicsLayer();
                  rumahsakit.add(graphicz);
                  map.addLayer(rumahsakit);
                  
}
@endforeach
</script>