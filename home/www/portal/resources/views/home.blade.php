

<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
-->



<!DOCTYPE html>
<html>

<!-- Head -->
<head>

    <title>Waiting Launch App </title>

    <!-- For-Mobile-Apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Countdown Timer Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps -->

    <!-- Style -->
        <link href="https://p.w3layouts.com/demos/july-2016/15-07-2016/countdown_timer_widget/web/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://p.w3layouts.com/demos/july-2016/15-07-2016/countdown_timer_widget/web/css/style.css" type="text/css"/>
    <!-- //Style -->

    <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <!-- Fonts -->

</head>
<!-- //Head -->

<!-- Body -->
<body>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="https://m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>

<!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
<div id="codefund"><!-- fallback content --></div>
<script src="https://codefund.io/properties/441/funder.js" async="async"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->



<script async src='/js/autotrack.js'></script>

<meta name="robots" content="noindex">
<body><link rel="stylesheet" href="https://p.w3layouts.com/images/demobar_w3_4thDec2019.css">
	<!-- Demo bar start -->
 


<!---728x90--->


    <div class="container">
        <h1>Launch App </h1>
        <div class="clear-loading spinner">
            <span></span>
        </div>
    </div>

    <div class="wrapper">
        <h2>Waiting!</h2>
        <h3>We are modifying our New website. <span class="sub-message">We'll be back online in :</span></h3>

        <div class="clock">
            <div class="column days">
                <div class="timer" id="days"></div>
                <div class="text">DAYS</div>
            </div>
            <div class="timer days">:</div>
            <div class="column">
                <div class="timer" id="hours"></div>
                <div class="text">HOURS</div>
            </div>
            <div class="timer">:</div>
            <div class="column">
                <div class="timer" id="minutes"></div>
                <div class="text">MINUTES</div>
            </div>
            <div class="timer">:</div>
            <div class="column">
                <div class="timer" id="seconds"></div>
                <div class="text">SECONDS</div>
            </div>
        </div>
    </div>

<!---728x90--->


    <div class="footer">
        <p> &copy; 2020 Launch App. All Rights Reserved | Design by BKPM</a></p>
    </div>

<!---728x90--->


    <!-- Custom-JavaScript-File-Links -->
        <script  src="https://p.w3layouts.com/demos/july-2016/15-07-2016/countdown_timer_widget/web/js/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://p.w3layouts.com/demos/july-2016/15-07-2016/countdown_timer_widget/web/js/moment.js"></script>
        <script type="text/javascript" src="https://p.w3layouts.com/demos/july-2016/15-07-2016/countdown_timer_widget/web/js/moment-timezone-with-data.js"></script>
        <script>$(function(){
    function timer(settings){
        var config = {
            endDate: '2022-12-30 19:45',
            newDate: '2022-03-30 19:45',
            timeZone: 'Asia/Jakarta',
            hours: $('#hours'),
            minutes: $('#minutes'),
            seconds: $('#seconds'),
            newSubMessage: 'and should be back online in a few minutes...'
        };
        function prependZero(number){
            return number < 10 ? '0' + number : number;
        }
        $.extend(true, config, settings || {});
        var currentTime = moment();
        var endDate = moment.tz(config.endDate, config.timeZone);
        var newDate = moment.tz(config.newDate, config.timeZone);
        var diffTime = endDate.valueOf() - currentTime.valueOf();
        var duration = moment.duration(diffTime, 'milliseconds');
        var days = duration.days();
        var interval = 1000;
        var subMessage = $('.sub-message');
        var clock = $('.clock');
        if(diffTime < 0){
            endEvent(subMessage, config.newSubMessage, clock);
            return;
        }
        if(days > 0){
            $('#days').text(prependZero(days));
            $('.days').css('display', 'inline-block');
        }
        var intervalID = setInterval(function(){
            duration = moment.duration(duration - interval, 'milliseconds');
            var hours = duration.hours(),
                minutes = duration.minutes(),
                seconds = duration.seconds();
            days = duration.days();
            if(hours  <= 0 && minutes <= 0 && seconds  <= 0 && days <= 0){
                clearInterval(intervalID);
                endEvent(subMessage, config.newSubMessage, clock);
                window.location.reload();
            }
            if(days === 0){
                $('.days').hide();
            }
            $('#days').text(prependZero(days));
            config.hours.text(prependZero(hours));
            config.minutes.text(prependZero(minutes));
            config.seconds.text(prependZero(seconds));
        }, interval);
    }
    function endEvent($el, newText, hideEl){
        $el.text(newText);
        hideEl.hide();
    }
    timer();
});</script>
    <!-- //Custom-JavaScript-File-Links -->

</body>
<!-- //Body -->

</html>

