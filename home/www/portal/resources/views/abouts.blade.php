

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
<title>Layer List Dijit</title>
<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

<style>
html, body, .container, #map {
    height:100%;
    width:100%;
    margin:0;
    padding:0;
    margin:0;
    font-family: "Open Sans";
}
#map {
    padding:0;
}
#layerListPane{
    width:25%;
}
.esriLayer{
  background-color: #fff;
}
.esriLayerList .esriList{
    border-top:none;
}
.esriLayerList .esriTitle {
  background-color: #fff;
  border-bottom:none;
}
.esriLayerList .esriList ul{
  background-color: #fff;
}
</style>
<script>var dojoConfig = { parseOnLoad: true };</script>
<script src="https://js.arcgis.com/3.34/"></script>
<script>
require([
     "esri/arcgis/utils",
 "esri/dijit/LayerList",
  "esri/layers/FeatureLayer",
  "dojo/domReady!"
], function(
      arcgisUtils,
Layerlist,
FeatureLayer
  
) {

   var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"]

               
            });
        map.addLayer(pelabuhan);

    //Create a map based on an ArcGIS Online web map id
    arcgisUtils.createMap("f1027161650b4741a2482df6e975908c", "map").then(function(response){
        var myWidget = new LayerList({
           map: response.map,
           layers: arcgisUtils.getLayerList(response)
        },"layerList");
        myWidget.startup();
    });
             
});
</script>
</head>
<body class="claro">
<div class="container" data-dojo-type="dijit/layout/BorderContainer"
data-dojo-props="design:'headline',gutters:false">
<div id="layerListPane" data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'right'">
    <div id="layerList"></div>
</div>
<div id="map" data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'center'"></div>
</div>
</body>
</html>
