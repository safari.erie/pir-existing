    @extends('frontend.layoutmap')

@section('content')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" crossorigin=""></script>
 <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin=""></script>
 <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"  crossorigin=""></script>
 <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"  crossorigin=""></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" crossorigin=""/>
   <link rel="stylesheet" href="tps://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"  crossorigin=""/>
  
<style>
.dataTables_filter input {
 width: 455px; 
left:0px;
}
</style>

<div class="row">
  <div class="col-sm-3"> <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

     <!--ITEMS LISTING
            =========================================================================================================-->

      <div  class="zink">

         <!--Display selector on the left-->
         <div align="center" class="">

             <a id="ts-display-list" class="btn">
                 Insentif Investasi
             </a>
             <br>
                  <a href="{{Config::get('app.url')}}/holiday/{{ Request::segment(2) }} "> <div align="center"  style="margin-top:10px; color:orange; " class="border-bottom pb-2">Tax Holiday</div></a>
            <a href="{{Config::get('app.url')}}/allow/{{ Request::segment(2) }} "> <div align="center" style="margin-top:10px;  color:white;" class="border-bottom pb-2">Tax Allowance</div></a>
           <a href="{{Config::get('app.url')}}/master/{{ Request::segment(2) }} ">  <div align="center" style="margin-top:10px; color:white; " class="border-bottom pb-2">Masterlist</div></a>
           <a href="{{Config::get('app.url')}}/super/{{ Request::segment(2) }} ">  <div align="center" style="margin-top:10px; color:white;" class="border-bottom pb-2">Super Deduction</div></a>
                <br>
         </div>

         <!--Display selector on the right-->


     </div>
  
  
     <!--end container-->
 </section></div>
  <div class="col-sm-9">  <section id="tbody" class="ts-box p-1 long" style="margin-top:10px; margin-left:10px; margin-right:10px;">
  

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="clearfix zink hijau">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 TAX Holiday
             </a>
         </div>

    


     </div>
     <br>
     <div align="center">
     <img alt=""  src="{{Config::get('app.url')}}/folderimage/holiday1.jpg" width="100%">
     </div>
     <br>
    <dt style="width:auto; margin-top:20px;"class="zink">Tax Holiday Procedure (1)</dt>
    <img alt=""  src="{{Config::get('app.url')}}/folderimage/alurholiday1.jpg" width="100%">

  <br>
<div class="col-sm-12" style="">
<table id="example" class="table table-striped table-bordered" style="pading:20px; width:100%;">
        <thead>
            <tr align="center">
                <th width="10px">No</th>
                <th width="20px">KBLI</th>
                <th width="40px">Bidang Usaha</th>
                          </tr>
        </thead>
        <tbody>
@foreach($kbli as $list)
            <tr >
                <td align="center" width="10px"></td>
                <td align="center" width="20px">{{$list->id_kbli}}</td>
                <td >{{$list->bidang_usaha}}</td>
                          </tr>
@endforeach
                    </tbody>
           </table>
</div>
</div>

<div >


     <!--end container-->
 </section>

</div>
</div>
<script>
</script>
<script>
$(document).ready(function() {
   var t = $('#example').DataTable();
  t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

    const postDetails = document.querySelector(".long");
    const postSidebar = document.querySelector(".when");
    const postDetailsz = document.querySelector(".ts-boxzzs");
    const postSidebarContent = document.querySelector(".ts-bosxzz > div");


    const controller = new ScrollMagic.Controller();


    const scene = new ScrollMagic.Scene({
        triggerElement: postSidebar,
        triggerHook: 0,
        duration: getDuration
    }).addTo(controller);

    //3
    if (window.matchMedia("(min-width: 768px)").matches) {
        scene.setPin(postSidebar, {
            pushFollowers: false
        });
    }

    //4
    window.addEventListener("resize", () => {
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        } else {
            scene.removePin(postSidebar, true);
        }
    });

    function getDuration() {
        return postDetails.offsetHeight - postSidebar.offsetHeight;
    }
</script>

<script>
  var LeafIconz = L.Icon.extend({
    options: {
     
        iconSize:     [25, 36],
        
    }
});
var  office = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=industry&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
	var mymap = L.map('mapid').setView([-6.2268898,106.8160027,15], 17);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);

	L.marker([-6.2268898,106.8160027,15], {icon: office}).addTo(mymap);





</script>

 
    @endsection