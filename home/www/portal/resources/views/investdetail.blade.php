    @extends('frontend.layoutmap')

    @section('content')

   
    <style type="text/css">
        g[class^='raphael-group-'][class$='-creditgroup'] {
            display: none !important;
        }
    </style>
    <style type="text/css">
        g[class$='creditgroup'] {
            display: none !important;
        }
    </style>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

    <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
    <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />


    <!-- Load Esri Leaflet from CDN -->


    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

                <div id="mydatas"></div>

                <!--end container-->
            </section>
        </div>
        <br>
        <div class="col-sm-9">
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix zink hijau">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            Realisasi Investasi
                        </a>
                    </div>




                </div>
               
            
                         
            </section>

           
         
        
            <div class="clearfix  btn-pro titlez">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="">
        Realisasi Investasi Per Provinsi
    </a>

    <div class="float-none float-sm-right pl-2 ts-



ter__vertical ">
<a>Pilih Tahun</a>&nbsp;
        <select style="color:grey; width:100px;" class="custom-select" id="sectorz" name="sorting">
        <option value="2020">2020</option>
            <option selected value="2019">2019</option>
            <option value="2018">2018</option>
            <option value="2017">2017</option>
            <option value="2016">2016 </option>



        </select>
    </div>
</div>

<!--Display selector on the right-->



</div>
<br>

<div id="chart2"></div>


<!--end container-->



                <!--end container-->
            </section>

          

</div>

        </div>

        <div>




            <!--end container-->
            </section>

        </div>
    </div>
    <script src="{{ url('assets/js/custom.js') }}"></script>
    <script src="https://127.0.0.1/map/html/assets/js/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script><br>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-apexcharts@1.3.6/dist/react-apexcharts.iife.min.js">
    </script>

    <script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Include fusioncharts core library file -->
    <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
    <!-- Include fusion theme file -->
    <script type="text/javascript"
        src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
    <!-- Include fusioncharts jquery plugin -->
    <script type="text/javascript"
        src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
    </script>
       <script>
        $(document).ready(function () {
     $.get("{{ Config::get('app.url') }}/chart/{{$pages}}/{{ Request::segment(3) }}/{{ Request::segment(4) }}", function (data) {
           
            $("#mydatas").append(data);




        });
            $.get("{{ Config::get('app.url') }}/chartsectordaerah/{{ Request::segment(3) }}/2019", function (data) {
                $("#chart2").html(data);
               
              
            });

            $('#sectorz').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartsectordaerah/{{ Request::segment(3) }}/"+tahun+"", function (data) {
            $("#chart2").html(data);
           
          
        });
});
           
        });
    </script>

    <body>
        
        <script>
            const postDetails = document.querySelector(".long");
            const postSidebar = document.querySelector(".when");
            const postDetailsz = document.querySelector(".ts-boxzzs");
            const postSidebarContent = document.querySelector(".ts-bosxzz > div");


            const controller = new ScrollMagic.Controller();


            const scene = new ScrollMagic.Scene({
                triggerElement: postSidebar,
                triggerHook: 0,
                duration: 400
            }).addTo(controller);

            //3
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            }

            //4
            window.addEventListener("resize", () => {
                if (window.matchMedia("(min-width: 768px)").matches) {
                    scene.setPin(postSidebar, {
                        pushFollowers: false
                    });
                } else {
                    scene.removePin(postSidebar, true);
                }
            });

            function getDuration() {
                return postDetails.offsetHeight - postSidebar.offsetHeight;
            }
        </script>



        <script src="https://js.arcgis.com/3.34/"></script>

        <script>
            require([
                    "esri/map",
                    "esri/layers/FeatureLayer",
                    "dojo/domReady!"
                ],
                function (
                    Map,
                    FeatureLayer
                ) {

                     @foreach($kota as $list){
map = new Map("map", {
  basemap: "hybrid",
  center: [{{$list->y}}, {{$list->x}}],
  zoom: 6
});
}
@endforeach
                    /****************************************************************
                     * Add feature layer - A FeatureLayer at minimum should point
                     * to a URL to a feature service or point to a feature collection 
                     * object.
                     ***************************************************************/

                    // Carbon storage of trees in Warren Wilson College.
                    var featureLayer = new FeatureLayer(
                        "https://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/Landscape_Trees/FeatureServer/0"
                    );

                    map.addLayer(featureLayer);

                });
        </script>
    
       
        
        @endsection