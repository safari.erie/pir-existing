
@extends('frontend.layoutmap_peluangproyek')

@section('head')

<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

<link rel="stylesheet" href="{{Config::get('app.url')}}/assets/peluang_proyek/css/plugins/bootstrap-switch-3.3.4/dist/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="{{Config::get('app.url')}}/assets/peluang_proyek/css/plugins/skdslider/skdslider.css">

<style>
#map {   
    height: 500px !important;
	padding: 0px  !important;
}

.kpi-value{
	font-size: 2vw;
	font-weight: bold;
	display: block;
	color: #FFFFFF;
}
.kpi-title{
	font-size: 1vw;
	font-weight: 300;
	text-transform: uppercase;
	align-self: end;
    color: #FFFFFF;
}

.yearsummary h3{
font-size: 2.5vw;
}

#base{
	top:18px !important;
	display:none;
}

.skdslider .slide-desc {
    color: #FFFFFF;
    left: 0%;
    padding: 0 15px 0 15px;
    position: absolute;
    bottom: 22%;
    width: 100%;
    display: inline-block;
}

.slide-desc h3{
    color: #FFFFFF;
}

#sliderSummary{
	height: 25vh;
}

@media only screen and (max-width: 768px) {
	.kpi-value{
	line-height: 0 !important;
}
.kpi-title{
	line-height: 0 !important;
}
#sliderSummary{
	height: 15vh !important;
}
  }


</style>

@endsection


@section('scripts')

<script src="https://js.arcgis.com/3.34/"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/js/ion.rangeSlider.min.js"></script>-->
<script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
<script src="{{Config::get('app.url')}}/assets/peluang_proyek/css/plugins/skdslider/skdslider.js"></script>
<script>

$(document).ready(function() {
    const postDetails = document.querySelector(".hasil");

    const postSidebar = document.querySelector(".pencarian");
    const postDetailsz = document.querySelector(".ts-boxzzs");
    const postSidebarContent = document.querySelector(".ts-bosxzz > div");

    function getDuration() {
        return (postDetails.offsetHeight - postSidebar.offsetHeight) + 500;
    }
	
    // LegendLayer
    $('#industri').change(function() {
        if (this.checked)
            $('#LegendIndustri').show();
        else
            $('#LegendIndustri').hide();

    });
    $('#infra').change(function() {
        if (this.checked)
            $('#LegendInfrastruktur').show();
        else
            $('#LegendInfrastruktur').hide();

    });

    $('#pangan').change(function() {
        if (this.checked)
            $('#LegendPangan').show();
        else
            $('#LegendPangan').hide();

    });

    $('#jasa').change(function() {
        if (this.checked)
            $('#LegendJasa').show();
        else
            $('#LegendJasa').hide();

    });
    $('#base').hide();
    $('#layerhidden').hide();
	
    $("#layerclick").click(function() {
        $('#layerhidden').toggle();
        $('#layeron').toggleClass("layer-pressed");
    });
	
    $("#baseclick").click(function() {
        $('#base').toggle();
        $('#baseon').toggleClass("layer-pressed");
    });
    $('#layer').hide();
    $('#legendlayer').hide();
    $('#provinsi').change(function(e) {
        $.ajax({
            url: "{{ Config::get('app.url') }}/getkota/" + $(this).val(),
            method: 'GET',
            success: function(data) {
                console.log(data);

                $('#kota').children('option:not(:first)').remove().end();

                $.each(data, function(index, kotaObj) {

                    $('#kota').append(' <option value = "' + kotaObj.id_daerah + '">  ' + kotaObj.bentuk_daerah +' ' + kotaObj.nama + ' </option>')
                });
            }
        });
    });

    $('.aku').on('click', function() {
        $('.collapse').collapse();
    })

    var sing = $.get("{{ Config::get('app.url') }}/map-home?keywords=", function(data) {
        $("#mydatas2").html(data);
    });

    setTimeout(function() {
        $('body').addClass('loaded');
        $('h1').css('color', '#FFFFF');
    }, 2000);
    $("#gelo").click(function() {
        $("#loader-wrapper").show();
		$sectorDdl = $('#filter-sector');
		
        var industri = [];
        var infra = [];
        var pariwisata = [];
        var smelter = [];
        var pangan = [];
        var jasa = [];
		
		var selectedSector = $sectorDdl.chosen().val();
		
		//diakalin kudu di ganti status checkbox nya
		//karena script map dari db (gw ga ada akses buat ngerubah coy)
		
		$( "#industri" ).prop( "checked", false );
		$( "#infra" ).prop( "checked", false );
		$( "#smelter" ).prop( "checked", false );
		$( "#pariwisata" ).prop( "checked", false );
		
		$sectorDdl.find('option').each(function(){
			var optionVal = $(this).attr('value');
			var sectorVal = $(this).attr('data-sectorval');
			if(optionVal !== '')
			{
				if(selectedSector.includes(optionVal) || selectedSector.length == 0)
				{
					if(optionVal == 'industri')
					{
						industri.push(sectorVal);
						$( "#industri" ).prop( "checked", true );
					}
					if(optionVal == 'infrastruktur')
					{
						infra.push(sectorVal);
						$( "#infra" ).prop( "checked", true );
					}
					if(optionVal == 'smelter')
					{
						smelter.push(sectorVal);
						$( "#smelter" ).prop( "checked", true );
					}
					if(optionVal == 'pariwisata')
					{
						pariwisata.push(sectorVal);
						$( "#pariwisata" ).prop( "checked", true );
					}
				}
			}
		});
		
		
        var str = $("#keywords").val();
        var min = $("#filter-min").val();
        var max = $("#filter-max").val();
        var prov = $("#provinsi").val();
        var kota = $("#kota").val();

        $.get("{{ Config::get('app.url') }}/map-home?keywords=" + str + "&category=" + kota + "&type=" + prov +
            "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
            "&pangan=" + pangan + "&jasa=" + jasa + "&pariwisata=" + pariwisata + "",

            function(data) {
                $("#mydatas2").html(data);
            });
        $.get("{{ Config::get('app.url') }}/dev_listingprioritas/en?keywords=" + str + "&category=" + kota + "&type=" + prov +
            "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
            "&pangan=" + pangan + "&jasa=" + jasa + "&pariwisata=" + pariwisata + "&smelter=" + smelter + "&lang={{$lang}}",
            function(data) {
                $("#list-project").html(data);

            });

    });


});
$("[name='my-checkbox']").bootstrapSwitch({

    onSwitchChange: function(event, state) {

        if (state) map.addLayer(wildfireRisk);
        else map.removeLayer(wildfireRisk);
    }
});

$('[data-toggle="checkbox-switch"]').bootstrapSwitch({

    onSwitchChange: function(event, state) {
		//console.log('checkbox change', state);
		//$(this).attr("checked", $(this).is(":checked") == true );
		$(this).trigger("change");
    }
});

$("[name='industsdri']").bootstrapSwitch({

    onSwitchChange: function(event, state) {
    }
});


$("[name='dadan']").bootstrapSwitch({
    onSwitchChange: function(event, state) {

        if (state) {
            map.addLayer(bandara);


        } else {
            map.removeLayer(bandara);
        }
    }
});


$("[name='dadan']").click(function(event) {
    event.preventDefault();
    if (map.hasLayer(sights)) {
        $(this).removeClass('selected');
        map.removeLayer(bandara);
    } else {
        map.addLayer(bandara);
        $(this).addClass('selected');
    }
});

//SLIDER SUMMARY
jQuery('#sliderSummary').skdslider({
          delay:5000,
          animationSpeed:2000,
          showNextPrev:false,
		  showNav: false,
          showPlayButton:false,
          autoSlide:false,
          animationType:'fading',
		  slideSelector: '.yearsummary'
});
    
</script>
@endsection

@section('content')

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<!-- Map
================================================== -->


<div id="map-container" class="fullwidth-home-map">

    <!--<div id="map" data-map-scroll="false">
        <!-- map goes here -->
    <!-- </div>-->
	
	<div class="claro">
			<div id="mydatas2" class="ts-map w-100 ts-min-h__50 vh ts-z-index__1"></div>
			
			<div id="layer" style="right:40px; top:0px; width:285px;display:none;" class="ts-form__map-search ts-z-index__2">
				<div class="zink ts-form-collapse "
						id="collapseExample">
						<div style="float:left;"><i class="fa fa-road aria-hidden=" true"></i>
							Boundaries</div>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<input
							align="center" style="left:20px;" type="checkbox" name="my-checkbox"
							id="osmCheck" checked data-size="mini">

					</div>
					<div class="zink ts-form-collapse "
					id="collapseExample">
					<div style="float:left;"><i class="fa fa-plane" aria-hidden="true"></i>
						International Port</div>
					&nbsp;&nbsp;<input data-checked="true" type="checkbox" name="dadan"
						id="osmCheck" data-size="mini">

				</div>
			</div>
			
                                
		 <div data-dojo-type="dijit/layout/BorderContainer" 
            data-dojo-props="design:'headline', gutters:false" 
            style="width:100%;height:100%;margin:0;display: none;">
            <div class="" id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:2px;">
			</div>
        </div>
	</div>
	
	<div  class="mb-0 flex-wrap">
			<!--end ts-form-collapse-->
			<div  style="width:35px;  right:22px; top:19px;position: absolute;" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
				<div id="layerclick"  class="">

					<i class="fa fa-map-o" aria-hidden="true"></i>

				</div>
				
			</div> 
			<div  style="width:35px;  right:22px; top:59px;position: absolute;" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
				<div id="baseclick"  class="">

					<i class="fa fa-th-large" aria-hidden="true"></i>

				</div>
			</div>
			<br>
		<div id="layerhidden" style="right: 65px; top: 12px; width: 270px;display:none;" class="ts-form__map-search ts-z-index__2">
		<form>
		<div align="center" class="zink">
			<div href=".ts-form-collapse" data-toggle="collapse">
				<i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer
			</div>
		</div>
			

		<div style="align-center;" class="zink "
		   id="">
		   <div style="float:left;"  > @lang('bahasa.bandara') : </div>
		   <dd style="margin-top:1px;"class="">
			  <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
				 name="bandara"/><span class="toogle "></span></div>
		   </dd>
		   <br>
		</div>
		<div style="align-center;" class="zink "
		   id="">
		   <div style="float:left;"  >@lang('bahasa.Pelabuhan') :  </div>
		   <dd style="margin-top:1px;"class="">
			  <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
				 name="pelabuhan" /><span class="toogle"></span></div>
		   </dd>
		   <br>
		</div>
		<div style="align-center;" class="zink "
		   id="">
		   <div style="float:left;"  >@lang('bahasa.rumah_sakit') :  </div>
		   <dd style="margin-top:1px;"class="">
			  <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
				 name="rumah_sakit" /><span class="toogle"></span></div>
		   </dd>
		   <br>
		</div>
		<div style="align-center;" class="zink "
		   id="">
		   <div style="float:left;"  >@lang('bahasa.Pendidikan') :  </div>
		   <dd style="margin-top:1px;"class="">
			  <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
				 name="pendidikan" /><span class="toogle"></span></div>
		   </dd>
		   <br>
		</div>
		<div style="align-center;" class="zink "
		   id="">
		   <div style="float:left;"  >Hotel :  </div>
		   <dd style="margin-top:1px;"class="">
			  <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
				 name="hotel" /><span class="toogle"></span></div>
		   </dd>
		   <br>
		</div>


			</form>  
			
		</div>
								
		<div id="legendlayer"  style="width:179px;  left:50px; bottom:15px;display:none;" class="ts-box p-1 ts-form__map-search ts-z-index__2">
	 
			<div id ="LegendIndustri" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/PeoplePlaces/esriBusinessMarker_66_Red.png" width="23px;">&nbsp;Industri</div>
			<div id ="LegendInfrastruktur" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png" width="23px;">&nbsp;Infrastruktur</div>
			<!-- <div id ="LegendPangan" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png" width="23px;">&nbsp;Pangan & Pertanian</div>
			<div id ="LegendJasa" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png" width="23px;">&nbsp;Jasa</div> -->
			<div id ="LegendPariwisata" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/OutdoorRecreation/Mountain.png" width="23px;">&nbsp;Pariwisata</div>
		 
		</div>

		<!--end ts-form-collapse-->
</div>

	<div class="main-search-inner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="main-search-input">

						<div class="main-search-input-item">
							<input class="custom-select titlez" type="text" id="keywords"
                                placeholder=" @lang('bahasa.pencarian')" name="keywords">
						</div>						
						<div class="main-search-input-item">
							 <select style="color:grey" class="chosen-select" id="provinsi" name="type" data-placeholder="@lang('bahasa.semuaprovinsi')">
                                <option value="">@lang('bahasa.semuaprovinsi')</option>
                                @foreach($propertiesx as $list)

                                <option  value="{{$list->id_daerah}}">{{$list->nama}}</option>


                                @endforeach
                            </select>
						</div>		
						<div class="main-search-input-item">
                            <select style="color:grey"  class="chosen-select" id="kota" name="status"  data-placeholder="@lang('bahasa.semuakotakab')">
                                <option value="">@lang('bahasa.semuakotakab')</option>

                            </select>
						</div>

						<div class="main-search-input-item nilai-investasi-control">
						<div class="panel-dropdown">
							<a href="#">@lang('bahasa.nilaiinvestasi')</a>
							<div class="panel-dropdown-content">
								<div class="wrapper" style="padding:20px;">
									<div class="range-slider">
										<input type="text" class="js-range-slider" value="" />
									</div>
									  <hr>
									<div class="extra-controls form-inline">
									  <div class="form-group">
										<input type="hidden" class="js-input-from form-control" value="0" id="filter-min" />
										<input type="hidden" class="js-input-to form-control"  id="filter-max" value="0" />
									</div>
								  </div>
								</div>
									<!--<div class="panel-buttons">
										<button class="panel-cancel">Disable</button>
										<button class="panel-apply">Apply</button>
									</div>-->
								</div>
									
							</div>
						</div>

						<div class="main-search-input-item sector-investment">
							<select id="filter-sector" data-placeholder="@lang('bahasa.semuasektor')" class="chosen-select" multiple="multiple">
								<option value="" data-sectorval=""></option>	
								<option value="industri" data-sectorval="2">@lang('bahasa.industri')</option>
								<option  value="infrastruktur" data-sectorval="3">@lang('bahasa.infrastruktur')</option>
								<option  value="smelter" data-sectorval="7">@lang('bahasa.smelter')</option>
								<option  value="pariwisata" data-sectorval="6">@lang('bahasa.pariwisata')</option>
							</select>
							
							<input type="checkbox" id="industri" name="industri" value="2" checked="checked" style="display:none;" />
							<input type="checkbox" id="infra" name="infra" value="3"  checked="checked" style="display:none;" />
							<input type="checkbox" id="pariwisata" value="6" name="pariwisata" checked="checked" style="display:none;" />
							<input type="checkbox"  id="smelter" value="7" name="smelter" checked="checked"  style="display:none;"/>
						</div>

						<button class="button" id="gelo">@lang('bahasa.cari')</button>

					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<?php

//$projectsTotal = Wordlist::where('id', '<=', $correctedComparisons)->get();
//$wordCount = $wordlist->count();

$kpiIndexes = array();
foreach ($propertiesz as $list)
{
	$yearKey = $list->tahun;
	
	if (array_key_exists($yearKey, $kpiIndexes) == false) {
		$kpiIndexes[$yearKey] = 
		array
		(
			'projects'     => 0,
			'total'   => 0,
			'kabupaten' => array(),
			'subsectors' => array()
		);
	}; 

	$kpiIndexes[$yearKey]["total"] += $list->nilai_investasi;
	$kpiIndexes[$yearKey]["projects"] += 1;
	array_push($kpiIndexes[$yearKey]["kabupaten"], $list->bentuk_daerah . $list->nama);
	array_push($kpiIndexes[$yearKey]["subsectors"], $list->sektor);
}

ksort($kpiIndexes);

?>


<!-- SLIDER -->

<!-- <div class="row margin-bottom-25" style="background-color:#F8F8F8;"> -->

<div id="sliderSummary" class="skdslider">
	@foreach ($kpiIndexes as $key => $list)

	<div class="yearsummary text-center">
		<img src="{{Config::get('app.url')}}/assets/peluang_proyek/css/plugins/skdslider/slides/background.jpg" /> 	
		<div class="slide-desc">
			<h3>@lang('bahasa.tahun') {{ $key }}</h3>
			<div class="col-xs-3 col-sm-3">
				<div class="kpi-inner"> 
					<span class="kpi-value">{{ $list["projects"] }}</span> 
					<span class="kpi-title">@lang('bahasa.proyek')</span> 
				</div>
			</div>
			<div class="col-xs-3 col-sm-3">
				<div class="kpi-inner"> 
					<span class="kpi-value"><?php if ($list["total"]  == 0) { ?>
									@currency($list["total"])
					<?php } else if ($list["total"]  < 1000000000) { ?>
									@currency($list["total"] / 1000000) @lang('bahasa.juta')
										<?php	} else if ($list["total"]  < 1000000000000) { ?>
									@currency($list["total"] / 1000000000) @lang('bahasa.miliar')

										<?php	} else if ($list["total"]  < 1000000000000000) { ?>
									@currency($list["total"] / 1000000000000) @lang('bahasa.triliun')
										<?php } ?></span> 
					<span class="kpi-title">@lang('bahasa.totalnilaiproyek')</span> 
				</div>
			</div>
			<div class="col-xs-3 col-sm-3">
				<div class="kpi-inner"> 
					<span class="kpi-value">{{ count(array_unique($list["kabupaten"])) }}</span> 
					<span class="kpi-title">@lang('bahasa.kabupaten')</span> 
				</div>
			</div>
			<div class="col-xs-3 col-sm-3">
				<div class="kpi-inner"> 
					<span class="kpi-value">{{ count(array_unique($list["subsectors"])) }}</span> 
					<span class="kpi-title">@lang('bahasa.asektor')</span> 
				</div>
			</div>
		</div>
     </div>


	@endforeach
</div>

<div class="padding-bottom-25"></div>

<!-- </div> -->


<!-- Content
================================================== -->
<div class="container">
	<div class="row">
		<div class="col-md-12" id="list-project">
			<!-- Layout Switcher -->
			<!--<div class="row margin-bottom-25 margin-top-40">
				<div class="col-md-12">
					<div class="layout-switcher">
						<a href="#" class="grid active">
							<i class="fa fa-th"></i>
						</a>
						<a href="project-opportunities-list.html" class="list">
							<i class="fa fa-align-justify"></i>
						</a>
					</div>
				</div>
			</div>-->
			<!-- Sorting - Filtering Section / End -->
			<div class="row">
			@foreach ($propertiesz as $list)
				
					<!-- Listing Item -->
					<div class="col-lg-3 col-md-6">
						<a href="{{ Config::get('app.url') }}/dev_propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}/{{Config::get('app.locale')}}" class="listing-item-container compact">
							<div class="listing-item">
								<img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" alt="{{$list->judul}}">
									<div class="listing-item-content">
										<span class="tag">{{$list->sektor}}</span>
										<h4>{{$list->judul}}</h4>
										<span style="display:block;">{{$list->bentuk_daerah}} {{$list->nama}}</span>
										<span class="badge badge-success">
											<strong><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')
                             
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')
                                                             <?php } ?></strong>
										</span>
									</div>
								</div>
							</a>
						</div>
						
						<!-- Listing Item / End -->
			@endforeach
			</div><!-- ROW / End -->
			
		</div>
	</div>
</div>
@endsection