       <!-- Include fusioncharts core library file -->
   


  
 <div id="asd">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
      FusionCharts.ready(function() {
        var myChartzssss = new FusionCharts({
          type: "scrollbar2d",
          renderAt: "asd",
          width: "100%",
          height: "350",
          dataFormat: "json",
          dataSource: {
                    chart: {
                        caption: "@lang('bahasa.komoditiregional')",
                        subcaption: "2016 - 2019",
                        plottooltext: "$dataValue",
                        yaxisname: "@lang('bahasa.total')",
                        xaxisname: "@lang('bahasa.komoditiz')",
                        theme: "fusion",
                        numberScaleValue: "1,1,1",
                    numberScaleUnit: " Kg, Kwintal, Ton",
                    
                    },
                    categories: [{
                        category: [@foreach($investasi as $r)

                            {
                                label: "({{$r->sektor}}) @lang('bahasa.komoditiz') {{$r->nama}}"
                            }, @endforeach
                        
                        ]
                    }],
                    dataset: [{
                        data: [@foreach($investasi as $r){
                                value: "{{$r->hasil}}"
                            }, @endforeach
                          


                        ]
                    }],
                    trendlines: [{
                    line: [{
                            startvalue: "16200",
                            color: "#5D62B5",
                            thickness: "1.5",
                            displayvalue: " ."
                        }
                     
                    ]
                }]
            
                }
        }).render();
      });

            </script>