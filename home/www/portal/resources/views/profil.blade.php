@extends('frontend.layoutmap')

@section('content')

<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<script type="text/javascript"
    src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript"
    src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
</script>

<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<script src="https://js.arcgis.com/3.34/"></script>

<script>
    var map;
    require([
        "esri/map",
        "esri/dijit/PopupTemplate",
        "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
"esri/config",
"esri/urlUtils",
        "esri/InfoTemplate",
        "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
        "dojo/parser",

        "dijit/layout/BorderContainer",
        "dijit/layout/ContentPane",
        "dijit/TitlePane",
        "dojo/domReady!"

    ], function (
        Map,
        PopupTemplate,
        FeatureLayer,
        arrayUtils,
        ArcGISDynamicMapServiceLayer,
        Geometry,
        Point,
        webMercatorUtils,
        Graphic,
        SimpleMarkerSymbol,
        SimpleLineSymbol,
        SimpleFillSymbol,
        PictureMarkerSymbol,
        Color,
esriConfig,
urlUtils,
        InfoTemplate,
        BasemapGallery,
        arcgisUtils,
        parser

    ) {
        parser.parse();

        var map = new Map("map", {
            basemap: "hybrid",
            center: [117.62527, -1.4509444],
            zoom: 5
        });
urlUtils.addProxyRule({
    urlPrefix: "https://Gistaru.atrbpn.go.id/",
    proxyUrl: "https://Gistaru.atrbpn.go.id/rtronline/proxy/proxy.ashx"
  });
 
        //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
        var basemapGallery = new BasemapGallery({
            showArcGISBasemaps: true,
            map: map
        }, "basemapGallery");
        basemapGallery.startup();

        basemapGallery.on("error", function (msg) {
            console.log("basemap gallery error:  ", msg);
        });



        var title1 = '<div align="center">Batas Wilayah Provinsi <br><a id="remove" style="" href="{{Config::get('app.url')}}/profil/detail/{kw}/id" target="_self" ><span>Click Detail</span></a> </div>';
        var isi1 =
            '<div style="float:left;"  >Provinsi :  </div><dd style="margin-left:60px;  ; "class="border-bottom pb-2"> {wa}</dd'
        var title2 = '<div align="center">Batas Wilayah Kecamatan</div>';
        var isi2 =
            '<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
        var popupprov =new PopupTemplate({
            title: title1,
            description: isi1

        });
      

        var popupkota = new PopupTemplate({
            title: title2,
            description: isi2

        });


        var title3 = '<div align="center">RTRW Kabupaten</div>';
        var isi3 =
            '<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

        var popuprtrw = new PopupTemplate({
            title: title3,
            description: isi3

        });

        $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken", {
                username: 'bkpm-sipd',
                password: 'Bkpm_S1pd@atrbpn123'




            },

    
            function (data, status) {



                var key = data;
                var token = '?token=' + key + '';
                var featureLayerz = new FeatureLayer(
                    "https://gistaru.atrbpn.go.id/arcgis/rest/services/013_RTR_KABUPATEN_KOTA_PROVINSI_RIAU/_1400_RIAU_PR_PERDA/MapServer/6" +
                    token, {
                        outFields: ["*"],
                        opacity: 0.5,
                        infoTemplate: popupTemplate
                    });
                map.addLayer(featureLayerz);

            });
        var featureLayerprovinsi = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0", {
                outFields: ["*"],

                infoTemplate: popupprov
            });
        map.addLayer(featureLayerprovinsi);
        var featurekota = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/2", {
                outFields: ["*"],

                infoTemplate: popupkota
            });
        map.addLayer(featurekota);

        // Layer Infrastruktur
        var title4 = '<div align="center">Infrastruktur Bandara</div>';
        var isi4 =
            '<div style="float:left;"  >Name Airport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_bandara = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var bandara = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/bandara_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_bandara
            });
        map.addLayer(bandara);

        var title5 = '<div align="center">Infrastruktur Bandara</div>';
        var isi5 =
            '<div style="float:left;"  >Name Seaport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pelabuhan = new PopupTemplate({
            title: title5,
            description: isi5
        });
        var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pelabuhan
            });
        map.addLayer(pelabuhan);
      
        var title7 = '<div align="center">Infrastruktur Bandara</div>';
        var isi7 =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title7,
            description: isi7
        });
        var rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(rumah_sakit);

        var title8 = '<div align="center">Infrastruktur Bandara</div>';
        var isi8 =
            '<div style="float:left;"  >Name Educational :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pendidikan = new PopupTemplate({
            title: title8,
            description: isi8
        });
        var pendidikan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pendidikan_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pendidikan
            });
        map.addLayer(pendidikan);
        var title9 = '<div align="center">Infrastruktur Bandara</div>';
        var isi9 =
            '<div style="float:left;"  >Name Hotel :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_hotel = new PopupTemplate({
            title: title9,
            description: isi9
        });
        var hotel = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/hotel_en/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_hotel
            });
        map.addLayer(hotel);

        // Akhir layer infrastruktur

        rumah_sakit.hide();
        pendidikan.hide();
        hotel.hide();
        pelabuhan.hide();
        bandara.hide();
        $('input:checkbox[name=bandara]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      bandara.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    bandara.hide();
                  }

         });
         $('input:checkbox[name=rumah_sakit]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rumah_sakit.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rumah_sakit.hide();
                  }

         });
         $('input:checkbox[name=pendidikan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pendidikan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pendidikan.hide();
                  }

         });
         $('input:checkbox[name=pelabuhan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pelabuhan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pelabuhan.hide();
                  }

         });
         $('input:checkbox[name=hotel]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     hotel.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                   hotel.hide();
                  }

         });


          
     

    });
</script>


 <div class="preloader">
  <div align="center" class="loading">
<img src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg " height="80">
<br>
    <img src="{{ Config::get('app.url') }}/gif/bkpm.gif" height="100">
    <div align="center" style="margin-bottom:100x;">
    <br>
   <a style="color:grey"></a>
</div>
    
  </div>
</div>
    
<script> $(document).ready(function () {
$('#remove').removeAttr('target');

Array.from(document.querySelectorAll('a[target="_blank"]'))
  .forEach(link => link.removeAttribute('target'));
var links = document.links, i, length;

for (i = 0, length = links.length; i < length; i++) {
    links[i].target == '_blank' && links[i].removeAttribute('target');
}
$(".preloader").delay(1500).fadeOut("slow");
                $('#submit_100').hide();
             
                $('.has-spinner').click(function () {

                    var btn = $(this);
                    $(btn).buttonLoader('start');
                    setTimeout(function () {
                        $(btn).buttonLoader('stop');
                    }, 5000);
                    var nameform = $('#identity').val();

                });

 $("#layerclick").click(function(){
        
        $('#layerhidden').toggle();
        $('#layeron').toggleClass("hijau");

    
});
$("#baseclick").click(function(){
        
        $('#base').toggle();
        $('#baseon').toggleClass("hijau");

    
});
$('#layerhidden').hide();
$('#base').hide();

});</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />




<body class="claro">
<div class="row">
    <div class="col-sm-3">
        <section id="tbody" class="when" style=" margin-top:10px;  ">

            <!--ITEMS LISTING
        =========================================================================================================-->

            <div class="wings zink" style="border-radius: 22px;">

                <!--Display selector on the left-->
                <div align="center" class="">

                    <a id="ts-display-list" class="btn">
                        Regional Profile
                    </a>
                    <br>
                                     <br>
                    <a href="#submenu2" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Jawa<i class="fa fa-caret-down"
                                    style="margin-left:119px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu2' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Jawa')
                         <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
			@endif
                        @endforeach
                    </div>
                    <a href="#submenu3" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Sumatera<i class="fa fa-caret-down"
                                    style="margin-left:91px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu3' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Sumatera')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <a href="#submenu4" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Kalimantan <i class="fa fa-caret-down"
                                    style="margin-left:76px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu4' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Kalimantan')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <a href="#submenu5" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Sulawesi<i class="fa fa-caret-down"
                                    style="margin-left:95px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu5' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Sulawesi')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
  <a href="#submenu7" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
     <span class="fa fa-user fa-fw mr-3"></span>
                                                       <span class="menu-collapsed">Bali & Nusa Tenggara<i class="fa fa-caret-down"
                                    style="margin-left:12px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>

  <div id='submenu7' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Bali dan Nusa Tenggara')
                         <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
			@endif
                        @endforeach
                    </div>

                    <a href="#submenu6" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Maluku<i class="fa fa-caret-down"
                                    style="margin-left:104px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu6' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Maluku')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach

                    </div>
                    <a href="#submenu8" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Papua<i class="fa fa-caret-down"
                                    style="margin-left:110px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu8' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Papua')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach

                    </div>




                   <br>
                <!--Display selector on the right-->


            </div>


            <!--end container-->
        </section>
    </div>
    <br>
    <div class="col-sm-9">
        <section class="single-pricing-wrap Efek5 p-1 long" style="margin-top:10px;  ">


            <!--ITEMS LISTING
        =========================================================================================================-->

          

                <!--Display selector on the left-->
                <div style="margin-top:10px;" class="clearfix  btn-pro titlez  ">

                
                       <div style="margin-top:10px; margin-bottom:10px;"> @lang('bahasa.peta')</div>
                       
                  
                </div>

                <div  style="width:35px;  right:14px; top:125px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="layerclick"  class="">

                        <i class="fa fa-map-o" aria-hidden="true"></i>

                    </div>
                </div> <div  style="width:35px;  right:14px; top:165px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="baseclick"  class="">

                        <i class="fa fa-th-large" aria-hidden="true"></i>

                    </div>
                </div>

            <br>
            <div id="layerhidden" style="right:40px; top:125px width:300px" class="ts-form__map-search ts-z-index__2">
            <form>
                        <div align="center" class="zink">
                            <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                                <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Infrastruktur
                            </div>
                        </div>
                        <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  > Bandara : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                    name="bandara" /><span class="toogle "></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pelabuhan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pelabuhan" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Rumah Sakit :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="rumah_sakit" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pendidikan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pendidikan" /><span class="toogle"></span></div></dd><br>
                </div>
            

                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Hotel :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="hotel" /><span class="toogle"></span></div></dd><br>
                </div>
                </form>  
                
            </div>
            <div data-dojo-type="dijit/layout/BorderContainer" 
            data-dojo-props="design:'headline', gutters:false" 
            style="width:100%;height:100%;margin:0;">
            <div class="" id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:2px;">
            
                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                    <div data-dojo-type="dijit/TitlePane" 
                         data-dojo-props="title:'Switch Basemap', open:true">
                      <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                        <div id="basemapGallery"></div>
                    </div>
                </div>
              </div>
            </div>
        </div>
   
       

      

            <!--ITEMS LISTING
        =========================================================================================================-->

            <div class="clearfix  btn-pro titlez ">

                <!--Display selector on the left-->
                <div class="">

               
                        @lang('bahasa.RealisasiInvestasiP')
                    
                        <div class="float-none float-sm-right pl-2 ts-center__vertical ">

<a>@lang('bahasa.tahun')</a>&nbsp;
    <select style="color:grey; width:100px;" class="custom-select" id="sectorz" name="sorting">
    <option value="2020">2020</option>
        <option selected value="2019">2019</option>
        <option value="2018">2018</option>
        <option value="2017">2017</option>
        <option value="2016">2016 </option>



    </select>
</div>
                </div>






            </div>
            <br>

            <div align="center" id="chart2">Data Belum Terdownload silahkan Refresh Kembali</div>

            <!--end container-->
        
        <br>
       
            <div style="" class="">

                <!--Display selector on the left-->
                <div style="margin-top:10px;" class="clearfix  btn-pro titlez ">

                
                    <div style="margin-top:10px; margin-bottom:10px;"> @lang('bahasa.provinsi')</div>
                    
               
             </div>
            </div>
            <br>
            <section style="margin-left:5px; margin-right:5px;">

                <div class="row">
                    @foreach($bentuk as $list)
                    <div class="col-sm-3">
                    <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card ">

                <!--Ribbon-->
                              
                   
           
                          <div align="center" style="max-height:200px; margin-top:10px; font-size:14px; border-radius: 8px; border-bottom-left-radius: 20px;"
                              class="ts-card zink hijau titlez ">

                              <!--Display selector on the left-->
                              <div class="">

                                  <a>
                                      {{$list->nama}}
                                  </a>
                              </div>




                          </div>
       
                          <div align="center" class="" style="padding:20px; border-radius: 11px;">
                                                   <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/{{Config::get('app.locale')}}" data-toggle="tooltip" data-placement="top" title="For Detail Province {{$list->nama}}">
                            <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/{{$list->logo}}"
                                height="110px" width="110px">
                         
                        </div>
               


        
                 <div style="margin-left:40px;" class="atbd_listing_meta">
                
                 </div>
                

               

            </div>
                  
                  
                        
                    </div>
                    @endforeach


            </section>

    </div>

    <div>
        <div id="map" 
        data-dojo-type="dijit/layout/ContentPane" 
        data-dojo-props="region:'center'" 
        style="padding:0;"></div>



        <!--end container-->
        </section>

    </div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script><br>

</script>

<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->

<body>

    <script>
$('a[target="_blank"]').each(function(){
   $(this).replaceWith($(this).text());
});
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: getDuration
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>




    <script>
        
        $(document).ready(function () {
     
            
           

       
            $('#layerhidden').hide();
      
                    $('#provinsi').change(function (e) {
                        $.ajax({
                            url: "{{Config::get('app.url')}}/getkota/" + $(this).val(),
                            method: 'GET',
                            success: function (data) {
                                console.log(data);

                                $('#kota').children('option:not(:first)').remove().end();

                                $.each(data, function (index, kotaObj) {

                                    $('#kota').append('<option value="' + kotaObj
                                        .id_daerah + '">  ' + kotaObj
                                        .bentuk_daerah +
                                        ' ' + kotaObj.nama + ' </option>')
                                });
                            }
                        });
                    });

                    $('.aku').on('click', function () {
                        $('.collapse').collapse();
                    })
                    $.get("{{Config::get('app.url')}}/cari?keywords=}", function (data) {
                        $("#mydata").html(data);

                    });
                    $.get("{{Config::get('app.url')}}('/map-home?keywords=", function (data) {
                        $("#mydatas").html(data);

                    });

                    $.get("{{Config::get('app.url')}}/daerah?industri=Industri", function (data) {


                        $("#daerah").append(data);
                        const postDetails = document.querySelector(".hasil");

                        const postSidebar = document.querySelector(".pencarian");
                        const postDetailsz = document.querySelector(".ts-boxzzs");
                        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


                        const controller = new ScrollMagic.Controller();


                        const scene = new ScrollMagic.Scene({
                            triggerElement: postSidebar,
                            triggerHook: 0,
                            duration: getDuration
                        }).addTo(controller);

                        //3
                        if (window.matchMedia("(min-width: 768px)").matches) {
                            scene.setPin(postSidebar, {
                                pushFollowers: false
                            });
                        }

                        //4
                        window.addEventListener("resize", () => {
                            if (window.matchMedia("(min-width: 768px)").matches) {
                                scene.setPin(postSidebar, {
                                    pushFollowers: false
                                });
                            } else {
                                scene.removePin(postSidebar, true);
                            }
                        });

                        function getDuration() {




                            return (postDetails.offsetHeight - postSidebar.offsetHeight) + 500;

                        }
                    });
    </script>

    <script> $('#sectorz').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartinvestasi/"+tahun+"/{{ Request::segment(2) }}", function (data) {
            $("#chart2").html(data);
           
          
        });
});</script>
<script>
          
          $.get("{{ Config::get('app.url') }}/chartinvestasi/2019/{{ Request::segment(2) }}", function (data) {
                $("#chart2").html(data);
               
              
            });
            
            
            </script>

    @endsection
