@extends('frontend.layoutmap2')

@section('content')


  
        <!-- ======================= End JQuery libs =========================== -->
<link rel="stylesheet" href="https://ihatetomatoes.net/demos/css3-preloader-transition/css/main.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--*********************************************************************************************************-->
<!-- MAIN ***************************************************************************************************-->
<style type="text/css">
	.back-link a {
		color: #4ca340;
		text-decoration: none; 
		border-bottom: 1px #4ca340 solid;
	}
	.back-link a:hover,
	.back-link a:focus {
		color: #408536; 
		text-decoration: none;
		border-bottom: 1px #408536 solid;
	}
	h1 {
		height: 100%;
		/* The html and body elements cannot have any padding or margin. */
		margin: 0;
		font-size: 14px;
		font-family: 'Open Sans', sans-serif;
		font-size: 32px;
		margin-bottom: 3px;
	}
	.entry-header {
		text-align: left;
		margin: 0 auto 50px auto;
		width: 80%;
        max-width: 978px;
		position: relative;
		z-index: 10001;
	}
	#demo-content {
		padding-top: 100px;
	}
	#carbonads-container {
		position: fixed;
		top: 20px;
		right: 20px;
		z-index: 1000000;
	}
	#carbonads-container {
		border: 2px #202020 solid;
	}
	#carbonads-container img {display: block;}
	</style>

<style>
    .wew {
        background-color: rgb(68, 38, 201);
        margin-top: 3px;
    }
    .checked {
        display: flex;
  color: orange;
}
</style>
<body  class="demo">


    <header id="ts-header" class="">
    <main id="ts-main">
  
        
           
           
<br>
       


            <!--Carousel-->
            <div class="owl-carousel ts-items-carousel"  data-owl-items="4" >
         
 @foreach ($propertiesx as $list) 

                <!--Item-->
                <div class="slide ">

                     <div class="card ts-item ts-card ">

            <div class="ts-ribbon-corner">
                                                            <span>National</span>
                                                        </div>
                        <!--Card Image-->
                        <a href="detail-01.html" class="card-img ts-item__image" data-bg-image="https://cdn.pixabay.com/photo/2018/04/30/08/00/industry-3362044_1280.jpg">
                            <div class="ts-item__info-badge">
                                                   <?php if ($list->nilai_investasi  < 1000000000) { ?>
		 Nilai Investasi : {{ $list->nilai_investasi / 1000000}} Juta
<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
		Nilai Investasi :  {{$list->nilai_investasi / 1000000000}} Milyar
                            
                           <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
		Nilai Investasi :  {{$list->nilai_investasi / 1000000000000}} Triliyun
                           <?php } ?> 
                            </div>
                            <figure class="ts-item__info">
                                <h4>{{$list->judul}} </h4>
                                <aside>
                                    <i class="fa fa-map-marker mr-2"></i>
                                    {{$list->alamat}}
                                </aside>
                            </figure>
                        </a>
 

                                                        <div class="card-body ts-item__body">
                                                            <div class="ts-description-lists zink">
                                                             
                                                                    Rating : &nbsp;&nbsp;<i style=color:#FFD700; " class="fa fa-star "></i><i style=color:#FFD700; " class="fa fa-star "></i><i style=color:#FFD700; " class="fa fa-star "></i><i style=color:#FFD700; " class="fa fa-star "></i><i style=color:#FFD700; " class="fa fa-star "></i></dt>

                                                            
                                                                </div>
                                                             
                                                                </div>
                                                            
              

                        <!--Card Footer-->
                        <a href="detail-01.html" class="card-footer">
                            <span class="ts-btn-arrow">Detail</span>
                        </a>
   

                    </div>
                    <!--end ts-item-->

                </div>
                <!--end slide-->

   @endforeach

            </div>
              

</section>

                   
    </main>

    <!--end #ts-main-->
    </div>
    </div>
    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->


    <!-- Footer area - footer_down -->

    <!-- End Footer area- footer_down -->


    <!--end page-->
    <!--ITEMS LISTING

    <!--end #ts-main-->

    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->


  
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/js/custom.js"></script>


    @endsection