<section>
                                <div class="container">

                                    <!--Featured Items-->
                                    <div class="row">

                                        @foreach ($propertiesx as $list)
                                        @if($list->sektor == 'Infrastruktur') 
                                        <!--Item-->
                                        <!--Item-->
                                        <div class="col-sm-8 col-lg-4">
                                            <div class="card ts-item ts-card">

                                                <!--Ribbon-->
                                                <div class="ts-ribbon-corner">
                                                    <span></span>
                                                </div>

                                                <!--Card Image-->
                                                <a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . $list->id_parent . "/" . clean($list->judul)); ?>"
                                                    class="card-img ts-item__image"
                                                    data-bg-image="https://cdn.pixabay.com/photo/2018/04/30/08/00/industry-3362044_1280.jpg">
                                                    <figure class="ts-item__info">
                                                        <h4>{{$list->judul}}</h4>
                                                        <aside>
                                                            <i class="fa fa-map-marker mr-2"></i>
                                                            {{$list->alamat}}
                                                        </aside>
                                                    </figure>
                                                    <div class="ts-item__info-badge">Nilai Investasi
                                                        ($list->price) </div>
                                                </a>


                                                <div class="card-body ts-item__body">
                                                    <div class="ts-description-lists">
                                                       

                                                    </div>
                                                </div>

                                                <!--Card Footer-->
                                                <a href="detail-01.html" class="card-footer">
                                                    <span class="ts-btn-arrow">Detail</span>
                                                </a>

                                            </div>
                                            <!--end ts-item ts-card-->
                                        </div>

                                        <?php } ?>
                                        <!--end Item col-md-4-->
                                        <?php endforeach; ?>

                                    </div>
                                    <!--Item-->
                                    @else
                                    <div align="center" class="margin-left:50px; ts-boxzzs ">
                                        <p>Oops.. Data <b></b> Not Found Please Search Again
                                    </div>
                                    @endif
                            </section>

                            <!--PAGINATION
            =========================================================================================================-->
                            <ul style="margin-left:450px;" class="pagination">
                                {!! $propertiesx->render() !!}
                            </ul>
                            @endif