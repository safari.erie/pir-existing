


			<div class="row">
			@foreach ($propertiesz as $list)
				<?php if($list->id_sektor_peluang == $infras || $list->id_sektor_peluang == $indus || $list->id_sektor_peluang == $pariwisatas || $list->id_sektor_peluang == $smelters) { ?>
					<!-- Listing Item -->
					<div class="col-lg-3 col-md-6">
						<a href="{{ Config::get('app.url') }}/dev_propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}/{{Config::get('app.locale')}}" class="listing-item-container compact">
							<div class="listing-item">
								<img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" alt="{{$list->judul}}">
									<div class="listing-item-content">
										<span class="tag">{{$list->sektor}}</span>
										<h4 style="color:#fff;">{{$list->judul}}</h4>
										<span style="display:block;">{{$list->bentuk_daerah}} {{$list->nama}}</span>
										<span class="badge badge-success">
											<strong><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')
                             
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')
                                                             <?php } ?></strong>
										</span>
									</div>
								</div>
							</a>
						</div>
						
						<!-- Listing Item / End -->
						<?php } ?>
			@endforeach
			</div><!-- ROW / End -->

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

                            <!--PAGINATION
            =========================================================================================================-->
                         