<meta charset="utf-8">
<title>{{ $_ENV['APP_NAME'] }}</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<!-- Favicons -->
<link href="{{ $_ENV['APP_URL'] }}/public/assets/img/favicon.png" rel="icon">
<link href="{{ $_ENV['APP_URL'] }}/public/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link href="{{ $_ENV['APP_URL'] }}/public/assets/css/font-family.css" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/animate/animate.min.css" rel="stylesheet">
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/ionicons/css/ionicons.css" rel="stylesheet">
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<!--link href="{{ $_ENV['APP_URL'] }}/public/assets/css/owl-carousel.css" rel="stylesheet"-->
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
<link href="{{ $_ENV['APP_URL'] }}/public/assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="{{ $_ENV['APP_URL'] }}/public/assets/css/style.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ $_ENV['APP_URL'] }}/public/assets/lib/elastislide/demo.css" />
<link rel="stylesheet" type="text/css" href="{{ $_ENV['APP_URL'] }}/public/assets/lib/elastislide/elastislide.css" />
<link rel="stylesheet" type="text/css" href="{{ $_ENV['APP_URL'] }}/public/assets/lib/elastislide/custom.css" />

<!-- rating -->
<link rel="stylesheet" type="text/css" href="{{ $_ENV['APP_URL'] }}/vendor/starrr/dist/starrr.css" />

<!-- readmore 
<!--link rel="stylesheet" type="text/css" href="{{ $_ENV['APP_URL'] }}/vendor/readmore/css/show-hide-text.min.css" /-->

<script src="{{ $_ENV['APP_URL'] }}/public/assets/js/modernizr.custom.17475.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>