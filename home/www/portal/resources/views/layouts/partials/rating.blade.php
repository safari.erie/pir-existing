<section id="rating" class="wow fadeInUp">
	<div class="container">
		<div class="section-header">
			<h2>@lang('messages.txtRating')</h2>
		</div>
	</div>
</section>

<section id="rating_detail" class="wow fadeInUp">
	<div class="container">
		<div class="row info">
		@if ($avgRating)
			@foreach ($avgRating as $rating)
		
			<div class="{{ $rating['avg']['div_class'] }}">
				<div class="info">
					<i class="{{ $rating['icon'] }}"></i>
					<h3>{{ $rating['name'] }}</h3>
					<div class='starrr'>
					@for ($i = 1; $i <= 5; $i++)
						@if ($i <= $rating['avg']['avg'])
							<span class="fa fa-star"></span>
						@else
							<span class="fa fa-star-o"></span>
						@endif
					@endfor
					</div>
				</div>
			</div>
			@endforeach
		@endif
		</div>
	</div>
</section>
<!--
<section id="rating_comment" class="wow fadeInUp">
	<div class="container">	
		@if ($postRate)
			@foreach ($postRate as $key=>$pr)
			<div class="row comment">
				<div class="col-lg-12">
					<div class="box wow fadeInLeft">
						<h1>{{ $pr->name }} <span class="date">[ {{ $pr->date_inserted }} ]</span></h1>
						<blockquote>
							<div class="row info">
								@foreach ($postRateDt[$pr->rating_post_id] as $dt)
								<div class="col-md-2">
									<i class="{{ $dt->icon }}"></i>
									<h3>{{ $dt->rating_name }}</h3>
									<div class='starrr'>
										@for ($i = 1; $i <= 5; $i++)
											@if ($i <= $dt->rate)
												<span class="fa fa-star star"></span>
											@else
												<span class="fa fa-star-o star"></span>
											@endif
										@endfor
									</div>
								</div>
								@endforeach
							</div>
							<p>" {{ $pr->message }} "</p>
						</blockquote>
					</div>
				</div>
			</div>
			@endforeach
		@endif
	</div>
</section>
-->
<section id="rating_post">
	<div class="container">
		@if ($hasRate <= 0)
		<p>@lang('messages.txtRatingDesc') (<b>SIPID</b>).</p>
		<div class="row">
			<div class="col-lg-6 content">
			@if ($ratingList)
				<table width="100%">
				@foreach ($ratingList as $rate)
					<tr>
						<td><i class="{{ $rate->icon }}"></i>{{ $rate->name }}</td>
						<td>
							<div class='starrr' id='star{{ $rate->rating_id }}'></div>
						</td>
					</tr>
				@endforeach
				</table>
			@endif
			</div>
			<div class="col-lg-6 content">
				<div class="form">
					{!! Form::open(['url' => 'post_rate', 'method' => 'post']) !!}
					{!! Form::hidden('session_id', ($session_id ? $session_id : 0) ) !!}
					{!! Form::hidden('ip', ($client_ip ? $client_ip : '') ) !!}
					
					
					@if ($ratingList)
						@foreach ($ratingList as $rate)
							{!! Form::hidden('txt_star'.$rate->rating_id, '', ['id' => 'txt_star'.$rate->rating_id] ) !!}
						@endforeach
					@endif
					
					<div class="form-group">
						{!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Nama']) !!}
					</div>
					<div class="form-group">
						{!! Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email']) !!}
					</div>
					<div class="form-group">
						{!! Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'Pesan', 'style' => 'height:100px']) !!}
					</div>
					<div class="form-group">
						<div class="g-recaptcha" data-sitekey="6LdXQHcUAAAAACgWFrhMCVs90Fv6rGxC7x_dgBMt"></div>
					</div>
					<div class="text-left"><button type="submit" class="btn">@lang('messages.txtSendMessage')</button></div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endif
	</div>
</section>
