<footer id="footer">
	<div class="container">
		<div class="copyright">
			<strong>SIPID</strong> &copy; 2018 All Rights Reserved.
		</div>
		<div class="credits">
			<a href="www.bkpm.go.id" target="_blank">Badan Koordinasi Penanaman Modal</a>
		</div>
	</div>
</footer><!-- #footer -->