<section id="maps">
	<div class="container">
		<div class="section-header">
		<h2>@lang('messages.txtMap')</h2>
		</div>

		<div class="row">
			<div class="container demo-1">
				<div class="main">
					<ul id="carousel" class="elastislide-list">
						<li>
							<a href="http://regionalinvestment.bkpm.go.id/profile" target="_blank" alt="Profile Daerah">
								<img class="thumb" src="{{ $_ENV['APP_URL'] }}/public/assets/img/maps/story_map.jpeg" alt="Profile Daerah" />
								<div id="title_thumb">@lang('messages.txtProfilDaerah')</div>
							</a>
						</li>
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/realisasiinvestasinasional" target="_blank" alt="Realisasi Investasi">
								<img class="thumb" src="{{ $_ENV['APP_URL'] }}/public/assets/img/maps/realisasi_investasi.jpeg" alt="Realisasi Investasi" />
								<div id="title_thumb">@lang('messages.txtRealisasiInvestasi')</div>
							</a>
						</li>
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/industrinasional" target="_blank" alt="Kawasan Industri">
								<img class="thumb" src="{{ $_ENV['APP_URL'] }}/public/assets/img/maps/kawasan_industri.jpeg" alt="Kawasan Industri" />
								<div id="title_thumb">@lang('messages.txtKawasanIndustri')</div>
							</a>
						</li>
						<li>
							<a href="https://regionalinvestment.bkpm.go.id/peluang" target="_blank" alt="Potensi">
								<img class="thumb" src="{{ $_ENV['APP_URL'] }}/public/assets/img/maps/peluang.jpeg" alt="Realisasi Investasi" />
								<div id="title_thumb">@lang('messages.txtPeluangInvestasi')</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
