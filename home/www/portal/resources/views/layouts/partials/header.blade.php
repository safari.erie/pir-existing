<header id="header">
	<div class="container">

		<div id="logo" class="pull-left">
			<!--h1><a href="#body" class="scrollto">Reve<span>al</span></a></h1-->
			<!-- Uncomment below if you prefer to use an image logo -->
			<a href="#body"><img src="{{ $_ENV['APP_URL'] }}/public/assets/img/logo-bkpm.png" alt="" title="" /></a>
			
		</div>

		<nav id="nav-menu-container">
			<ul class="nav-menu">
				<li class="menu-active"><a href="#body">@lang('messages.txtHome')</a></li>
				<li><a href="#maps">@lang('messages.txtMap')</a></li>
				<li><a href="#guides">@lang('messages.txtGuide')</a></li>
				<!--li><a href="#news">@lang('messages.txtNews')</a></li-->
				<li><a href="#rating">@lang('messages.txtRating')</a></li>
				<li><a href="#contact">@lang('messages.txtContact')</a></li>
				<li><a href="setLang/en"><img src="{{ $_ENV['APP_URL'] }}/public/assets/img/en.png" /></a></li>
				<li><a href="setLang/id"><img src="{{ $_ENV['APP_URL'] }}/public/assets/img/id.png" /></a></li>
			</ul>
		</nav><!-- #nav-menu-container -->
	</div>
</header><!-- #header -->
