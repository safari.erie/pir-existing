

@extends('frontend.layoutmap')

@section('content')

<script src="{{ Config::get('app.url') }}/assets/js/custom.js"></script>



<link rel="stylesheet" href="https://www.cssscript.com/demo/pretty-checkbox-replacement-css/beautiful-checkbox.css" />


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<script src="https://js.arcgis.com/3.34/"></script>


<body class="claro">

      


  
  
    <main id="ts-main">
        <br>
        <div class="row">
            <div class="col-sm-3">
                <section>


              

                    <section style="background-color:white; left:5px; border-radius: 10px; width:100%;"class=" pencarian zink">
                        <div class="">

                            <!--Display selector on the left-->
                            <div style="background-color:#1066b9; border-bottom-left-radius:15px; " class="zink titlez" align="center">

                                <i class="fa fa-th-large"></i>&nbsp @lang('bahasa.Pencarian_Lanjutan')


                            </div>
                        </div>


                        <div class="">

                            <!--Keyword-->


                            <div style="bottom:40px;" align="center">Pencarian</div>
                            <input class="custom-select titlez" type="text" style="height:38px" id="keywords"
                                placeholder=" @lang('bahasa.pencarian')" name="keywords">

                            <div style="bottom:40px;" align="center">Provinsi</div>

                            <select style="color:grey" class="custom-select titlez" id="provinsi" name="type">
                                <option value="">@lang('bahasa.indonesia')</option>
                                @foreach($propertiesx as $list)

                                <option  value="{{$list->id_daerah}}">{{$list->nama}}</option>


                                @endforeach
                            </select>

                            <div align="center">Kabupaten/kota</div>
                            <select style="color:grey"  class="custom-select titlez" id="kota" name="status">
                                <option value="">@lang('bahasa.Semua')</option>

                            </select>
                          


                           <br>
                           
                           <h5>Filter by Sector</h5>
                            <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="industri" name="industri" value="2" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a style="color:grey">&nbsp;@lang('bahasa.Industri')</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="infra" name="infra" value="3"  checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a style="color:grey">&nbsp;@lang('bahasa.Infrastruktur')</a>
					</div>
				</div>	
                <!-- <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="jasa" name="jasa" value="5" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a style="color:grey">&nbsp;Jasa</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="pangan" name="pangan" value="4" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a style="color:grey">&nbsp;Pangan dan Pertanian</a>
					</div>
				</div>	 -->
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="pariwisata" value="6" name="pariwisata" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a style="color:grey">&nbsp;@lang('bahasa.Pariwisata')</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox"  id="smelter" value="7" name="smelter" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a style="color:grey">&nbsp;Smelter</a>
					</div>
				</div>	
                              
                              
                           
           
             

                            <!--Row - Min price & Max price-->
                            <div class="wrapper" style="padding:10px;">
                                <div  style="color:grey" class="range-slider">
                                    <input type="text" class="js-range-slider" value="" />
                                </div>
                                <input  type="hidden" id="min" class="js-input-from form-control" value="0" />
                                <input type="hidden" id="max" class="js-input-to form-control" value="0" />




                                <!--Submit button-->
                                <div class="form-group my-2">
                                    <div type="submit" style="background-color:royalblue" class="btn" id="gelo">@lang('bahasa.cari')
                                    </div>
                                </div>



                        </form>

                    </section>
                    </aside>
            </div>
            <div class="col-sm-9">
                <section style="left:5px; height:440px; width:100%;"  class=" ts-box p-1">
                    
                    <div class="">

    <div style="background-color:#1066b9;" class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                    <a style="color:white; href="#" id="ts-display-grid" "
                                                class=" btn active ">
                                                @lang('bahasa.Peta')

                                    </a>
                                   
                                </div>

                           

                            </div>

    
                        <section  class="mb-0 flex-wrap">
                           
                            <div id="mydatas2" class="ts-map w-100 ts-min-h__50 vh ts-z-index__1">
                            </div>
                        
                            <div id="layer" style="right:40px; top:0px; width:285px" class="ts-form__map-search ts-z-index__2">
                                <div class="zink ts-form-collapse "
                                id="collapseExample">
                                <div style="float:left;"><i class="fa fa-road aria-hidden=" true"></i>
                                    Boundaries</div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<input
                                    align="center" style="left:20px;" type="checkbox" name="my-checkbox"
                                    id="osmCheck" checked data-size="mini">
    
                            </div>
                            <div class="zink ts-form-collapse "
                            id="collapseExample">
                            <div style="float:left;"><i class="fa fa-plane" aria-hidden="true"></i>
                                International Port</div>
                            &nbsp;&nbsp;<input data-checked="true" type="checkbox" name="dadan"
                                id="osmCheck" data-size="mini">

                        </div>
                    </div>

                </div>
          
                <!--end ts-form-collapse-->
                <div  style="width:35px;  right:14px; top:125px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="layerclick"  class="">

                        <i class="fa fa-map-o" aria-hidden="true"></i>

                    </div>
                    
                </div> <div  style="width:35px;  right:14px; top:165px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="baseclick"  class="">

                        <i class="fa fa-th-large" aria-hidden="true"></i>

                    </div>
                </div>

            <br>
            <div id="layerhidden" style="right:40px; top:50px; width:300px;" class="ts-form__map-search ts-z-index__2">
            <form>
                        <div align="center" class="zink">
                            <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                                <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer
                            </div>
                        </div>
                        <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  > @lang('bahasa.bandara') : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                    name="bandara" /><span class="toogle "></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >@lang('bahasa.Pelabuhan') :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pelabuhan" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >@lang('bahasa.rumah_sakit') :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="rumah_sakit" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >@lang('bahasa.Pendidikan') :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pendidikan" /><span class="toogle"></span></div></dd><br>
                </div>
            

                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Hotel :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="hotel" /><span class="toogle"></span></div></dd><br>
                </div>
                </form>  
                
            </div>
                                    
                                    <div id="legendlayer"  style="width:179px;  left:50px; bottom:15px" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                                 
                                        <div id ="LegendIndustri" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/PeoplePlaces/esriBusinessMarker_66_Red.png" width="23px;">&nbsp;Industri</div>
                                        <div id ="LegendInfrastruktur" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png" width="23px;">&nbsp;Infrastruktur</div>
                                        <!-- <div id ="LegendPangan" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png" width="23px;">&nbsp;Pangan & Pertanian</div>
                                        <div id ="LegendJasa" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png" width="23px;">&nbsp;Jasa</div> -->
                                        <div id ="pariwisata" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/OutdoorRecreation/Mountain.png" width="23px;">&nbsp;Pariwisata</div>
                                     
                                    </div>
                                
                                                             <div data-dojo-type="dijit/layout/BorderContainer" 
            data-dojo-props="design:'headline', gutters:false" 
            style="width:100%;height:100%;margin:0;">
            <div class="" id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:2px;">
           
                        </div>
        </div>

                            
          
                            <!--end ts-form-collapse-->



                </section>
                </section>
                





                <br>







                <!--end form-row-->


             
     

                <!-- filter-horizontal -->


<br>
                <div class="post-details">
                    <section style="bottom:10px; left:10px; " id="tbody" class="ts-box p-1 hasil">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control ">
                            <div style="background-color:#1066b9;" class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                    <a style="color:white; href="#" id="ts-display-grid" "
                                                class=" btn active ">
                                                <i class=" fa fa-th-large"></i>
                                    </a>
                                    <!--<a style="color:white; href="#" id="ts-display-list" class="btn">
                                        <i class="fa fa-th-list"></i>-->
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                                <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                                   <!-- <label for="sorting" style="color:white; "
                                        style="background-color:royalblue" class="mb-0 mr-2 text-nowrap">Sort
                                        by:</label>
                                    <select  style="background-color:#1066b9"  class="zink" id="sorting" name="sorting">
                                        <option style="color:white;" value="">
                                            <p style="color:white;">Default</p>
                                        </option>
                                        <option style="color:white;" value="1">Price lowest first</option>
                                        <option style="color:white;" value="2">Price highest first</option>
                                        <option style="color:white;" value="3">Distance</option>
                                    </select>-->
                                </div>

                            </div>
                            <br>
                            <div id="daerah">

                             
<section  >

   

    <div class="container">
   
                                       <!--Featured Items-->
                                       <div  class="row">
   
                                           @foreach ($propertiesz as $list)
                                           
                            
                                           <!--Item-->
                                           <!--Item-->
                                      
                                           <div  class="col-sm-8 col-lg-4">
                                               <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card ">
   
                                                   <!--Ribbon-->
                                                  
                                                    <div style="color:royalblue" class="ts-ribbon-corner">
                                                      <span>@lang('bahasa.prioritas')</span>
                                                             </div>
                                                      
                                                             
   
                                                   <!--Card Image-->
                                                   <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}/id"
                                                       class="card-img ts-item__image"
                                                       >
                                                         <img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                  
                                                   
                                                   
                                                       <figure class="ts-item__info">
                                                           <h6>{{$list->judul}}</h6>
                                                         
                                                       </figure>
                                                       <div style="background-color:#e70d0de1; border-radius: 10px; color:white;" class="ts-item__info-badge">{{$list->sektor}}
   </div>
                                                   </a>
   
   
                                                   <div class="card-body ts-item__body">
                                                    <div class="atbd_listing_meta">
						<span id="openmap" class="atbd_meta atbd_badge_open">@lang('bahasa.estimasi')</span>
                                                        <span style="background-color:royalblue; font-size:13px; border-radius: 10px"  class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000) Juta
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000) Miliar
                             
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000000) Triliun
                                                             <?php } ?></span>
                                                        
                                                        <div class="atbd_listing_data_list">
                                                            <ul>
                                                                <li>
                                                                    <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                                </li>
                                                                <li>
                                                                
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                   </div>
   
                                                   <div class="atbd_listing_bottom_content">
                                                    <div class="atbd_content_left">
                                                        <div class="atbd_listing_category">
                                                            <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}/id"style="color:dimgrey;" ><span style="color:dimgrey;" class="fa fa-eye"></span>@lang('bahasa.detail')</a>
                                                        </div>
                                                    </div>
                                                    <ul class="atbd_content_right">
                                                        <li class="atbd_count"></span></li>
                                                        <li class="atbd_save">
                                                          
                                                        </li>
                                                    </ul>

                                                </div>
   
                                               </div>
                                               <!--end ts-item ts-card-->
                                           </div>
   
                                      
                                         
                                        <a>
   
                                 
                                          
                                           <!--end Item col-md-4-->
                                       
   
                                           @endforeach
                                                                                      <!--Item-->
                                     
                               </section>
   <div class="center">
@if ($propertiesz->lastPage() > 1)
<ul class="pagination">
    <li class="{{ ($propertiesz->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ Config::get('app.url')}}/{{ Request::segment(1) }}/{{ Request::segment(2) }}?page={{($propertiesz->currentPage()-1) }}" "><<<&nbsp;&nbsp;&nbsp;</a>
    </li>
    @for ($i = 1; $i <= $propertiesz->lastPage(); $i++)
        <li class="{{ ($propertiesz->currentPage() == $i) ? ' active' : '' }} page-item">
            <a class="page-link"  href="https://regionalinvestment.bkpm.go.id/portal/peluang/id?page={{$i}}">{{ $i }}</a>
        </li>
    @endfor
    <li class="{{ ($propertiesz->currentPage() == $propertiesz->lastPage()) ? ' disabled' : '' }}">
        <a href="{{ Config::get('app.url')}}/{{ Request::segment(1) }}/{{ Request::segment(2) }}?page={{($propertiesz->currentPage()+1) }}" >&nbsp;&nbsp;&nbsp;>>></a>
    </li>
</ul>
@endif    
</div>

                            </div>
<style>.center {


  margin: auto;


  width: 30%;





  padding: 10px;


}

</style>
    </main>
 


</body> 

</html>
    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
    <script src="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/js/ion.rangeSlider.min.js"></script>
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
    <script>
        var $range = $(".js-range-slider"),
            $inputFrom = $(".js-input-from"),
            $inputTo = $(".js-input-to"),
            instance,
            min = 1,
            max = 100,
            from = 0,
            to = 0;

        $range.ionRangeSlider({
            skin: "round",
            type: "double",
            min: min,
            max: max,
            from: 1,
            to: 100,
            prefix: "Rp.",
            min_postfix: "--",
            postfix: " M",
            max_postfix: "++",
            onStart: updateInputs,
            onChange: updateInputs
        });
        instance = $range.data("ionRangeSlider");

        function updateInputs(data) {

            if (data.from < 2) {
                from = data.from * 1000000;
            } else {
                from = data.from * 1000000000;
            }
            if (data.to < 99) {
                to = data.to * 1000000000;
            } else {
                to = data.to * 1000000000000;
            }
            $inputFrom.prop("value", from);
            $inputTo.prop("value", to);
        }

        $inputFrom.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < min) {
                val = min;
            } else if (val > to) {
                val = to;
            }

            instance.update({
                from: val
            });
        });

        $inputTo.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < from) {
                val = from;
            } else if (val > max) {
                val = max;
            }

            instance.update({
                to: val
            });
        });
    </script>
 
  
    <script>
    
        $(document).ready(function () {
            const postDetails = document.querySelector(".hasil");

const postSidebar = document.querySelector(".pencarian");
const postDetailsz = document.querySelector(".ts-boxzzs");
const postSidebarContent = document.querySelector(".ts-bosxzz > div");


const controller = new ScrollMagic.Controller();


const scene = new ScrollMagic.Scene({
    triggerElement: postSidebar,
    triggerHook: 0,
    duration: getDuration
}).addTo(controller);

//3
if (window.matchMedia("(min-width: 768px)").matches) {
    scene.setPin(postSidebar, {
        pushFollowers: false
    });
}

//4
window.addEventListener("resize", () => {
    if (window.matchMedia("(min-width: 768px)").matches) {
        scene.setPin(postSidebar, {
            pushFollowers: false
        });
    } else {
        scene.removePin(postSidebar, true);
    }
});

function getDuration() {




    return (postDetails.offsetHeight - postSidebar.offsetHeight) + 500;

}
            // LegendLayer
            $('#industri').change(function(){
        if(this.checked)
            $('#LegendIndustri').show();
        else
            $('#LegendIndustri').hide();

    });
    $('#infra').change(function(){
        if(this.checked)
            $('#LegendInfrastruktur').show();
        else
            $('#LegendInfrastruktur').hide();

    });
            
    $('#pangan').change(function(){
        if(this.checked)
            $('#LegendPangan').show();
        else
            $('#LegendPangan').hide();

    });
            
    $('#jasa').change(function(){
        if(this.checked)
            $('#LegendJasa').show();
        else
            $('#LegendJasa').hide();

    });
    $('#base').hide();
    $('#layerhidden').hide();
    $("#layerclick").click(function(){
        
        $('#layerhidden').toggle();
        $('#layeron').toggleClass("hijau");

    
});
$("#baseclick").click(function(){
        
        $('#base').toggle();
        $('#baseon').toggleClass("hijau");

    
});
            $('#layer').hide();
            $('#legendlayer').hide();
            $('#provinsi').change(function (e) {
                $.ajax({
                    url: "{{ Config::get('app.url') }}/getkota/" + $(this).val(),
                    method: 'GET',
                    success: function (data) {
                        console.log(data);

                        $('#kota').children('option:not(:first)').remove().end();

                        $.each(data, function (index, kotaObj) {

                            $('#kota').append('<option value="' + kotaObj
                                .id_daerah + '">  ' + kotaObj.bentuk_daerah +
                                ' ' + kotaObj.nama + ' </option>')
                        });
                    }
                });
            });

            $('.aku').on('click', function () {
                $('.collapse').collapse();
            })
        
           var sing = $.get("{{ Config::get('app.url') }}/map-home?keywords=", function (data) {
                $("#mydatas2").html(data);
          
          
            });

         

            setTimeout(function () {
                $('body').addClass('loaded');
                $('h1').css('color', '#FFFFF');
            }, 2000);
            $("#gelo").click(function () {
                $("#loader-wrapper").show();
                var industri = [];

                $("input:checkbox[name=industri]:checked").each(function () {
                    industri.push($(this).val());
                });
                var infra = [];

                $("input:checkbox[name=infra]:checked").each(function () {
                    infra.push($(this).val());
                });
                var pariwisata = [];

            $("input:checkbox[name=pariwisata]:checked").each(function () {
    pariwisata.push($(this).val());
        });

    var smelter = [];

            $("input:checkbox[name=smelter]:checked").each(function () {
    smelter.push($(this).val());
        });

                var pangan = [];

                $("input:checkbox[name=pangan]:checked").each(function () {
                    pangan.push($(this).val());
                });
                var jasa = [];

                $("input:checkbox[name=jasa]:checked").each(function () {
                    jasa.push($(this).val());
                });
                var str = $("#keywords").val();
                var min = $("#min").val();
                var max = $("#max").val();
                var prov = $("#provinsi").val();
                var kota = $("#kota").val();



                // alert("{{ Config::get('app.url') }}/cari?keywords=" + str + "&category=" + kota + "&type=" + prov +
                //     "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
                //     "&pangan=" + pangan + "&jasa=" + jasa + "&pariwisata=" + pariwisata + ""
                // );
  
                $.get("{{ Config::get('app.url') }}/map-home?keywords=" + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
                    "&pangan=" + pangan + "&jasa=" + jasa +  "&pariwisata=" + pariwisata + "",
                    
                    function (data) {
                        $("#mydatas2").html(data);
                       
                 
                    });
                $.get("{{ Config::get('app.url') }}/listingprioritas/en?keywords="  + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
                    "&pangan=" + pangan + "&jasa=" + jasa +  "&pariwisata=" + pariwisata +  "&smelter=" + smelter +  "",
                    function (data) {
                        $("#daerah").html(data);

                    });

            });



        });
        $("[name='my-checkbox']").bootstrapSwitch({

            onSwitchChange: function (event, state) {

                if (state) map.addLayer(wildfireRisk);
                else map.removeLayer(wildfireRisk);
            }
        });

        $("[name='industsdri']").bootstrapSwitch({

            onSwitchChange: function (event, state) {

           
            }
        });


        $("[name='dadan']").bootstrapSwitch({
            onSwitchChange: function (event, state) {

                if (state) {
                    map.addLayer(bandara);


                } else {
                    map.removeLayer(bandara);
                }
            }
        });


        $("[name='dadan']").click(function (event) {
            event.preventDefault();
            if (map.hasLayer(sights)) {
                $(this).removeClass('selected');
                map.removeLayer(bandara);
            } else {
                map.addLayer(bandara);
                $(this).addClass('selected');
            }
        });
    </script>

    @endsection