
   <!-- Include fusioncharts core library file -->


 <div id="chart-container">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
                FusionCharts.ready(function(){
                    var chartObj = new FusionCharts({
            type: 'scrollcombidy2d',
            renderAt: 'chart-container',
            width: '100%',
            height: '430',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fusion",
                    "caption": "Realisasi Investasi Per Provinsi",
                    "subCaption": "2016 - 2019",
                 
                    "numberScaleValue": "10,100,1000",
                    "numberScaleUnit": " juta, Miliar, Triliun",
                        
                        "valuePadding": "5",
                    "numberPrefix": "Rp ",
                   
                  
                   
                    "numVisiblePlot": "12",
                    "flatScrollBars": "1",
                    "scrollheight": "10"
                },
                "categories": [{
                    "category": [@foreach($investasi as $chart){ 
                            "label": "Province {{$chart->nama}}"
                        }, @endforeach
                       
                    ]
                }],
                "dataset":  [{
                        "seriesName": "PMA (US$. ribu)",
                        
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pma}}",
                                "tooltext": "Sektor {{$chart->nama}} : @currency2($chart->investasi_pma) "
                            }, @endforeach
                           
                        ]
                    },
                    {
                        "seriesName": "PMDN (Rp. Juta)",
                      
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pmdn}}",
                                "tooltext": "Sektor {{$chart->nama}} : @currency($chart->investasi_pmdn) "
                            },@endforeach
                          
                        ]
                    },
                
                  
                ]
            }
        });
                    chartObj.render();
                });
            </script>