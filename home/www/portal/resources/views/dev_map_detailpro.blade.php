

@extends('frontend.layoutmap')

@section('content')
<script src="{{Config::get('app.url')}}assets/js/custom.js"></script>
  <script src="{{Config::get('app.url')}}js/jquery.magnific-popup.min.js"></script>

<html>  
<head> 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
  <title>Basemap gallery</title>
  <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
  <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
  <style> 
    html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
    #map{
      padding:0;
    }
p {
    line-height: 1.5rem;
    color: rgb(0 0 0 / 76%);
}
  </style> 
  

    

  <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
  <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
  <style> 
    html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
    #map{
      padding:0;
    }
  </style> 
  
  <script src="https://js.arcgis.com/3.34/"></script>
  <script> 
var map;
require([
  "esri/map",
   "esri/dijit/PopupTemplate",
     "esri/layers/FeatureLayer",
    "dojo/_base/array",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/geometry/Geometry",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
    "esri/InfoTemplate",
   "esri/dijit/BasemapGallery",
	"esri/dijit/HomeButton",

    "esri/arcgis/utils",
  "dojo/parser",

  "dijit/layout/BorderContainer", 
  "dijit/layout/ContentPane", 
  "dijit/TitlePane", 
      "dijit/layout/AccordionContainer",
  "dojo/domReady!",
 "esri/dijit/Legend"
], function(
  Map,
        PopupTemplate, 
     FeatureLayer, 
     arrayUtils, 
     ArcGISDynamicMapServiceLayer, 
     Geometry, 
     Point, 
     webMercatorUtils,  
     Graphic, 
     SimpleMarkerSymbol, 
     SimpleLineSymbol, 
     SimpleFillSymbol,
     PictureMarkerSymbol,  
     Color, 
     InfoTemplate,
   BasemapGallery,
HomeButton,

    arcgisUtils,
  parser,
Legend
) {
  parser.parse();

   @foreach($propertiesx as $list){
map = new Map("map", {
  basemap: "hybrid",
  center: [{{$list->y}}, {{$list->x}}],
  zoom: 12
});
}
  //add the legend
      map.on("layers-add-result", function (evt) {
        var layerInfo = arrayUtils.map(evt.layers, function (layer, index) {
          return {layer:layer.layer, title:layer.layer.name};
        });
        if (layerInfo.length > 0) {
          var legendDijit = new Legend({
            map: map,
            layerInfos: layerInfo
          }, "legendDiv");
          legendDijit.startup();
        }
      });

@endforeach
  var home = new HomeButton({
        map: map
      }, "HomeButton");
      home.startup();
    //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
  var basemapGallery = new BasemapGallery({
    showArcGISBasemaps: true,
    map: map
  }, "basemapGallery");
  basemapGallery.startup();
  
  basemapGallery.on("error", function(msg) {
    console.log("basemap gallery error:  ", msg);
  });

   // Layer Infrastruktur
   var title4 = '<div align="center">Infrastruktur Bandara</div>';
        var isi4 =
            '<div style="float:left;"  >Name Airport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_bandara = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var bandara = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/bandara_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_bandara
            });
        map.addLayer(bandara);

        var title5 = '<div align="center">Infrastruktur Seaport</div>';
        var isi5 =
            '<div style="float:left;"  >Name Seaport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pelabuhan = new PopupTemplate({
            title: title5,
            description: isi5
        });
        var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pelabuhan
            });
        map.addLayer(pelabuhan);
        @foreach($test as $list)
        var title{{$list->id_rest}}{{$list->status}} = '<div align="center">Infrastruktur </div>';
        var isi{{$list->id_rest}}{{$list->status}} =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_kontur{{$list->id_rest}}{{$list->status}} =new InfoTemplate("{{$list->nama_rest}}", "${*}");       
        var kontur{{$list->id_rest}}{{$list->status}}= new FeatureLayer(
            "{{$list->rest_peluang}}", {
                outFields: ["*"],

                infoTemplate: Popup_kontur{{$list->id_rest}}{{$list->status}}
            });
@if($list->status == '2')
        map.addLayer(kontur{{$list->id_rest}}{{$list->status}});
@else
 map.addLayer(kontur{{$list->id_rest}}{{$list->status}});
@endif
        @endforeach
        var title7 = '<div align="center">Infrastruktur Hospital</div>';
        var isi7 =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title7,
            description: isi7
        });
        var rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(rumah_sakit);

        var title8 = '<div align="center">Infrastruktur Education</div>';
        var isi8 =
            '<div style="float:left;"  >Name Educational :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pendidikan = new PopupTemplate({
            title: title8,
            description: isi8
        });
        var pendidikan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pendidikan_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pendidikan
            });
        map.addLayer(pendidikan);
        var title9 = '<div align="center">Infrastruktur Hotel</div>';
        var isi9 =
            '<div style="float:left;"  >Name Hotel :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_hotel = new PopupTemplate({
            title: title9,
            description: isi9
        });
        var hotel = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/hotel_en/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_hotel
            });
        map.addLayer(hotel);
        // Akhir layer infrastruktur

        rumah_sakit.hide();
        pendidikan.hide();
        hotel.hide();
        pelabuhan.hide();
        bandara.hide();
     

   var title1 ='<div align="center">Batas Wilayah Provinsi</div>';
var isi1 ='<div style="float:left;"  >Nama Provinsi :  </div><dd style="margin-left:103px;  ; "class="border-bottom pb-2"> {Provinsi}</dd>'
var title2 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi2 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
var title4 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi4 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
       var popupprov = new PopupTemplate({
    title: "Batas Wilayah Provinsi",

    fieldInfos: [

      { fieldName: "wa", visible: true, label: "Provinsi :", format: { places: 0   } },
       
    ],

  
           });
      

    var popupkota = new PopupTemplate({
    title: "Boundaries Kabupaten",

    fieldInfos: [

      { fieldName: "namobj", visible: true, label: "Name Kabupaten :", format: { places: 0   } },

     
    ],

  
           });
      

    var popupkabupaten = new PopupTemplate({
    title: "Batas Wilayah Kabupaten/kota",

    fieldInfos: [

      { fieldName: "wadmkk", visible: true, label: "Kabupaten/kota :", format: { places: 0   } },
      { fieldName: "wadmpr", visible: true, label: "Provinsi :", format: { places: 0   } },
  
     
    ],

  
           });
      
  

    var title3 ='<div align="center">RTRW Kabupaten</div>';
var isi3 ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popuprtrw = new PopupTemplate({
    title: "Layer RTRW",

    fieldInfos: [

      { fieldName: "NAMOBJ", visible: true, label: "Name  :", format: { places: 0   } },
      { fieldName: "WADMKK", visible: true, label: "Kab/kota :", format: { places: 0   } },
      { fieldName: "WADMPR", visible: true, label: "Province :", format: { places: 0   } },
      { fieldName: "NOTHPR", visible: true, label: "Rule :", format: { places: 0   } },
      { fieldName: "PP", visible: true, label: "File1:", format: { places: 0   } },
      { fieldName: "BA", visible: true, label: "File2:", format: { places: 0   } },
      { fieldName: "STSDRH ", visible: true, label: "Status Regional:", format: { places: 0   } },
      { fieldName: "STSDAT", visible: true, label: "Data Status:", format: { places: 0   } },
    
    ],

  
           });
      

        var result = '';
   var view_data;
   function nameToCode(nameGroup, callback) {
  

  var jqxhr = $.post( "https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken", { username: 'bkpm-sipd', password: 'Bkpm_S1pd@atrbpn123' })
  .done(function( data ) {
    callback(data);
  });
 
  
  } 

  
  nameToCode("some text", function(variable)
{
    

 

      var key = variable;
            var tokenz='?token=GM2cH2xFViccaPnjbJLtU1kyTkv7L1ua_byrJ-kGEWXYCJntGvUa4VrhiPSlM70ciHevyQoF-hsEX3M3e-NOo4fiScj_gMT9eFlYPrP4hV8ZmtTvUSWsv2VZfJYAi7S4RzWZ3trO2fi5nEDYMLK2Xg..';
            @foreach($propertiesz as $list)
                  var linkzz='{{$list->rtrw}}'                     
                          @endforeach              
          var rtrwprov = new FeatureLayer('{{$list->rtrw}}?token=GM2cH2xFViccaPnjbJLtU1kyTkv7L1ua_byrJ-kGEWXYCJntGvUa4VrhiPSlM70ciHevyQoF-hsEX3M3e-NOo4fiScj_gMT9eFlYPrP4hV8ZmtTvUSWsv2VZfJYAi7S4RzWZ3trO2fi5nEDYMLK2Xg..',{
           mode: FeatureLayer.MODE_ONDEMAND,
              outFields: ["*"],
              opacity:0.5,
        infoTemplate: popuprtrw
         });
         map.addLayer(rtrwprov);
         $('input:checkbox[name=rtrw]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rtrwprov.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rtrwprov.hide();
                   
                  }

         });
       
        });
        @foreach($test as $list)
@if($list->status == '2')
kontur{{$list->id_rest}}{{$list->status}}.show();
@else
  kontur{{$list->id_rest}}{{$list->status}}.hide();
@endif
        $('input:checkbox[name=pro{{$list->id_rest}}{{$list->status}}]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      kontur{{$list->id_rest}}{{$list->status}}.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    kontur{{$list->id_rest}}{{$list->status}}.hide();
                  }

         });
         @endforeach
         $('input:checkbox[name=bandara]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      bandara.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    bandara.hide();
                  }

         });
         $('input:checkbox[name=rumah_sakit]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rumah_sakit.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rumah_sakit.hide();
                  }

         });
         $('input:checkbox[name=pendidikan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pendidikan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pendidikan.hide();
                  }

         });
         $('input:checkbox[name=pelabuhan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pelabuhan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pelabuhan.hide();
                  }

         });
         $('input:checkbox[name=hotel]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     hotel.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                   hotel.hide();
                  }

         });
         map.on("extent-change",function(evt)
      {
        console.log("extent change",map.getZoom());
        
        
        if ( map.getZoom() > 9 ) {
          featureLayerprovinsi.hide();
            } else {
              featureLayerprovinsi.show();
            }
      
        
      } 
         
      
    );
    var featureLayerprovinsi = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0",{
      outFields: ["*"],
      
infoTemplate: popupprov
 });
map.addLayer(featureLayerprovinsi);


var featurekabupaten = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_kabupaten/MapServer/0",{
    opacity: 0.5,
      outFields: ["*"],
      
infoTemplate: popupkabupaten
 });
map.addLayer(featurekabupaten);

                


        <?php foreach ($propertiesz as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
         
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> </dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Infrastruktur') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> <br></dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("openmap").innerHTML = "{{$list->y}}, {{$list->x}}";
           alert('as');
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

    
       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Jasa') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> </dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Industri') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> <br></dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/PeoplePlaces/esriBusinessMarker_67_Yellow.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach
<?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->id_sektor_peluang == '6') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> </dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/OutdoorRecreation/Mountain.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach


       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->id_sektor_peluang == '7') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> </dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/PeoplePlaces/esriBusinessMarker_66_Red.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach


       Rumah_sakit.hide();
      $('input:checkbox[name=rumah]').change(function () {
          if ($(this).is(':checked')) {
              //map.graphics(layer1).hide();
             
              Rumah_sakit.show();
          
              // map.graphics.hide();  
          } else {
            Rumah_sakit.hide();
          }


          function hideLayer1() {

              dd.push(layer1);
              
          }

          function hideLayer2() {}
      });

});


</script> 
</head> 

<body class="claro"> 
  <div class=" row">

            <div class="col-md-4">
                <div class="widget">
                    <section style="top:19px;" class="ts-box p-1 pencarian when ">
                        <div style="background-color:royalblue; color:white;" class=" wings  ">

                            <!--Display selector on the left-->
                            <div class="zink titlez " align="center">

                                <i class="fa fa-search"> Info Peluang</i>


                            </div>
                        
                        <br>
                       
@foreach($propertiesz as $list)
 @foreach($bentuk as $lisa)

               
 <div style="float:left;"  >@lang('bahasa.sektor')&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->sektor}}</dd>
                        <div style="float:left;"  >@lang('bahasa.proyek')&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   </div><dd style="margin-left:75px;  "class="border-bottom pb-2"> {{$list->judul}}</dd>
                        <div style="float:left;"  >@lang('bahasa.provinsi')&nbsp;&nbsp;&nbsp;:    </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$lisa->nama}}</dd>
                        <div style="float:left;"  >@lang('bahasa.kota')&nbsp;:    </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->nama_kabkot}}</dd>

@endforeach
                       
                      <div style="float:left;"  >@lang('bahasa.luas')&nbsp;:  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">{{$list->rest}}<br></dd>
 
<div style="float:left;"  >Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">&nbsp; {{$list->tahun}}</dd>
<div style="float:left;"  ><div style="float:left;">@lang('bahasa.investasi')</div> <br> @lang('bahasa.nilai')   </div><dd style="margin-left:75px; margin-top:20px; text-align:justify; "class="border-bottom pb-2">: <?php if ($list->nilai_investasi  < 1000000000) { ?>
                               @currency($list->nilai_investasi / 1000000) Juta
                                <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000) Milyar

                                <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000000) Triliyun
                                <?php } ?></dd>
                                <div style="float:left;"  >IRR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">@if($list->irr == '') <br>@else {{$list->irr}}@endif </dd>
                                 <div style="float:left;"  >NPV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">@if($list->npv == '') <br>@else {{$list->npv}}@endif </dd>
                                  <div style="float:left;"  >PP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">@if($list->pp == '') <br>@else {{$list->pp}}@endif </dd>
<div style="float:left;"  >@lang('bahasa.sumber')&nbsp;&nbsp;&nbsp;&nbsp;:  </div><dd style="margin-left:75px;  "class="border-bottom pb-2">{{$list->instansi}}</dd>
<div style="float:left;"  ></div><dd style="margin-left:75px; text-align:justify; "class=""> <button data-toggle="modal" data-target="#myModal" class="btn zink hijau">Unduh Data</button></dd>
@endforeach
                        
                        </form>

                    </section>
                    </aside>
                </div>
            </div>
            

            <div class="col-sm-8">
                <div class="post-details">
             
          

  




<div id="layerclick"  style="width:35px;  right:30px; top:150px"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
    <div id="layeron" class="">

        <i class="fa fa-map-o" aria-hidden="true"></i>

    </div>
</div>

<div id="basemapclick"  style="width:35px;  right:30px; top:190px"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
    <div id="basemapon" class="">

        <i class="fa fa-th-large" aria-hidden="true"></i>

    </div>
</div>

<div id="layerpro"  style="width:35px;  right:30px; top:230px"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
    <div id="basemapon2" class="">

        <i class="fa fa-th-large" aria-hidden="true"></i>

    </div>
</div>
</div>
        
            <div id="layer" style=" width:300px; right:55px; top:120px" class="ts-form__map-search ts-z-index__2">


              
                   

                     <div style ="align-center;"class="zink" id="collapseExample">
                     <form>
                        <div align="center" class="zink">
                            <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                                <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Infrastruktur
                            </div>
                        </div>
                        <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >RTRW :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input checked type='checkbox'
                                    name="rtrw" /><span class="toogle on"></span></div></dd><br>
                </div>
                        <div style="align-center;" class="zink "
                            id="collapseExample">
                     
                           
                            <div style="float:left;"  > Bandara : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                    name="bandara" /><span class="toogle "></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pelabuhan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pelabuhan" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Rumah Sakit :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="rumah_sakit" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pendidikan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pendidikan" /><span class="toogle"></span></div></dd><br>
                </div>
            

                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Hotel :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="hotel" /><span class="toogle"></span></div></dd><br>
                </div>
                </form>      </div>
                   

            </div>

            <div id="layerpro2" style=" width:350px; right:50px; top:120px" class="ts-form__map-search ts-z-index__2">


              
                   <style>
.scroll{
 
 width: 350px;
  overflow: scroll;
overflow-x: hidden;
 padding: 15px;
  height: 350px;
 }
</style>

<div style ="align-center; right:25px; top:15px;"class="zink scroll" id="collapseExample">
<form>
   <div align="center" class="">
       <div href=".ts-form-collapse" data-toggle="collapse" style=" right:0px;" class="zink aku  ">
           <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Detail
       </div>
   </div>
   @foreach($test as $list)
@if($list->status =='2')
   <div style="align-center; width:350px; right:10px;" style=" " class="zink   "
       id="collapseExample">
       <div style="float:left;"  >{{$list->nama_rest}}   </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:10px;" class="box-1"><input checked  type='checkbox'
               name="pro{{$list->id_rest}}{{$list->status}}" /><span class="toogle on"></span></div></dd><br>
</div>@else
 <div style="align-center; width:350px; right:10px;" class="zink "
       id="collapseExample">
       <div style="float:left;"  >{{$list->nama_rest}}   </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:10px;" class="box-1"><input type='checkbox'
               name="pro{{$list->id_rest}}{{$list->status}}" /><span class="toogle on"></span></div></dd><br>
</div>
@endif
  @endforeach
</form>      </div>


</div>


        </section>
          <section id="tbody" class="ts-boxz p-1" style="margin-top:20px;">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control zink">
                            <div class="container clearfix zink titlez ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn titlez">
                                        Peta
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                             

                            </div>
                            </section>

       

<style>
    html, body, #map {
      padding:0;
      margin:0;
      height:100%;
    }
    #HomeButton {
      position: absolute;
      top: 220px;
      left: 20px;
      z-index: 50;
    }
  </style>
<br>
<a align="center" href="{{Config::get('app.url')}}/propertyprofull/{{Request::Segment(2)}}/{{Request::Segment(3)}}" class="zink ts-form__map-search ts-z-index__2" width="200px">Full Peta</a>
                     <br>
<br>
                   <div data-dojo-type="dijit/layout/BorderContainer" 
       data-dojo-props="design:'headline', gutters:false" 
       style="width:100%;height:100%;margin:0;">

    <div id="map" 
         data-dojo-type="dijit/layout/ContentPane" 
         data-dojo-props="region:'center'" 
         style="padding:0;" class="map">
 
      <div id="hide"style="position:absolute; right:55px; top:10px; z-Index:999;">
        <div data-dojo-type="dijit/TitlePane" 
             data-dojo-props="title:'Switch Basemap', open:true">
          <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
            <div id="basemapGallery"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
   
          <div class="ts-form__map-search ts-z-index__2" id="HomeButton"></div>      
                            </section>
                            <section id="tbody" class="ts-boxz p-1" style="margin-top:20px;">

<!--ITEMS LISTING
=========================================================================================================-->
<section id="display-control zink">
    <div class="container clearfix zink ">

        <!--Display selector on the left-->
        <div class="float-left">
        
            <a id="ts-display-list" class="btn">
                Gambar & Video
            </a>
        </div>

        <!--Display selector on the right-->
     

    </div>
    </section>
    <br>
    
<div class="row">
<div class="col-sm-6">  @foreach($propertiesz as $list) <div class="owl-carousel ts-gallery-carousel" data-owl-auto-height="1" data-owl-dots="1" data-owl-loop="1">

<!--Slide-->
<!--Slide-->
<div class="slide">
<div class="ts-image" data-bg-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}">
<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
</div>
</div>
<div class="slide">
<div class="ts-image" data-bg-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar1}}">
<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar1}}" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
</div>
</div>
<div class="slide">
<div class="ts-image" data-bg-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar2}}">
<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar2}}" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
</div>
</div>
<div class="slide">
<div class="ts-image" data-bg-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar3}}">
<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar3}}" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
</div>
</div>

<div class="slide">
<div class="ts-image" data-bg-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar4}}">
<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar4}}" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
</div>
</div>




@endforeach
</div></div>

<div class="col-sm-6"><section style="bottom:20px; "id="video">

<br>

    <div class="embed-responsive embed-responsive-16by9 rounded ts-shadow__md">
  @if (count($propertiesx))   
  
<video controls="true" class="embed-responsive-item">
<source src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->video}}" type="video/mp4" />
</video>

               @else
  <p>Kosong</p>
  @endif
    </div>



</section>
</div>
</div>
</section>

                   <section  class="ts-boxz p-1 long" style="margin-top:20px;">

            
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Deskripsi
                                    </a>
                                </div>

                               
                               

                            </div>
                       <?php foreach ($propertiesz as $list) { ?>
  
                           <div style="margin-left:20px; margin-right:20px; color:black; margin-top:20px;"><?php echo $list->deskripsi ?> </div>
                         
                       <?php } ?>
                          
                             
                            </section>
                           
                    
                    
                     
                           
                            
                            
                  
                         <div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->
    <div class="modal-content">

      <!--Modal cascading tabs-->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            <br>
            <div  align="center">
            <label>Send To email Download Data</label> <div style ="margin-left:20px; margin-right:20px; float:right;" class="form-group">
                    <button type="submit" class="btn btn-md btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
            <br>
            @if(\Session::has('alert-failed'))
                <div class="alert alert-failed">
                    <div>{{Session::get('alert-failed')}}</div>
                </div>
            @endif
            @if(\Session::has('alert-success'))
                <div class="alert alert-success">
                    <div>{{Session::get('alert-success')}}</div>
                </div>
            @endif
            @foreach($propertiesz as $list)
            <form action="{{Config::get('app.url')}}/sendEmail/{{$list->id_info_peluang_da}}" method="post">
            @endforeach
                {{ csrf_field() }}
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="nama">Nama:</label>
                    <input type="text" class="form-control" id="name" name="nama"/>
                </div>
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="judul">Asal Negara:</label>
                    <input type="text" class="form-control" id="judul" name="judul"/>
                </div>
                 <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="judul">Phone:</label>
                    <input type="text" class="form-control" id="judul" name="phone"/ >
                </div>

                 <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    @foreach($propertiesz as $list)
                    <input type="hidden" class="form-control" id="judul" name="daerah" Value="{{$list->id_daerah}}"/>
                    @endforeach
  		@foreach($propertiesz as $list)
                    <input type="hidden" class="form-control" id="prioritas" name="prioritas" Value="{{$list->prioritas}}"/>
                    @endforeach

                </div>

                  <div id="map" 
         data-dojo-type="dijit/layout/ContentPane" 
         data-dojo-props="region:'center'" 
         style="padding:0;"></div>

                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Send Email</button>
                </div>
                
            </form>
        </div>
        <!-- /.content -->
    </section>
          
         

          </div>
          <!--/.Panel 7-->

      </div>
    </div>
    <!--/.Content-->
  </div>
  </div>   
                            

 

    </main>

    <!--end #ts-main-->
    </div>
    </div>
    <br>
                           


</body> 

</html>
<script src="{{Config::get('app.url')}}js/owl.carousel.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="{{Config::get('app.url')}}js/popper.min.js"></script>

  
 
   <script>  
   $(document).ready(function () {
         
   

         // LegendLayer
  
         

            $("#layerclick").click(function(){
            
            $('#layer').toggle();
            $('#layeron').toggleClass("hijau");

        
});


$("#basemapclick").click(function(){
            
            $('#hide').toggle();
            $('#basemapon').toggleClass("hijau");

        
});
$("#layerpro").click(function(){
            
            $('#layerpro2').toggle();
            $('#basemapon2').toggleClass("hijau");

        
});
$('#layerpro2').hide();
$('#layer').hide();
$('#hide').hide();
            $('#legendlayer').hide();
        
});

</script>
   <script>
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: getDuration
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
           return (postDetails.offsetHeight - postSidebar.offsetHeight) + 930;
        }
    </script>

    @endsection