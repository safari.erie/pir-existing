  <!--*********************************************************************************************************-->
    <!--HEADER **************************************************************************************************-->
    <!--*********************************************************************************************************-->
    <!DOCTYPE html>
    
  <style>
  .img {
right:10px;
align:left;
  }

  .asu {
      color: rgba(0, 0, 0, 0.747);
  }
  </style>
  
    <header id="ts-header" class="head">

        <!-- SECONDARY NAVIGATION
        ================""=============================================================================================-->
     <nav id="ts-secondary-navigation" class="navbar p-0">
            <div class="container justify-content-end justify-content-sm-between">

                <!--Left Side-->
                <div class="navbar-nav asu d-none d-sm-block">
                    <!--Phone-->
                                      <!--Email-->
                  
                </div>
                <div>
                   
               <a >
                   
                </a></div>
               
                <!--Right Side-->
                <div class="navbar-nav flex-row">

                 
               
                    <!--Language Select-->
                    <select class="custom-select asu bg-transparent ts-text-small border-left border-right ts-selected" id="language" name="language"  onchange="javascript:location.href = this.value;">
                  
                   
        @if (App::isLocale('en'))
    <option selected value="en">English</option>
   <option value="id">Indonesia</option>
@elseif (App::isLocale('id'))
 <option value="en">English</option>
   <option selected value="id">Indonesia</option>


@endif
        
     

                    </select>
                   
 <li class="asu">
   
                            <a class="btn asu btn-outline-primary btn-sm m-1 px-3" href="https://admin.regionalinvestment.bkpm.go.id/">
                                <i class="fa fa-lock small asu mr-2"></i>
                                Login Admin
                            </a>
                        </li>
                </div>
                <!--end navbar-nav-->
            </div>
            <!--end container-->
        </nav>
        <!--PRIMARY NAVIGATION
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
            <div class="container">

                <!--Brand Logo-->
                <a  href="index-map-leaflet-fullscreen.html">
                    <img src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg" class=""  width="160px" alt="">
                </a>

                <!--Responsive Collapse Button-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Collapsing Navigation-->
                <div class="collapse navbar-collapse" id="navbarPrimary">

                    <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav">

                        <!--HOME (Main level)
                        =============================================================================================-->
                        <li class="nav-item ">

                            <!--Main level link-->
                            <a style="margin-left:11px;"class="nav-link active" href="{{config::get('app.url')}}/pir/{{Config::get('app.locale')}}">
                            @lang('bahasa.home')
                            
                            </a>

                      
                        <li class="nav-item ">

                            <!--Main level link-->
                            <a style="margin-left:11px;"class="nav-link" href="{{config::get('app.url')}}/profil/{{Config::get('app.locale')}}">@lang('bahasa.regionalprof')</a>

                            <!-- List (1st level) -->
                           
                        <li class="nav-item ">

                            <!--Main level link-->
                            <a style="margin-left:11px;" class="nav-link" href="{{config::get('app.url')}}/dev_peluang/{{Config::get('app.locale')}}">@lang('bahasa.projectoppo')</a>

                            <!-- List (1st level) -->
                         
                        <!--end PAGES nav-item-->

                        <!--ABOUT US (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a style="margin-left:11px;"class="nav-link" href="{{config::get('app.url')}}/peluangdaerah/{{Config::get('app.locale')}}">@lang('bahasa.regionaloppo')</a>
                        </li>
                        <!--end ABOUT US nav-item-->

                        <!--CONTACT (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <div style="margin-left:11px;" class="nav-link mr-2" >@lang('bahasa.infra')</div>
                            <ul class="ts-child">

<!-- AGENCY (1st level)
=====================================================================================-->
<li class="nav-item ts-has-child">

    <a href="{{config::get('app.url')}}/infranasional/{{Config::get('app.locale')}}" class="nav-link">@lang('bahasa.submenuinfra')</a>
   


</li>
<li class="nav-item ts-has-child">

     <a href="{{config::get('app.url')}}/industrinasional/{{Config::get('app.locale')}}" class="nav-link">@lang('bahasa.submenuindus')</a>


</li>


</ul>
                        </li>
                        <!--end CONTACT nav-item-->
<li class="nav-item">
                            <div style="margin-left:11px;"class="nav-link mr-2" >@lang('bahasa.insentif')</div>
                            <ul class="ts-child">

<!-- AGENCY (1st level)
=====================================================================================-->
<li class="nav-item ts-has-child">

    <a href="{{config::get('app.url')}}/holiday/{{ Config::get('app.locale') }}" class="nav-link">Tax Holiday</a>
   

</li>

<li class="nav-item ts-has-child">

       <a href="{{config::get('app.url')}}/allow/{{Config::get('app.locale')}}" class="nav-link">Tax Allowance</a>
    

</li>

<li class="nav-item ts-has-child">

      <a href="{{config::get('app.url')}}/master/{{Config::get('app.locale')}}" class="nav-link">Master List</a>
   

</li>

<li class="nav-item ts-has-child">
    <a href="{{config::get('app.url')}}/super/{{Config::get('app.locale')}}" class="nav-link">Super Deduction</a>


</li>


</ul>
                        </li>
                        <li class="nav-item">
                            <div style="margin-left:11px;"class="nav-link mr-2" >@lang('bahasa.umkm')</div>
                            <ul class="ts-child">

                                <!-- AGENCY (1st level)
                                =====================================================================================-->
                                <li class="nav-item ts-has-child">

                                    <a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fumkm" class="nav-link">UMKM</a>
                                

                                </li>
<li class="nav-item ts-has-child">

    <a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpma" class="nav-link">PMA</a>
                                  


</li>

<li class="nav-item ts-has-child">

       
                                    <a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpmdn" class="nav-link">PMDN</a>


</li>

                            </ul>
                        </li>
                       
                    </ul>
                    <!--end Left navigation main level-->

                    <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                  
                    <!--end Right navigation-->

                </div>
                <!--end navbar-collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end #ts-primary-navigation.navbar-->

    </header>
    <!--end Header-->
    <script>
 </script>