<!DOCTYPE html>
<html lang="en">
    
	@include('frontend.head')
    <body> 

        <!-- layout-->
      
            <!-- End Login Client -->

            @include('frontend.header')
            
           
           
            
					@yield('content')
 
                    
              
           @include('frontend.footer')
        </div>
        <!-- End layout-->
   
        <!-- ======================= JQuery libs =========================== -->
        <!-- Core JS Libraries -->
               
        <!--Nav-->
     
        <!-- ======================= End JQuery libs =========================== -->
    </body>
</html>