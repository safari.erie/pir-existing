<!DOCTYPE html>
<html lang="en-US">
    
<head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		
        <meta name="keywords" content="BKPM, Potensi daerah, Investasi, Perusahaan, Industri, " />
        <meta name="description" content="BKPM">
        <meta name="author" content="deto">   
        <title>PIR Informasi Geospasial</title>
        <!-- Mobile Metas -->
         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Your styles -->
        <link href="{{Config::get('app.url')}}/assets/peluang_proyek/css/style.css" rel="stylesheet" media="screen">  
     
        <!-- Skins Theme -->
        <link href="{{Config::get('app.url')}}/assets/peluang_proyek/css/main-color.css" rel="stylesheet" media="screen">
        <!-- Skins Theme -->
		     

		@yield('head')
    </head>
    <body> 

        <!-- layout-->
               <!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container">

	<!-- Header -->
	<div id="header">
		<div class="container-fluid">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="{{Config::get('app.url')}}"><img src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg" alt=""></a>
				</div>

				<!-- Mobile Navigation -->
				<div class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>

				<!-- Main Navigation -->
				<nav id="navigation" class="style-1">
					<ul id="responsive">

						<li><a  href="{{config::get('app.url')}}/pir/{{Config::get('app.locale')}}">@lang('bahasa.home')</a>
							
						</li>
						<li><a href="{{config::get('app.url')}}/profil/{{Config::get('app.locale')}}">@lang('bahasa.regionalprof')</a></li>
						

						

						<li><a  class="current" href="#">@lang('bahasa.peluanginvestasi')</a>
							<ul>
								<li><a  class="current" href="{{config::get('app.url')}}/dev_peluang/{{Config::get('app.locale')}}">@lang('bahasa.projectoppo')</a>
									
								</li>
								<li><a href="{{config::get('app.url')}}/peluangdaerah/{{Config::get('app.locale')}}">@lang('bahasa.regionaloppo')</a>
									
								</li>
								
							</ul>
						</li>
						<li><a href="#">@lang('bahasa.infra')</a>
							<ul>
								<li><a href="{{config::get('app.url')}}/infranasional/{{Config::get('app.locale')}}">@lang('bahasa.submenuinfra')</a></li>
						<li><a href="{{config::get('app.url')}}/industrinasional/{{Config::get('app.locale')}}">@lang('bahasa.submenuindus')</a></li>
								
							</ul>
						</li>
						<li><a href="#">@lang('bahasa.insentif')</a>
							<ul>
								<li><a href="{{config::get('app.url')}}/holiday/{{ Config::get('app.locale') }}">Tax Holiday</a></li>
						<li><a href="{{config::get('app.url')}}/allow/{{Config::get('app.locale')}}">Tax Allowance</a></li>
						<li><a href="{{config::get('app.url')}}/master/{{Config::get('app.locale')}}">Master List</a></li>
						<li><a href="{{config::get('app.url')}}/super/{{Config::get('app.locale')}}">Super Deduction</a></li>
								
							</ul>
						</li>
						<li><a href="#">@lang('bahasa.umkm')</a>
							<ul>
								<li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fumkm">UMKM</a></li>
						<li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpma">PMA</a></li>
						<li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpmdn">PMDN</a></li>						
							</ul>
						</li>
						

						
						
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
			<div class="right-side">
			<ul class="nav-horizontal">
				<li>
					<!--Language Select-->
                    <select class="custom-select asu bg-transparent ts-text-small border-left border-right ts-selected" id="language" name="language"  onchange="javascript:location.href = this.value;">

					@if (App::isLocale('en'))
						<option selected value="en">English</option>
						<option value="id">Indonesia</option>
					@elseif (App::isLocale('id'))
						<option value="en">English</option>
						<option selected value="id">Indonesia</option>
					@endif
                    </select>
				</li>
				<li>
					<a class="btn asu btn-outline-primary btn-sm m-1 px-3" href="https://admin.regionalinvestment.bkpm.go.id/">
						<i class="fa fa-lock small asu mr-2"></i>
						Login Admin
					</a>
				</li>
			</ul>
			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->


		@yield('content')


<!-- Footer
================================================== -->
<div id="footer" >
	<!-- Main -->
	<div class="container" >
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg" alt="">
				<br><br>
				<p>
				@lang('bahasa.deskripsipir')
				<!-- PIR is a geospatial based information system for investment potential and opportunities in 34 Provinces and 514 Regencies / Cities in Indonesia and is part of the website of the Investment Coordinating Board (BKPM). PIR contains information on regional profiles including demographic data, commodities, income and Regional Minimum Wages (UMR), as well as supporting infrastructure-->
				</p> 
			</div>

			<div class="col-md-4 col-sm-6 ">
				<h4>@lang('bahasa.helpfullinks')</h4>
				<ul class="footer-links">
					<li><a href="{{config::get('app.url')}}/infranasional/{{Config::get('app.locale')}}">@lang('bahasa.submenuinfra')</a></li>
					<li><a href="{{config::get('app.url')}}/industrinasional/{{Config::get('app.locale')}}">@lang('bahasa.submenuindus')</a></li>
					<li><a href="{{config::get('app.url')}}/holiday/{{ Config::get('app.locale') }}">Tax Holiday</a></li>
					<li><a href="{{config::get('app.url')}}/allow/{{Config::get('app.locale')}}">Tax Allowance</a></li>
					<li><a href="{{config::get('app.url')}}/master/{{Config::get('app.locale')}}">Masterlist</a></li>
					<li><a href="{{config::get('app.url')}}/super/{{Config::get('app.locale')}}">Super Deduction</a></li>
				</ul>

				<ul class="footer-links">
					<li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpma">PMA</a></li>
					<li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpmdn">PMDN</a></li>
					<li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fumkm">UMKM</a></li>
					
				</ul>
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>@lang('bahasa.hubungikami')</h4>
				<div class="text-widget">
					
					<span>Jl. Jend. Gatot Subroto No. 44, Jakarta 12190</span> <br>
					<span>P.O. Box 3186, Indonesia</span> <br>
					@lang('bahasa.phone') : <span>+6221 525 2008  </span><br>
					Contact Center: <span>0807 100 2576   </span><br>
					Fax: <span>+6221 525 4945 </span><br>
					E-Mail:<span> <a href="#">office@bkpm.com</a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="https://www.facebook.com/BKPMINDONESIA"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/bkpm"><i class="icon-twitter"></i></a></li>
					<li><a class="youtube" href="https://www.youtube.com/user/THEBKPMVIDEO "><i class="icon-youtube"></i></a></li>
					<li><a class="linkedin" href="https://www.linkedin.com/company/indonesia-investment-coordinating-board "><i class="icon-linkedin"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/bkpm_id "><i class="icon-instagram"></i></a></li>
				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2021 BKPM. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->
           
           
            
 
        <!-- End layout-->
   
        <!-- ======================= JQuery libs =========================== -->
        
		
<!-- Scripts
================================================== -->
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/jquery-migrate-3.3.1.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/mmenu.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/chosen.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/slick.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/waypoints.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/counterup.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/tooltips.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/custom.js"></script>



        <!-- ======================= End JQuery libs =========================== -->
		
		@yield('scripts')
    </body>
</html>