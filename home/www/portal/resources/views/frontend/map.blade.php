




                             <div data-dojo-type="dijit/layout/BorderContainer" 
            data-dojo-props="design:'headline', gutters:false" 
            style="width:100%;height:100%;margin:0;">
            <div class="" id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:2px;">
           
                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                    <div data-dojo-type="dijit/TitlePane" 
                         data-dojo-props="title:'Switch Basemap', open:true">
                      <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                        <div id="basemapGallery"></div>
                    </div>
                </div>
              </div>
            </div>
        </div>
         
           
       
             
         
<script> 
var map;
require([
  "esri/map",
   "esri/dijit/PopupTemplate",
     "esri/layers/FeatureLayer",
    "dojo/_base/array",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/geometry/Geometry",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
    "esri/InfoTemplate",
   "esri/dijit/BasemapGallery",
"esri/dijit/Legend",
    "esri/arcgis/utils",
  "dojo/parser",

  "dijit/layout/BorderContainer", 
  "dijit/layout/ContentPane", 
  "dijit/TitlePane",
  "dojo/domReady!"
], function(
  Map,
        PopupTemplate, 
     FeatureLayer, 
     arrayUtils, 
     ArcGISDynamicMapServiceLayer, 
     Geometry, 
     Point, 
     webMercatorUtils,  
     Graphic, 
     SimpleMarkerSymbol, 
     SimpleLineSymbol, 
     SimpleFillSymbol,
     PictureMarkerSymbol,  
     Color, 
     InfoTemplate,
   BasemapGallery,
Legend,
    arcgisUtils,
  parser
) {
  parser.parse();

 


  var map = new Map("map", {
            basemap: "osm",
            center: [117.62527, -1.4509444],
            zoom: 5
        });

  //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
  var basemapGallery = new BasemapGallery({
    showArcGISBasemaps: true,
    map: map
  }, "basemapGallery");
  basemapGallery.startup();
  
  basemapGallery.on("error", function(msg) {
    console.log("basemap gallery error:  ", msg);
  });

    //add the legend
      map.on("layers-add-result", function (evt) {
        var layerInfo = arrayUtils.map(evt.layers, function (layer, index) {
          return {layer:layer.layer, title:layer.layer.name};
        });
        if (layerInfo.length > 0) {
          var legendDijit = new Legend({
            map: map,
            layerInfos: layerInfo
          }, "legendDiv");
          legendDijit.startup();
        }
      });
var title2 ='<div align="center">Batas Wilayah Kabupaten</div>';
var isi2 ='<div style="float:left;"  >Nama Kabupaten :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
var title4 ='<div align="center">Batas Wilayah Kota</div>';
var isi4 ='<div style="float:left;"  >Nama Kab/kota :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2">{namobj}</dd>'
       var popupprov = new PopupTemplate({
    title: "Boundaries {ta}",

    fieldInfos: [

      { fieldName: "wa", visible: true, label: "Province :", format: { places: 0   } },
      { fieldName: "ta", visible: true, label: "Boundaries :", format: { places: 0   } },
      { fieldName: "dh", visible: true, label: "Rules :", format: { places: 0   } },
      { fieldName: "lu", visible: true, label: "Large(km) :", format: { places: 0   } },
     
    ],

  
           });
      

    var popupkota = new PopupTemplate({
        title: title2,
      description: isi2
      
    });

    var popupkabupaten = new PopupTemplate({
    title: "Boundaries {ta}",

    fieldInfos: [
        { fieldName: "namobj", visible: true, label: "Kab/kota :", format: { places: 0   } },

     
    ],

  
           });
      
  

    var title3 ='<div align="center">RTRW Kabupaten</div>';
var isi3 ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popuprtrw = new PopupTemplate({
            title: title3,
          description: isi3
          
        });
      

          $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken",
    {
      username: 'bkpm-sipd',
      password: 'Bkpm_S1pd@atrbpn123'
     
      
      
      
    },
   
        
    function(data,status){
  


    });
    var featureLayerprovinsi = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0",{
      outFields: ["*"],
      
infoTemplate: popupprov
 });
 map.addLayer(featureLayerprovinsi);
map.on("extent-change",function(evt)
      {
        console.log("extent change",map.getZoom());
        
        
        if ( map.getZoom() < 9 ) {
            featurekabupaten.hide();
            featureLayerprovinsi.show();
            } else if(( map.getZoom() > 9 )) {
                featurekabupaten.show();
                featureLayerprovinsi.hide();
            }
      
        
      } 
         
      
    );


var featurekabupaten = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_kabupaten/MapServer/0",{
    opacity: 0.5,
      outFields: ["*"],
      
infoTemplate: popupkabupaten
 });
map.addLayer(featurekabupaten);

                    // Layer Infrastruktur
                    var title4 = '<div align="center">Infrastruktur Airport</div>';
        var isi4 =
            '<div style="float:left;"  >Name Airport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_bandara = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var bandara = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/bandara_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_bandara
            });
        map.addLayer(bandara);

        var title5 = '<div align="center">Infrastruktur Seaport</div>';
        var isi5 =
            '<div style="float:left;"  >Name Seaport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kelas}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pelabuhan = new PopupTemplate({
            title: title5,
            description: isi5
        });
        var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pelabuhan
            });
        map.addLayer(pelabuhan);
      
        var title7 = '<div align="center">Infrastruktur Hospital</div>';
        var isi7 =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title7,
            description: isi7
        });
        var rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(rumah_sakit);

        var title8 = '<div align="center">Infrastruktur Eduacational</div>';
        var isi8 =
            '<div style="float:left;"  >Name Educational :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pendidikan = new PopupTemplate({
            title: title8,
            description: isi8
        });
        var pendidikan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pendidikan_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pendidikan
            });
        map.addLayer(pendidikan);
        var title9 = '<div align="center">Infrastruktur Hotel</div>';
        var isi9 =
            '<div style="float:left;"  >Name Hotel :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_hotel = new PopupTemplate({
            title: title9,
            description: isi9
        });
        var hotel = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/hotel_en/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_hotel
            });
        map.addLayer(hotel);

        // Akhir layer infrastruktur

        rumah_sakit.hide();
        pendidikan.hide();
        hotel.hide();
        pelabuhan.hide();
        bandara.hide();
        $('input:checkbox[name=bandara]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      bandara.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    bandara.hide();
                  }

         });
         $('input:checkbox[name=rumah_sakit]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rumah_sakit.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rumah_sakit.hide();
                  }

         });
         $('input:checkbox[name=pendidikan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pendidikan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pendidikan.hide();
                  }

         });
         $('input:checkbox[name=pelabuhan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pelabuhan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pelabuhan.hide();
                  }

         });
         $('input:checkbox[name=hotel]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     hotel.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                   hotel.hide();
                  }

         });

                    // Akhir layer infrastruktur


        <?php foreach ($projecs as $list): ?>
           
        
              <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
         
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_prov}}, Provinsi {{$list->nama_daerah}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> {{$list->rest_api}}</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
           
        
        <?php } ?>

      
    
    
       @endforeach

       <?php foreach ($projecs as $list): ?>
            
        
              <?php if($list->sektor == 'Infrastruktur') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
          
           
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">Provinsi {{$list->nama_daerah}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr><tr valign="top"> <td colspan="2"><a title="Go to detail" class="action zoomTo" href="{{Config::get('app.url')}}/dev_propertypro/{{$list->id_info_peluang_da}}/{{$list->id_parent}}/id"><span>Click Detail</span></a></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/infrastruktur.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layerinfra = new esri.layers.GraphicsLayer();
              layerinfra.add(graphic);
              map.addLayer(layerinfra);
              if ($("#infra").is(":checked")) {
                           layerinfra.show();
                        } else {
                          layerinfra.hide();
                        }
         
          
        
        <?php } ?>

      
    
      @endforeach
      

       <?php foreach ($projecs as $list): ?>
          
        
              <?php if($list->sektor == 'Pariwisata') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">Provinsi {{$list->nama_daerah}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr><tr valign="top"> <td colspan="2"><a title="Go to detail" class="action zoomTo" href="{{Config::get('app.url')}}/dev_propertypro/{{$list->id_info_peluang_da}}/{{$list->id_parent}}/id"><span>Click Detail</span></a></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/pariwisata.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer33 = new esri.layers.GraphicsLayer();
              layer33.add(graphic);
              map.addLayer(layer33);
              if ($("#pariwisata").is(":checked")) {
                           layer33.show();
                        } else {
                          layer33.hide();
                        }
         
          
        
        <?php } ?>

      
    
     
       @endforeach

 <?php foreach ($projecs as $list): ?>
          
        
              <?php if($list->sektor == 'Smelter') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">Provinsi {{$list->nama_daerah}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr><tr valign="top"> <td colspan="2"><a title="Go to detail" class="action zoomTo" href="{{Config::get('app.url')}}/dev_propertypro/{{$list->id_info_peluang_da}}/{{$list->id_parent}}/id"><span>Click Detail</span></a></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/smelting.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer66 = new esri.layers.GraphicsLayer();
              layer66.add(graphic);
              map.addLayer(layer66);
              if ($("#smelter").is(":checked")) {
                           layer66.show();
                        } else {
                          layer66.hide();
                        }
         
          
        
        <?php } ?>

      
    
     
       @endforeach

       <?php foreach ($projecs as $list): ?>
           
        
              <?php if($list->sektor == 'Jasa') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_prov}}, Provinsi {{$list->nama_daerah}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->rest}} / ha</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
          
        
        <?php } ?>

      
    
      @endforeach
      

       <?php foreach ($projecs as $list): ?>
             
        
              <?php if($list->sektor == 'Industri') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">Provinsi {{$list->nama_daerah}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr><tr valign="top"> <td colspan="2"><a title="Go to detail" class="action zoomTo" href="{{Config::get('app.url')}}/dev_propertypro/{{$list->id_info_peluang_da}}/{{$list->id_parent}}/id"><span>Click Detail</span></a></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/industri.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layerindus = new esri.layers.GraphicsLayer();
              layerindus.add(graphic);
              map.addLayer(layerindus);
              if ($("#industri").is(":checked")) {
                            layerindus.show();
                        } else {
                            layerindus.hide();
                        }
            
          
        
        <?php } ?>

      
    
    
       @endforeach

  

});


</script> 

   