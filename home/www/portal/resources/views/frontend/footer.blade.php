

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<nav id="mobilez" class="nav ts-z-index__2">
  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">dashboard</i>
    <span class="nav__text">Dashboard</span>
  </a>

  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">devices</i>
    <span class="nav__text">Devices</span>
  </a>
  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">lock</i>
    <span class="nav__text">Privacy</span>
  </a>
  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">settings</i>
    <span class="nav__text">Settings</span>
  </a>
</nav>

<div class="container-fluid footer">
			<div class="foot-content">
			<div id="mobile">
				
<div class="row">
  <div class="col-sm-3"><h4 >
 
						<ul>
							<li>
								<h4 href="{{Config::get('app.url')}}/infranasional/id">
								<a style="color:white;"href="{{config::get('app.url')}}/peluang/id"><b>&nbsp;@lang('bahasa.1')</b></a>
								</h4>
							</li>
							<li>
								<h4 href="en/about-bkpm/timeline/index.htm">
								<a style="color:white;"href="{{config::get('app.url')}}/peluangdaerah/id"><b>&nbsp;@lang('bahasa.2')</b></a>
								</h4>
							</li>
						</ul></div>
  <div class="col-sm-2"><h4><b>@lang('bahasa.3')</b></h4>
						<ul>
						
							<li>
								<a href="{{Config::get('app.url')}}/infranasional/id">@lang('bahasa.3')</a>
							</li>
							<li>
								<a href="{{Config::get('app.url')}}/industrinasional/id">@lang('bahasa.4')</a>
							</li>
							<BR>
							
							
						</ul></div>
  <div class="col-sm-2"><h4><b>@lang('bahasa.5')</b></h4>
						<ul class="mhf">
							<li>
								<a href="{{Config::get('app.url')}}/holiday"
									target="_blank">· Tax Holiday</a>
							</li>
							<li>
								<a href="{{Config::get('app.url')}}/allow"
									target="_blank">· Tax Allowance</a>
							</li>
							<li>
								<a href="{{Config::get('app.url')}}/master"
									target="_blank">· Masterlist</a>
							</li>
							<li>
								<a href="{{Config::get('app.url')}}/super"
									target="_blank">· Super Deduction</a>
							</li>
							<br>
						
						
						</ul></div>
  <div class="col-sm-3"><h4><b>@lang('bahasa.6')</b></h4>
						<ul class="mhf">
							<li>
								<a href="https://spipise.bkpm.go.id/Tracking_en.zul?pId="
									target="_blank">· SPIPISE Tracking</a>
							</li>
							<li>
								<a href="https://oss.go.id"
									target="_blank">· Online Single Submission</a>
							</li>
							<li>
								<a href="https://nswi.bkpm.go.id/"
									target="_blank">· National Single Window for <BR> &nbsp;&nbsp;Investment</a>
							</li>
							<li>
								<a href="https://digisign.bkpm.go.id:2727/verifikasi"
									target="_blank">· License Verification</a>
							</li>
							<li>
								<a href="https://www.industrialestateindonesia.com/"
									target="_blank">· Indonesia Industrial Estate Directory</a>
							</li>
							<br>
						
						
						</ul></div>
  <div class="col-sm-2"><h4><b>UMKM</b></h4>
						<ul class="mhf">
							<li>
								<a href="javascript:if(confirm('http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpma'"
									target="_blank">· PMA</a>
							</li>
							<li>
								<a href="javascript:if(confirm('http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpmdn'"
									target="_blank">· PMDN</a>
							</li>
							<li>
								<a href="javascript:if(confirm('http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fumkm'"
									target="_blank">· UMKM</a>
							</li>
						
							<br>
						
						
						</ul>
                        
                        </div>
						</div>
                       
</div>
<hr>
<div class="row">
  <div class="col-sm-4"><strong>© 2020 -All rights reserved</strong>
						<br>
						BADAN KOORDINASI PENANAMAN MODAL
						<br>
						Jl. Jend. Gatot Subroto No. 44, Jakarta 12190
						<br>
						P.O. Box 3186, Indonesia
						<br></div>
  <div class="col-sm-2"><ul>
							<li>
								<span class="green">P.</span> +6221 525 2008 (Hunting)
								<br>
							</li>
							<li>
								<span class="green">F.</span> +6221 525 4945

							</li>
							<li>
								<span class="green" >C.</span> 0807 100 2576 (Contact
								Center)

							</li>
						</ul></div>
  <div class="col-sm-2"><h5>Follow BKPM on : <br>
				
				<a href="https://www.facebook.com/BKPMINDONESIA" target="_blank">
					<img src="//www.bkpm.go.id/assets/icon/facebook.png">
				</a>
					&nbsp;&nbsp;
				<a href="https://twitter.com/bkpm" target="_blank">
					<img src="//www.bkpm.go.id/assets/icon/twitter.png">
				</a>
				&nbsp;&nbsp;
				<a href="https://www.youtube.com/user/THEBKPMVIDEO" target="_blank">
					<img src="//www.bkpm.go.id/assets/icon/youtube.png">
				</a>	
				&nbsp;&nbsp;
				<a href="https://www.instagram.com/bkpm_id" target="_blank">
					<img src="//www.bkpm.go.id/assets/icon/instagram.png">
				</a>
				&nbsp;&nbsp;
				<a href="https://www.linkedin.com/company/indonesia-investment-coordinating-board" target="_blank">
					<img src="//www.bkpm.go.id/assets/icon/linkedin.png">
				</a>
			</h5>
					
</div>
	</footer>

	</div>

	<script>


$(document).ready(function(event, state){
	 $("#mobilez").hide();
     if ($(window).width() < 600) {
                             $("#ts-header").addClass("fixed-top");
                             $("#mobile").hide();
							 $("#mobilez").show();
                         } else {

                         }
						 });
						 </script>