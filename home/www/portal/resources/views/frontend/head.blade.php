
<head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		
        <meta name="keywords" content="BKPM, Potensi daerah, Investasi, Perusahaan, Industri, " />
        <meta name="description" content="BKPM">
        <meta name="author" content="deto">   
        <title>PIR Informasi Geospasial</title>
        <!-- Mobile Metas -->
         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Your styles -->
        <link href="{{Config::get('app.url')}}/assets/frontend/css/style.css" rel="stylesheet" media="screen">  
     
        <!-- Skins Theme -->
		

        <!-- Skins Theme -->
		     
        <!-- Favicons -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />



<link rel="stylesheet" href="{{Config::get('app.url')}}/assets/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

      
  <link rel="stylesheet" href="{{Config::get('app.url')}}/assets/css/style.css" rel="stylesheet" media="screen" class="skin">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css">
 
        <!-- Skins Changer-->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		 <script src="{{Config::get('app.url')}}/assets/frontend/js/jquery.min.js"></script> 
      

         <script src="{{ Config::get('app.url')}}/assets/js/popper.min.js"></script>

<script src="{{ Config::get('app.url')}}/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>

    </head>