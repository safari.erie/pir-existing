    @extends('frontend.layoutmap')

    @section('content')

    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <style type="text/css">
        g[class^='raphael-group-'][class$='-creditgroup'] {
            display: none !important;
        }
    </style>
    <style type="text/css">
        g[class$='creditgroup'] {
            display: none !important;
        }
    </style>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

    <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
    <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />


    <!-- Load Esri Leaflet from CDN -->


    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

                <div id="mydatas"></div>

                <!--end container-->
            </section>
        </div>
        <br>
        <div class="col-sm-9">
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix zink hijau">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            Potensi Daerah

                        </a>
                    </div>




                </div>


                
            </section>
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

               
             
            </section>
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->
            <div class="clearfix  btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="btn">

    Komoditi Daerah
    </a>

    <div class="float-none float-sm-right pl-2 ts-center__vertical ">
    <a>Pilih Tahun</a>&nbsp;
     <select style="color:grey; width:100px;" class="custom-select" id="sectorz2" name="sorting">
     <option value="2020">2020</option>
         <option selected value="2019">2019</option>
         <option value="2018">2018</option>
         <option value="2017">2017</option>
         <option value="2016">2016 </option>



     </select>
     </div>
</div>






</div>
                <br>

                <div id="sector2">FusionCharts will render here</div>

                <!--end container-->
            </section>

            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

            <div class="clearfix  btn-pro titlez">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="btn">
    Upah Minimum Regional Daerah
    </a>

</div>






</div>
<br>

<div id="chart-container3">FusionCharts will render here</div>

            </section>
            <section class="ts-box p-1 long" style=" margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

           
                <!--end container-->
            </section>

            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->
            <div class="clearfix  btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="btn">

        Produk Domestik Regional Bruto Berdasarkan Harga Konstan
    </a>
    <div class="float-none float-sm-right pl-2 ts-center__vertical ">
    <a>Pilih Tahun</a>&nbsp;
     <select style="color:grey; width:100px;" class="custom-select" id="sectorz4" name="sorting">
     <option value="2020">2020</option>
         <option selected value="2019">2019</option>
         <option value="2018">2018</option>
         <option value="2017">2017</option>
         <option value="2016">2016 </option>



     </select>
     </div>
 
</div>






</div>
<br>

<div id="sector4">FusionCharts will render here</div>

                <!--end container-->
            </section>




        </div>

    </div>

    <div>




        <!--end container-->
        </section>

    </div>
    </div>
    <script src="{{ url('assets/js/custom.js') }}"></script>
    <script src="https://127.0.0.1/map/html/assets/js/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script><br>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-apexcharts@1.3.6/dist/react-apexcharts.iife.min.js">
    </script>

    <script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Include fusioncharts core library file -->
    <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
    <!-- Include fusion theme file -->
    <script type="text/javascript"
        src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
    <!-- Include fusioncharts jquery plugin -->
    <script type="text/javascript"
        src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
    </script>
    <script>
        $(document).ready(function () {
                $.get("{{ Config::get('app.url') }}/chart/{{$pages}}/{{ Request::segment(3) }}/{{ Request::segment(4) }}", function (data) {
           
            $("#mydatas").append(data);




        });

        });
    </script>

    <body>

        <script type="text/javascript">
            const dataSource = {
                chart: {
                    caption: "Investment Realization by Sector Last Year 2020",
                    placevaluesinside: "1",
                    showvalues: "0",
                    plottooltext: "<b>$ $dataValue</b> $seriesName",
                    theme: "fusion"
                },
                categories: [{
                    category: [{
                            label: "Sector A"
                        },
                        {
                            label: "Sector B"
                        },
                        {
                            label: "Sector C"
                        },
                        {
                            label: "Sector D"
                        },
                        {
                            label: "Sector E"
                        }
                    ]
                }],
                dataset: [{
                        seriesname: "Foreign Direct Investment (USD $)",
                        color: " #5D62B5",

                        data: [{
                                value: "17000"

                            },
                            {
                                value: "19500"
                            },
                            {
                                value: "12500"
                            },
                            {
                                value: "14500"
                            },
                            {
                                value: "17500"
                            }
                        ]
                    },
                    {
                        seriesname: "Domestic Direct Investment (Million Rupiah)",
                        color: "#29C3BE",
                        data: [{
                                value: "25400"
                            },
                            {
                                value: "29800"
                            },
                            {
                                value: "21800"
                            },
                            {
                                value: "19500"
                            },
                            {
                                value: "21200"
                            }
                        ]
                    }
                ],
                trendlines: [{
                    line: [{
                            startvalue: "16200",
                            color: "#5D62B5",
                            thickness: "1.5",
                            displayvalue: "2017's Avg."
                        },
                        {
                            startvalue: "23500",
                            color: "#29C3BE",
                            thickness: "1.5",
                            displayvalue: "2018's Avg."
                        }
                    ]
                }]
            };

            FusionCharts.ready(function () {
                var myChart = new FusionCharts({
                    type: "msbar2d",
                    renderAt: "chart-container",
                    width: "100%",
                    height: "350",
                    dataFormat: "json",
                    dataSource
                }).render();
            });
        </script>
         <script>
        FusionCharts.ready(function () {


            var visitChart = new FusionCharts({
                        type: "line",
                        renderAt: "chart-container3",
                        id: "demoChart",
                        width: "100%",
                        height: "400",
                        dataFormat: "json",
                        dataSource: {
                            "chart": {
                                "theme": "fusion",
                                "caption": "Province Umr (Last 5 Years)",
                                "subcaption": "2015-2020",
                                "xaxisname": "Quarter",
                                "yaxisname": "(IDR per Month)",
                                "numberPrefix": "Rp.",
                                "rotateValues": "0",
                                "yAxisMinValue":'2000000',
                                "placeValuesInside": "0",
                                "valueFontColor": "#000000",
                                "valueBgColor": "#FFFFFF",
                                "valueBgAlpha": "50",
                                //Disabling number scale compression
                                "formatNumberScale": "0",
                                //Defining custom decimal separator
                                "decimalSeparator": ",",
                                //Defining custom thousand separator
                                "thousandSeparator": ".",

                                "showValues": "1"
                            },
                            "data": [@foreach($umr as $umr){
                                @if ($umr->tahun == '2016')
                                    "label": "2016",
                                    "value": "{{$umr->umr_2016}}"
                                    @elseif ($umr->tahun == '2017')
                                    "label": "2017",
                                    "value": "{{$umr->umr_2017}}"
                                    @elseif ($umr->tahun == '2018')
                                    "label": "2018",
                                    "value": "{{$umr->umr_2018}}"
                                    @elseif ($umr->tahun == '2019')
                                    "label": "2019",
                                    "value": "{{$umr->umr_2019}}"
                                    @elseif ($umr->tahun == '2020')
                                    "label": "2020",
                                    "value": "{{$umr->umr_2020}}"
                                    @endif
                                }, @endforeach
                            
                            ]
                        }
                    }

                )
                .render();

        
        });
    </script>
        <script>
            const postDetails = document.querySelector(".long");
            const postSidebar = document.querySelector(".when");
            const postDetailsz = document.querySelector(".ts-boxzzs");
            const postSidebarContent = document.querySelector(".ts-bosxzz > div");


            const controller = new ScrollMagic.Controller();


            const scene = new ScrollMagic.Scene({
                triggerElement: postSidebar,
                triggerHook: 0,
                duration: 1200
            }).addTo(controller);

            //3
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            }

            //4
            window.addEventListener("resize", () => {
                if (window.matchMedia("(min-width: 768px)").matches) {
                    scene.setPin(postSidebar, {
                        pushFollowers: false
                    });
                } else {
                    scene.removePin(postSidebar, true);
                }
            });

            function getDuration() {
                return postDetails.offsetHeight - postSidebar.offsetHeight;
            }
        </script>



        <script src="https://js.arcgis.com/3.34/"></script>

        <script>
            require([
                    "esri/map",
                    "esri/layers/FeatureLayer",
                    "dojo/domReady!"
                ],
                function (
                    Map,
                    FeatureLayer
                ) {

                    var map = new Map("map", {
                        basemap: "hybrid",
                        center: [117.62527, -1.4509444],
                        zoom: 5
                    });

                    /****************************************************************
                     * Add feature layer - A FeatureLayer at minimum should point
                     * to a URL to a feature service or point to a feature collection 
                     * object.
                     ***************************************************************/

                    // Carbon storage of trees in Warren Wilson College.
                    var featureLayer = new FeatureLayer(
                        "https://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/Landscape_Trees/FeatureServer/0"
                    );

                    map.addLayer(featureLayer);

                });
        </script>
       
        <script>
         
        </script>
        <script>
         $.get("{{ Config::get('app.url') }}/chartkomoditidaerah/{{ Request::segment(3) }}/2017/{{ Request::segment(4) }}", function (data) {
                $("#sector2").html(data);
               
              
            });
             $('#sectorz2').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartkomoditidaerah/{{ Request::segment(3) }}/"+tahun+"/{{ Request::segment(4) }}", function (data) {
            $("#sector2").html(data);
           
          
        });
});
$.get("{{ Config::get('app.url') }}/chartpdrbdaerah/{{ Request::segment(3) }}/2017", function (data) {
                $("#sector4").html(data);
               
              
            });
            $('#sectorz4').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartpdrbdaerah/{{ Request::segment(3) }}/"+tahun+"", function (data) {
            $("#sector4").html(data);
           
          
        });
});
        </script>

        @endsection