@extends('frontend.layoutmap_peluangproyek')

@section('head')

<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<style>
@page { size: landscape; }
.scroll{
 
 width: 350px;
  overflow: scroll;
overflow-x: hidden;
 padding: 15px;
  height: 350px;
 }
 
 #HomeButton{
	position: absolute;
	top: 90px;
	left: 23px;
	z-index: 50;
 }
 
 #map-container.fullwidth-home-map {
    height: 100%;
    margin-bottom: 0;
	min-height: 400px;
}

#titlebar {
    padding: 10px 0;
}

.btndraw{
	padding: 9px;
	width: 35px;
}

.btndraw.fa, .btnerasedraw.fa{
	font: normal normal normal 14px/1 FontAwesome !important;
}

.dijitButtonText{
	font-size: smaller;
}

.dijitMenuItemLabel {
    padding: 5px;
}

.dijitArrowButtonInner::after{
	content: "\f107";
	font-family: "FontAwesome";
	font-size: 18px;
	margin: 1px 0 0 0;
	right: -2px;
	line-height: 11px;
	position: relative;
	width: auto;
	height: auto;
	display: inline-block;
	color: #c0c0c0;
	float: right;
	font-weight: normal;
	transition: transform 0.3s;
	transform: translate3d(0,0,0) rotate(0deg);
}

.layerBox{display: none;}

.mapControlBtn{cursor: pointer;}


      #feedback {
        background: #fff;
        color: #000;
        position: absolute;
        font-family: arial;
        height: auto;
        right: 20px;
        margin: 5px;
        padding: 10px;
        top: 20px;
        width: 300px;
        z-index: 40;
      }
      #feedback a {
        border-bottom: 1px solid #888;
        color: #444;
        text-decoration: none;
      }
      #feedback a:hover, #feedback a:active, #feedback a:visited {
        border: none;
        color: #444;
        text-decoration: none;
      }
	  
      #note { font-size: 80%; font-weight: 700; padding: 0 0 10px 0; }
	  
      #info { padding: 10px 0 0 0; }
	  
	  .btnerasedraw, #clearDrawClick .dijitButtonNode { border: none; vertical-align: top;}
	  
</style>

@endsection

@section('scripts')

<script src="https://js.arcgis.com/3.34/"></script>
<script src="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/js/ion.rangeSlider.min.js"></script>
<script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="{{Config::get('app.url')}}/assets/peluang_proyek/scripts/printThis.js"></script>

<script> 
var map, toolbarDraw, toolbarMeasurement, symbol, geomTask, printer;
require([
"esri/map",
"esri/dijit/PopupTemplate",
"esri/layers/FeatureLayer",
"dojo/_base/array",
"esri/layers/ArcGISDynamicMapServiceLayer",
"esri/geometry/Geometry",
"esri/geometry/Point",
"esri/geometry/webMercatorUtils",
"esri/graphic",
"esri/symbols/SimpleMarkerSymbol",
"esri/symbols/SimpleLineSymbol",
"esri/symbols/SimpleFillSymbol",
"esri/symbols/PictureMarkerSymbol",
"esri/Color",
"esri/InfoTemplate",
"esri/dijit/BasemapGallery",
"esri/dijit/HomeButton",

//MEASUREMENT
"dojo/dom",
"esri/Color",
"dojo/keys",
"dojo/parser",

"esri/config",
"esri/urlUtils",

"esri/sniff",
//"esri/SnappingManager",
"esri/dijit/Measurement",
"esri/renderers/SimpleRenderer",
"esri/tasks/GeometryService",

//DRAW BARYMORE
"esri/toolbars/draw",

"dijit/form/Button", 
"dijit/registry",
"esri/arcgis/utils",

//PRINT
"esri/dijit/Print",
"esri/tasks/PrintTemplate",
"esri/request",
// "esri/layers/ArcGISTiledMapServiceLayer",
// "esri/layers/LayerDrawingOptions",
// "esri/renderers/ClassBreaksRenderer",
// "dojo/query", "dojo/dom-construct", 
// "dijit/form/CheckBox",

"dijit/layout/BorderContainer", 
"dijit/layout/ContentPane", 
"dijit/WidgetSet",
"dijit/TitlePane", 
"dijit/layout/AccordionContainer",
"dojo/domReady!",
"esri/dijit/Legend",


], function(
Map,
PopupTemplate, 
FeatureLayer, 
arrayUtils, 
ArcGISDynamicMapServiceLayer, 
Geometry, 
Point, 
webMercatorUtils,  
Graphic, 
SimpleMarkerSymbol, 
SimpleLineSymbol, 
SimpleFillSymbol,
PictureMarkerSymbol,  
Color, 
InfoTemplate,
BasemapGallery,
HomeButton,

//MEASUREMENT
dom, Color, keys, parser,
esriConfig, urlUtils, has, //SnappingManager, 
Measurement, SimpleRenderer, GeometryService,

//Draw
Draw,
Button,

registry,
arcgisUtils,

//print
Print,
PrintTemplate,
esriRequest,
// ArcGISTiledMapServiceLayer,
// LayerDrawingOptions,
// ClassBreaksRenderer,
// query, domConstruct, 

Legend
) {
  parser.parse();
  urlUtils.addProxyRule({
    urlPrefix: "https://Gistaru.atrbpn.go.id/",
    proxyUrl: "https://Gistaru.atrbpn.go.id/rtronline/proxy/proxy.ashx"
  });

  printUrl = "https://regionalinvestment.bkpm.go.id/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
  esriConfig.defaults.io.proxyUrl = "/proxy/";

   @foreach($propertiesx as $list){
map = new Map("map", {
  basemap: "osm",
  center: [{{$list->y}}, {{$list->x}}],
  zoom: 12
});
}
  //add the legend
      map.on("layers-add-result", function (evt) {
        var layerInfo = arrayUtils.map(evt.layers, function (layer, index) {
          return {layer:layer.layer, title:layer.layer.name};
        });
        if (layerInfo.length > 0) {
          var legendDijit = new Legend({
            map: map,
            layerInfos: layerInfo
          }, "legendDiv");
          legendDijit.startup();
        }
      });

@endforeach

  var home = new HomeButton({
        map: map
      }, "HomeButton");
      home.startup();
    //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
  var basemapGallery = new BasemapGallery({
    showArcGISBasemaps: true,
    map: map
  }, "basemapGallery");
  basemapGallery.startup();
  
  basemapGallery.on("error", function(msg) {
    console.log("basemap gallery error:  ", msg);
  });
  
  //MEASUREMENT
	  var measurement = new Measurement({
	  map: map,	  
      defaultLengthUnit: esri.Units.KILOMETERS,
	 }, dom.byId("measurementDiv"));
	 
	  measurement.on("measure-start", function(evt){
		 map.infoWindow.hide();
		map.setInfoWindowOnClick(false);
	  });
	  measurement.on("measure-end", function(evt){
		map.setInfoWindowOnClick(true);
	  });

	measurement.startup();

	// print dijit
	// printer = new Print({
	  // map: map,
	  // url: "https://utility.arcgisonline.com/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task"
	// }, dom.byId("printButton"));
	// printer.startup();
	
	 // add graphics for pools with permits
        // var permitUrl = "https://regionalinvestment.bkpm.go.id/arcgis/rest/services/PoolPermits/MapServer/1";
        // var poolFeatureLayer = new FeatureLayer(permitUrl, {
          // "mode": FeatureLayer.MODE_SNAPSHOT
        // });
        // map.addLayer(poolFeatureLayer);

        // // get print templates from the export web map task
        // var printInfo = esriRequest({
          // "url": printUrl,
          // "content": { "f": "json" }
        // });
        // printInfo.then(handlePrintInfo, handleError);

        // function handlePrintInfo(resp) {
          // var layoutTemplate, templateNames, mapOnlyIndex, templates;

          // layoutTemplate = arrayUtils.filter(resp.parameters, function(param, idx) {
            // return param.name === "Layout_Template";
          // });
          
          // if ( layoutTemplate.length === 0 ) {
            // console.log("print service parameters name for templates must be \"Layout_Template\"");
            // return;
          // }
          // templateNames = layoutTemplate[0].choiceList;

          // // remove the MAP_ONLY template then add it to the end of the list of templates 
          // mapOnlyIndex = arrayUtils.indexOf(templateNames, "MAP_ONLY");
          // if ( mapOnlyIndex > -1 ) {
            // var mapOnly = templateNames.splice(mapOnlyIndex, mapOnlyIndex + 1)[0];
            // templateNames.push(mapOnly);
          // }
          
          // // create a print template for each choice
          // templates = arrayUtils.map(templateNames, function(ch) {
            // var plate = new PrintTemplate();
            // plate.layout = plate.label = ch;
            // plate.format = "PDF";
            // plate.layoutOptions = { 
              // "authorText": "Made by:  PIR",
              // "copyrightText": "BKPM 2021",
              // "legendLayers": [], 
              // "titleText": "Peluang Proyek", 
              // "scalebarUnit": "Miles" 
            // };
            // return plate;
          // });

          // // create the print dijit
          // printer = new Print({
            // "map": map,
            // "templates": templates,
            // url: printUrl
          // }, dom.byId("printButton"));
          // printer.startup();
        // }

        // function handleError(err) {
          // console.log("Something broke: ", err);
        // }
		
	//DRAW
	// CREATE dijits BUTTON, connect onClick event
	// listeners for buttons to activate drawing tools
	map.on("load", createToolbar);
	
	//DRAW BUTTON
	var btnEraseDraw = new Button({
		iconClass: "btnerasedraw fa fa-eraser",
		showLabel: false,
		label: "Erase Draw", // analogous to title when showLabel is false
		onClick: function(){map.graphics.clear();}
	}, "eraseDrawArea");
	btnEraseDraw.startup();
	
	var btnDrawPoint = new Button({
		iconClass: "btndraw fa fa-circle-o",
		showLabel: false,
		label: "Point", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnDrawPoint");
	btnDrawPoint.startup();
	
	var btnMultiPoint = new Button({
		iconClass: "btndraw fa fa-clone",
		showLabel: false,
		label: "Multi Point", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnMultiPoint");
	btnMultiPoint.startup();
	
	var btnLine = new Button({
		iconClass: "btndraw fa fa-minus",
		showLabel: false,
		label: "Line", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnLine");
	btnLine.startup();
	
	var btnPolyline = new Button({
		iconClass: "btndraw fa fa-square-o",
		showLabel: false,
		label: "Polyline", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnPolyline");
	btnPolyline.startup();
	
	var btnPolygon = new Button({
		iconClass: "btndraw fa fa-square",
		showLabel: false,
		label: "Polygon", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnPolygon");
	btnPolygon.startup();
	
	var btnFreehandPolyline = new Button({
		iconClass: "btndraw fa fa-pencil-square-o",
		showLabel: false,
		label: "Freehand Polyline", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnFreehandPolyline");
	btnFreehandPolyline.startup();
	
	var btnFreehandPolygon = new Button({
		iconClass: "btndraw fa fa-pencil-square",
		showLabel: false,
		label: "Freehand Polygon", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnFreehandPolygon");
	btnFreehandPolygon.startup();
	
	var btnArrow = new Button({
		iconClass: "btndraw fa fa-long-arrow-down",
		showLabel: false,
		label: "Arrow", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnArrow");
	btnArrow.startup();
	
	var btnTriangle = new Button({
		iconClass: "btndraw fa fa-caret-up",
		showLabel: false,
		label: "Triangle", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnTriangle");
	btnTriangle.startup();
	
	var btnCircle = new Button({
		iconClass: "btndraw fa fa-circle",
		showLabel: false,
		label: "Circle", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnCircle");
	btnCircle.startup();
	
	var btnEllipse = new Button({
		iconClass: "btndraw fa fa-dot-circle-o",
		showLabel: false,
		label: "Ellipse", // analogous to title when showLabel is false
		onClick: activateTool
	}, "btnEllipse");
	btnEllipse.startup();

	function activateTool() {
	  var tool = this.label.toUpperCase().replace(/ /g, "_");
	  toolbarDraw.activate(Draw[tool]);
	  map.hideZoomSlider();
	  map.setInfoWindowOnClick(false);
	}

	function createToolbar(themap) {
	  toolbarDraw = new Draw(map);
	  toolbarDraw.on("draw-end", addToMap);
	}

	function addToMap(evt) {
	  var symbol;
	  toolbarDraw.deactivate();
	  map.showZoomSlider();
	  map.setInfoWindowOnClick(true);
	  switch (evt.geometry.type) {
		case "point":
		case "multipoint":
		  symbol = new SimpleMarkerSymbol();
		  break;
		case "polyline":
		  symbol = new SimpleLineSymbol();
		  break;
		default:
		  symbol = new SimpleFillSymbol();
		  break;
	  }
	  var graphic = new Graphic(evt.geometry, symbol);
	  map.graphics.add(graphic);
	}

   // Layer Infrastruktur
   var title4 = '<div align="center">Infrastruktur Bandara</div>';
        var isi4 =
            '<div style="float:left;"  >Name Airport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_bandara = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var bandara = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/bandara_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_bandara
            });
        map.addLayer(bandara);

        var title5 = '<div align="center">Infrastruktur Seaport</div>';
        var isi5 =
            '<div style="float:left;"  >Name Seaport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pelabuhan = new PopupTemplate({
            title: title5,
            description: isi5
        });
        var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pelabuhan
            });
        map.addLayer(pelabuhan);
        @foreach($test as $list)
        var title{{$list->id_rest}}{{$list->status}} = '<div align="center">Infrastruktur </div>';
        var isi{{$list->id_rest}}{{$list->status}} =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_kontur{{$list->id_rest}}{{$list->status}} =new InfoTemplate("{{$list->nama_rest}}", "${*}");       
        var kontur{{$list->id_rest}}{{$list->status}}= new FeatureLayer(
            "{{$list->rest_peluang}}", {
                outFields: ["*"],

                infoTemplate: Popup_kontur{{$list->id_rest}}{{$list->status}}
            });
@if($list->status == '2')
        map.addLayer(kontur{{$list->id_rest}}{{$list->status}});
@else
 map.addLayer(kontur{{$list->id_rest}}{{$list->status}});
@endif
        @endforeach
        var title7 = '<div align="center">Infrastruktur Hospital</div>';
        var isi7 =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title7,
            description: isi7
        });
        var rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(rumah_sakit);

        var title8 = '<div align="center">Infrastruktur Education</div>';
        var isi8 =
            '<div style="float:left;"  >Name Educational :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pendidikan = new PopupTemplate({
            title: title8,
            description: isi8
        });
        var pendidikan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pendidikan_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pendidikan
            });
        map.addLayer(pendidikan);
        var title9 = '<div align="center">Infrastruktur Hotel</div>';
        var isi9 =
            '<div style="float:left;"  >Name Hotel :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_hotel = new PopupTemplate({
            title: title9,
            description: isi9
        });
        var hotel = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/hotel_en/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_hotel
            });
        map.addLayer(hotel);
        // Akhir layer infrastruktur

        rumah_sakit.hide();
        pendidikan.hide();
        hotel.hide();
        pelabuhan.hide();
        bandara.hide();
     

   var title1 ='<div align="center">Batas Wilayah Provinsi</div>';
var isi1 ='<div style="float:left;"  >Nama Provinsi :  </div><dd style="margin-left:103px;  ; "class="border-bottom pb-2"> {Provinsi}</dd>'
var title2 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi2 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
var title4 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi4 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
       var popupprov = new PopupTemplate({
    title: "Batas Wilayah Provinsi",

    fieldInfos: [

      { fieldName: "wa", visible: true, label: "Provinsi :", format: { places: 0   } },
       
    ],

  
           });
      

    var popupkota = new PopupTemplate({
    title: "Boundaries Kabupaten",

    fieldInfos: [

      { fieldName: "namobj", visible: true, label: "Name Kabupaten :", format: { places: 0   } },

     
    ],

  
           });
      

    var popupkabupaten = new PopupTemplate({
    title: "Batas Wilayah Kabupaten/kota",

    fieldInfos: [

      { fieldName: "wadmkk", visible: true, label: "Kabupaten/kota :", format: { places: 0   } },
      { fieldName: "wadmpr", visible: true, label: "Provinsi :", format: { places: 0   } },
  
     
    ],

  
           });
      
  

    var title3 ='<div align="center">RTRW Kabupaten</div>';
var isi3 ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popuprtrw = new PopupTemplate({
    title: "Layer RTRW",

    fieldInfos: [

      { fieldName: "NAMOBJ", visible: true, label: "Name  :", format: { places: 0   } },
      { fieldName: "WADMKK", visible: true, label: "Kab/kota :", format: { places: 0   } },
      { fieldName: "WADMPR", visible: true, label: "Province :", format: { places: 0   } },
      { fieldName: "NOTHPR", visible: true, label: "Rule :", format: { places: 0   } },
      { fieldName: "PP", visible: true, label: "File1:", format: { places: 0   } },
      { fieldName: "BA", visible: true, label: "File2:", format: { places: 0   } },
      { fieldName: "STSDRH ", visible: true, label: "Status Regional:", format: { places: 0   } },
      { fieldName: "STSDAT", visible: true, label: "Data Status:", format: { places: 0   } },
    
    ],

  
           });
      

            $('input:checkbox[name=rtrw]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rtrwprov.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rtrwprov.hide();
                   
                  }

         });
       
        
        @foreach($test as $list)
@if($list->status == '2')
kontur{{$list->id_rest}}{{$list->status}}.show();
@else
  kontur{{$list->id_rest}}{{$list->status}}.hide();
@endif
        $('input:checkbox[name=pro{{$list->id_rest}}{{$list->status}}]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      kontur{{$list->id_rest}}{{$list->status}}.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    kontur{{$list->id_rest}}{{$list->status}}.hide();
                  }

         });
         @endforeach
         $('input:checkbox[name=bandara]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      bandara.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    bandara.hide();
                  }

         });
         $('input:checkbox[name=rumah_sakit]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rumah_sakit.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rumah_sakit.hide();
                  }

         });
         $('input:checkbox[name=pendidikan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pendidikan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pendidikan.hide();
                  }

         });
         $('input:checkbox[name=pelabuhan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pelabuhan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pelabuhan.hide();
                  }

         });
         $('input:checkbox[name=hotel]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     hotel.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                   hotel.hide();
                  }

         });
         map.on("extent-change",function(evt)
      {
        console.log("extent change",map.getZoom());
        
        
        if ( map.getZoom() > 9 ) {
          featureLayerprovinsi.hide();
            } else {
              featureLayerprovinsi.show();
            }
      
        
      } 
         
      
    );
    var featureLayerprovinsi = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0",{
      outFields: ["*"],
      
infoTemplate: popupprov
 });
map.addLayer(featureLayerprovinsi);


var featurekabupaten = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_kabupaten/MapServer/0",{
    opacity: 0.5,
      outFields: ["*"],
      
infoTemplate: popupkabupaten
 });
map.addLayer(featurekabupaten);
 @foreach($propertiesz as $list)
                  var linkzz='{{$list->rtrw}}'                     
                          @endforeach              
          var rtrwprov = new FeatureLayer('{{$list->rtrw}}',{
           mode: FeatureLayer.MODE_ONDEMAND,
              outFields: ["*"],
              opacity:0.5,
        infoTemplate: popuprtrw
         });
         map.addLayer(rtrwprov);

                


        <?php foreach ($propertiesz as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
         
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama_kabkot}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> </dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Infrastruktur') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">{{$list->bentuk_daerah}} {{$list->nama_kabkot}}, Provinsi {{$lisa->nama}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/infrastruktur.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("openmap").innerHTML = "{{$list->y}}, {{$list->x}}";
           alert('as');
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

    
       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Jasa') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           
           var content ='';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Industri') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           
           
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">{{$list->bentuk_daerah}} {{$list->nama_kabkot}}, Provinsi {{$lisa->nama}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/industri.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach
<?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->id_sektor_peluang == '6') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           
           
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">{{$list->bentuk_daerah}} {{$list->nama_kabkot}}, Provinsi {{$lisa->nama}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/pariwisata.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach


       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->id_sektor_peluang == '7') { ?>
          
           
           
          var content ='<div class="esriViewPopup"> <div class="mainSection"> <div class="header" dojoattachpoint="_title">{{$list->sektor}}</div><div class="hzLine"></div><div><table class="attrTable" cellspacing="0px" cellpadding="0px"> <tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.proyek') :</td><td class="attrValue">{{$list->judul}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.lokasi') :</td><td class="attrValue">{{$list->bentuk_daerah}} {{$list->nama_kabkot}}, Provinsi {{$lisa->nama}}</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.luasarea') :</td><td class="attrValue">{{$list->rest}} / ha</td></tr><tr valign="top"> <td class="attrName" style="width: 35%;">@lang('bahasa.nilai') :</td><td class="attrValue"><span class="esriNumericValue"><?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')<?php } ?></span></td></tr></table> </div><div class="break"></div></div></div>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://regionalinvestment.bkpm.go.id/portal/assets/peluang_proyek/images/map-icon/smelting.png", 41, 64);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach


       rumah_sakit.hide();
      $('input:checkbox[name=rumah]').change(function () {
          if ($(this).is(':checked')) {
              //map.graphics(layer1).hide();
             
              rumah_sakit.show();
          
              // map.graphics.hide();  
          } else {
            rumah_sakit.hide();
          }


          function hideLayer1() {

              dd.push(layer1);
              
          }

          function hideLayer2() {}
      });

});


</script> 

   <script>  
   $(document).ready(function () {

$(".mapControlBtn").click(function(){
	var targetLayer = $(this).attr('data-target');
	$('.layerBox').not($('#'+targetLayer)).hide();
	$(".mapControlBtn").not(this).find('div').removeClass("layer-pressed");
	$('#' + targetLayer).toggle();
	$(this).find('div').toggleClass("layer-pressed"); 
});

$('#layerpro2').hide();
$('#layer').hide();
$('#hide').hide();
$('#legendlayer').hide();
$('#layerMeasurement').hide();
$('#layerDraw').hide();
        
});

</script>
<script>
	const postDetails = document.querySelector(".long");
	const postSidebar = document.querySelector(".when");
	const postDetailsz = document.querySelector(".ts-boxzzs");
	const postSidebarContent = document.querySelector(".ts-bosxzz > div");


	// const controller = new ScrollMagic.Controller();


	// const scene = new ScrollMagic.Scene({
		// triggerElement: postSidebar,
		// triggerHook: 0,
		// duration: getDuration
	// }).addTo(controller);

	// //3
	// if (window.matchMedia("(min-width: 768px)").matches) {
		// scene.setPin(postSidebar, {
			// pushFollowers: false
		// });
	// }

	// //4
	// window.addEventListener("resize", () => {
		// if (window.matchMedia("(min-width: 768px)").matches) {
			// scene.setPin(postSidebar, {
				// pushFollowers: false
			// });
		// } else {
			// scene.removePin(postSidebar, true);
		// }
	// });

	function getDuration() {
	   return (postDetails.offsetHeight - postSidebar.offsetHeight) + 930;
	}
	
	function printDiv(divName) {
	 
	   $('#map').printThis({
		   importStyle: true,
		   canvas: true,
		   copyTagClasses: true
	   });
	  
	  // var divToPrint=document.getElementById(divName);
	  // var newWin=window.open('','Print-Window');
	  // newWin.document.open();
	  // newWin.document.write('<html><head><style>@page { size: landscape; }</style></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
	  // newWin.document.close();
	}
	
	function clearDrawMap(){
		map.graphics.clear();
	}
</script>
@endsection

@section('content')

<!-- MAP
================================================== -->
<div id="map-container" class="fullwidth-home-map">

<section id="tbody" class="ts-boxz p-1" style="">
   <div data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design:'headline', gutters:false" style="width:100%;height:100%;margin:0;">
      <div id="map" data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'center'" style="padding:0;" class="map">
         <div id="hide"style="position:absolute; right:55px; top:10px; z-Index:999;">
            <div data-dojo-type="dijit/TitlePane" 
               data-dojo-props="title:'Switch Basemap', open:true">
               <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                  <div id="basemapGallery"></div>
               </div>
            </div>
         </div>
		 
		 
		 <!--<div id="feedback" class="shadow">
          <div id="info">
            <div id="printButton"></div>
          </div>
        </div>-->
      </div>
		<div id="header" data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'top'">
		</div>
   </div>
   <div class="ts-form__map-search ts-z-index__2" id="HomeButton"></div>
   
</section>


<div id="layerclick" data-target="layer"  style="width:35px;  right:20px; top:20px;position: absolute;"  align="center" class="mapControlBtn ts-box p-1 ts-form__map-search ts-z-index__2">
   <div id="layeron" class="">
      <i class="fa fa-map-o" aria-hidden="true"></i>
   </div>
</div>

<div id="basemapclick" data-target=""  style="width:35px;  right:20px; top:60px;position: absolute;"  align="center" class="mapControlBtn ts-box p-1 ts-form__map-search ts-z-index__2">
   <div id="basemapon" class="">
      <i class="fa fa-th-large" aria-hidden="true"></i>
   </div>
</div>

<div id="layerpro" data-target="layerpro2"  style="width:35px;  right:20px; top:100px;position: absolute;"  align="center" class="mapControlBtn ts-box p-1 ts-form__map-search ts-z-index__2">
   <div id="basemapon2" class="">
      <i class="fa fa-th-large" aria-hidden="true"></i>
   </div>
</div>
<div id="measurementClick" data-target="layerMeasurement"  style="width:35px;  right:20px; top:140px;position: absolute;"  align="center" class="mapControlBtn ts-box p-1 ts-form__map-search ts-z-index__2">
   <div id="" class="">
      <i class="fa fa-arrows-h" aria-hidden="true"></i>
   </div>
</div>
<div id="drawClick" data-target="layerDraw"  style="width:35px;  right:20px; top:180px;position: absolute;"  align="center" class="mapControlBtn ts-box p-1 ts-form__map-search ts-z-index__2">
   <div id="" class="">
      <i class="fa fa-pencil" aria-hidden="true"></i>
   </div>
</div>
<div id="clearDrawClick" style="width:35px;  right:20px; top:220px;position: absolute;cursor:pointer;"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
   <div id="eraseDrawArea" class=""></div>
</div>
<div id="printClick" style="width:35px;  right:20px; top:260px;position: absolute;cursor:pointer;"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2" onClick="printDiv('map-container')">
   <div id="" class="">
      <i class="fa fa-print" aria-hidden="true"></i>
   </div>
</div>

<div id="layerMeasurement" style=" width:300px; right:55px; top:10px" class="layerBox ts-form__map-search ts-z-index__2">
<div style =""class="zink" id="">
      <form>
         <div align="center" class="">
            <div href=".ts-form-collapse" data-toggle="collapse" class="">
               <i class="fa fa-arrows-h" aria-hidden="true"></i>&nbsp;Measurement
            </div>
         </div>
         <div style="align-center; width:340px; right:10px; padding-top: 5px;" style=" " class="zink   "
            id="measurementDiv">
         </div>
      </form>
   </div>
</div>

<div id="layerDraw" style=" width:255px; right:55px; top:10px" class="layerBox ts-form__map-search ts-z-index__2">
<div style =""class="zink" id="">
      <form>
         <div align="center" class="">
            <div href=".ts-form-collapse" data-toggle="collapse" class="">
               <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Draw
            </div>
         </div>
		 
         <div style="align-center;" class="zink ">
            <div >
			<button id="btnDrawPoint">Point</button>
			<button id="btnMultiPoint">Multi Point</button>
			<button id="btnLine">Line</button>
			<button id="btnPolyline">Polyline</button>
			<button id="btnPolygon">Polygon</button>
			<button id="btnFreehandPolyline">Freehand Polyline</button>
			<button id="btnFreehandPolygon">Freehand Polygon</button>
			<button id="btnArrow">Arrow</button>
			<button id="btnTriangle">Triangle</button>
			<button id="btnCircle">Circle</button>
			<button id="btnEllipse">Ellipse</button>
			</div>
         </div>
      </form>
   </div>
</div>

<div id="layer" style=" width:300px; right:55px; top:10px" class="layerBox ts-form__map-search ts-z-index__2">
   <div style ="align-center;"class="zink" id="collapseExample">
      <form>
         <div align="center" class="zink">
            <div href=".ts-form-collapse" data-toggle="collapse" class="">
               <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer @lang('bahasa.infrastruktur')
            </div>
         </div>
         <div style="align-center;" class="zink "
            id="collapseExample">
            <div style="float:left;"  >RTRW :  </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:20px;" class="box-1"><input checked type='checkbox'
                  name="rtrw" /><span class="toogle on"></span></div>
            </dd>
            <br>
         </div>
         <div style="align-center;" class="zink "
            id="collapseExample">
            <div style="float:left;"  > @lang('bahasa.bandara') : </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                  name="bandara" /><span class="toogle "></span></div>
            </dd>
            <br>
         </div>
         <div style="align-center;" class="zink "
            id="collapseExample">
            <div style="float:left;"  >@lang('bahasa.pelabuhan') :  </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                  name="pelabuhan" /><span class="toogle"></span></div>
            </dd>
            <br>
         </div>
         <div style="align-center;" class="zink "
            id="collapseExample">
            <div style="float:left;"  >@lang('bahasa.rumahsakit') :  </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                  name="rumah_sakit" /><span class="toogle"></span></div>
            </dd>
            <br>
         </div>
         <div style="align-center;" class="zink "
            id="collapseExample">
            <div style="float:left;"  >@lang('bahasa.pendidikan') :  </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                  name="pendidikan" /><span class="toogle"></span></div>
            </dd>
            <br>
         </div>
         <div style="align-center;" class="zink "
            id="collapseExample">
            <div style="float:left;"  >Hotel :  </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                  name="hotel" /><span class="toogle"></span></div>
            </dd>
            <br>
         </div>
      </form>
   </div>
</div>

<div id="layerpro2" style=" width:370px; right:55px; top:10px" class="layerBox ts-form__map-search ts-z-index__2">
   <div style =""class="zink scroll" id="collapseExample">
      <form>
         <div align="center" class="">
            <div href=".ts-form-collapse" data-toggle="collapse" class="">
               <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Detail
            </div>
         </div>
         @foreach($test as $list)
         @if($list->status =='2')
         <div style="align-center; width:340px; right:10px;" style=" " class="zink   "
            id="collapseExample">
            <div style="float:left;"  >{{$list->nama_rest}}   </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:10px;" class="box-1"><input checked  type='checkbox'
                  name="pro{{$list->id_rest}}{{$list->status}}" /><span class="toogle on"></span></div>
            </dd>
            <br>
         </div>
         @else
         <div style="align-center; width:340px; right:10px;" class="zink "
            id="collapseExample">
            <div style="float:left;"  >{{$list->nama_rest}}   </div>
            <dd style="margin-top:1px;"class="">
               <div style="float:right; margin-right:10px;" class="box-1"><input type='checkbox'
                  name="pro{{$list->id_rest}}{{$list->status}}" /><span class="toogle on"></span></div>
            </dd>
            <br>
         </div>
         @endif
         @endforeach
      </form>
   </div>
</div>
</section>


                            
</div> <!-- END MAP AREA -->

<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-8 col-md-8 padding-right-30">

			<!-- Titlebar -->
			<div id="titlebar" class="listing-titlebar">
				<div class="listing-titlebar-title">
					<h2>{{$propertiesz[0]->judul}} <span class="listing-tag">{{$propertiesz[0]->sektor}}</span></h2>
					<span>
						<a href="#listing-location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							{{$propertiesz[0]->nama_kabkot}}, {{$bentuk[0]->nama}}
						</a>
					</span>
				</div>
			</div>

			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav" style="">
					<li><a href="#overview" class="active">@lang('bahasa.overview')</a></li>
					<li><a href="#listing-gallery">@lang('bahasa.gallery')</a></li>
					<li><a href="#listing-video">Video</a></li>
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="overview" class="listing-section">

				<!-- Description -->

				<div class="show-more">
				<?php foreach ($propertiesz as $list) { ?>
  
				   <?php echo $list->deskripsi ?>
				   
			   <?php } ?>
			   
			   </div>
				<a href="#" class="show-more-button" data-more-title="@lang('bahasa.showmore')" data-less-title="@lang('bahasa.showless')"><i class="fa fa-angle-down"></i></a>
				
			</div>



			<div class="clearfix"></div>
			
			<!-- GALLERY -->
			  <div id="listing-gallery" class="listing-section">
				
				<h3 class="listing-desc-headline margin-top-70">@lang('bahasa.gallery')</h3>
				<div class="listing-slider-small mfp-gallery-container margin-bottom-0">
				@foreach($propertiesz as $list)
								
					<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" data-background-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" class="item mfp-gallery" title="gambar 1"></a>
					<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar1}}" data-background-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar1}}" class="item mfp-gallery" title="gambar 2"></a>
					<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar2}}" data-background-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar2}}" class="item mfp-gallery" title="gambar 3"></a>
					<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar3}}" data-background-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar3}}" class="item mfp-gallery" title="gambar 4"></a>
					<a href="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar4}}" data-background-image="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar4}}" class="item mfp-gallery" title="gambar 5"></a>

				@endforeach
				</div>

				<div class="clearfix"></div>

				<!-- Reviews -->
				

				<!-- Pagination -->
				
				<!-- Pagination / End -->
			</div>
			<div class="clearfix"></div>
			<div id="listing-video" class="listing-section">
				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Video</h3>
				@if (count($propertiesx))   
				  
				<video controls="true" width="100%">
					<source src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->video}}" type="video/mp4">
				</video>

				@else
				  <p>@lang('bahasa.kosong')</p>
				@endif
			</div>

			<div class="clearfix"></div>
				


		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-10 sticky">

			<!-- RIGHT BAR -->
			<div class="verified-badge">
				<i class="sl sl-icon-check"></i> @lang('bahasa.infopeluang')
			</div>

			
@foreach($propertiesz as $list)

<!-- Info detail -->
			<div class="boxed-widget">
				@foreach($bentuk as $lisa)
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.asektor')</span> 
					<a href="#">{{$list->sektor}}</a></h4>
				</div>
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.provinsi')</span> 
					<a href="#">{{$lisa->nama}}</a></h4>
				</div>
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.kota')</span> 
					<a href="#">{{$list->nama_kabkot}}</a></h4>
				</div>
				@endforeach
				
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.proyek')</span> 
					<a href="#">{{$list->judul}}</a></h4>
					
				</div>
				
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.tahun')</span> 
					<a href="#">{{$list->tahun}}</a></h4>
					
				</div>

				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.nilaiinvestasi')</span> 
					<a href="#"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                               @currency($list->nilai_investasi / 1000000) @lang('bahasa.juta')
                                <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000) @lang('bahasa.miliar')

                                <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000000) @lang('bahasa.triliun')
                                <?php } ?></a></h4>
					
				</div>
				<div class="hosted-by-title">
					<h4><span>IRR</span> 
					<a href="#">@if($list->irr == '') <br>@else {{$list->irr}}@endif</a></h4>
				</div>
				<div class="hosted-by-title">
					<h4><span>NPV</span> 
					<a href="#">@if($list->npv == '') <br>@else {{$list->npv}}@endif</a></h4>
				</div>
				<div class="hosted-by-title">
					<h4><span>Payback Period</span> 
					<a href="#">@if($list->pp == '') <br>@else {{$list->pp}}@endif</a></h4>
				</div>
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.sumber')</span> 
					<a href="#">{{$list->instansi}}</a></h4>
				</div>
				<div class="hosted-by-title">
					<h4><span>@lang('bahasa.kontak')</span> 
				    <div style="display:block;"><i class="sl sl-icon-phone"></i> @if($list->telepon != null && $list->telepon != '') {{$list->telepon}} @else N/A @endif</div>
					<div  style="display:block;"><i class="fa fa-user"></i> @if($list->jabatan1 != null && $list->jabatan1 != '') {{$list->jabatan1}} @else N/A @endif</div></h4>
				</div>			
@endforeach			
			<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
					<div class="small-dialog-header" style="padding:50px 0px 0px 38px;">
						<h3>@lang('bahasa.detaildokumen')</h3>
						<p>
            @lang('bahasa.detaildokumenhint')
            <!-- Mohon masukan data anda terlebih dahulu, tautan untuk unduh dokumen akan dikirimkan melalui email. -->
            </p>
					</div>
					@if(\Session::has('alert-failed'))
						<div class="alert alert-danger">
							<div>{{Session::get('alert-failed')}}</div>
						</div>
					@endif
					@if(\Session::has('alert-success'))
						<div class="alert alert-success">
							<div>{{Session::get('alert-success')}}</div>
						</div>
					@endif
					
					<form action="{{Config::get('app.url')}}/sendEmail/{{$propertiesz[0]->id_info_peluang_da}}" method="post">
						{{ csrf_field() }}
						
						<input type="email" placeholder="Email" name="email">
						<input type="text" placeholder="@lang('bahasa.namalengkap')" name="nama">
						<input type="number" placeholder="@lang('bahasa.telepon')" name="phone">
						<input type="text" placeholder="@lang('bahasa.negara')" name="judul">

						<input type="hidden" class="form-control" id="judul" name="daerah" Value="{{$propertiesz[0]->id_daerah}}"/>
						<input type="hidden" class="form-control" id="prioritas" name="prioritas" Value="{{$propertiesz[0]->prioritas}}"/>

						  <div id="map" 
				 data-dojo-type="dijit/layout/ContentPane" 
				 data-dojo-props="region:'center'" 
				 style="padding:0;"></div>

						<button type="submit" class="panel-apply">@lang('bahasa.kirimemail')</button>
					</form>	
					<div style="padding-bottom: 20px;"></div>
				</div>

				<a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="fa fa-file"></i> @lang('bahasa.detaildokumen')</a>

			
			

		</div>
		<!-- Sidebar / End -->

	</div>
</div>
</div>
 
@endsection