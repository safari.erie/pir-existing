@extends('frontend.layoutmap')

@section('content')
<html>
    <head>
<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<script src="{{Config::get('app.url')}}js/owl.carousel.min.js"></script>


<script src="https://js.arcgis.com/3.34/"></script>
<script> 
    var map;
    require([
      "esri/map",
       "esri/dijit/PopupTemplate",
         "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
        "esri/InfoTemplate",
       "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
      "dojo/parser",

      "dijit/layout/BorderContainer", 
      "dijit/layout/ContentPane", 
      "dijit/TitlePane",
      "dojo/domReady!"
    ], function(
      Map,
            PopupTemplate, 
         FeatureLayer, 
         arrayUtils, 
         ArcGISDynamicMapServiceLayer, 
         Geometry, 
         Point, 
         webMercatorUtils,  
         Graphic, 
         SimpleMarkerSymbol, 
         SimpleLineSymbol, 
         SimpleFillSymbol,
         PictureMarkerSymbol,  
         Color, 
         InfoTemplate,
       BasemapGallery,
        arcgisUtils,
      parser
    ) {
      parser.parse();

     
@foreach($propertiesx as $list){
  map = new Map("map", {
      basemap: "hybrid",
      center: [{{$list->y}}, {{$list->x}}],
      zoom: 7
  });
}
@endforeach

      //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
      var basemapGallery = new BasemapGallery({
        showArcGISBasemaps: true,
        map: map
      }, "basemapGallery");
      basemapGallery.startup();
      
      basemapGallery.on("error", function(msg) {
        console.log("basemap gallery error:  ", msg);
      });

      

       var title ='<div align="center">RTRW Kabupaten</div>';
var isi ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popupTemplate = new PopupTemplate({
            title: title,
          description: isi
          
        });

          $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken",
    {
      username: 'bkpm-sipd',
      password: 'Bkpm_S1pd@atrbpn123'
     
      
      
      
    },
    function(data,status){
        
        var key = data;
        var token='?token='+key+'';
      var featureLayer = new FeatureLayer("https://gistaru.atrbpn.go.id/arcgis/rest/services/013_RTR_KABUPATEN_KOTA_PROVINSI_RIAU/_1400_RIAU_PR_PERDA/MapServer/6"+ token,{
          outFields: ["*"],
          opacity:0.5,
    infoTemplate: popupTemplate
     });
    map.addLayer(featureLayer);
    
  
            <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
            <?php if($list->sektor == 'Infrastruktur') { ?>
             
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"></dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Miliar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliun<?php } ?></dd>';
             
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

        
           <?php } ?>

          @endforeach
           @endforeach

               <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
            <?php if($list->sektor == 'Jasa') { ?>
            
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

        
        <?php } ?>
          @endforeach
           @endforeach

               <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
            <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
            
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

        
        <?php } ?>
          @endforeach
           @endforeach

            <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
            <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
            
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

        
        <?php } ?>
          @endforeach
           @endforeach

              <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
            <?php if($list->sektor == 'Industri') { ?>
            
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

        
        <?php } ?>
          @endforeach
           @endforeach
      

  
          $('input:checkbox[name=dadan]').change(function () {
              if ($(this).is(':checked')) {
                  //map.graphics(layer1).hide();
                  featureLayer.hide();
                  
              
                  // map.graphics.hide();  
              } else {
                  featureLayer.show();
              }

              function hideLayer1() {

                  dd.push(layer1);
                  
              }

              function hideLayer2() {}
          });

    });

      });
    
  </script> 

<?php

function clean($string) {
$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>


<style> 
html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
#map{
  padding:0;
}
</style> 



<!-- Load Esri Leaflet from CDN -->
<body class="claro">

<div class="row">
    <div class="col-sm-3">
        <section id="tbody" class="when" style=" margin-top:10px; ">

            <div id="mydatas"></div>

            <!--end container-->
        </section>
    </div>
    <br>
    <div class="col-sm-9">
        <section class="single-pricing-wrap p-1" style="margin-top:10px; ">

            <!--ITEMS LISTING
        =========================================================================================================-->

        <section id="tbody" class="single-pricing-wrap p-1" style="">
           
        
                        <div style="margin-top:10px; ">
                            <div class="btn-pro">

                                <!--Display selector on the left-->
                                <div class="">

                                    <a id="ts-display-list" class="">
                                        Peta Peluang Investasi Daerah
                                    </a>
                                </div>




                            </div>
                            
                            <br>
                            <div data-dojo-type="dijit/layout/BorderContainer" 
                            data-dojo-props="design:'headline', gutters:false" 
                            style="width:100%;height:100%;margin:0;">
                     
                         <div id="map" 
                              data-dojo-type="dijit/layout/ContentPane" 
                              data-dojo-props="region:'center'" 
                              style="padding:0;">
                     
                           <div style="position:absolute; right:20px; top:10px; z-Index:999;">
                             <div data-dojo-type="dijit/TitlePane" 
                                  data-dojo-props="title:'Switch Basemap', open:false">
                               <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                                 <div id="basemapGallery"></div>
                               </div>
                             </div>
                           </div>
                     
                         </div>
                                     
                            <br>
         
            <div class="clearfix btn-pro ">

                <!--Display selector on the left-->
                <div class="">

                    <a id="ts-display-list" class="">
                        Peluang Investasi Daerah
                    </a>
                </div>

                <!--Display selector on the right-->


            </div>
            <br>
            {{-- Memanggil project --}}
            <div id="owl" class="owl-carousel ts-items-carousel" data-owl-items="4" data-owl-nav="1">
    @foreach ($projecs as $list)
  

   
<!--Item-->
<!--Item-->

    <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

     <?php  if($list->prioritas == '1' ) { ?>
  <div class="ts-ribbon-corner">
    <span>Prioritas</span>
           </div>
    
        <?php }; ?>


        <!--Card Image-->
        <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{Request::Segment(3)}}/id"
            class="card-img ts-item__image"
            >
            <img src="{{ Config::get('app.url') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                  
                                                   
            <figure class="ts-item__info">
             <h6>{{$list->judul}}</h6>
           
         </figure>
         <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">{{$list->sektor}}
</div>
     </a>


     <div class="card-body ts-item__body">
      <div class="atbd_listing_meta">
   <span id="openmap" class="atbd_meta atbd_badge_open">Detail Proyek</span>
          <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
              @currency($list->nilai_investasi / 1000000) Juta
               <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
              @currency($list->nilai_investasi / 1000000000) Miliar

               <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
              @currency($list->nilai_investasi / 1000000000000) Triliun
               <?php } ?></span>
       
          <div class="atbd_listing_data_list">
            <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
          </div>
      </div>
     </div>

     <div class="atbd_listing_bottom_content">
      <div class="atbd_content_left">
          <div class="atbd_listing_category">
             <a style="color:white;" href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{Request::Segment(3)}}/id"><span style="color:white;" class="fa fa-eye"></span>Lihat Detail</a>
          </div>
      </div>
      <ul class="atbd_content_right">
          <li class="atbd_count"></span></li>
          <li class="atbd_save">
            
          </li>
      </ul>

  </div>

 </div>
 <!--end ts-item ts-card-->





<!--end Item col-md-4-->




    @endforeach
  

         </div>
        </section>

      

</div>

    </div>

    <div>




        <!--end container-->
        </section>

    </div>
</div>
<div id="map" 
     data-dojo-type="dijit/layout/ContentPane" 
     data-dojo-props="region:'center'" 
     style="padding:0;"></div>
</body>
<script src="{{Config::get('app.url')}}assets/js/custom.js"></script>



<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<script type="text/javascript"
    src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript"
    src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
</script>
      <script>
    $(document).ready(function () {
@foreach($bentuk as $list)
        $.get("{{Config::get('app.url')}}/chart/{{$pages}}/{{$list->id_daerah}}/{{Request::Segment(4)}}", function (data) {
            @endforeach
            $("#mydatas").append(data);




        });

    });
</script>

<body>
    
    <script>
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 700
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>



 
    
    @endsection