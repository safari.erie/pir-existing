    @extends('frontend.layoutmap')

    @section('content')
    <html>
        <head>
             <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
    <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
    <script src="https://js.arcgis.com/3.34/"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script> $(document).ready(function () {

$(".preloader").delay(1500).fadeOut("slow");
                $('#submit_100').hide();
             
                $('.has-spinner').click(function () {

                    var btn = $(this);
                    $(btn).buttonLoader('start');
                    setTimeout(function () {
                        $(btn).buttonLoader('stop');
                    }, 5000);
                    var nameform = $('#identity').val();

                });

              

 $("#layerclick").click(function(){
    
        $('#layerhidden').toggle();
        $('#layeron').toggleClass("hijau");

    
});
$("#baseclick").click(function(){
   
        $('#base').toggle();
        $('#baseon').toggleClass("hijau");

    
});
$('#layerhidden').hide();
$('#base').hide();

});</script>
    <script>
$(document).ready(function() {

    $('#example5').DataTable( {
        "order": [[ 2, "asc" ]],
       "pageLength": 5
         
       } );
    $('#example4').DataTable( {
        "order": [[ 2, "asc" ]],
       "pageLength": 5
         
       } );
    $('#example3').DataTable( {
       
        "pageLength": 5
          
        } );
        $('#example2').DataTable( {
       
       "pageLength": 5
         
       } );
    $('#example1').DataTable( {
        "pageLength": 5
    } );

 

  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>


  <style> 

tr.hide-table-padding td {
  padding: 0;
}

.expand-button {
	position: relative;
}

.accordion-toggle .expand-button:after
{
  position: absolute;
  left:.75rem;
  top: 50%;
  transform: translate(0, -50%);
  content: '-';
}
.accordion-toggle.collapsed .expand-button:after
{
  content: '+';
}
    html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
    #map{
      padding:0;
    }
    .esriPopup .sizer {
    position: relative;
    width: 1100px;
    z-index: 1;
}
  </style> 



    <!-- Load Esri Leaflet from CDN -->
<body class="claro">

    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="" style=" margin-top:10px; ">

                <div id="mydatas"></div>

                <!--end container-->
            
        </div>
        <br>
        <div class="col-sm-9">
       

                <!--ITEMS LISTING
            =========================================================================================================-->

            <section id="tbody" class="single-pricing-wrap p-1 long" style="">
               
                            <div style="">
                                <div class="btn-pro titlez">

                                    <!--Display selector on the left-->
                                    <div class="">

                                        <a  class="">
                                      
                                            Peta Infrastruktur
                                        </a>
                                    </div>




                                </div>
                                <div  style="width:35px;  right:14px; top:125px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="layerclick"  class="">

                        <i class="fa fa-map-o" aria-hidden="true"></i>

                    </div>
                </div> <div  style="width:35px;  right:14px; top:165px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="baseclick"  class="">

                        <i class="fa fa-th-large" aria-hidden="true"></i>

                    </div>
                </div>
                <div id="layerhidden" style="right:40px; top:125px width:300px" class="ts-form__map-search ts-z-index__2">
                <form>
                    <div align="center" class="zink">
                        <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                            <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Infrastruktur
                        </div>
                    </div>
                    <div style="align-center;" class="zink "
                        id="collapseExample">
                        <div style="float:left;"  > RTRW : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                        name="dadan" /><span class="toogle "></span></div></dd><br>
            </div>
          
            </form>
            </div>
                                <div data-dojo-type="dijit/layout/BorderContainer" 
                                data-dojo-props="design:'headline', gutters:false" 
                                style="width:100%;height:100%;margin:0;">
                                <div id="map" 
                                data-dojo-type="dijit/layout/ContentPane" 
                                data-dojo-props="region:'center'" 
                                style="padding:0;"></div></div>
                                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                    <div data-dojo-type="dijit/TitlePane" 
                         data-dojo-props="title:'Switch Basemap', open:true">
                      <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                        <div id="basemapGallery"></div>
                    </div>
                </div>
              </div>
                  </div>
                                <div id="mydata"></div>
                                         
                                <br>
             
                <div class="clearfix btn-pro titlez  ">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="">
                           Bandara

                        </a>
                    </div>

                    <!--Display selector on the right-->


                </div>
              
                {{-- Memanggil project --}}


<br>
<div style="padding:20px;">
<table id="example1" class="display" style="width:100%">
      <thead>
          <tr align="center" >
              <th align="center" width="10">Detail</th>
             
              <th width="300">Nama Bandara</th>
              <th  width="100">Kelas Bandara</th>
              <th width="50">Lihat Peta</th>
          </tr>
      </thead>
      <tbody>
      
  @foreach ($projecs as $list)
   @foreach($bentuk as $lisa)
   
          <tr>
              <td data-toggle="modal" data-target="#exampleModalz{{$list->id_bandara}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->nama}}</td>
              <td align="center">{{ $list->kelas}}</td>
              <td><button class="zink" onclick="bandara{{$list->id_bandara}}()">Zoom Map</button></td>
              
          </tr>
          <div class="modal fade" id="exampleModalz{{$list->id_bandara}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
     
         @if($list->kelas == 'Bintang 5')
           <div class="ts-ribbon-corner">
            <span>Five Star</span>
                   </div>
@endif

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  Bandara

  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Nama Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->nama}}</dd>
       <div style="float:left; color:grey"  >Kategori Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->kategori}}</dd>
       <div style="float:left; color:grey"  >Jadwal Bandara   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jam_operasional}}</dd>
       <div style="float:left; color:grey"  >Kategori Bandara   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->maskapai}}</dd>
       <div style="float:left; color:grey"  >IATA Bandara   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->iata}}</dd>
       <div style="float:left; color:grey"  >Kelas Bandara   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->kelas}}</dd>
       <div style="float:left; color:grey"  >Alamat Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Informasi Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->keterangan}}</dd>
       <div style="float:left; color:grey"  >Sumber Data  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->instansi}}</dd>
    
     
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Infrastruktur Bandara</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 






         
          @endforeach
      @endforeach
         
      </tbody>
     
  </table>

</div>
<div style="padding:20px;">
              
      <div class="clearfix btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="">
       Pelabuhan

    </a>
</div>

<!--Display selector on the right-->


</div>

{{-- Memanggil project --}}


  <br>
  <table id="example2" class="display" style="width:100%">
        <thead>
            <tr align="center" >
                <th align="center" width="10">Detail</th>
               
                <th width="300">Nama Pelabuhan</th>
                <th  width="100">Kelas Pelabuhan</th>
                <th width="50">Lihat Peta</th>
            </tr>
        </thead>
        <tbody>
         @foreach ($pelabuhan as $list)
     @foreach($bentuk as $lisa)
     
            <tr>
                <td data-toggle="modal" data-target="#exampleModal{{$list->id_pelabuhan}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
         
                <td >{{ $list->nama}}</td>
                <td align="center">{{ $list->kelas}}</td>
                <td><button class="zink" onclick="pelabuhan{{$list->id_pelabuhan}}()">Zoom Map</button></td>
                
            </tr>
            <div class="modal fade" id="exampleModal{{$list->id_pelabuhan}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div  class="col-sm-12">
    
    <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

        <!--Ribbon-->
       
       
           @if($list->kelas == 'Bintang 5')
             <div class="ts-ribbon-corner">
              <span>Five Star</span>
                     </div>
@endif

        <!--Card Image-->
      

        </a>


        <div class="card-body ts-item__body">
         <div class="atbd_listing_meta">

         <div align="center"  >
         
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  Infrastruktur Pelabuhan

    </div>
  
    </span>
          
            
         </div>
         <div align="left"  >
         <br>
         <div style="float:left; color:grey"  >Nama Pelabuhan   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->nama}}</dd>
         <div style="float:left; color:grey"  >Kategori Pelabuhan  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->fungsi}}</dd>
         <div style="float:left; color:grey"  >Telepon Pelabuhan   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telepon}}</dd>
         <div style="float:left; color:grey"  >Kelas Pelabuhan   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->kelas}}</dd>
         <div style="float:left; color:grey"  >Alamat Pelabuhan  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
         <div style="float:left; color:grey"  >Keterangan Pelabuhan  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->keterangan}}</dd>
         <div style="float:left; color:grey"  >Sumber Data  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->instansi}}</dd>
      
       
            </div>
        </div>

        <div class="atbd_listing_bottom_content">
         <div class="atbd_content_left">
             <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Infrastruktur Pelabuhan</a>
             </div>
         </div>
         <ul class="atbd_content_right">
             <li class="atbd_count"></span></li>
             <li class="atbd_save">
               
             </li>
         </ul>

   



 


           
            @endforeach
        @endforeach
           
        </tbody>
       
    </table>
    </div>
             <div class="clearfix btn-pro titlez ">

                 <!--Display selector on the left-->
                 <div class="">

                     <a id="ts-display-list" class="">
                        Hotel

                     </a>
                 </div>

                 <!--Display selector on the right-->


             </div>
             
             {{-- Memanggil project --}}


<br>
<div style="padding:20px">
<table id="example3" class="display" style="width:100%">
      <thead>
          <tr align="center" >
              <th align="center" width="10">Detail</th>
             
              <th width="300">Nama Hotel</th>
              <th  width="100">Kelas Hotel</th>
              <th width="50">Lihat Peta</th>
          </tr>
      </thead>
      <tbody>
      
  @foreach ($hotel as $list)
  
   
          <tr>
              <td data-toggle="modal" data-target="#examplehotel{{$list->id_hotel}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->nama}}</td>
              <td align="center">{{ $list->kelas}}</td>
              <td><button class="zink" onclick="hotel{{$list->id_hotel}}()">Zoom Map</button></td>
              
          </tr>
          <div class="modal fade" id="examplehotel{{$list->id_hotel}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
     
         @if($list->kelas == 'Bintang 5')
           <div class="ts-ribbon-corner">
            <span>Five Star</span>
                   </div>
@endif

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  Infrastruktur Hotel

  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Nama Hotel  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->nama}}</dd>
       <div style="float:left; color:grey"  >Telepon Hotel   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telp}}</dd>
       <div style="float:left; color:grey"  >Kelas Hotel   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->kelas}}</dd>
       <div style="float:left; color:grey"  >Alamat Hotel   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Link Hotel  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->link}}</dd>
       <div style="float:left; color:grey"  >Sumber Data  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->instansi}}</dd>
    
     
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Infrastruktur Hotel</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 






         
         
      @endforeach
         
      </tbody>
     
  </table>
  </div>

      <div class="clearfix btn-pro titlez ">

                 <!--Display selector on the left-->
                 <div class="">

                     <a id="ts-display-list" class="">
                        Perguruan Tinggi

                     </a>
                 </div>

                 <!--Display selector on the right-->


             </div>
             
             {{-- Memanggil project --}}


<br>
<div style="padding:20px">
<table id="example4" class="display" style="width:100%">
      <thead>
          <tr align="center" >
              <th align="center" width="10">Detail</th>
             
              <th width="300">Nama Perguruan Tinggi</th>
              <th  width="100">Kategori Perguruan Tinggi</th>
              <th width="50">Lihat Peta</th>
          </tr>
      </thead>
      <tbody>
      
  @foreach ($pendidikan as $list)
   @foreach($bentuk as $lisa)
   
          <tr>
              <td data-toggle="modal" data-target="#exampleuniversity{{$list->id_pendidikan}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->nama}}</td>
              <td align="center">{{ $list->kategori}}</td>
              <td><button class="zink" onclick="pendidikan{{$list->id_pendidikan}}()">Zoom Map</button></td>
              
          </tr>
          <div class="modal fade" id="exampleuniversity{{$list->id_pendidikan}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
     
         @if($list->kategori == 'Bintang 5')
           <div class="ts-ribbon-corner">
            <span>Five Star</span>
                   </div>
@endif

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  Infrastruktur Perguruan Tinggi

  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Nama Perguruan Tinggi   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->nama}}</dd>
       <div style="float:left; color:grey"  >Telepon Perguruan Tinggi   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telp}}</dd>
       <div style="float:left; color:grey"  >Kategori Perguruan Tinggi   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->kategori}}</dd>
       <div style="float:left; color:grey"  >Alamat Perguruan Tinggi  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Link Perguruan Tinggi  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->link}}</dd>
       <div style="float:left; color:grey"  >Sumber Data  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->instansi}}</dd>
    
     
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Infrastruktur Perguruan Tinggi</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 






         
          @endforeach
      @endforeach
         
      </tbody>
     
  </table>
  </div>

  

   <div class="clearfix btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="">
       Rumah Sakit 

    </a>
</div>

<!--Display selector on the right-->


</div>

{{-- Memanggil project --}}


<br>
<div style="padding:20px">
<table id="example5" class="display" style="width:100%">
      <thead>
          <tr align="center" >
              <th align="center" width="10">Detail</th>
             
              <th width="300">Nama Rumah Sakit</th>
              <th  width="100">Kategori Rumah Sakit</th>
              <th width="50">Lihat Peta</th>
          </tr>
      </thead>
      <tbody>
      
  @foreach ($rumahsakit as $list)
   @foreach($bentuk as $lisa)
   
          <tr>
              <td data-toggle="modal" data-target="#examplerumahsakit{{$list->id_rumah_sakit}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->nama}}</td>
              <td align="center">{{ $list->kategori}}</td>
              <td><button class="zink" onclick="rumahsakit{{$list->id_rumah_sakit}}()">Zoom Map</button></td>
              
          </tr>
          <div class="modal fade" id="examplerumahsakit{{$list->id_rumah_sakit}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
     
         @if($list->kategori == 'Bintang 5')
           <div class="ts-ribbon-corner">
            <span>Five Star</span>
                   </div>
@endif

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  Infastruktur Rumah Sakit
  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Nama Rumah Sakit  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->nama}}</dd>
       <div style="float:left; color:grey"  >Teleppon Rumah Sakit  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telp}}</dd>
       <div style="float:left; color:grey"  >Kategori Rumah Sakit   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->kategori}}</dd>
       <div style="float:left; color:grey"  >Alamat Rumah Sakit   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Link Rumah Sakit </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->link}}</dd>
       <div style="float:left; color:grey"  >Sumber Data  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->instansi}}</dd>
    
     
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Infrastruktur Rumah Sakit</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 






         
          @endforeach
      @endforeach
         
      </tbody>
     
  </table>
  </div>



            <!--end container-->
            </section>

        </div>
    </div>
   
</body>
    <script>(function () {

"use strict";

if ( document.getElementById("ts-header").classList.contains("fixed-top") ){
    if( !document.getElementsByClassName("ts-homepage")[0] ) {
        document.getElementById("ts-main").style.marginTop = document.getElementById("ts-header").offsetHeight + "px";
    }
}

})();

$(document).ready(function($) {
"use strict";

$('.navbar-nav .nav-link:not([href="#"])').on('click', function(){
    $('.navbar-collapse').collapse('hide');
});

$(".ts-img-into-bg").each(function() {
    $(this).css("background-image", "url("+ $(this).find("img").attr("src") +")" );
});

//  Background

$("[data-bg-color], [data-bg-image], [data-bg-pattern]").each(function() {
    var $this = $(this);

    if( $this.hasClass("ts-separate-bg-element") ){
        $this.append('<div class="ts-background">');

        // Background Color

        if( $("[data-bg-color]") ){
            $this.find(".ts-background").css("background-color", $this.attr("data-bg-color") );
        }

        // Background Image

        if( $this.attr("data-bg-image") !== undefined ){
            $this.find(".ts-background").append('<div class="ts-background-image">');
            $this.find(".ts-background-image").css("background-image", "url("+ $this.attr("data-bg-image") +")" );
            $this.find(".ts-background-image").css("background-size", $this.attr("data-bg-size") );
            $this.find(".ts-background-image").css("background-position", $this.attr("data-bg-position") );
            $this.find(".ts-background-image").css("opacity", $this.attr("data-bg-image-opacity") );

            $this.find(".ts-background-image").css("background-size", $this.attr("data-bg-size") );
            $this.find(".ts-background-image").css("background-repeat", $this.attr("data-bg-repeat") );
            $this.find(".ts-background-image").css("background-position", $this.attr("data-bg-position") );
            $this.find(".ts-background-image").css("background-blend-mode", $this.attr("data-bg-blend-mode") );
        }

        // Parallax effect

        if( $this.attr("data-bg-parallax") !== undefined ){
            $this.find(".ts-background-image").addClass("ts-parallax-element");
        }
    }
    else {

        if(  $this.attr("data-bg-color") !== undefined ){
            $this.css("background-color", $this.attr("data-bg-color") );
            if( $this.hasClass("btn") ) {
                $this.css("border-color", $this.attr("data-bg-color"));
            }
        }

        if( $this.attr("data-bg-image") !== undefined ){
            $this.css("background-image", "url("+ $this.attr("data-bg-image") +")" );

            $this.css("background-size", $this.attr("data-bg-size") );
            $this.css("background-repeat", $this.attr("data-bg-repeat") );
            $this.css("background-position", $this.attr("data-bg-position") );
            $this.css("background-blend-mode", $this.attr("data-bg-blend-mode") );
        }

        if( $this.attr("data-bg-pattern") !== undefined ){
            $this.css("background-image", "url("+ $this.attr("data-bg-pattern") +")" );
        }

    }
});

$(".ts-password-toggle").on("click",function() {
    var $parent = $(this).closest(".ts-has-password-toggle");
    var $this = $(this);
    var $password = $parent.find("input");
    if ($password.attr("type") === "password") {
        $password.attr("type", "text");
        $this.find("i").removeClass("fa-eye").addClass("fa-eye-slash");
    } else {
        $password.attr("type", "password");
        $this.find("i").removeClass("fa-eye-slash").addClass("fa-eye");
    }
});

$(function () {
   
});

$("select").each(function () {
    isSelected( $(this) );
}).on("change", function () {
    isSelected( $(this) );
});

if ($(".ts-video").length > 0) {
    $(this).fitVids();
}

// Owl Carousel

var $owlCarousel = $(".owl-carousel");

if( $owlCarousel.length ){
    $owlCarousel.each(function() {

        var items = parseInt( $(this).attr("data-owl-items"), 10);
        if( !items ) items = 1;

        var nav = parseInt( $(this).attr("data-owl-nav"), 2);
        if( !nav ) nav = 0;

        var dots = parseInt( $(this).attr("data-owl-dots"), 2);
        if( !dots ) dots = 0;

        var center = parseInt( $(this).attr("data-owl-center"), 2);
        if( !center ) center = 0;

        var loop = parseInt( $(this).attr("data-owl-loop"), 2);
        if( !loop ) loop = 0;

        var margin = parseInt( $(this).attr("data-owl-margin"), 2);
        if( !margin ) margin = 0;

        var autoWidth = parseInt( $(this).attr("data-owl-auto-width"), 2);
        if( !autoWidth ) autoWidth = 0;

        var navContainer = $(this).attr("data-owl-nav-container");
        if( !navContainer ) navContainer = 0;

        var autoplay = parseInt( $(this).attr("data-owl-autoplay"), 2);
        if( !autoplay ) autoplay = 0;

        var autoplayTimeOut = parseInt( $(this).attr("data-owl-autoplay-timeout"), 10);
        if( !autoplayTimeOut ) autoplayTimeOut = 5000;

        var autoHeight = parseInt( $(this).attr("data-owl-auto-height"), 2);
        if( !autoHeight ) autoHeight = 0;

        var fadeOut = $(this).attr("data-owl-fadeout");
        if( !fadeOut ) fadeOut = 0;
        else fadeOut = "fadeOut";

        if( $("body").hasClass("rtl") ) var rtl = true;
        else rtl = false;

        if( items === 1 ){
            $(this).owlCarousel({
                navContainer: navContainer,
                animateOut: fadeOut,
                autoplayTimeout: autoplayTimeOut,
                autoplay: 1,
                autoheight: autoHeight,
                center: center,
                loop: loop,
                margin: margin,
                autoWidth: autoWidth,
                items: 1,
                nav: nav,
                dots: dots,
                rtl: rtl,
                navText: []
            });
        }
        else {
            $(this).owlCarousel({
                navContainer: navContainer,
                animateOut: fadeOut,
                autoplayTimeout: autoplayTimeOut,
                autoplay: autoplay,
                autoheight: autoHeight,
                center: center,
                loop: loop,
                margin: margin,
                autoWidth: autoWidth,
                items: 1,
                nav: nav,
                dots: dots,
                rtl: rtl,
                navText: [],
                responsive: {
                    1368: {
                        items: items
                    },
                    991: {
        items: 2,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      },
                    450: {
                        items: 2
                    },
                    0: {
                        items: 1
                    }
                }
            });
        }

        if( $(this).find(".owl-item").length === 1 ){
            $(this).find(".owl-nav").css( { "opacity": 0,"pointer-events": "none"} );
        }

    });
}

// Magnific Popup

var $popupImage = $(".popup-image");

if ( $popupImage.length > 0 ) {
    $popupImage.magnificPopup({
        type:'image',
        fixedContentPos: false,
        gallery: { enabled:true },
        removalDelay: 300,
        mainClass: 'mfp-fade',
        callbacks: {
            // This prevents pushing the entire page to the right after opening Magnific popup image
            open: function() {
                $(".page-wrapper, .navbar-nav").css("margin-right", getScrollBarWidth());
            },
            close: function() {
                $(".page-wrapper, .navbar-nav").css("margin-right", 0);
            }
        }
    });
}

var $videoPopup = $(".video-popup");

if ( $videoPopup.length > 0 ) {
    $videoPopup.magnificPopup({
        type: "iframe",
        removalDelay: 300,
        mainClass: "mfp-fade",
        overflowY: "hidden",
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
            '<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
            '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/',
                    id: 'v=',
                    src: '//www.youtube.com/embed/%id%?autoplay=1'
                },
                vimeo: {
                    index: 'vimeo.com/',
                    id: '/',
                    src: '//player.vimeo.com/video/%id%?autoplay=1'
                },
                gmaps: {
                    index: '//maps.google.',
                    src: '%id%&output=embed'
                }
            },
            srcAction: 'iframe_src'
        }
    });
}

$(".ts-form-email [type='submit']").each(function(){
    var text = $(this).text();
    $(this).html("").append("<span>"+ text +"</span>").prepend("<div class='status'><i class='fas fa-circle-notch fa-spin spinner'></i></div>");
});

$(".ts-form-email .btn[type='submit']").on("click", function(){
    var $button = $(this);
    var $form = $(this).closest("form");
    var pathToPhp = $(this).closest("form").attr("data-php-path");
    $form.validate({
        submitHandler: function() {
            $button.addClass("processing");
            $.post( pathToPhp, $form.serialize(),  function(response) {
                $button.addClass("done").find(".status").append(response).prop("disabled", true);
            });
            return false;
        }
    });
});

if( $("input[type=file].with-preview").length ){
    $("input.file-upload-input").MultiFile({
        list: ".file-upload-previews"
    });
}

if( $(".ts-has-bokeh-bg").length ){

    $("#ts-main").prepend("<div class='ts-bokeh-background'><canvas id='ts-canvas'></canvas></div>");
    var canvas = document.getElementById("ts-canvas");
    var context = canvas.getContext("2d");
    var maxRadius  = 50;
    var minRadius  = 3;
    var colors = ["#5c81f9",  "#66d3f7"];
    var numColors  =  colors.length;

    for(var i=0;i<50;i++){
        var xPos       =  Math.random()*canvas.width;
        var yPos       =  Math.random()*10;
        var radius     =  minRadius+(Math.random()*(maxRadius-minRadius));
        var colorIndex =  Math.random()*(numColors-1);
        colorIndex     =  Math.round(colorIndex);
        var color      =  colors[colorIndex];
        drawCircle(context, xPos, yPos, radius, color);
    }
}

function drawCircle(context, xPos, yPos, radius, color)
{
    context.beginPath();
    context.arc(xPos, yPos, radius, 0, 360, false);
    context.fillStyle = color;
    context.fill();
}

heroPadding();

var $scrollBar = $(".scrollbar-inner");
if( $scrollBar.length ) {
    $scrollBar.scrollbar();
}

initializeSly();
hideCollapseOnMobile();

});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// On RESIZE actions

var resizeId;
$(window).on("resize", function(){
clearTimeout(resizeId);
resizeId = setTimeout(doneResizing, 250);
});

// Do after resize

function doneResizing(){
//heroPadding();
hideCollapseOnMobile();
}

function isSelected($this){
if( $this.val() !== "" ) $this.addClass("ts-selected");
else $this.removeClass("ts-selected");
}

function initializeSly() {
$(".ts-sly-frame").each(function () {

    var horizontal = parseInt( $(this).attr("data-ts-sly-horizontal"), 2);
    if( !horizontal ) horizontal = 0;

    var scrollbar = $(this).attr("data-ts-sly-scrollbar");
    if( !scrollbar ) scrollbar = 0;

    $(this).sly({
        horizontal: horizontal,
        smart: 1,
        elasticBounds: 1,
        speed: 300,
        itemNav: 'basic',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        scrollBar: $(scrollbar),
        dragHandle: 1,
        scrollTrap: 1,
        clickBar: 1,
        scrollBy: 1,
        dynamicHandle: 1
    }, {
        load: function () {
            $(this.frame).addClass("ts-loaded");
        }
    });
});
}

function heroPadding() {
var $header = $("#ts-header");
var $hero = $("#ts-hero");

if( $header.hasClass("fixed-top") ){
    if( $hero.find(".ts-full-screen").length ) {
        $hero.find(".ts-full-screen").css("padding-top", $(".fixed-top").height() );
    }
    else {
        $hero.css("padding-top", $(".fixed-top").height() );
    }
}
else {
    if( $hero.find(".ts-full-screen").length ) {
        $hero.find(".ts-full-screen").css("min-height", "calc( 100vh - " + $header.height() + "px" );
    }
}
}

// Smooth Scroll

$(".ts-scroll").on("click", function(event) {
if (
    location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
    &&
    location.hostname === this.hostname
) {
    var target = $(this.hash);
    var headerHeight = 0;
    var $fixedTop = $(".fixed-top");
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if( $fixedTop.length ){
        headerHeight = $fixedTop.height();
    }
    if (target.length) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: target.offset().top - headerHeight
        }, 1000, function() {
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
                return false;
            } else {
                $target.attr('tabindex','-1');
                $target.focus();
            }
        });
    }
}
});

function hideCollapseOnMobile() {
if ($(window).width() < 575) {
    $(".ts-xs-hide-collapse.collapse").removeClass("show");
}
}</script>
   

       <script>
        $(document).ready(function () {

           
            

            
            $.get("{{ Config::get('app.url') }}/map-infra?type={{Request::Segment(3)}}", function (data) {
                $("#mydata").html(data);
                
            

            });

            
          
        
@foreach($bentuk as $list)
            $.get("{{ Config::get('app.url') }}/chart/{{$pages}}/{{$list->id_daerah}}/{{Request::Segment(4)}}", function (data) {
                @endforeach
                $("#mydatas").append(data);


                const postDetails = document.querySelector(".long");
            const postSidebar = document.querySelector(".when");
            const postDetailsz = document.querySelector(".ts-boxzzs");
            const postSidebarContent = document.querySelector(".ts-bosxzz > div");


            const controller = new ScrollMagic.Controller();


            const scene = new ScrollMagic.Scene({
                triggerElement: postSidebar,
                triggerHook: 0,
                duration: getDuration
            }).addTo(controller);

            //3
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            }

            //4
            window.addEventListener("resize", () => {
                if (window.matchMedia("(min-width: 768px)").matches) {
                    scene.setPin(postSidebar, {
                        pushFollowers: false
                    });
                } else {
                    scene.removePin(postSidebar, true);
                }
            });
           
            function getDuration() {
                return postDetails.offsetHeight - postSidebar.offsetHeight;
            }

            });
          

        });

     
    </script>
    <script>
    
    @foreach($pelabuhan as $list)
function pelabuhan{{$list->id_pelabuhan}}() {
   
    var center{{$list->id_pelabuhan}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_pelabuhan}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_pelabuhan}} ,17);
      
       
}
@endforeach
@foreach($projecs as $list)
function bandara{{$list->id_bandara}}() {
   
    var center{{$list->id_bandara}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_bandara}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_bandara}} ,17);
       
}
@endforeach
@foreach($hotel as $list)
function hotel{{$list->id_hotel}}() {
   
    var center{{$list->id_hotel}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_hotel}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_hotel}} ,17);
       
}
@endforeach
@foreach($pendidikan as $list)
function pendidikan{{$list->id_pendidikan}}() {
   
    var center{{$list->id_pendidikan}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_pendidikan}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_pendidikan}} ,17);
       
}
@endforeach
@foreach($rumahsakit as $list)
function rumahsakit{{$list->id_rumah_sakit}}() {
   
    var center{{$list->id_rumah_sakit}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_rumah_sakit}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_rumah_sakit}} ,17);
       
}
@endforeach
</script>

   

     
        
        @endsection