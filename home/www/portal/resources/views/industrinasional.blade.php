@extends('frontend.layoutmap')

@section('content')
<html>
    <head>
<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
	
<?php

function clean($string) {
$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>


<style> 
html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
#map{
  padding:0;
}

.esriPopup .sizer {
    position: relative;
    width: 300px;
    z-index: 1;
}
</style> 



<!-- Load Esri Leaflet from CDN -->
<body class="claro">

<div class="row">
    <div class="col-sm-0">
        <section id="tbody" class="when" style=" margin-top:10px; ">

            <div id="mydatass"></div>

            <!--end container-->
        </section>
    </div>
    <br>
    <div id="legendlayer1" style="width: 280px; left: 30px;  bottom: 10px;" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                                 <div class="zink" align="center" >Legend Industrial Estate</div>
                                        <div id="Legendblock" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Assigned.png" width="23px;">&nbsp;Industrial Estate and Block site plan</div>
                                        <div id="Legendindustri" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Complete-No-Information-Found.png" width="23px;">&nbsp;Industrial Estate</div>
                                        <div id="Legendkek" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Complete-Evacuated.png" width="23px;">&nbsp;Special Economic Zone</div>
   
                                    </div>
    <div id="legendlayer2" style="width: 280px; left: 30px;  top: 900px;" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                                 <div class="zink" align="center" >Legend Industrial Estate</div>
                                        <div id="Legendblock" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Assigned.png" width="23px;">&nbsp;Industrial Estate and Block site plan</div>
                                        <div id="Legendindustri" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Complete-No-Information-Found.png" width="23px;">&nbsp;Industrial Estate</div>
                                        <div id="Legendkek" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Complete-Evacuated.png" width="23px;">&nbsp;Special Economic Zone</div>
   
                                    </div>



    <div class="col-sm-12">
           <section id="tbody" class="ts-box p-1" style="margin-left:20px; margin-top:40px; margin-right:20px;">

            <!--ITEMS LISTING
        =========================================================================================================-->

        <section id="tbody" class="single-pricing-wrap p-1" style="">
           
        
                        <div style="margin-top:10px; ">
                            <div class="btn-pro">

                                <!--Display selector on the left-->
                                <div class="">

                                    <a id="ts-display-list" class="">
                                    @lang('bahasa.regionalmap')
                                    </a>
                                </div>




                            </div>
                                  <div  style="width:35px;  right:14px; top:110px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                        <div id="layerclick"  class="">

                            <i class="fa fa-map-o" aria-hidden="true"></i>

                        </div>
                    </div>
                    <div  style="width:35px;  right:14px; top:150px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                        <div id="baseclick"  class="">

                            <i class="fa fa-th-large" aria-hidden="true"></i>

                        </div>
                    </div>

                            <br>
                               <div id="layerhidden" style="right:40px; top:125px width:300px" class="ts-form__map-search ts-z-index__2">
                     <form>
                        <div align="center" class="zink">
                            <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                                <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Infrastruktur
                            </div>
                        </div>
                        <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  > Bandara : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                    name="bandara" /><span class="toogle "></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pelabuhan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pelabuhan" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Rumah Sakit :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="rumah_sakit" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pendidikan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pendidikan" /><span class="toogle"></span></div></dd><br>
                </div>
            

                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Hotel :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="hotel" /><span class="toogle"></span></div></dd><br>
                </div>
                </form>  
                </div>
                            <div id="kawasan"></div>
                          <div data-dojo-type="dijit/layout/BorderContainer" 
                            data-dojo-props="design:'headline', gutters:false" 
                            style="width:100%;height:100%;margin:0;">
                     <div class="row"><div class="col-sm-0"></div><div class="col-sm-12">  <div id="map" 
                              data-dojo-type="dijit/layout/ContentPane" background-color:fff;
                              data-dojo-props="region:'center'" 
                              style="padding:0;">
                                  
                         </div></div></div>
<body class="claro">
                         <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                        <div data-dojo-type="dijit/TitlePane" 
                             data-dojo-props="title:'Switch Basemap', open:true">
                          <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                            <div id="basemapGallery"></div>
                        </div>
                    </div>
                  </div>
</body>
                      <br>
            
                            <div data-target="#demo1" class="clearfix btn-pro ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="">
        @lang('bahasa.blok_kawasan')
    </a>
</div>

<!--Display selector on the right-->


</div>
<br>
{{-- Memanggil project --}}


<div id="demo1">
<div style="padding:20px;">

<table id="example1" class="table table-striped" style="width:100%">
<thead>

<tr align="center" >
<th align="center" width="10">Detail</th>

<th width="300">@lang('bahasa.nama_kawasan_industri_blok')</th>
<th  width="100">@lang('bahasa.kategori_blok')</th>
<th width="50">@lang('bahasa.lihat_peta_blok')</th>
</tr>
</thead>
<tbody>


@foreach($projecs as $list)
@if($list->id_kategori == '1')

   
          <tr>
              <td data-backdrop="false" data-toggle="modal" data-target="#exampleModalz{{$list->id_kawasan_industri}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->kawasan}}</td>
              <td align="center">{{ $list->kategori}}</td>
              <td><button class="zink" onclick="block{{$list->id_kawasan_industri}}()">Lihat Peta</button></td>
              
          </tr>
          <div class="modal fade" id="exampleModalz{{$list->id_kawasan_industri}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
   

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  {{$list->kawasan}}

  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Alamat   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Kabupaten/Kota  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->regency}}</dd>
       <div style="float:left; color:grey"  >Provinsi   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->provinsi}}</dd>
       <div style="float:left; color:grey"  >Luas (Ha)   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->luas / 1}}  Ha</dd>
       <div style="float:left; color:grey"  >Telepon  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telepon}}</dd>
       <div style="float:left; color:grey"  >Fax   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->fax}} </dd>
       <div style="float:left; color:grey"  >Email   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->email}}</dd>
       <div style="float:left; color:grey"  >Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->bandara}}</dd>
       <div style="float:left; color:grey"  >Jarak Ke Bandara   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_bandara}} Km</dd>
       <div style="float:left; color:grey"  >Pelabuhan   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->pelabuhan}}</dd>
       <div style="float:left; color:grey"  >Jarak Ke Pelabuhan  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_pelabuhan}} Km</dd>
       <div style="float:left; color:grey"  >Ibukota    </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->ibukota}}</dd>
       <div style="float:left; color:grey"  >Jarak Ke Ibukota   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_ibukota}} Km</dd>
       <div style="float:left; color:grey"  >Sumber  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->sumber}}</dd>
       
         
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Indonesia  {{$list->kategori}}</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 





@endif
         
          @endforeach
     
</tbody>

</table>

</div>
</div>

<div data-toggle="collapse" data-target="#demo" class="clearfix btn-pro ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="">
        @lang('bahasa.kawasan_industri')

    </a>
</div>

<!--Display selector on the right-->


</div>
<br>
{{-- Memanggil project --}}


<div id="demo1">
<div style="padding:20px;">

<table id="example2" class="table table-striped" style="width:100%">
<thead>

<tr align="center" >
<th align="center" width="10">Detail</th>

<th width="300">@lang('bahasa.nama_kawasan_industri')</th>
<th  width="100">@lang('bahasa.kategori_kawasan_industri')</th>
<th width="50">@lang('bahasa.lihat_peta_kawasan_industri')</th>
</tr>
</thead>
<tbody>


@foreach($projecs as $list)
@if($list->id_kategori == '2')

   
          <tr>
              <td data-backdrop="false" data-toggle="modal" data-target="#exampleModalz{{$list->id_kawasan_industri}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->kawasan}}</td>
              <td align="center">{{ $list->kategori}}</td>
              <td><button class="zink" onclick="blockz{{$list->id_kawasan_industri}}()">Lihat Peta</button></td>
              
          </tr>
          <div class="modal fade" id="exampleModalz{{$list->id_kawasan_industri}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
   

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  {{$list->kawasan}}

  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Alamat   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Kabupaten/Kota   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->regency}}</dd>
       <div style="float:left; color:grey"  >Provinsi   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->provinsi}}</dd>
       <div style="float:left; color:grey"  >Luas (Ha)   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->luas / 1}}  Ha</dd>
       <div style="float:left; color:grey"  >Telepon   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telepon}}</dd>
       <div style="float:left; color:grey"  >Fax   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->fax}} </dd>
       <div style="float:left; color:grey"  >Email   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->email}}</dd>
       <div style="float:left; color:grey"  >Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->bandara}}</dd>
       <div style="float:left; color:grey"  >Jarak ke Bandara   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_bandara}} Km</dd>
       <div style="float:left; color:grey"  >Pelabuhan   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->pelabuhan}}</dd>
       <div style="float:left; color:grey"  >Jarak ke Pelabuhan  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_pelabuhan}} Km</dd>
       <div style="float:left; color:grey"  >Ibukota    </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->ibukota}}</dd>
       <div style="float:left; color:grey"  >Jarak ke Ibukota   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_ibukota}} Km</dd>
       <div style="float:left; color:grey"  >Sumber  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->sumber}}</dd>
       
         
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Indonesia {{$list->kategori}}</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 





@endif
         
          @endforeach


</tbody>

</table>
</div>
<div data-toggle="collapse"  data-target="#demo" class="clearfix btn-pro ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="">
        @lang('bahasa.kawasan_ekonomi_khusus')
    </a>
</div>

<!--Display selector on the right-->


</div>
<br>
{{-- Memanggil project --}}


<div id="demo1">
<div style="padding:20px;">

<table id="example3" class="table table-striped" style="width:100%">
<thead>

<tr align="center" >
<th align="center" width="10">Detail</th>

<th width="300">@lang('bahasa.nama_kek')</th>
<th  width="100">@lang('bahasa.kategori_kek')</th>
<th width="50">@lang('bahasa.lihat_peta_kek')</th>
</tr>
</thead>
<tbody>


@foreach($projecs as $list)
@if($list->id_kategori == '3')

   
          <tr>
              <td data-backdrop="false" data-toggle="modal" data-target="#exampleModalz{{$list->id_kawasan_industri}}" align="center"><i style="color:grey;" class="fa fa-eye"></td>
       
              <td >{{ $list->kawasan}}</td>
              <td align="center">{{ $list->kategori}}</td>
              <td><button class="zink" onclick="blockz{{$list->id_kawasan_industri}}()">Lihat Peta</button></td>
              
          </tr>
         
          <div class="modal fade" id="exampleModalz{{$list->id_kawasan_industri}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div  class="col-sm-12">
  
  <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

      <!--Ribbon-->
     
   

      <!--Card Image-->
    

      </a>


      <div class="card-body ts-item__body">
       <div class="atbd_listing_meta">

       <div align="center"  >
       
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price">  {{$list->kawasan}}

  </div>

  </span>
        
          
       </div>
       <div align="left"  >
       <br>
       <div style="float:left; color:grey"  >Nama   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->alamat}}</dd>
       <div style="float:left; color:grey"  >Kabupaten/Kota  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->regency}}</dd>
       <div style="float:left; color:grey"  >Provinsi   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->provinsi}}</dd>
       <div style="float:left; color:grey"  >Luas (Ha)   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->luas / 1}}  Ha</dd>
       <div style="float:left; color:grey"  >Telepon   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->telepon}}</dd>
       <div style="float:left; color:grey"  >Fax   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->fax}} </dd>
       <div style="float:left; color:grey"  >Email   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->email}}</dd>
       <div style="float:left; color:grey"  >Bandara </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->bandara}}</dd>
       <div style="float:left; color:grey"  >Jarak ke Bandara  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_bandara}} Km</dd>
       <div style="float:left; color:grey"  >Pelabuhan  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->pelabuhan}}</dd>
       <div style="float:left; color:grey"  >Jarak ke Palabuhan   </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_pelabuhan}} Km</dd>
       <div style="float:left; color:grey"  >Ibukota    </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->ibukota}}</dd>
       <div style="float:left; color:grey"  >Jarak ke Ibukota  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->jarak_ibukota}} Km</dd>
       <div style="float:left; color:grey"  >Sumber  </div><dd style="margin-left:140px;  ; "class="border-bottom pb-2"> : {{$list->sumber}}</dd>
       
         
          </div>
      </div>

      <div class="atbd_listing_bottom_content">
       <div class="atbd_content_left">
           <div class="atbd_listing_category">
               <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Indonesia {{$list->kategori}}</a>
           </div>
       </div>
       <ul class="atbd_content_right">
           <li class="atbd_count"></span></li>
           <li class="atbd_save">
             
           </li>
       </ul>

 





@endif
         
          @endforeach

</tbody>

</table>
<!--end container-->
</section>

</div>
</div>



        <!--end container-->
        </section>

    </div>
</div>
 
</body>

<body>
    
    <script>
 $("#layerclick").click(function(){
         
         $('#layerhidden').toggle();
         $('#layeron').toggleClass("hijau");

     
});
$("#baseclick").click(function(){
         
         $('#base').toggle();
         $('#baseon').toggleClass("hijau");

     
});
 $('#layerhidden').hide();
 $('#base').hide();


        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 400
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>

<script>
$("#legendlayer1").hide();

$("#legendlayer2").show();
    @foreach($projecs as $list)
    @if($list->id_kawasan_industri == '29')
function block29() {
    $("#kawasan").show();
$("#legendlayer1").hide();
$("#legendlayer2").show();


    $.get("{{ Config::get('app.url') }}/chartkawasan/29/32", function (data) {
                $("#kawasan").html(data);
       
            });
   
    var center29 = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center29);
              document.body.scrollTop = 360;
  document.documentElement.scrollTop = 360;
       map.centerAndZoom(center29 ,15);
      
       
}
@else
function block{{$list->id_kawasan_industri}}() {
    $("#kawasan").show();
$("#legendlayer1").hide();
$("#legendlayer2").show();

    $.get("{{ Config::get('app.url') }}/chartkawasan/{{$list->id_kawasan_industri}}/{{$list->id_parent}}", function (data) {
                $("#kawasan").html(data);
       
            });
   
    var center{{$list->id_kawasan_industri}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_kawasan_industri}});
              document.body.scrollTop = 360;
  document.documentElement.scrollTop = 360;
       map.centerAndZoom(center{{$list->id_kawasan_industri}} ,15);
      
       
}
@endif
@endforeach
@foreach($projecs as $list)
function blockz{{$list->id_kawasan_industri}}() {

         $("#kawasan").hide();
       $("#legendlayer2").hide();
$("#legendlayer1").show();

   
    var center{{$list->id_kawasan_industri}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_kawasan_industri}});
              document.body.scrollTop = 360;
  document.documentElement.scrollTop = 360;
       map.centerAndZoom(center{{$list->id_kawasan_industri}} ,15);
      
       
}
@endforeach
</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
	


<!-- Include fusioncharts core library file -->
<script type="text/javascript"
    src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
</script>
   <script>
    $(document).ready(function () {
 $.get("{{ Config::get('app.url') }}/chartkawasan/indonesia/11", function (data) {
                $("#kawasan").html(data);
               
              
            });

                  $('#example1').DataTable( {
        "pageLength": 5
    } );
    $('#example2').DataTable( {
        "pageLength": 5
    } );
    $('#example3').DataTable( {
        "pageLength": 5
    } );

    });
</script>

<script src="https://js.arcgis.com/3.34/"></script>

<script> 
    var map;
    require([
      "esri/map",
       "esri/dijit/PopupTemplate",
         "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
        "esri/InfoTemplate",
       "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
      "dojo/parser",

      "dijit/layout/BorderContainer", 
      "dijit/layout/ContentPane", 
      "dijit/TitlePane",
      "dojo/domReady!"
    ], function(
      Map,
            PopupTemplate, 
         FeatureLayer, 
         arrayUtils, 
         ArcGISDynamicMapServiceLayer, 
         Geometry, 
         Point, 
         webMercatorUtils,  
         Graphic, 
         SimpleMarkerSymbol, 
         SimpleLineSymbol, 
         SimpleFillSymbol,
         PictureMarkerSymbol,  
         Color, 
         InfoTemplate,
       BasemapGallery,
        arcgisUtils,
      parser
    ) {
      parser.parse();

     
@foreach($kota as $list){
  map = new Map("map", {
      basemap: "hybrid",
         center: [117.62527, -1.4509444],
      zoom: 5
  });
}
@endforeach

      //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
      var basemapGallery = new BasemapGallery({
        showArcGISBasemaps: true,
        map: map
      }, "basemapGallery");
      basemapGallery.startup();
      
      basemapGallery.on("error", function(msg) {
        console.log("basemap gallery error:  ", msg);
      });

      



           var popupTemplate = new PopupTemplate({
    title: "{kawasan}",

    fieldInfos: [

      { fieldName: "Regency", visible: true, label: "Regency :", format: { places: 0   } },
      { fieldName: "blok_kawasan_industri", visible: true, label: "Industrial Site Block :", format: { places: 0   } },
      { fieldName: "status_blok_kawasan_industri", visible: true, label: "Status of Industrial Site Block :", format: { places: 0   } },
      { fieldName: "luas", visible: true, label: "Large (ha) :", format: { places: 0   } },
      { fieldName: "fungsi", visible: true, label: "Functionality :", format: { places: 0   } },
      { fieldName: "perusahaan", visible: true, label: "Company :", format: { places: 0   } },
      { fieldName: "asal_negara", visible: true, label: "Origin Country of Tenant :", format: { places: 0   } },
    ],

  
           });
      

        var featureLayer = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/industri/Blog_Kawasan_Industri/FeatureServer/0",{
          outFields: ["*"],
          opacity:0.5,
    infoTemplate: popupTemplate
     });
    map.addLayer(featureLayer);
    var border = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/industri/Block_Industri_Border/MapServer/0",{
          outFields: ["*"],
          opacity:0.5
     });
    map.addLayer(border);

            <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
                
            
                  
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div >sas</dd>';
             
        var image1 = "https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Assigned.png";
          var image2 = "https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Complete-No-Information-Found.png";
        var image3 = "https://static.arcgis.com/images/Symbols/Emergency-Management/z-Location-Checks-Complete-Evacuated.png";
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              @if($list->id_kategori == '1')
              var symbol = new esri.symbol.PictureMarkerSymbol(image1, 25, 25);
                @elseif($list->id_kategori == '2')
                var symbol = new esri.symbol.PictureMarkerSymbol(image2, 25, 25);
              @elseif($list->id_kategori == '3')
                var symbol = new esri.symbol.PictureMarkerSymbol(image3, 25, 25);
                  @endif
              pointInfoTemplate = new PopupTemplate({
    
    title: "{{$list->kawasan}}",

    fieldInfos: [

      { fieldName: "Regencys", visible: true, label: "Address : {{$list->nama}} ", format: { places: 0   } },
      { fieldName: "blok_kawasan_industri", visible: true, label: "Regency :", format: { places: 0   } },
      { fieldName: "status_blok_kawasan_industri", visible: true, label: "Province:  {{$list->provinsi}}", format: { places: 0   } },
      { fieldName: "luas", visible: true, label: "Large (ha) : {{$list->luas}}", format: { places: 0   } },
      { fieldName: "fungsi", visible: true, label: "Phone : {{$list->telepon}}", format: { places: 0   } },
      { fieldName: "perusahaan", visible: true, label: "fax : {{$list->fax}}", format: { places: 0   } },
      { fieldName: "asal_negara1", visible: true, label: "email : {{$list->email}}", format: { places: 0   } },
      { fieldName: "asal_negara2", visible: true, label: "Airport : {{$list->bandara}}", format: { places: 0   } },
      { fieldName: "asal_negara3", visible: true, label: "Airport Distance (Km) : {{$list->jarak_bandara}}", format: { places: 0   } },
      { fieldName: "asal_negara4", visible: true, label: "Port : {{$list->pelabuhan}}", format: { places: 0   } },
      { fieldName: "asal_negara5", visible: true, label: "Port Distance (Km) : {{$list->jarak_pelabuhan}}", format: { places: 0   } },
      { fieldName: "asal_negara7", visible: true, label: "Capital City Distance (Km) : {{$list->email}}", format: { places: 0   } },
      { fieldName: "asal_negara6", visible: true, label: "Capital City : {{$list->email}}", format: { places: 0   } },
      { fieldName: "asal_negara8", visible: true, label: "Source : {{$list->email}}", format: { places: 0   } },
      
    ]
              });
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

              layer1.on('click', function () {
                document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
            })
      
          @endforeach
           @endforeach

              
             // Layer Infrastruktur
                    var title4 = '<div align="center">Infrastruktur Airport</div>';
        var isi4 =
            '<div style="float:left;"  >Name Airport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_bandara = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var bandara = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/bandara_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_bandara
            });
        map.addLayer(bandara);

        var title5 = '<div align="center">Infrastruktur Seaport</div>';
        var isi5 =
            '<div style="float:left;"  >Name Seaport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pelabuhan = new PopupTemplate({
            title: title5,
            description: isi5
        });
        var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pelabuhan
            });
        map.addLayer(pelabuhan);
      
        var title7 = '<div align="center">Infrastruktur Hospital</div>';
        var isi7 =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title7,
            description: isi7
        });
        var rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(rumah_sakit);

        var title8 = '<div align="center">Infrastruktur Eduacational</div>';
        var isi8 =
            '<div style="float:left;"  >Name Educational :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pendidikan = new PopupTemplate({
            title: title8,
            description: isi8
        });
        var pendidikan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pendidikan_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_pendidikan
            });
        map.addLayer(pendidikan);
        var title9 = '<div align="center">Infrastruktur Hotel</div>';
        var isi9 =
            '<div style="float:left;"  >Name Hotel :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_hotel = new PopupTemplate({
            title: title9,
            description: isi9
        });
        var hotel = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/hotel_en/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_hotel
            });
        map.addLayer(hotel);

        // Akhir layer infrastruktur

        rumah_sakit.hide();
        pendidikan.hide();
        hotel.hide();
        pelabuhan.hide();
        bandara.hide();
  $('input:checkbox[name=bandara]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      bandara.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    bandara.hide();
                  }

         });
         $('input:checkbox[name=rumah_sakit]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rumah_sakit.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rumah_sakit.hide();
                  }

         });
         $('input:checkbox[name=pendidikan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pendidikan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pendidikan.hide();
                  }

         });
         $('input:checkbox[name=pelabuhan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pelabuhan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pelabuhan.hide();
                  }

         });
         $('input:checkbox[name=hotel]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     hotel.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                   hotel.hide();
                  }

         });

                    // Akhir layer infrastruktur

  

    });

  
    
  </script> 

        @endsection
