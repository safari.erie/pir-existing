@extends('frontend.layoutmap')

@section('content')
<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<script src="https://js.arcgis.com/3.34/"></script>
 <script>
        const postDetails = document.querySelector(".longz");
        const postSidebar = document.querySelector(".when");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 400
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>

<script>

        $.get("{{Config::get('app.url')}}/chart/{{$pages}}/{{Request::Segment(2)}}/{{Request::Segment(3)}}", function (data) {
          
            $("#mydatas").append(data);




        });
            </script>
<script>
    var map;
    require([
        "esri/map",
        "esri/dijit/PopupTemplate",
        "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
        "esri/InfoTemplate",
        "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
        "dojo/parser",

        "dijit/layout/BorderContainer",
        "dijit/layout/ContentPane",
        "dijit/TitlePane",
        "dojo/domReady!"
    ], function (
        Map,
        PopupTemplate,
        FeatureLayer,
        arrayUtils,
        ArcGISDynamicMapServiceLayer,
        Geometry,
        Point,
        webMercatorUtils,
        Graphic,
        SimpleMarkerSymbol,
        SimpleLineSymbol,
        SimpleFillSymbol,
        PictureMarkerSymbol,
        Color,
        InfoTemplate,
        BasemapGallery,
        arcgisUtils,
        parser
    ) {
        parser.parse();
  @foreach($query2 as $list)
        var map = new Map("map", {
            basemap: "hybrid",
            center: [{{$list->y}}, {{$list->x}}],
            zoom: 15
        });
  @endforeach

   
        //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
        var basemapGallery = new BasemapGallery({
            showArcGISBasemaps: true,
            map: map
        }, "basemapGallery");
        basemapGallery.startup();

        basemapGallery.on("error", function (msg) {
            console.log("basemap gallery error:  ", msg);
        });



        @foreach($query as $list)
    var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='Province : {{$list->nama_prov}}<br>>Province : {{$list->nama}}<br>Telepon : {{$list->telp}} <br> Alamat : {{$list->alamat}}'
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">{{$list->jenis}}</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/OCHA/Infrastructure/infrastructure_hotel_bluebox.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer1 = new esri.layers.GraphicsLayer();
                  layer1.add(graphic);
                  map.addLayer(layer1);

                  
          

@endforeach

    });
</script>


    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

<div class="row">
  <div class="col-sm-3"> <section id="tbody" class=""  style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

     <!--ITEMS LISTING
            =========================================================================================================-->

    

     <div id="mydatas"></div>

  
  
     <!--end container-->
 </section></div>
  <div class="col-sm-9">  <section id="tbody" class="ts-box p-1" style="margin-top:10px; margin-left:10px; margin-right:10px;">
  

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="clearfix zink hijau">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 Kontak dan Lokasi
             </a>
         </div>

    


     </div>
     <br>
     <body class="claro">
     <div data-dojo-type="dijit/layout/BorderContainer" 
            data-dojo-props="design:'headline', gutters:false" 
            style="width:100%;height:400px;margin:0;">
            <div class="" id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:2px;">
            
                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                    <div data-dojo-type="dijit/TitlePane" 
                         data-dojo-props="title:'Switch Basemap', open:true">
                      <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                        <div id="basemapGallery"></div>
                    </div>
                </div>
              </div>
            </div>
        </div>
        </body>
     <br>
  @foreach($query as $list)
     <div style="text-align:justify; bottom:30px;" class="tulisan"><dt style="width:auto; margin-top:20px;"class="zink">{{$list->nama_prov}}</dt>
     <dd style="margin-top:10px; margin-left:10px; text-align:justify;" class="border-top">
   
     Nama : {{$list->nama_prov}}<br><br>
  Alamat : {{$list->alamat}}<br><br>
  Telepon : {{$list->telp}}<br><br>
  Email : {{$list->website}}<br><br>

</div>

<div >
@endforeach

   



     <!--end container-->
 </section>

</div>
</div>



 
    @endsection