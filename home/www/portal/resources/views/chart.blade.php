        <!--ITEMS LISTING
            =========================================================================================================-->
<div class="wings when">

                    <!--Display selector on the left-->
                    <div align="center" class="">

                        <a style="color:white;"id="ts-display-list" class="btn titlez">
                            @lang('bahasa.Profildaerah')<br>
  				@foreach($bentuk as $list)
                             <div align="center">{{$list->nama}}</div>
                             @endforeach

                      

                        </a>
                        <br>
                       @foreach($bentuk as $list)
                            <div align="center" style=" font-size:14px" class=" ">
                            
                                           <img src="http://admin.regionalinvestment.bkpm.go.id/attachment/{{$list->id_daerah}}/attachment/{{$list->logo}}"
                                    height="110px" width="110px">
                              
                            </div>
                            @endforeach
                                                      @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/id" 
                     @if($pages == 'profil')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-home fa-fw mr-3"></span>
                                <span class="menu-collapsed">@lang('bahasa.profile') </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/invest/detail/{{$list->id_daerah}}/id" 
                     @if($pages == 'invest')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-money fa-fw mr-3"></span>
                                <span class="menu-collapsed">@lang('bahasa.investreali')</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                          @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/potensial/detail/{{$list->id_daerah}}/id" 
                     @if($pages == 'potensial')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-bar-chart fa-fw mr-3"></span>
                                <span class="menu-collapsed">@lang('bahasa.RegionalPotential') </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                        @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/oppo/detail/{{$list->id_daerah}}/id" 
                     @if($pages == 'oppo')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-dollar fa-fw mr-3"></span>
                                <span >@lang('bahasa.investoppo')</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/industri/detail/{{$list->id_daerah}}/id"    @if($pages == 'industri')
                        style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                          @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-industry fa-fw mr-3"></span>
                                <span class="menu-collapsed">@lang('bahasa.indusestate') </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                        @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/infra/detail/{{$list->id_daerah}}/id"    @if($pages == 'infra')
                        style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                          @endforeach  <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-road fa-fw mr-3"></span>
                                <span class="menu-collapsed">@lang('bahasa.infra') </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/contactprovinsi/{{$list->id_daerah}}/id"    @if($pages == 'kontak')
                        style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                          @endforeach 
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-id-card-o fa-fw mr-3"></span>
                                <span class="menu-collapsed">@lang('bahasa.contact') </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>

                        </a>
<br>
@if( Request::segment(3) < 100)

          <div class="d-flex w-100 justify-content-start align-items-center">
                      <select style="color:rgba(49, 18, 18, 0.795)" class="custom-select titlez" id="kota" name="status" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
<option value="">Kabupaten/Kota</option>

                                {{-- Memanggil Kota --}}
                                @foreach($kota as $list)
                              
                                @if($list->nama == "sasadsd")

                                <div align="center" style="margin-top:5px; margin-left:20px; margin-right:20px;"
                                    class="">

                                    @else
                    
                                <option value="{{Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}/id">{{$list->bentuk_daerah}} {{$list->nama}}</option>

                                        <br>
                                        @endif
                                    </div>
                                    @endforeach
                                    </select>
                            </div>
@else 
  @foreach($bentuk as $list)
                        <a href="{{Config::get('app.url')}}/profil/detail/{{$list->id_parent}}/id"  style="border-bottom-left-radius: 20px; width:180px; background-color:grey; color:white;"  class=" list-group-item-action list-group-item   flex-column align-items-start titlez">
                        
                          @endforeach  <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-arrow-circle-left fa-fw mr-3"></span>
                                <span class="menu-collapsed">Kembali  </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

@endif
                      





<br>
                    </div>

                    <!--Display selector on the right-->


                </div>
       