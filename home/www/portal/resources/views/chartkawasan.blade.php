  
<div id="chart-container">FusionCharts XT will load here!</div>
    <script type="text/javascript">
		FusionCharts.ready(function(){
			var chartObj = new FusionCharts({
    type: 'pie2d',
    renderAt: 'chart-container',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption":"Status Block Site Plan",
            @foreach($kawasan as $list)
            @if(Request::segment(2) == 'indonesia')
            "subCaption": "Total in Indonesia",
            @elseif(Request::segment(2) == 'province')


	 "subCaption": "Total in Province",
  	@else
            "subCaption": "Total in {{$list->kawasan}}",
          
            @endif
            @endforeach
            "numberPrefix": "",
         
            "showPercentInTooltip": "0",
            "decimals": "1",
            "useDataPlotColorForLabels": "1",
            //Theme
            "theme": "fusion"
        },
        "data": [@foreach($kawasan as $list){
            "label": "Total Sold",
		"color": "cc0cb2",
            "value": "{{$list->total_sold}}"
           
        },
        {
            "label": "Total future development",
            "value": "{{$list->total_future_development}}"
        
    },   
    {
            "label": "Total Not for sale",
  		 "color": "ba9d0b",
            "value": "{{$list->total_not_for_sale}}"
        
    },
    {
            "label": "Total Available",
		"color": "48f542",
            "value": "{{$list->total_avail}}"
        
    },
    
     @endforeach
        ]
    }
}
);
			chartObj.render();
		});
	</script>