@extends('frontend.layoutmap')

@section('content')




<link rel="stylesheet" href="https://www.cssscript.com/demo/pretty-checkbox-replacement-css/beautiful-checkbox.css" />

<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
<script src="https://torfsen.github.io/leaflet.zoomhome/dist/leaflet.zoomhome.min.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<!-- Load Esri Leaflet from CDN -->
<script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
    crossorigin=""></script>

<style>
    .wew {
        background-color: rgb(68, 38, 201);
        margin-top: 3px;
    }

 

    .leaflet-top {
    top: 40px;
}

    .checked {
        display: flex;
        color: orange;
    }
</style>

<body class="demo">

    <div id="loader-wrapper" style="background:#fffff;">
        <div id="loader"></div>


        <div style="background:#fffff;" class="loader-section section-left"></div>
        <div style="background:#fffff;" class="loader-section section-right"></div>

    </div>
    <main id="ts-main">
        <br>
        <div class="row">
            <div class="col-sm-3">
                <section>




                    <section style="background-color:#0517bbc5; border-radius: 10px"class=" pencarian zink">
                        <div class="">

                            <!--Display selector on the left-->
                            <div class="zink " align="center">

                                <i class="fa fa-th-large"> Advanced
                                    Search</i>


                            </div>
                        </div>
                        <br>

                        <div class="">

                            <!--Keyword-->


                            <div style="bottom:40px;" align="center">Pencarian</div>
                            <input class="custom-select title" type="text" style="height:38px" id="keywords"
                                placeholder=" Pencarian" name="keywords">

                            <div style="bottom:40px;" align="center">Provinsi</div>

                            <select class="custom-select" id="provinsi" name="type">
                                <option value="">Indonesia</option>
                                @foreach($propertiesx as $list)

                                <option value="{{$list->id_daerah}}">{{$list->nama}}</option>


                                @endforeach
                            </select>

                            <div align="center">Kabupaten/kota</div>
                            <select class="custom-select" id="kota" name="status">
                                <option value="">Semua</option>

                            </select>



                           <br>
                           <h5>Filter by Sector</h5>
                            <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="industri" name="industri" value="Industri" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a>&nbsp;Industri</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="infra" name="infra" value="Infrastruktur"  checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a>&nbsp;Infrastruktur</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="jasa" name="jasa" value="Jasa" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a>&nbsp;Jasa</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" id="pangan" name="pangan" value="Pangan dan Pertanian" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a>&nbsp;Pangan dan Pertanian</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox" checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a>&nbsp;Pariwisata</a>
					</div>
				</div>	
                <div class="checkbox rounded-10 tiny m-b-2">
					<div class="checkbox-overlay">
						<input type="checkbox"  checked="checked" />
						<div class="checkbox-container">
							<div class="checkbox-checkmark"></div>
						</div>
						<a>&nbsp;Kawasan Industri / KEK</a>
					</div>
				</div>	
                             
                              
                           
           
             

                            <!--Row - Min price & Max price-->
                            <div class="wrapper" style="padding:10px;">
                                <div class="range-slider">
                                    <input type="text" class="js-range-slider" value="" />
                                </div>
                                <input type="hidden" id="min" class="js-input-from form-control" value="0" />
                                <input type="hidden" id="max" class="js-input-to form-control" value="0" />




                                <!--Submit button-->
                                <div class="form-group my-2">
                                    <div type="submit" class="btn  hijau" id="gelo">Search
                                    </div>
                                </div>



                        </form>

                    </section>
                    </aside>
            </div>

            <div class="col-sm-9">
                <section style="height:440px; width:100%;"  class=" ts-box p-1">


                    <div class="">



    
                        <section  class="mb-0 flex-wrap">
                            <div id="mydatas" class="ts-map w-100 ts-min-h__50 vh ts-z-index__1">

                            </div>
                            <div id="layer" style="right:40px; top:0px; width:285px" class="ts-form__map-search ts-z-index__2">
                            <div class="zink ts-form-collapse "
                            id="collapseExample">
                            <div style="float:left;"><i class="fa fa-road aria-hidden=" true"></i>
                                Boundaries</div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<input
                                align="center" style="left:20px;" type="checkbox" name="my-checkbox"
                                id="osmCheck" checked data-size="mini">

                        </div>
                        <div class="zink ts-form-collapse "
                            id="collapseExample">
                            <div style="float:left;"><i class="fa fa-plane" aria-hidden="true"></i>
                                International Port</div>
                            &nbsp;&nbsp;<input data-checked="true" type="checkbox" name="dadan"
                                id="osmCheck" data-size="mini">

                        </div>
                    </div>

                </div>
                <!--end ts-form-collapse-->
                            <div id="layerclick" >


                                <!--Form-->
                               
                                    <div  style="width:35px;  right:14px; top:15px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                                        <div  class="">

                                            <i class="fa fa-map-o" aria-hidden="true"></i>

                                        </div>
                                    </div>
                              

                                </div>
                            <!--end ts-form-->
                            <div style="  right:-60px; bottom:0px;" class="ts-form__map-search ts-z-index__2">
                                <img src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg">
                            </div>



                    


                                <!--Form-->
                          
                                <div id="legendclick" >


                                    <!--Form-->
                                   
                                        <div  style="width:35px;  left:14px; bottom:15px" id="legendlayeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                                            <div  class="">
    
                                                <i class="fa fa-th-list" aria-hidden="true"></i>
    
                                            </div>
                                        </div>
                                  
    
                                    </div>
                                    
                                    <div id="legendlayer"  style="width:179px;  left:50px; bottom:15px" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                                 
                                        <div id ="LegendIndustri" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png" width="23px;">&nbsp;Industri</div>
                                        <div id ="LegendInfrastruktur" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png" width="23px;">&nbsp;Infrastruktur</div>
                                        <div id ="LegendPangan" style="margin-top:5px;"><img src="https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png" width="23px;">&nbsp;Pangan & Pertanian</div>
                                        <div id ="LegendJasa" style="margin-top:5px;"><img src="http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png" width="23px;">&nbsp;Jasa</div>

                                    </div>
                                
                                 


                        
                            <!--end ts-form-collapse-->

                   


                </section>
                </section>
                





                <br>







                <!--end form-row-->



                <!-- filter-horizontal -->



                <div class="post-details">
                    <section style="bottom:10px;" id="tbody" class="ts-box hasil">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                    <a style="color:white; href="#" id="ts-display-grid" "
                                                class=" btn active ">
                                                <i class=" fa fa-th-large"></i>
                                    </a>
                                    <a style="color:white; href="#" id="ts-display-list" class="btn">
                                        <i class="fa fa-th-list"></i>
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                                <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                                    <label for="sorting" style="color:white; "
                                        style="background-color:rgb(68, 38, 201);" class="mb-0 mr-2 text-nowrap">Sort
                                        by:</label>
                                    <select class="zink" id="sorting" name="sorting">
                                        <option style="color:white;" value="">
                                            <p style="color:white;">Default</p>
                                        </option>
                                        <option style="color:white;" value="1">Price lowest first</option>
                                        <option style="color:white;" value="2">Price highest first</option>
                                        <option style="color:white;" value="3">Distance</option>
                                    </select>
                                </div>

                            </div>
                            <br>
                            <div id="daerah">

                             
<section  >

   

    <div class="container">
   
                                       <!--Featured Items-->
                                       <div  class="row">
   
                                           @foreach ($propertiesz as $list)
                                           
                               <?php if($list->sektor == 'Infrastruktur' ) { ?>
                                           <!--Item-->
                                           <!--Item-->
                                      
                                           <div  class="col-sm-8 col-lg-4">
                                               <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card ">
   
                                                   <!--Ribbon-->
                                                   <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
                                                    <div class="ts-ribbon-corner">
                                                      <span>Prioritas</span>
                                                             </div>
                                                      
                                                          <?php }; ?>
       
   
                                                   <!--Card Image-->
                                                   <a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . clean($list->id_parent)); ?>"
                                                       class="card-img ts-item__image"
                                                       data-bg-image="https://1.bp.blogspot.com/-N31gDhrSYZ0/Xggmzk8qkNI/AAAAAAAABLc/ERh9sQlbewkRVOOazZswKEZXbUdT_GHwQCLcBGAsYHQ/s1600/Infrastruktur.jpg">
                                                       
                                                       <figure class="ts-item__info">
                                                           <h6>{{$list->judul}}</h6>
                                                         
                                                       </figure>
                                                       <div style="background-color:#e70d0de1; border-radius: 10px; color:white;" class="ts-item__info-badge">Infrastruktur
   </div>
                                                   </a>
   
   
                                                   <div class="card-body ts-item__body">
                                                    <div class="atbd_listing_meta">
                                                        <span style="background-color:#0517bbc5; border-radius: 10px"  class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000) Juta
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000) Milyar
                             
                                                             <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                            @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                             <?php } ?></span>
                                                        <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                        <div class="atbd_listing_data_list">
                                                            <ul>
                                                                <li>
                                                                    <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                                </li>
                                                                <li>
                                                                
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                   </div>
   
                                                   <div class="atbd_listing_bottom_content">
                                                    <div class="atbd_content_left">
                                                        <div class="atbd_listing_category">
                                                            <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
                                                        </div>
                                                    </div>
                                                    <ul class="atbd_content_right">
                                                        <li class="atbd_count"></span></li>
                                                        <li class="atbd_save">
                                                          
                                                        </li>
                                                    </ul>

                                                </div>
   
                                               </div>
                                               <!--end ts-item ts-card-->
                                           </div>
   
                                      
                                         
                                        <a>
   
                                            <?php } else if($list->sektor == 'Jasa') { ?>
                                           <!--Item-->
                                           <!--Item-->
                                           <div id="jasaz" class="col-sm-8 col-lg-4">
                                               <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">
   
                                                   <!--Ribbon-->
                                                   <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
                                                    <div class="ts-ribbon-corner">
                                                      <span>Prioritas</span>
                                                             </div>
                                                      
                                                          <?php }; ?>
       
   
                                                   <!--Card Image-->
                                                   <a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . clean($list->id_parent)); ?>"
                                                       class="card-img ts-item__image"
                                                       data-bg-image="https://i1.wp.com/www.maxmanroe.com/vid/wp-content/uploads/2019/11/Pengertian-Jasa-Adalah.jpg?fit=700%2C387&ssl=1">
                                                       <figure class="ts-item__info">
                                                        <h6>{{$list->judul}}</h6>
                                                      
                                                    </figure>
                                                    <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Jasa
</div>
                                                </a>


                                                <div class="card-body ts-item__body">
                                                 <div class="atbd_listing_meta">
                                                     <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000) Juta
                                                          <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000000) Milyar
                          
                                                          <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                          <?php } ?></span>
                                                     <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                     <div class="atbd_listing_data_list">
                                                         <ul>
                                                             <li>
                                                                 <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                             </li>
                                                             <li>
                                                             
                                                             </li>
                                                         </ul>
                                                     </div>
                                                 </div>
                                                </div>

                                                <div class="atbd_listing_bottom_content">
                                                 <div class="atbd_content_left">
                                                     <div class="atbd_listing_category">
                                                        <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
                                                     </div>
                                                 </div>
                                                 <ul class="atbd_content_right">
                                                     <li class="atbd_count"></span></li>
                                                     <li class="atbd_save">
                                                       
                                                     </li>
                                                 </ul>

                                             </div>

                                            </div>
                                            <!--end ts-item ts-card-->
                                        </div>

                                   
   
                                            <?php } else if($list->sektor == 'Pangan dan Pertanian') { ?>
                                           <!--Item-->
                                           <!--Item-->
                                           <div id="jasaz" class="col-sm-8 col-lg-4">
                                               <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">
   
                                                   <!--Ribbon-->
                                                   <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
                                                    <div class="ts-ribbon-corner">
                                                      <span>Prioritas</span>
                                                             </div>
                                                      
                                                          <?php }; ?>
       
   
                                                   <!--Card Image-->
                                                   <a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . clean($list->id_parent)); ?>"
                                                       class="card-img ts-item__image"
                                                       data-bg-image="https://img2.pngdownload.id/20180326/bqe/kisspng-kentucky-agriculture-natural-resource-agricultural-agriculture-5ab96fc040fc11.1686302015221022082662.jpg">
                                                       <figure class="ts-item__info">
                                                        <h6>{{$list->judul}}</h6>
                                                      
                                                    </figure>
                                                    <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Pangan & Pertanian
</div>
                                                </a>


                                                <div class="card-body ts-item__body">
                                                 <div class="atbd_listing_meta">
                                                     <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000) Juta
                                                          <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000000) Milyar
                          
                                                          <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                          <?php } ?></span>
                                                     <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                     <div class="atbd_listing_data_list">
                                                         <ul>
                                                             <li>
                                                                 <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                             </li>
                                                             <li>
                                                             
                                                             </li>
                                                         </ul>
                                                     </div>
                                                 </div>
                                                </div>

                                                <div class="atbd_listing_bottom_content">
                                                 <div class="atbd_content_left">
                                                     <div class="atbd_listing_category">
                                                        <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
                                                     </div>
                                                 </div>
                                                 <ul class="atbd_content_right">
                                                     <li class="atbd_count"></span></li>
                                                     <li class="atbd_save">
                                                       
                                                     </li>
                                                 </ul>

                                             </div>

                                            </div>
                                            <!--end ts-item ts-card-->
                                        </div>

                                   
                                       
                                            <?php } else if($list->sektor == 'Industri')  { ?>
                                              
                                           <!--Item-->
                                           <!--Item-->
                                           <div id="jasaz" class="col-sm-8 col-lg-4">
                                               <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">
   
                                                <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
                                             <div class="ts-ribbon-corner">
                                               <span>Prioritas</span>
                                                      </div>
                                               
                                                   <?php }; ?>

   
                                                   <!--Card Image-->
                                                   <a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . clean($list->id_parent)); ?>"
                                                       class="card-img ts-item__image"
                                                       data-bg-image="https://eljohnnews.com/wp-content/uploads/2019/01/agap2_technologies-5-1920-1080.jpg">
                                                       <figure class="ts-item__info">
                                                        <h6>{{$list->judul}}</h6>
                                                      
                                                    </figure>
                                                    <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Industri
</div>
                                                </a>


                                                <div class="card-body ts-item__body">
                                                 <div class="atbd_listing_meta">
                                                     <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000) Juta
                                                          <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000000) Milyar
                          
                                                          <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                         @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                          <?php } ?></span>
                                                     <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                     <div class="atbd_listing_data_list">
                                                         <ul>
                                                             <li>
                                                                 <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                             </li>
                                                             <li>
                                                             
                                                             </li>
                                                         </ul>
                                                     </div>
                                                 </div>
                                                </div>

                                                <div class="atbd_listing_bottom_content">
                                                 <div class="atbd_content_left">
                                                     <div class="atbd_listing_category">
                                                        <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
                                                     </div>
                                                 </div>
                                                 <ul class="atbd_content_right">
                                                     <li class="atbd_count"></span></li>
                                                     <li class="atbd_save">
                                                       
                                                     </li>
                                                 </ul>

                                             </div>

                                            </div>
                                            <!--end ts-item ts-card-->
                                        </div>

                                   
                                          <?php }?>
   
                                          
                                           <!--end Item col-md-4-->
                                       
   
                                           @endforeach
   
                                       </div>
                                       <!--Item-->
                                     
                               </section>
   
                            </div>

    </main>


    </div>
    </div>



    <script src="assets/js/custom.js"></script>
    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
    <script src="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/js/ion.rangeSlider.min.js"></script>
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
    <script>
        var $range = $(".js-range-slider"),
            $inputFrom = $(".js-input-from"),
            $inputTo = $(".js-input-to"),
            instance,
            min = 1,
            max = 100,
            from = 0,
            to = 0;

        $range.ionRangeSlider({
            skin: "round",
            type: "double",
            min: min,
            max: max,
            from: 1,
            to: 100,
            prefix: "Rp.",
            min_postfix: "--",
            postfix: " M",
            max_postfix: "++",
            onStart: updateInputs,
            onChange: updateInputs
        });
        instance = $range.data("ionRangeSlider");

        function updateInputs(data) {

            if (data.from < 2) {
                from = data.from * 1000000;
            } else {
                from = data.from * 1000000000;
            }
            if (data.to > 99) {
                to = data.to * 10000000000000000;
            } else {
                to = data.to * 10000000000000000;
            }
            $inputFrom.prop("value", from);
            $inputTo.prop("value", to);
        }

        $inputFrom.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < min) {
                val = min;
            } else if (val > to) {
                val = to;
            }

            instance.update({
                from: val
            });
        });

        $inputTo.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < from) {
                val = from;
            } else if (val > max) {
                val = max;
            }

            instance.update({
                to: val
            });
        });
    </script>

    <script>
    
        $(document).ready(function () {
         
            // LegendLayer
            $('#industri').change(function(){
        if(this.checked)
            $('#LegendIndustri').show();
        else
            $('#LegendIndustri').hide();

    });
    $('#infra').change(function(){
        if(this.checked)
            $('#LegendInfrastruktur').show();
        else
            $('#LegendInfrastruktur').hide();

    });
            
    $('#pangan').change(function(){
        if(this.checked)
            $('#LegendPangan').show();
        else
            $('#LegendPangan').hide();

    });
            
    $('#jasa').change(function(){
        if(this.checked)
            $('#LegendJasa').show();
        else
            $('#LegendJasa').hide();

    });
            
            
            $("#layerclick").click(function(){
            
                $('#layer').toggle();
                $('#layeron').toggleClass("hijau");

            
});

$("#legendclick").click(function(){
            
            $('#legendlayer').toggle();
            $('#legendlayeron').toggleClass("hijau");

        
});
            $('#layer').hide();
            $('#legendlayer').hide();
            $('#provinsi').change(function (e) {
                $.ajax({
                    url: "{{ url('/getkota') }}/" + $(this).val(),
                    method: 'GET',
                    success: function (data) {
                        console.log(data);

                        $('#kota').children('option:not(:first)').remove().end();

                        $.each(data, function (index, kotaObj) {

                            $('#kota').append('<option value="' + kotaObj
                                .id_daerah + '">  ' + kotaObj.bentuk_daerah +
                                ' ' + kotaObj.nama + ' </option>')
                        });
                    }
                });
            });

            $('.aku').on('click', function () {
                $('.collapse').collapse();
            })
            $.get("{{ url('/cari?keywords=') }}", function (data) {
                $("#mydata").html(data);

            });
            $.get("{{ url('/map-home?keywords=') }}", function (data) {
                $("#mydatas").html(data);
               
          
            });

            $.get("{{ url('/daerah?industri=Industri') }}", function (data) {
                

               
                const postDetails = document.querySelector(".hasil");

                const postSidebar = document.querySelector(".pencarian");
                const postDetailsz = document.querySelector(".ts-boxzzs");
                const postSidebarContent = document.querySelector(".ts-bosxzz > div");


                const controller = new ScrollMagic.Controller();


                const scene = new ScrollMagic.Scene({
                    triggerElement: postSidebar,
                    triggerHook: 0,
                    duration: getDuration
                }).addTo(controller);

                //3
                if (window.matchMedia("(min-width: 768px)").matches) {
                    scene.setPin(postSidebar, {
                        pushFollowers: false
                    });
                }

                //4
                window.addEventListener("resize", () => {
                    if (window.matchMedia("(min-width: 768px)").matches) {
                        scene.setPin(postSidebar, {
                            pushFollowers: false
                        });
                    } else {
                        scene.removePin(postSidebar, true);
                    }
                });

                function getDuration() {




                    return (postDetails.offsetHeight - postSidebar.offsetHeight) + 500;

                }
            });

         

            setTimeout(function () {
                $('body').addClass('loaded');
                $('h1').css('color', '#FFFFF');
            }, 2000);
            $("#gelo").click(function () {
                $("#loader-wrapper").show();
                var industri = [];

                $("input:checkbox[name=industri]:checked").each(function () {
                    industri.push($(this).val());
                });
                var infra = [];

                $("input:checkbox[name=infra]:checked").each(function () {
                    infra.push($(this).val());
                });
                var pangan = [];

                $("input:checkbox[name=pangan]:checked").each(function () {
                    pangan.push($(this).val());
                });
                var jasa = [];

                $("input:checkbox[name=jasa]:checked").each(function () {
                    jasa.push($(this).val());
                });
                var str = $("#keywords").val();
                var min = $("#min").val();
                var max = $("#max").val();
                var prov = $("#provinsi").val();
                var kota = $("#kota").val();



                alert("{{ url('/cari?keywords=') }}" + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
                    "&pangan=" + pangan + "&jasa=" + jasa + ""
                );
                $.get("{{ url('/cari?keywords=') }}" + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "",
                    function (data) {
                        $("#mydata").html(data);

                    });
                $.get("{{ url('/map-home?keywords=') }}" + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
                    "&pangan=" + pangan + "&jasa=" + jasa + "",
                    
                    function (data) {
                        $("#mydatas").html(data);
                        if ($("#industri").is(":checked")) {
                            map.addLayer(indus);
                        } else {
                            indus.remove();
                        }
                           if ($("#infra").is(":checked")) {
                            map.addLayer(cities);
                        } else {
                             cities.remove();
                        }
                           if ($("#pangan").is(":checked")) {
                            map.addLayer(pangans);
                        } else {
                            pangans.remove();
                        }
                          if ($("#jasa").is(":checked")) {
                            map.addLayer(jasas);
                        } else {
                            jasas.remove();
                        }
                        
                    });
                $.get("{{ url('/daerah?keywords=') }}"  + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +
                    "&pangan=" + pangan + "&jasa=" + jasa + "",
                    function (data) {
                        $("#daerah").html(data);

                     if ($("#jasa").is(":checked")) {
                         

 $.get("{{ url('/daerah?keywords=') }}"  + str + "&category=" + kota + "&type=" + prov +
                    "&min=" + min + "&max=" + max + "&industri=" + industri + "&infra=" + infra +"&pangan=" + pangan + "&jasa=" + jasa + "");
                             
                        } 
                    });

            });



        });
        $("[name='my-checkbox']").bootstrapSwitch({

            onSwitchChange: function (event, state) {

                if (state) map.addLayer(wildfireRisk);
                else map.removeLayer(wildfireRisk);
            }
        });

        $("[name='industsdri']").bootstrapSwitch({

            onSwitchChange: function (event, state) {

           
            }
        });


        $("[name='dadan']").bootstrapSwitch({
            onSwitchChange: function (event, state) {

                if (state) {
                    map.addLayer(bandara);


                } else {
                    map.removeLayer(bandara);
                }
            }
        });


        $("[name='dadan']").click(function (event) {
            event.preventDefault();
            if (map.hasLayer(sights)) {
                $(this).removeClass('selected');
                map.removeLayer(bandara);
            } else {
                map.addLayer(bandara);
                $(this).addClass('selected');
            }
        });
    </script>
    <script>


    </script>




    @endsection