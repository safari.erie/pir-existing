    @extends('frontend.layoutmap')

    @section('content')
<html>
   
    <style type="text/css">
        g[class^='raphael-group-'][class$='-creditgroup'] {
            display: none !important;
        }
    </style>
    <style type="text/css">
        g[class$='creditgroup'] {
            display: none !important;
        }
    </style>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
   <script>
     $(document).ready(function () {

        $('#sectorz2').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartkomoditidaerah/{{ Request::segment(3) }}/"+tahun+"/{{ Request::segment(4) }}", function (data) {
            $("#sector2").html(data);
           
          
        });
});

        $('#sectorz').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartsectordaerah/{{ Request::segment(3) }}/"+tahun+"/{{ Request::segment(4) }}", function (data) {
            $("#sector").html(data);
           
          
        });
});
$('#sectorz4').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartpdrbdaerah/{{ Request::segment(3) }}/"+tahun+"", function (data) {
            $("#sector4").html(data);
           
          
        });
});
           
  
  $("#layerclick").click(function(){
         
         $('#layerhidden').toggle();
         $('#layeron').toggleClass("hijau");

     
});
$("#baseclick").click(function(){
         
         $('#base').toggle();
         $('#baseon').toggleClass("hijau");

     
});
 $('#layerhidden').hide();
 $('#base').hide();

});
   
     
     </script>
 <script>
    $(document).ready(function () {
        
        $.get("{{ Config::get('app.url') }}/chart/{{$pages}}/{{ Request::segment(3) }}/{{ Request::segment(4) }}", function (data) {
           
            $("#mydatas").append(data);




        });

    });
</script>

<body>
   




<script src="https://js.arcgis.com/3.34/"></script>

<script> 
var map;
require([
  "esri/map",
   "esri/dijit/PopupTemplate",
     "esri/layers/FeatureLayer",
    "dojo/_base/array",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/geometry/Geometry",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
"esri/config",
"esri/urlUtils",

    "esri/InfoTemplate",
  "esri/tasks/query",
        "esri/tasks/QueryTask",
   "esri/dijit/BasemapGallery",
    "esri/arcgis/utils",
  "dojo/parser",

  "dijit/layout/BorderContainer", 
  "dijit/layout/ContentPane", 
  "dijit/TitlePane",
  "dojo/domReady!"
], function(
  Map,
        PopupTemplate, 
     FeatureLayer, 
     arrayUtils, 
     ArcGISDynamicMapServiceLayer, 
     Geometry, 
     Point, 
     webMercatorUtils,  
     Graphic, 
     SimpleMarkerSymbol, 
     SimpleLineSymbol, 
     SimpleFillSymbol,
     PictureMarkerSymbol,  
     Color, 
esriConfig,
urlUtils,

     InfoTemplate,
    Query, 
QueryTask, 
BasemapGallery,
    arcgisUtils,
  parser
) {
  parser.parse();

 
@foreach($propertiesx as $list){
map = new Map("map", {
  basemap: "hybrid",
  center: [{{$list->y}}, {{$list->x}}],
@if(Request::segment(3) < 100)
  zoom: 6
@elseif(Request::segment(3) > 100)
zoom: 10
@endif
});
}
@endforeach
urlUtils.addProxyRule({
    urlPrefix: "https://Gistaru.atrbpn.go.id/",
    proxyUrl: "https://Gistaru.atrbpn.go.id/rtronline/proxy/proxy.ashx"
  });

  //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
  var basemapGallery = new BasemapGallery({
    showArcGISBasemaps: true,
    map: map
  }, "basemapGallery");
  basemapGallery.startup();
  
  basemapGallery.on("error", function(msg) {
    console.log("basemap gallery error:  ", msg);
  });

  

var title2 ='<div align="center">Batas Wilayah Kabupaten</div>';
var isi2 ='<div style="float:left;"  >Nama Kabupaten :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
var title4 ='<div align="center">Batas Wilayah Kota</div>';
var isi4 ='<div style="float:left;"  >Nama Kab/kota :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2">{namobj}</dd>'
       var popupprov = new PopupTemplate({
    title: "Batas Wilayah {ta}",

    fieldInfos: [

      { fieldName: "provinsi", visible: true, label: "Provinsi :", format: { places: 0   } },
     
    ],

   
           });
      

    var popupkota = new PopupTemplate({
        title: title2,
      description: isi2
      
    });

    var popupkabupaten = new PopupTemplate({
    title: "Batas Wilayah {ta_1}",

    fieldInfos: [
 { fieldName: "wa_1", visible: true, label: " Kab/Kota :", format: { places: 0   } },

        { fieldName: "wa", visible: true, label: "Provinsi :", format: { places: 0   } },


    ],

  
           });
      
  

    var title3 ='<div align="center">RTRW Kabupaten</div>';
var isi3 ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popuprtrw =  new PopupTemplate({
    title: "Layer RTRW",

    fieldInfos: [

      { fieldName: "NAMOBJ", visible: true, label: "Name  :", format: { places: 0   } },
      { fieldName: "WADMKK", visible: true, label: "Kab/kota :", format: { places: 0   } },
      { fieldName: "WADMPR", visible: true, label: "Province :", format: { places: 0   } },
      { fieldName: "NOTHPR", visible: true, label: "Rule :", format: { places: 0   } },
      { fieldName: "PP", visible: true, label: "File1:", format: { places: 0   } },
      { fieldName: "BA", visible: true, label: "File2:", format: { places: 0   } },
      { fieldName: "STSDRH ", visible: true, label: "Status Regional:", format: { places: 0   } },
      { fieldName: "STSDAT", visible: true, label: "Data Status:", format: { places: 0   } },
    
    ],

  
           });
      

      

 
  
  

  

 

                @foreach($rtrw as $list)
                  var linkzz='{{$list->rtrw}}'                     
                          @endforeach              
          var rtrwprov = new FeatureLayer(linkzz ,{
          
              outFields: ["*"],
              opacity:0.5,
        infoTemplate: popuprtrw
         });
         map.addLayer(rtrwprov);
         $('input:checkbox[name=rtrw]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rtrwprov.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rtrwprov.hide();
                   
                  }

         });
       
        

    var featureLayerprovinsi = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/Provinsi/MapServer/1",{
      outFields: ["*"],
opacity:0.5,
	 definitionExpression: "id_daerah = '{{ Request::segment(3) }}'",    
infoTemplate: popupprov
 });
 map.addLayer(featureLayerprovinsi);
map.on("extent-change",function(evt)
      {
        console.log("extent change",map.getZoom());
        
        
        if ( map.getZoom() < 9 ) {
            featurekabupaten.show();
            featureLayerprovinsi.show();
            } else if(( map.getZoom() > 9 )) {
                featurekabupaten.show();
                featureLayerprovinsi.hide();
            }
      
        
      } 
         
      
    );


var featurekabupaten = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Latihan/kabupaten/MapServer/0",{
    opacity: 0.5,
 definitionExpression: "idbkpm = '{{request::segment(3)}}'", 
      outFields: ["*"],
      
infoTemplate: popupkabupaten
 });
map.addLayer(featurekabupaten);

                    // Layer Infrastruktur
                    var title4 = '<div align="center">Infrastruktur Airport</div>';
        var isi4 =
            '<div style="float:left;"  >Name Airport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_bandara = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var bandara = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/bandara_id/MapServer/0", {
                outFields: ["*"],
@if(Request::segment(3) < 100)
  definitionExpression: "id_parent = '{{ Request::segment(3) }}'", 
@elseif(Request::segment(3) > 100)
definitionExpression: "id_daerah = '{{ Request::segment(3) }}'", 
@endif
                infoTemplate: Popup_bandara
            });
        map.addLayer(bandara);

        var title5 = '<div align="center">Infrastruktur Seaport</div>';
        var isi5 =
            '<div style="float:left;"  >Name Seaport :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pelabuhan = new PopupTemplate({
            title: title5,
            description: isi5
        });
        var pelabuhan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pelabuhan_id2/MapServer/0", {
                outFields: ["*"],
	 @if(Request::segment(3) < 100)
  definitionExpression: "id_parent = '{{ Request::segment(3) }}'", 
@elseif(Request::segment(3) > 100)
definitionExpression: "id_daerah = '{{ Request::segment(3) }}'", 
@endif

                infoTemplate: Popup_pelabuhan
            });
        map.addLayer(pelabuhan);
      
        var title7 = '<div align="center">Infrastruktur Hospital</div>';
        var isi7 =
            '<div style="float:left;"  >Name Hospital :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title7,
            description: isi7
        });
        var rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],
@if(Request::segment(3) < 100)
  definitionExpression: "id_parent = '{{ Request::segment(3) }}'", 
@elseif(Request::segment(3) > 100)
definitionExpression: "id_daerah = '{{ Request::segment(3) }}'", 
@endif

                 infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(rumah_sakit);

        var title8 = '<div align="center">Infrastruktur Eduacational</div>';
        var isi8 =
            '<div style="float:left;"  >Name Educational :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_pendidikan = new PopupTemplate({
            title: title8,
            description: isi8
        });
        var pendidikan = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/pendidikan_id/MapServer/0", {
                outFields: ["*"],
@if(Request::segment(3) < 100)
  definitionExpression: "id_parent = '{{ Request::segment(3) }}'", 
@elseif(Request::segment(3) > 100)
definitionExpression: "id_daerah = '{{ Request::segment(3) }}'", 
@endif

 
                infoTemplate: Popup_pendidikan
            });
        map.addLayer(pendidikan);
        var title9 = '<div align="center">Infrastruktur Hotel</div>';
        var isi9 =
            '<div style="float:left;"  >Name Hotel :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Category </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{kategori}</dd><div style="float:left;"  >Address </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> :{alamat}</dd>'
        var Popup_hotel = new PopupTemplate({
            title: title9,
            description: isi9
        });
        var hotel = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/hotel_lok_id/MapServer/0", {
                outFields: ["*"],
   @if(Request::segment(3) < 100)
  definitionExpression: "id_parent = '{{ Request::segment(3) }}'", 
@elseif(Request::segment(3) > 100)
definitionExpression: "id_daerah = '{{ Request::segment(3) }}'", 
@endif

                infoTemplate: Popup_hotel
            });
        map.addLayer(hotel);

        // Akhir layer infrastruktur

        rumah_sakit.hide();
        pendidikan.hide();
        hotel.hide();
        pelabuhan.hide();
        bandara.hide();
        $('input:checkbox[name=bandara]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      bandara.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    bandara.hide();
                  }

         });
         $('input:checkbox[name=rumah_sakit]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rumah_sakit.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rumah_sakit.hide();
                  }

         });
         $('input:checkbox[name=pendidikan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pendidikan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pendidikan.hide();
                  }

         });
         $('input:checkbox[name=pelabuhan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     pelabuhan.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    pelabuhan.hide();
                  }

         });
         $('input:checkbox[name=hotel]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                     hotel.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                   hotel.hide();
                  }

         });

                    // Akhir layer infrastruktur


        

});


</script> 

    <script>
        FusionCharts.ready(function () {


            var visitChart = new FusionCharts({
                        type: "line",
                        renderAt: "chart-container3",
                        id: "demoChart",
                        width: "100%",
                        height: "400",
                        dataFormat: "json",
                        dataSource: {
                            "chart": {
                                "theme": "fusion",
                                "caption": "UMR Provinsi",
                                "subcaption": "2016 - 2019",
                                "xaxisname": "Tahun",
                                "yaxisname": "(IDR per Month)",
                                "numberPrefix": "Rp.",
                                "rotateValues": "0",
                                "yAxisMinValue":'2000000',
                                "placeValuesInside": "0",
                                "valueFontColor": "#000000",
                                "valueBgColor": "#FFFFFF",
                                "valueBgAlpha": "50",
                                //Disabling number scale compression
                                "formatNumberScale": "0",
                                //Defining custom decimal separator
                                "decimalSeparator": ",",
                                //Defining custom thousand separator
                                "thousandSeparator": ".",

                                "showValues": "1"
                            },
                            "data": [@foreach($umr as $umr){
                                @if ($umr->tahun == '2016')
                                    "label": "2016",
                                    "value": "{{$umr->umr_2016}}"
                                    @elseif ($umr->tahun == '2017')
                                    "label": "2017",
                                    "value": "{{$umr->umr_2017}}"
                                    @elseif ($umr->tahun == '2018')
                                    "label": "2018",
                                    "value": "{{$umr->umr_2018}}"
                                    @elseif ($umr->tahun == '2019')
                                    "label": "2019",
                                    "value": "{{$umr->umr_2019}}"
                                    @elseif ($umr->tahun == '2020')
                                    "label": "2020",
                                    "value": "{{$umr->umr_2020}}"
                                    @endif
                                }, @endforeach
                            
                            ]
                        }
                    }

                )
                .render();

        
        });
    </script>
    <script>FusionCharts.ready(function() {
var myChart1 = new FusionCharts({
type: "msline",
renderAt: "chart-container4",
width: "100%",
height: "400",
dataFormat: "json",
dataSource: {
                chart: {
                    "theme": "fusion",
                    "caption": "Pertumbuhan Jumlah Penduduk",
                    "subcaption": "2016 - 2019",
                    "yaxisname": "(IDR per Month)",
                    "numberPrefix": "Rp.",
                    "rotateValues": "0",
                    "placeValuesInside": "0",
                    "valueFontColor": "#000000",
                    "valueBgColor": "#FFFFFF",
                    "valueBgAlpha": "50",
   			"useForwardSteps": "0"
                    //Disabling number scale compression
                    "formatNumberScale": "0",
                    //Defining custom decimal separator
                    "decimalSeparator": ",",
                    //Defining custom thousand separator
                    "thousandSeparator": ".",


                },
                categories: [{
                    category: [{
                            label: "2012"
                        },
             
                        {
                            label: "2013"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
         
            "dashed": "1"
        },
                        {
                            label: "2014"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
        
            "dashed": "1"
        },
                        {
                            label: "2015"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
         
            "dashed": "1"
        },
                        {
                            label: "2016"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
       
            "dashed": "1"
        },
                    ]
                }],
                dataset: [{
                        seriesname: "Agriculture",
                        data: [{
                                value: "1800000"
                            },
                            {
                                value: "2200000"
                            },
                            {
                                value: "2400000"
                            },
                            {
                                value: "2600000"
                            },
                            {
                                value: "3400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Animal Husbandry",
                        data: [{
                                value: "3800000"
                            },
                            {
                                value: "1200000"
                            },
                            {
                                value: "2900000"
                            },
                            {
                                value: "3600000"
                            },
                            {
                                value: "3400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Plantation",
                        data: [{
                                value: "1800000"
                            },
                            {
                                value: "1200000"
                            },
                            {
                                value: "1900000"
                            },
                            {
                                value: "1600000"
                            },
                            {
                                value: "2400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Fishery",
                        
                        data: [{
                                value: "1300000"
                            },
                            {
                                value: "1200000"
                            },
                            {
                                value: "1500000"
                            },
                            {
                                value: "1600000"
                            },
                            {
                                value: "2400000"
                            }
                        ]
                    }
                ],
            }
}).render();
});</script>
  
  <script>
    FusionCharts.ready(function() {
var myChart2 = new FusionCharts({
type: "mscombi2d",
renderAt: "chart-container6",
width: "100%",
height: "430",
dataFormat: "json",
dataSource: {
                chart: {
                    "theme": "fusion",
                    "caption": "Pertumbuhan Jumlah Penduduk",
                    "subcaption": "2016-2019",
                    "rotateValues": "0",
                    "placeValuesInside": "0",
                    "valueFontColor": "#000000",
                    "valueBgColor": "#FFFFFF",
                    "decimals": "4",
                    "yAxisNamePadding": "22",
                "numberScaleValue": "60",
                    "yAxisMinValue":'2500000',
                   "useForwardSteps": "0",
                    "numDivLines": "2",
                    "adjustDiv": "222",
                    "valueBgAlpha": "50",
                    //Disabling number scale compression
                    
                    //Defining custom decimal separator
                    "decimalSeparator": ",",
                    //Defining custom thousand separator
                    "thousandSeparator": ".",
"showValues": "0"

                },
                categories: [{
                    category: [@foreach($demografi as $demo){
                            label: "{{$demo->tahun}}"
                        }, @endforeach
             
                     
                    ]
                }],
                dataset: [{
                        seriesname: "@lang('bahasa.wanita')",
                        data: [@foreach($demografi as $demo)
                        
                        { 
                            @if($demo->tahun == 2016)
                                value: "{{$demo->wanita_2016}}"
                                @elseif($demo->tahun == 2016)
                                value: "{{$demo->wanita_2016}}"
                                @elseif($demo->tahun == 2017)
                                value: "{{$demo->wanita_2017}}"
                                @elseif($demo->tahun == 2018)
                                value: "{{$demo->wanita_2018}}"
                                @elseif($demo->tahun == 2019)
                                value: "{{$demo->wanita_2019}}"
                                @elseif($demo->tahun == 2020)
                                value: "{{$demo->wanita_2020}}"
                                @endif
                            },
                             @endforeach
                         
                        ]
                    },
                    {
                        seriesname: "@lang('bahasa.pria')",
                        data: [@foreach($demografi as $demo)
                        
                        { 
                            @if($demo->tahun == 2016)
                                value: "{{$demo->pria_2016}}"
                                @elseif($demo->tahun == 2016)
                                value: "{{$demo->pria_2016}}"
                                @elseif($demo->tahun == 2017)
                                value: "{{$demo->pria_2017}}"
                                @elseif($demo->tahun == 2018)
                                value: "{{$demo->pria_2018}}"
                                @elseif($demo->tahun == 2019)
                                value: "{{$demo->pria_2019}}"
                                @elseif($demo->tahun == 2020)
                                value: "{{$demo->pria_2020}}"
                                @endif
                            },
                             @endforeach
                        ]
                    },
                   {
                        
			showLegend: "0",
			renderAs:"line",
			showvalue:"0",
			showToolTip: "0",
                        data: [@foreach($demografi as $demo)
                        
                        { 
			tooltext: "garis pria",
			color:"29c3be",
			showToolTip: "0",
                            @if($demo->tahun == 2016)
                                value: "{{$demo->pria_2016}}"
                                @elseif($demo->tahun == 2016)
                                value: "{{$demo->pria_2016}}"
                                @elseif($demo->tahun == 2017)
                                value: "{{$demo->pria_2017}}"
                                @elseif($demo->tahun == 2018)
                                value: "{{$demo->pria_2018}}"
                                @elseif($demo->tahun == 2019)
                                value: "{{$demo->pria_2019}}"
                                @elseif($demo->tahun == 2020)
                                value: "{{$demo->pria_2020}}"
                                @endif
                            },
                             @endforeach
                        ]
                    },
                    {
                        
			showLegend: "0",
			renderAs:"line",
			showvalue:"0",
			showToolTip: "0",

                        data: [@foreach($demografi as $demo)
                        
                        { 
			tooltext: "garis wanita",
			color:"5d62b5",
			showToolTip: "0",
                            @if($demo->tahun == 2016)
				
                                    value: "{{$demo->wanita_2016}}"
                                @elseif($demo->tahun == 2016)
                                value: "{{$demo->wanita_2016}}"
                                @elseif($demo->tahun == 2017)
                                value: "{{$demo->wanita_2017}}"
                                @elseif($demo->tahun == 2018)
                                value: "{{$demo->wanita_2018}}"
                                @elseif($demo->tahun == 2019)
                                value: "{{$demo->wanita_2019}}"
                                @elseif($demo->tahun == 2020)
                                value: "{{$demo->wanita_2020}}"
                                @endif
                            },
                             @endforeach
                        ]
                    },
                  




                ],
            }
}).render();
});
  </script>
     <body class="claro">
    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

                <div id="mydatas"></div>

                <!--end container-->
            </section>
        </div>
        <br>
        
        <div class="col-sm-9">
            <section class="ts-box p-1 longz" style="margin-top:10px; margin-left:5px; margin-right:5px;">


                             <div class="row">
                    <div class="col-sm-1">
                                               </div>
                        
                        <div class="col-sm-12">
                            <div style="margin-right:5px; margin-left:5px;">
                                <div class="clearfix  btn-pro titlez">

                                    <!--Display selector on the left-->
                                    <div class="">

                                        <a id="ts-display-list" class="btn">
                                           @lang('bahasa.peta')
                                        </a>
                                    </div>




                                </div>
                                <div data-dojo-type="dijit/layout/BorderContainer" 
                                data-dojo-props="design:'headline', gutters:false" 
                                style="width:100%;height:100%;margin:0;">
                                <div id="map" 
                                data-dojo-type="dijit/layout/ContentPane" 
                                data-dojo-props="region:'center'" 
                                style="padding:0;">
                                <div style=" width:200px;  top:200px;" class="ts-form__map-search ts-z-index__2">

                            <!--Form-->
                            <form>
                                <!-- <div align="center" class="zink">
                                    <div href=".ts-form-collapse" data-toggle="collapse" class="">

                                        <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Legenda

                                    </div>

                                </div>

                                <div class="ts-box p-1">
                                    <div style="margin-top:5px;"><img
                                            src="https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png"
                                            width="23px;">&nbsp;Industri</div>
                                    <div style="margin-top:5px;"><img
                                            src="https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png"
                                            width="23px;">&nbsp;Infrastruktur</div>
                                    <div style="margin-top:5px;"><img
                                            src="https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png"
                                            width="23px;">&nbsp;Pangan & Pertanian</div>
                                    <div style="margin-top:5px;"><img
                                            src="http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png"
                                            width="23px;">&nbsp;Jasa</div>


                                </div>


 -->


                                <!--end ts-form-collapse-->

                            </form>
                            <!--end ts-form-->
                            </div>
                            <div  style="width:35px;  right:14px; top:70px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                        <div id="layerclick"  class="">

                            <i class="fa fa-map-o" aria-hidden="true"></i>

                        </div>
                    </div>
                    <div  style="width:35px;  right:14px; top:110px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                        <div id="baseclick"  class="">

                            <i class="fa fa-th-large" aria-hidden="true"></i>

                        </div>
                    </div>
                     <div id="layerhidden" style="right:40px; top:125px width:300px" class="ts-form__map-search ts-z-index__2">
                     <form>

                        <div align="center" class="zink">
                            <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                                <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer                            </div>
                        </div>
    <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >RTRW :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input checked type='checkbox'
                                    name="rtrw" /><span class="toogle on"></span></div></dd><br>
                </div>

                        <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  > @lang('bahasa.bandara') : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                    name="bandara" /><span class="toogle "></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >@lang('bahasa.Pelabuhan') :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pelabuhan" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >@lang('bahasa.rumah_sakit') :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="rumah_sakit" /><span class="toogle"></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >@lang('bahasa.Pendidikan') :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="pendidikan" /><span class="toogle"></span></div></dd><br>
                </div>
            

                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Hotel :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="hotel" /><span class="toogle"></span></div></dd><br>
                </div>
                </form>  
                </div>

                             
                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                        <div data-dojo-type="dijit/TitlePane" 
                             data-dojo-props="title:'Switch Basemap', open:true">
                          <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                            <div id="basemapGallery"></div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>

            </section>
         
            <section id="tbody" class="ts-box p-1 " style="margin-top:10px; margin-left:10px; margin-right:10px;">

              

                <div class="clearfix  btn-pro titlez">

          
                    <div class="">

                        <a id="ts-display-list" class="btn">
                             @lang('bahasa.peluang')
                         
                        </a>
                    </div>



                </div>
                <br>
                {{-- Memanggil project --}}
                <div id="owl" class="owl-carousel ts-items-carousel" data-owl-items="4" data-owl-nav="1">
        @foreach ($projecs as $list)
  

   
<!--Item-->
<!--Item-->

    <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

     <?php  if($list->prioritas == '1' ) { ?>
  <div class="ts-ribbon-corner">
    <span>@lang('bahasa.prioritas')</span>
           </div>
    
        <?php }; ?>


        <!--Card Image-->
        <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{Request::Segment(3)}}/id"
            class="card-img ts-item__image"
            >
            <img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/Peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                  
                                                   
            <figure class="ts-item__info">
             <h6>{{$list->judul}}</h6>
           
         </figure>
         <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">{{$list->sektor}}
</div>
     </a>


     <div class="card-body ts-item__body">
      <div class="atbd_listing_meta">
<span id="openmap" style="font-size:11px"class="atbd_meta atbd_badge_open">@lang('bahasa.estimasi')</span>

          <span style="background-color:#0517bbc5; font-size: 13px; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
              @currency($list->nilai_investasi / 1000000) Juta
               <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
              @currency($list->nilai_investasi / 1000000000) Miliar

               <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
              @currency($list->nilai_investasi / 1000000000000) Triliun
               <?php } ?></span>          <div class="atbd_listing_data_list">
            <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
          </div>
      </div>
     </div>

     <div class="atbd_listing_bottom_content">
      <div class="atbd_content_left">
          <div class="atbd_listing_category">
             <a style="color:white;" href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{Request::Segment(3)}}/id"><span style="color:white;" class="fa fa-eye"></span>Lihat Detail</a>
          </div>
      </div>
      <ul class="atbd_content_right">
          <li class="atbd_count"></span></li>
          <li class="atbd_save">
            
          </li>
      </ul>

  </div>

 </div>
 <!--end ts-item ts-card-->





<!--end Item col-md-4-->




    @endforeach      

             </div>
            </section>
            <section class="ts-box p-1 " style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez ">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                             @lang('bahasa.sektor')                        </a>
                        <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                        <a> @lang('bahasa.tahun')</a>&nbsp;
                         <select style="color:grey; width:100px;" class="custom-select" id="sectorz" name="sorting">
                         <option value="2020">2020</option>
                             <option selected value="2019">2019</option>
                             <option value="2018">2018</option>
                             <option value="2017">2017</option>
                             <option value="2016">2016 </option>



                         </select>
                         </div>
                    </div>






                </div>
                <br>

                <div id="sector">FusionCharts will render here</div>

                <!--end container-->
            </section>

            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez ">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">

                        @lang('bahasa.komoditi')                        </a>

                        <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                        <a>@lang('bahasa.tahun')</a>&nbsp;
                         <select style="color:grey; width:100px;" class="custom-select" id="sectorz2" name="sorting">
                         <option value="2020">2020</option>
                             <option selected value="2019">2019</option>
                             <option value="2018">2018</option>
                             <option value="2017">2017</option>
                             <option value="2016">2016 </option>



                         </select>
                         </div>
                    </div>






                </div>
                <br>

                <div id="sector2">FusionCharts will render here</div>

                <!--end container-->
            </section>
             <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                             @lang('bahasa.pertumbuhan')
                        </a>

                    </div>






                </div>
                <br>

                <div id="chart-container6">FusionCharts will render here</div>

                <!--end container-->
            </section>
            <section class="ts-box p-1 long" style=" margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">

                            @lang('bahasa.umr')
                        </a>

                     
                    </div>






                </div>
                
                <br>

                <div id="chart-container3">FusionCharts will render here</div>

                <!--end container-->
            </section>

            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

            <div class="clearfix  btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="btn">


        @lang('bahasa.bruto')
    </a>
    <div class="float-none float-sm-right pl-2 ts-center__vertical ">
    <a>Pilih Tahun</a>&nbsp;
     <select style="color:grey; width:100px;" class="custom-select" id="sectorz4" name="sorting">
     <option value="2020">2020</option>
         <option selected value="2019">2019</option>
         <option value="2018">2018</option>
         <option value="2017">2017</option>
         <option value="2016">2016 </option>



     </select>
     </div>
 
</div>



                </div>
                <br>

                <div id="sector4">FusionCharts will render here</div>

                <!--end container-->
            </section>

         <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

            <div class="clearfix  btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="btn">


         @lang('bahasa.jumlah')
    </a>
   </div>



                </div>
                <br>

                <div class="modal-body">
      <div class="col-sm-12">
    
    <div style="background-color:#0517bbc5; border-radius: 10px " class="card ts-item ts-card ">

        <!--Ribbon-->
       
       
           
        <!--Card Image-->
      

        


        <div class="card-body ts-item__body">
         <div class="atbd_listing_meta">

         <div align="center">
         
<span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"> Jumlah seluruh Infrastruktur

    </span></div>
  
    
          
            
         </div>
         <div align="left">
         <br>
         <div style="float:left; color:grey">Jumlah Bandara   </div><dd style="margin-left:140px;  ; " class="border-bottom pb-2"> :  @foreach($bandara as $list){{$list->bandara}}@endforeach</dd>
         <div style="float:left; color:grey">Jumlah Pelabuhan  </div><dd style="margin-left:140px;  ; " class="border-bottom pb-2"> : @foreach($pelabuhan as $list){{$list->pelabuhan}}@endforeach</dd>
         <div style="float:left; color:grey">Jumlah Rumah Sakit   </div><dd style="margin-left:140px;  ; " class="border-bottom pb-2"> : @foreach($rumahsakit as $list){{$list->rumahsakit}}@endforeach</dd>
         <div style="float:left; color:grey">Jumlah Hotel   </div><dd style="margin-left:140px;  ; " class="border-bottom pb-2"> : @foreach($hotel as $list){{$list->hotel}}@endforeach</dd>
         <div style="float:left; color:grey">Jumlah Pendidikan  </div><dd style="margin-left:140px;  ; " class="border-bottom pb-2"> : @foreach($pendidikan as $list){{$list->pendidikan}}@endforeach</dd>
            
            </div>
        </div>

        <div class="atbd_listing_bottom_content">
               <ul class="atbd_content_right">
             <li class="atbd_count"></li>
             <li class="atbd_save">
               
             </li>
         </ul>

   



 


           
                              
            </div></div></div></div>
                <!--end container-->
            </section>


        </div>

        <div>


            <div id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:0;"></div>

            <!--end container-->
            </section>

        </div>


    </div>
</body>
    <script src="{{Config::get('app.url') }}/assets/js/custom.js"></script>
     <script src="{{Config::get('app.url')}}/map/html/assets/js/owl.carousel.min.js"></script>
<script>
          $.get("{{ Config::get('app.url') }}/chartsectordaerah/{{ Request::segment(3) }}/2019/{{ Request::segment(4) }}", function (data) {
                $("#sector").html(data);
               
              
            });
            $.get("{{ Config::get('app.url') }}/chartkomoditidaerah/{{ Request::segment(3) }}/2019/{{ Request::segment(4) }}", function (data) {
                $("#sector2").html(data);
               
              
            });

            $.get("{{ Config::get('app.url') }}/chartpdrbdaerah/{{ Request::segment(3) }}/2019", function (data) {
                $("#sector4").html(data);
               
              
            });
       </script>

 

    <script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Include fusioncharts core library file -->
    <!-- Include fusioncharts jquery plugin -->
    <script type="text/javascript"
        src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
    </script>
     

    <script>
        const postDetails = document.querySelector(".longz");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 3200
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });300
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>


        @endsection
