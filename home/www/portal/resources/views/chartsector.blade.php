       <!-- Include fusioncharts core library file -->
   



 <div id="sssss">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
      FusionCharts.ready(function(){
                    var chr22zz = new FusionCharts({
                        type: 'scrollcombidy2d',
            renderAt: 'sssss',
            width: '100%',
            height: '430',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fusion",
                    "caption": "Realisasi Investasi Berdasarkan Sektor",
                    "subCaption": "2016 - 2019",
                 
                    "numberScaleValue": "10,100,1000",
                    "numberScaleUnit": " juta, Miliar, Triliun",
                        "showDivLineSecondaryValue":'0',
"showSecondaryLimits":'0',
                        "valuePadding": "5",
                    "numberPrefix": "Rp ",
                    "snumberPrefix": "%",
                  
                   
                    "numVisiblePlot": "12",
                    "flatScrollBars": "1",
                    "scrollheight": "10"
                },
                "categories": [{
                    "category": [@foreach($investasi as $chart){ 
                            "label": "Sektor {{$chart->sektor}}"
                        }, @endforeach
                       
                    ]
                }],
                "dataset": [{
                        "seriesName": "PMA (US$. ribu)",
                        
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pma}}",
                                "tooltext": "Sektor {{$chart->sektor}} : @currency2($chart->investasi_pma) "
                            }, @endforeach
                           
                        ]
                    },
                    {
                        "seriesName": "PMDN (Rp. Juta)",
                      
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pmdn}}",
                                "tooltext": "Sektor {{$chart->sektor}} : @currency($chart->investasi_pmdn) "
                            },@endforeach
                          
                        ]
                    },
                
                  
                ]
            }
        });
                    chr22zz.render();
                });
            </script>