<!DOCTYPE html>
<html lang="en">
<head>
	<!-- head -->
	@include('layouts.partials.head')
</head>

<body id="body">
	<div id="preloader"></div>
	<!-- top bar -->
	@include('layouts.partials.topbar')
	
	<!-- header -->
	@include('layouts.partials.header')
	
	<!-- intro -->
	@include('layouts.partials.intro')

	<main id="main">
		<!-- maps -->
		@include('layouts.partials.map')
	
		<!-- news feeder -->
		@include('layouts.partials.news_feeder')
		
		<!-- guide -->
		@include('layouts.partials.guide')
		
		<!-- rating -->
		@include('layouts.partials.rating')
		
    <!--contact-->
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Kontak Kami</h2>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Alamat</h3>
              <address>Jl. Jend. Gatot Subroto No. 44, Kebayoran Baru Jakarta 12190 P.O. Box 3186, Indonesia</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Telpon</h3>
              <p>021 5255284</p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p>sipid@bkpm.go.id</p>
            </div>
          </div>

        </div>
      </div>

    </section><!-- #contact -->
	
	<!--script src="//www.powr.io/powr.js?external-type=html"></script> 
 <div class="powr-rss-feed" id="c2912dbd_1535028924"></div-->

  </main>

	<!-- footer -->
	@include('layouts.partials.footer')

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- script -->
	@include('layouts.partials.script')
</body>
</html>
