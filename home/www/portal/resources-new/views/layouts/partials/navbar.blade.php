<div class="navbar navbar-fixed-top navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon icon-bar"></span>
				<span class="icon icon-bar"></span>
				<span class="icon icon-bar"></span>
			</button>
			<a href="#" class="navbar-brand"><img src="{{ $_ENV['APP_URL'] }}/public/assets/images/logo.png" class="img-responsive" alt="logo"></a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#home" class="smoothScroll">Beranda</a></li>
				<li><a href="#maps" class="smoothScroll">Peta</a></li>
				<li><a href="#rating" class="smoothScroll">Rating</a></li>
				<li><a href="#guide" class="smoothScroll">Petunjuk</a></li>
				<li><a href="#contact" class="smoothScroll">Kontak Kami</a></li>
			</ul>
		</div>
	</div>
</div>