<section id="intro" class="wow fadeIn">

	<div class="intro-content">
		<h2 style="color:#FFFFFF">SIPID</h2>
		<h3 style="color:#FFFFFF">Sistem Informasi Potensi Investasi Daerah</h3>
	</div>

	<div id="intro-carousel" class="owl-carousel" >
		<div class="item" style="background-image: url('{{ $_ENV['APP_URL'] }}/public/assets/img/intro-carousel/1.jpg');"></div>
		<div class="item" style="background-image: url('{{ $_ENV['APP_URL'] }}/public/assets/img/intro-carousel/2.jpg');"></div>
		<div class="item" style="background-image: url('{{ $_ENV['APP_URL'] }}/public/assets/img/intro-carousel/3.jpg');"></div>
		<div class="item" style="background-image: url('{{ $_ENV['APP_URL'] }}/public/assets/img/intro-carousel/4.jpg');"></div>
		<div class="item" style="background-image: url('{{ $_ENV['APP_URL'] }}/public/assets/img/intro-carousel/5.jpg');"></div>
	</div>

</section><!-- #intro -->