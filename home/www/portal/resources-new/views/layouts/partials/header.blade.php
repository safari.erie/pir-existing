<header id="header">
	<div class="container">

		<div id="logo" class="pull-left">
			<!--h1><a href="#body" class="scrollto">Reve<span>al</span></a></h1-->
			<!-- Uncomment below if you prefer to use an image logo -->
			<a href="#body"><img src="{{ $_ENV['APP_URL'] }}/public/assets/img/logo-bkpm.png" alt="" title="" /></a>
			
		</div>

		<nav id="nav-menu-container">
			<ul class="nav-menu">
				<li class="menu-active"><a href="#body">Beranda</a></li>
				<li><a href="#maps">Peta</a></li>
				<li><a href="#guides">Petunjuk</a></li>
				<!--li><a href="#news">Berita</a></li-->
				<li><a href="#rating">Rating</a></li>
				<li><a href="#contact">Kontak Kami</a></li>
			</ul>
		</nav><!-- #nav-menu-container -->
	</div>
</header><!-- #header -->