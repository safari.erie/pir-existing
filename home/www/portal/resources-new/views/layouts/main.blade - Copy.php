
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#" class="js dj_webkit dj_chrome dj_contentbox">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="http://portal.ina-sdi.or.id/home/sites/default/files/android-icon-36x36.png" type="image/png">
<meta name="description" content="Ina-Geoportal. Ina-Geoportal (Indonesia-Geospatial Portal) adalah Portal Geospasial Indonesia yang dibangun dengan partisipasi berbagai kementrian dan lembaga serta pemerintah daerah di Indonesia. Diluncurkan pada tanggal 17 Oktober 2011 dalam acara Geospasial untuk Negeri.">
<meta name="generator" content="Drupal 7 (http://drupal.org)">
<link rel="canonical" href="http://portal.ina-sdi.or.id/home/">
<link rel="shortlink" href="http://portal.ina-sdi.or.id/home/">
  <title>Geospasial untuk Negeri |</title>
  <style>
@import url("http://portal.ina-sdi.or.id/home/modules/system/system.base.css?p04mox");
</style>
<style>
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.core.min.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.theme.min.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/libraries/ms-Dropdown/css/msdropdown/dd.css?p04mox");
</style>
<style>
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/ldap/ldap_user/ldap_user.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/counter/counter.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/modules/field/theme/field.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/modules/node/node.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/views/css/views.css?p04mox");
</style>
<style>
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/ctools/css/ctools.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/ldap/ldap_servers/ldap_servers.admin.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/ctools/css/modal.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/modal_forms/css/modal_forms_popup.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/panels/css/panels.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/modules/locale/locale.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/lang_dropdown/msdropdown/css/msdropdown/ldsSkin.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/drupal-map-gallery-webmaps-master/mapgallery_group_field/gallery_grid/css/reset.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/drupal-map-gallery-webmaps-master/mapgallery_group_field/gallery_grid/css/960.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/drupal-map-gallery-webmaps-master/mapgallery_group_field/gallery_grid/css/common.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/modules/drupal-map-gallery-webmaps-master/mapgallery_group_field/gallery_grid/css/themes.css?p04mox");
</style>
<style>
@import url("http://portal.ina-sdi.or.id/home/sites/all/themes/cdn/css/style.css?p04mox");
@import url("http://portal.ina-sdi.or.id/home/sites/all/themes/cdn/css/bootstrap.css?p04mox");
</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script type="text/javascript" src="https://www.w3counter.com/track/pv?id=109318&amp;userAgent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F68.0.3440.84%20Safari%2F537.36&amp;webpageName=Geospasial%20untuk%20Negeri%20%7C&amp;ref=&amp;url=http%3A%2F%2Fportal.ina-sdi.or.id%2Fhome%2F&amp;width=1366&amp;height=768&amp;rand=171" async=""></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/esri/nls/jsapi_en-us.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/esri/layers/VectorTileLayerImpl.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/dojox/gfx/svg.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/esri/arcgis/Portal.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/dojox/lang/aspect.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/dojox/gfx/filters.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/dojox/gfx/svgext.js"></script><script type="text/javascript" charset="utf-8" src="http://js.arcgis.com/3.19/dojox/main.js"></script><script async="" src="https://www.google-analytics.com/analytics.js"></script><script src="http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script src="http://portal.ina-sdi.or.id/home/misc/jquery.once.js?v=1.2"></script>
<script src="http://portal.ina-sdi.or.id/home/misc/drupal.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/equalheights/jquery.equalheights.js?v=1.0"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/replace/ui/ui/minified/jquery.ui.core.min.js?v=1.10.2"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/replace/ui/external/jquery.cookie.js?v=67fb34f6a866c40d0570"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/replace/misc/jquery.form.min.js?v=2.69"></script>
<script src="http://portal.ina-sdi.or.id/home/misc/ajax.js?v=7.53"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/jquery_update/js/jquery_update.js?v=0.0.1"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/equalheights/equalheights.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/iframe/iframe.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/themes/bootstrap/js/misc/_progress.js?v=7.53"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/ctools/js/modal.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/modal_forms/js/modal_forms_popup.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/libraries/ms-Dropdown/js/msdropdown/jquery.dd.min.js?v=3.5.2"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/views/js/base.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/ctools/js/jump-menu.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/views/js/ajax_view.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/libraries/imagesloaded/jquery.imagesloaded.min.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/google_analytics/googleanalytics.js?p04mox"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-99820227-2", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/modules/lang_dropdown/lang_dropdown.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/themes/bootstrap/js/misc/ajax.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/themes/bootstrap/js/modules/views/js/ajax_view.js?p04mox"></script>
<script src="http://portal.ina-sdi.or.id/home/sites/all/themes/bootstrap/js/modules/ctools/js/modal.js?p04mox"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/home\/","pathPrefix":"","ajaxPageState":{"theme":"cdn","theme_token":"Ohbit7pJrDyPMZYKvzHyAetHNSWfYGRi3UirlE1GLfA","jquery_version":"1.10","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/equalheights\/jquery.equalheights.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/jquery_update\/replace\/misc\/jquery.form.min.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/modules\/equalheights\/equalheights.js":1,"sites\/all\/modules\/iframe\/iframe.js":1,"sites\/all\/themes\/bootstrap\/js\/misc\/_progress.js":1,"sites\/all\/modules\/ctools\/js\/modal.js":1,"sites\/all\/modules\/modal_forms\/js\/modal_forms_popup.js":1,"sites\/all\/libraries\/ms-Dropdown\/js\/msdropdown\/jquery.dd.min.js":1,"sites\/all\/modules\/views\/js\/base.js":1,"sites\/all\/modules\/ctools\/js\/jump-menu.js":1,"sites\/all\/modules\/views\/js\/ajax_view.js":1,"sites\/all\/libraries\/imagesloaded\/jquery.imagesloaded.min.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/modules\/lang_dropdown\/lang_dropdown.js":1,"sites\/all\/themes\/bootstrap\/js\/misc\/ajax.js":1,"sites\/all\/themes\/bootstrap\/js\/modules\/views\/js\/ajax_view.js":1,"sites\/all\/themes\/bootstrap\/js\/modules\/ctools\/js\/modal.js":1},"css":{"modules\/system\/system.base.css":1,"misc\/ui\/jquery.ui.core.css":1,"misc\/ui\/jquery.ui.theme.css":1,"sites\/all\/libraries\/ms-Dropdown\/css\/msdropdown\/dd.css":1,"sites\/all\/modules\/ldap\/ldap_user\/ldap_user.css":1,"sites\/all\/modules\/counter\/counter.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/ldap\/ldap_servers\/ldap_servers.admin.css":1,"sites\/all\/modules\/ctools\/css\/modal.css":1,"sites\/all\/modules\/modal_forms\/css\/modal_forms_popup.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"modules\/locale\/locale.css":1,"sites\/all\/modules\/lang_dropdown\/msdropdown\/css\/msdropdown\/ldsSkin.css":1,"sites\/all\/modules\/drupal-map-gallery-webmaps-master\/mapgallery_group_field\/gallery_grid\/css\/reset.css":1,"sites\/all\/modules\/drupal-map-gallery-webmaps-master\/mapgallery_group_field\/gallery_grid\/css\/960.css":1,"sites\/all\/modules\/drupal-map-gallery-webmaps-master\/mapgallery_group_field\/gallery_grid\/css\/common.css":1,"sites\/all\/modules\/drupal-map-gallery-webmaps-master\/mapgallery_group_field\/gallery_grid\/css\/themes.css":1,"sites\/all\/modules\/drupal-map-gallery-webmaps-master\/mapgallery_group_field\/gallery_grid\/css\/grid960.css":1,"sites\/all\/themes\/cdn\/css\/style.css":1,"sites\/all\/themes\/cdn\/css\/bootstrap.css":1}},"CToolsModal":{"loadingText":"Loading...","closeText":"Close Window","closeImage":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022img-responsive\u0022 src=\u0022http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/ctools\/images\/icon-close-window.png\u0022 alt=\u0022Close window\u0022 title=\u0022Close window\u0022 \/\u003E","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022img-responsive\u0022 src=\u0022http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/ctools\/images\/throbber.gif\u0022 alt=\u0022Loading\u0022 title=\u0022Loading...\u0022 \/\u003E"},"modal-popup-small":{"modalSize":{"type":"fixed","width":300,"height":300},"modalOptions":{"opacity":0.85,"background":"#000"},"animation":"fadeIn","modalTheme":"ModalFormsPopup","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022img-responsive\u0022 src=\u0022http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/modal_forms\/images\/loading_animation.gif\u0022 alt=\u0022Loading...\u0022 title=\u0022Loading\u0022 \/\u003E","closeText":"Close"},"modal-popup-medium":{"modalSize":{"type":"fixed","width":550,"height":450},"modalOptions":{"opacity":0.85,"background":"#000"},"animation":"fadeIn","modalTheme":"ModalFormsPopup","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022img-responsive\u0022 src=\u0022http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/modal_forms\/images\/loading_animation.gif\u0022 alt=\u0022Loading...\u0022 title=\u0022Loading\u0022 \/\u003E","closeText":"Close"},"modal-popup-large":{"modalSize":{"type":"scale","width":0.8,"height":0.8},"modalOptions":{"opacity":0.85,"background":"#000"},"animation":"fadeIn","modalTheme":"ModalFormsPopup","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022img-responsive\u0022 src=\u0022http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/modal_forms\/images\/loading_animation.gif\u0022 alt=\u0022Loading...\u0022 title=\u0022Loading\u0022 \/\u003E","closeText":"Close"},"lang_dropdown":{"language":{"jsWidget":{"languageicons":{"en":"http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/languageicons\/flags\/en.png","id":"http:\/\/portal.ina-sdi.or.id\/home\/sites\/all\/modules\/languageicons\/flags\/id.png"},"widget":"msdropdown","visibleRows":"2","roundedCorner":1,"animStyle":"fadeIn","event":"click","mainCSS":"ldsSkinNoLabel"}}},"views":{"ajax_path":"\/home\/views\/ajax","ajaxViews":{"views_dom_id:eca65c89b965707fd2efef35986f60de":{"view_name":"galeri_simpul_view","view_display_id":"block_1","view_args":"","view_path":"node","view_base_path":null,"view_dom_id":"eca65c89b965707fd2efef35986f60de","pager_element":0},"views_dom_id:f32455d1de24ea62d6735640770171cd":{"view_name":"galeri_simpul_view","view_display_id":"block","view_args":"","view_path":"node","view_base_path":null,"view_dom_id":"f32455d1de24ea62d6735640770171cd","pager_element":"3"},"views_dom_id:5e4d049b3fdebe865f7b79ac75daf33a":{"view_name":"berita","view_display_id":"block_1","view_args":"","view_path":"node","view_base_path":"berita","view_dom_id":"5e4d049b3fdebe865f7b79ac75daf33a","pager_element":0}}},"urlIsAjaxTrusted":{"\/home\/":true},"equalHeightsModule":{"classes":[{"selector":".equalHeight","mediaquery":"(min-width: 768px)","minheight":"","maxheight":"","overflow":"auto"}],"imagesloaded_ie8":1},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-node i18n-en" style="">
  <div id="skip-link">

    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>

 </div>
  
   
<script>
	var userName = "";
	var xhr = new XMLHttpRequest();
        xhr.open('POST', "https://portal.ina-sdi.or.id/statistic/VisitorAPI/SaveAction/?Action_Type=1&User_Name="+userName+"&User_Data=localhost" , true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send();
</script>	

	<div class="header_gradient" style="padding:0px; height:50px; margin-bottom:0px;margin-right:0px; border:0px solid #000000;  ">	
	        <div class="xxx-language">  <div class="region region-language">
    <section id="block-lang-dropdown-language" class="block block-lang-dropdown clearfix">

      
  <form class="lang_dropdown_form language" id="lang_dropdown_form_language" action="/home/" method="post" accept-charset="UTF-8"><div><div class="form-item form-item-lang-dropdown-select form-type-select form-group"><div class="ddOutOfVision" id="lang-dropdown-select-language_msddHolder" style="height: 0px; overflow: hidden; position: absolute;"><select style="width:50px" class="lang-dropdown-select-element form-control form-select" id="lang-dropdown-select-language" name="lang_dropdown_select" tabindex="-1"><option value="en" selected="selected" data-image="http://portal.ina-sdi.or.id/home/sites/all/modules/languageicons/flags/en.png">en</option><option value="id" data-image="http://portal.ina-sdi.or.id/home/sites/all/modules/languageicons/flags/id.png">id</option></select></div><div class="ldsSkinNoLabel ddcommon borderRadius" id="lang-dropdown-select-language_msdd" tabindex="0" style="width: 50px;"><div class="ddTitle borderRadiusTp"><span class="divider"></span><span class="ddArrow arrowoff"></span><span class="ddTitleText " id="lang-dropdown-select-language_title"><img src="http://portal.ina-sdi.or.id/home/sites/all/modules/languageicons/flags/en.png" class="fnone"><span class="ddlabel">en</span><span class="description" style="display: none;"></span></span></div><input id="lang-dropdown-select-language_titleText" type="text" autocomplete="off" class="text shadow borderRadius" style="display: none;"><div class="ddChild ddchild_ border shadow" id="lang-dropdown-select-language_child" style="z-index: 9999; display: none; position: absolute; visibility: visible; height: 57px;"><ul><li class="enabled _msddli_ selected"><img src="http://portal.ina-sdi.or.id/home/sites/all/modules/languageicons/flags/en.png" class="fnone"><span class="ddlabel">en</span><div class="clear"></div></li><li class="enabled _msddli_"><img src="http://portal.ina-sdi.or.id/home/sites/all/modules/languageicons/flags/id.png" class="fnone"><span class="ddlabel">id</span><div class="clear"></div></li></ul></div></div></div><input type="hidden" name="en" value="/home/">
<input type="hidden" name="id" value="/home/?language=id">
<noscript><div>
<button type="submit" id="edit-submit" name="op" value="Go" class="btn btn-default form-submit">Go</button>

</div></noscript><input type="hidden" name="form_build_id" value="form-xVRJiJbHwt9b07oHTFX-h2dUQ-BmcRlxxOkHCizUAbU">
<input type="hidden" name="form_id" value="lang_dropdown_form">
</div></form>
</section>
  </div>
	
      </div> <!-- /.section, /#sidebar-first -->
     
  </div>

<header id="navbar" role="banner" class="navbar container-fluid navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
              <a class="logo navbar-btn pull-left" href="/home/" title="Home">
          <img src="http://portal.ina-sdi.or.id/home/sites/default/files/logo.png" alt="Home">
        </a>
		
      
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
		
      
	  </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first leaf active"><a href="/home/" title="" class="active">HOME</a></li>
<li class="leaf"><a href="/home/berita2" title="">NEWS</a></li>
<li class="leaf"><a href="http://portal.ina-sdi.or.id/geoportal/" title="">DATA</a></li>
<li class="leaf"><a href="/home/faq">FAQ</a></li>
<li class="leaf"><a href="http://portal.ina-sdi.or.id/geoportal/viewer/" title="" target="_blank">MAPS</a></li>
<li class="leaf"><a href="/home/doc2" title="">DOCUMENT</a></li>
<li class="last leaf"><a href="http://portal.ina-sdi.or.id/statistic/index.html" title="">STATISTIC</a></li>
</ul>                                <ul class="menu nav navbar-nav secondary"><li class="first leaf"><a href="https://portal.ina-sdi.or.id/home/saml_login" title="">LOGIN</a></li>
<li class="last leaf"><a href="https://tanahair.indonesia.go.id/portal-web/register" title="">REGISTER</a></li>
</ul>                            </nav>
      </div>
	  
    
	</div>


	</header>

	  
        <div class="highlighted jumbotron">  <div class="region region-highlighted">
    <section id="block-block-8" class="block block-block clearfix">

      
  <div class="header_tag">SPATIAL DATA IN ONE PORTAL</div><form action="http://portal.ina-sdi.or.id/geoportal/index.html?" class="form-wrapper1"><input name="filter" placeholder="Search Data" required="" type="text"><button type="submit"><i class="glyphicon glyphicon-search"></i></button></form>
</section>
  </div>
</div>
            
	  
	
<!--section id="gk-header"></section-->
<div class="main-container container-fluid">

  <header role="banner" id="page-header">
    
      </header> <!-- /#page-header -->

  <div class="row">

    
    <section class="col-sm-12">
    
	      <a id="main-content"></a>
                                                                <div class="region region-content">
    <section id="block-views-maps-block-block" class="block block-views clearfix">

      
  <div class="view view-maps-block view-id-maps_block view-display-id-block view-dom-id-7f6c574bd7e276296e23b0dda8ef5080">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
          <!-- JAVASCRIPTS -->


    <script>var dojoConfig = { parseOnLoad: true };</script>
    <script src="https://portal.ina-sdi.or.id/arcgisjavascripts/init.js"></script>

    <script>
    dojo.require('esri.arcgis.Portal');
    dojo.require("esri.IdentityManager");
    dojo.require("dojox.lang.aspect");

    var displayOptions = {
      templateUrl: 'http://portal.ina-sdi.or.id/portal/home/webmap/viewer.html',
      themeName:'gray',
      numItemsPerPage: 4,
      group: {
        "owner": "portaladmin",
        "title": "Metaportal_Gallery"  
      },
       portalUrl: 'http://portal.ina-sdi.or.id/portal'
    };
	
    var portal;
    var group;
    var nextQueryParams;
    var queryParams;

    function init() {
      portal = new esri.arcgis.Portal(displayOptions.portalUrl);
      dojo.connect(portal, 'onLoad', loadPortal);
      dojox.lang.aspect.advise(portal, "queryItems", {
        afterReturning: function (queryItemsPromise) {
          queryItemsPromise.then(function (result) {
            nextQueryParams = result.nextQueryParams;
            queryParams = result.queryParams;
          });
        }
      });
    }

    function loadPortal() {
        var params = {
          q: 'title: ' + displayOptions.group.title + ' AND owner:' + displayOptions.group.owner
        };
        portal.queryGroups(params).then(function(groups){
        //get group title and thumbnail url
        if (groups.results.length > 0) {
          group = groups.results[0];
          if (group.thumbnailUrl) {
            dojo.create('img', {
              src: group.thumbnailUrl,
              width: 64,
              height: 64,
              alt: group.title
            }, dojo.byId('groupThumbnail'));
          }

          //dojo.byId('groupTitle').innerHTML = group.title;
		  //dojo.byId('sidebar').innerHTML = group.snippet;

          //Retrieve the web maps and applications from the group and display
          var params = {
            q: ' type: Web Map',
            num: displayOptions.numItemsPerPage
          };
          group.queryItems(params).then(updateGrid);
        }
      });
    }

    function updateGrid(queryResponse) {
      //update the gallery to get the next page of items

      var galleryList = dojo.byId('galleryList');
      dojo.empty(galleryList);  //empty the gallery to remove existing items

      //navigation buttons
      //(queryResponse.results.length < 6) ? esri.hide(dojo.byId('next')) : esri.show(dojo.byId('next'));
      //(queryResponse.queryParams.start > 1) ? esri.show(dojo.byId('prev')) : esri.hide(dojo.byId('prev'));

      //Build the thumbnails for each item the thumbnail when clicked will display the web map in a template or the web application
      var frag = document.createDocumentFragment();
      dojo.forEach(queryResponse.results, function (item) {
        if (item.id) {
          var url = (item.type === 'Web Map') ?
            displayOptions.templateUrl + '?webmap=' + item.id + '&theme=' + displayOptions.themeName :
            item.url;

          var li = dojo.create('li', {}, frag);
          var a = dojo.create('a', {
            href: url,
            target: '_blank',
			innerHTML: '<div></div class="thumb_img"> <img src="' + item.thumbnailUrl + '" / ><div class="title_thumb">' + item.title + '</div>'
         
          }, li);
        }
      });

      dojo.place(frag, galleryList);
    }

    function getNext() {
      if (nextQueryParams.start > -1) {
        group.queryItems(nextQueryParams).then(updateGrid);
      }
    }

    function getPrevious() {
      if (nextQueryParams.start !== 1) { //we aren't at the beginning keep querying.
        var params = queryParams;
        params.start = params.start - params.num;
        group.queryItems(params).then(updateGrid);
      }
    }

    dojo.ready(init);
    </script>



    <div id="mainContent">
      <div class="container_16">


          <a id="prev" class="prevButton" href="javascript:getPrevious();">       
		  <svg viewBox="0 0 100 100"><path class="arrow" d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" transform=" translate(15,0)"> </path></svg>
		  </a>
          <ul id="galleryList" class="gallery"><li><a href="http://portal.ina-sdi.or.id/portal/home/webmap/viewer.html?webmap=c1f92af87e234116b8ee1a49fd675110&amp;theme=gray" target="_blank"><div></div> <img src="http://portal.ina-sdi.or.id/portal/sharing/rest/content/items/c1f92af87e234116b8ee1a49fd675110/info/thumbnail/thumbnail.png"><div class="title_thumb">Citra Satelit Resolusi Tinggi</div></a></li><li><a href="http://portal.ina-sdi.or.id/ksp" target="_blank"><div></div> <img src="http://portal.ina-sdi.or.id/portal/sharing/rest/content/items/109f534eb61b4011a3520eb67d671220/info/thumbnail/thumbnail.png"><div class="title_thumb">Kebijakan Satu Peta</div></a></li><li><a href="http://portal.ina-sdi.or.id/batimetri/" target="_blank"><div></div> <img src="http://portal.ina-sdi.or.id/portal/sharing/rest/content/items/ecf34754353b494c868a732c7c09cc61/info/thumbnail/thumbnail.png"><div class="title_thumb">Peta Batimetri Indonesia</div></a></li><li><a href="https://portal.ina-sdi.or.id/portal/apps/webappviewer/index.html?id=c0284df2acce49ac96290f4f6b2727db" target="_blank"><div></div> <img src="http://portal.ina-sdi.or.id/portal/sharing/rest/content/items/c0284df2acce49ac96290f4f6b2727db/info/thumbnail/ago_downloaded.jpg"><div class="title_thumb">Peta Dampak Banjir Belitung</div></a></li></ul>
          <a id="next" class="nextButton" href="javascript:getNext();"> 
		  <svg viewBox="0 0 100 100">  <path class="arrow" d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z " transform="translate(85,100) rotate(180) ">  </path></svg>
		  </a>

      </div>
    </div>


    </div>
    </div>
  
  
  
  
  
  
</div>
</section>
<section id="block-block-1" class="block block-block clearfix">

      
  <div class="row"><div class="col-md-4"><h4 style="text-align: center;">&nbsp;</h4><h4 style="text-align: center;">MAP VIEWER</h4><p style="text-align: center;"><a href="http://portal.ina-sdi.or.id/geoportal/viewer/"><img alt="" src="http://portal.ina-sdi.or.id/home/sites/default/files/map_viewer.png" style="width: 100px; height: 100px;"></a></p><p style="text-align: center;">Visualisasi dan Analisa</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p></div><div class="col-md-4"><h4 style="text-align: center;">&nbsp;</h4><h4 style="text-align: center;">SOCIAL MEDIA APPS</h4><p style="text-align: center;"><a href="http://portal.ina-sdi.or.id/portal/apps/PublicInformation/index.html?appid=aa1a9eeccbcf434ea2cf361f71145e7c"><img alt="" src="http://portal.ina-sdi.or.id/home/sites/default/files/twitter.png" style="width: 100px; height: 100px;"></a></p><p style="text-align: center;">Pencarian Keyword berdasarkan peta</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p></div><div class="col-md-4"><h4 style="text-align: center;">&nbsp;</h4><h4 style="text-align: center;">DOWNLOAD</h4><p style="text-align: center;"><img alt="" src="http://portal.ina-sdi.or.id/home/sites/default/files/maps-download.png" style="width: 100px; height: 100px;"></p><p style="text-align: center;">Download Metadata</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p></div></div>
</section>
<section id="block-views-galeri-simpul-view-block-1" class="block block-views clearfix">

        <h2 class="block-title">Status Simpul Jaringan</h2>
    
  <div class="view view-galeri-simpul-view view-id-galeri_simpul_view view-display-id-block_1 view-dom-id-eca65c89b965707fd2efef35986f60de jquery-once-1-processed">
        
  
  
      <div class="view-content">
      <form action="/home/" method="post" id="ctools-jump-menu" accept-charset="UTF-8"><div><div class="form-item form-item-jump form-type-select form-group"><select class="ctools-jump-menu-select ctools-jump-menu-change form-control form-select ctools-jump-menu-processed" id="edit-jump" name="jump"><option value="" selected="selected">- Pilih Simpul -</option><option value="6debb88185a5652e4e4da7ea2cf5efb3::/home/node/129">  
          Badan Informasi Geospasial  </option><option value="6756277ac1dcc1ff617649bd9f845fd2::/home/node/131">  
          Badan Meteorologi, Klimatologi, dan Geofisika  </option><option value="d64e278e383cdd723c6790f8c2790598::/home/node/133">  
          Badan Nasional Penanggulangan Bencana  </option><option value="dfe03f8a8709e8d6833d32f98b82c552::/home/node/114">  
          Kabupaten Klaten  </option><option value="7949a2d9151400ff185881e9df2ade0d::/home/node/117">  
          Kabupaten Sumbawa  </option><option value="87fd525aa841d90dbf30655a32100486::/home/node/143">  
          Kabupaten Tulungagung  </option><option value="81d7091a1613d6ebc4cc5efab1a12a8f::/home/node/130">  
          Kementerian Dalam Negeri  </option><option value="4025928ae877634fa72281ce5fe075ca::/home/node/84">  
          Kementerian Desa, PDT, dan Transmigrasi  </option><option value="4ad8d7e79d4c59933369a9303fe6d705::/home/node/135">  
          Kementerian Energi dan Sumber Daya Mineral  </option><option value="7ae2df250de592fe134f48c63e74f58d::/home/node/77">  
          Kementerian Kelautan dan Perikanan  </option><option value="fbd4786c98fb4f9864535e7bebfda57e::/home/node/75">  
          Kementerian Kesehatan  </option><option value="b29d28c0ae291ae69ec1e7682305df90::/home/node/80">  
          Kementerian Lingkungan Hidup dan Kehutanan  </option><option value="3ea5d6814bc1068960c560c5c98cb33e::/home/node/81">  
          Kementerian Pekerjaan Umum dan Perumahan Rakyat  </option><option value="952b87520ac3876c641336c0c91bf38f::/home/node/144">  
          Kementerian Perhubungan  </option><option value="63e31bab986eccccfd577197042637b5::/home/node/101">  
          Kementerian Pertanian  </option><option value="3779a51b6cee555649fe50cada959488::/home/node/145">  
          Komisi Pemilihan Umum  </option><option value="035c23ffe53e27641c22e27d059f2c58::/home/node/137">  
          Kota Bontang  </option><option value="148f9545ba42d3889ddff8c919cb5503::/home/node/146">  
          Kota Denpasar  </option><option value="a95686c56ef75231a7d7d70d1f2286f3::/home/node/138">  
          Kota Depok  </option><option value="daa355e69ccdbe28bd3eb59730a0281a::/home/node/139">  
          Kota Mojokerto  </option><option value="7f2a48c17ef5ca276ad2176aedcc00d5::/home/node/128">  
          Provinsi Aceh  </option><option value="090391d4fc9af86672d216bb2b03f338::/home/node/172">  
          Provinsi Bali  </option><option value="bedbc6a20e0e02b58e69ad8165f5c862::/home/node/161">  
          Provinsi Banten  </option><option value="20abcf20cd9308feff6e18d3cf0197eb::/home/node/152">  
          Provinsi Bengkulu  </option><option value="c9add8323553ae5c5496ca796f4fd1c7::/home/node/132">  
          Provinsi D.I Yogyakarta  </option><option value="16a520958ddc283a7ed2a6d7f36d5ba7::/home/node/74">  
          Provinsi DKI Jakarta  </option><option value="7c99e744131eb43fc578363e45b8a3a2::/home/node/163">  
          Provinsi Gorontalo  </option><option value="31f00fbf0a71ecba79c7f7a5609300a5::/home/node/154">  
          Provinsi Jambi  </option><option value="360c09e7ad6acabc0d6849f150f3d41d::/home/node/141">  
          Provinsi Jawa Barat  </option><option value="69461fe1654b79a433adfdad9122dc02::/home/node/83">  
          Provinsi Jawa Tengah  </option><option value="586a384fe4b793e6218713fd8c57df4c::/home/node/169">  
          Provinsi Jawa Timur  </option><option value="0f49e32e90fc23769042372bf2e5c0f2::/home/node/175">  
          Provinsi Kalimantan Barat  </option><option value="46086e051b881f4b916e8768ffe29b8c::/home/node/147">  
          Provinsi Kalimantan Selatan  </option><option value="6ccb67ca82f427598812a834beae3f18::/home/node/176">  
          Provinsi Kalimantan Tengah  </option><option value="69e241c8cedc53b450949c0a73bd5c54::/home/node/142">  
          Provinsi Kalimantan Timur  </option><option value="719a6d0b6f6bb121487714aa0af9767e::/home/node/174">  
          Provinsi Kalimantan Utara  </option><option value="9e38e926d4b496052d85c2331907b772::/home/node/136">  
          Provinsi Kepulauan Bangka Belitung  </option><option value="015ba401075f7d1a8f72d31045632cf0::/home/node/151">  
          Provinsi Kepulauan Riau  </option><option value="4391904213bcc39e513e293f0aad848a::/home/node/155">  
          Provinsi Lampung  </option><option value="179fd14bb52b0c4a5f20de55b3e3942b::/home/node/159">  
          Provinsi Maluku  </option><option value="6de649daac6a78ee34c6c7ec3b8421d0::/home/node/160">  
          Provinsi Maluku Utara  </option><option value="1f6fde64a26bbebce2f6f5242a7891ee::/home/node/171">  
          Provinsi Nusa Tenggara Barat  </option><option value="c3300eba89639478133116804a2baaeb::/home/node/170">  
          Provinsi Nusa Tenggara Timur  </option><option value="e9d81edd85b6a68f6d57b19ea5419fc9::/home/node/157">  
          Provinsi Papua  </option><option value="a84fc05bc7e8a923ad64c5094873205c::/home/node/158">  
          Provinsi Papua Barat  </option><option value="ae1e1b6899edc5a31c01aca28631bd06::/home/node/156">  
          Provinsi Riau  </option><option value="f0c13388919fbe413c5e1c74c4690c95::/home/node/165">  
          Provinsi Sulawesi Barat  </option><option value="f81157e829455db18be8767a1377e01e::/home/node/168">  
          Provinsi Sulawesi Selatan  </option><option value="14723008197bfde35d23328ad00cdd7d::/home/node/140">  
          Provinsi Sulawesi Tengah  </option><option value="56647d0c2adf173f35a57b94266b1e2a::/home/node/167">  
          Provinsi Sulawesi Tenggara  </option><option value="50dad394daf76d71f4207ecfaf847628::/home/node/164">  
          Provinsi Sulawesi Utara  </option><option value="2051652869979da44dfac1dc49c00fe0::/home/node/149">  
          Provinsi Sumatera Barat  </option><option value="1f600e2e20d0c28e60835bc98478c74e::/home/node/153">  
          Provinsi Sumatera Selatan  </option><option value="7155927a487366ee62ae7e0a31d40d00::/home/node/150">  
          Provinsi Sumatera Utara  </option></select></div><button class="ctools-jump-menu-button ctools-jump-menu-hide btn btn-default form-submit ctools-jump-menu-processed" type="submit" id="edit-go" name="op" value="Go" style="display: none;">Go</button>
<input type="hidden" name="form_build_id" value="form-QDem8gQZ16DY_UIZ2WzvuyX8eNgl38EMf-2n-2sE05E">
<input type="hidden" name="form_id" value="ctools_jump_menu">
</div></form>    </div>
  
  
  
  
  
  
</div>
</section>
<section id="block-views-galeri-simpul-view-block" class="block block-views clearfix">

      
  <div class="view view-galeri-simpul-view view-id-galeri_simpul_view view-display-id-block view-dom-id-f32455d1de24ea62d6735640770171cd jquery-once-1-processed">
        
  
  
      <div class="view-content">
      
<div id="views-bootstrap-thumbnail-1" class="views-bootstrap-thumbnail-plugin-style">
  
          <div class="row">
                  <div class="col col-lg-2">
            <div class="thumbnail">
                
  <div class="views-field views-field-field-logo-simpul logo_simpul">        <div class="field-content logo_simpul"><a href="/home/node/176"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/logo_simpul/public/LOGO%20PROVINSI%20KALIMANTAN%20TENGAH.png?itok=7G_OFl7G" width="80" height="80" alt=""></a></div>  </div>  
  <div class="views-field views-field-title judul_simpul">        <div class="field-content judul_simpul"><a href="/home/node/176">Provinsi Kalimantan Tengah</a></div>  </div>  
  <div class="thumb_simpul">        <div class="thumb_simpul"><div class="simpul_off">Tidak Terhubung</div></div>  </div>            </div>
          </div>
                  <div class="col col-lg-2">
            <div class="thumbnail">
                
  <div class="views-field views-field-field-logo-simpul logo_simpul">        <div class="field-content logo_simpul"><a href="/home/node/175"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/logo_simpul/public/logo-prop-kalbar.png?itok=d_HQ1V3S" width="80" height="80" alt=""></a></div>  </div>  
  <div class="views-field views-field-title judul_simpul">        <div class="field-content judul_simpul"><a href="/home/node/175">Provinsi Kalimantan Barat</a></div>  </div>  
  <div class="thumb_simpul">        <div class="thumb_simpul"><div class="simpul_on">Terhubung</div></div>  </div>            </div>
          </div>
                  <div class="col col-lg-2">
            <div class="thumbnail">
                
  <div class="views-field views-field-field-logo-simpul logo_simpul">        <div class="field-content logo_simpul"><a href="/home/node/174"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/logo_simpul/public/Emblem_of_North_Kalimantan.png?itok=AiiWBd1W" width="80" height="80" alt=""></a></div>  </div>  
  <div class="views-field views-field-title judul_simpul">        <div class="field-content judul_simpul"><a href="/home/node/174">Provinsi Kalimantan Utara</a></div>  </div>  
  <div class="thumb_simpul">        <div class="thumb_simpul"><div class="simpul_on">Terhubung</div></div>  </div>            </div>
          </div>
                  <div class="col col-lg-2">
            <div class="thumbnail">
                
  <div class="views-field views-field-field-logo-simpul logo_simpul">        <div class="field-content logo_simpul"><a href="/home/node/172"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/logo_simpul/public/download_1.png?itok=yKlNcRnJ" width="80" height="80" alt=""></a></div>  </div>  
  <div class="views-field views-field-title judul_simpul">        <div class="field-content judul_simpul"><a href="/home/node/172">Provinsi Bali</a></div>  </div>  
  <div class="thumb_simpul">        <div class="thumb_simpul"><div class="simpul_on">Terhubung</div></div>  </div>            </div>
          </div>
                  <div class="col col-lg-2">
            <div class="thumbnail">
                
  <div class="views-field views-field-field-logo-simpul logo_simpul">        <div class="field-content logo_simpul"><a href="/home/node/171"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/logo_simpul/public/logo%20NTB.png?itok=y4P3EWkP" width="80" height="80" alt=""></a></div>  </div>  
  <div class="views-field views-field-title judul_simpul">        <div class="field-content judul_simpul"><a href="/home/node/171">Provinsi Nusa Tenggara Barat</a></div>  </div>  
  <div class="thumb_simpul">        <div class="thumb_simpul"><div class="simpul_off">Tidak Terhubung</div></div>  </div>            </div>
          </div>
                  <div class="col col-lg-2">
            <div class="thumbnail">
                
  <div class="views-field views-field-field-logo-simpul logo_simpul">        <div class="field-content logo_simpul"><a href="/home/node/170"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/logo_simpul/public/LOGO%20PROVINSI%20NTT.png?itok=De7_E4BS" width="80" height="80" alt=""></a></div>  </div>  
  <div class="views-field views-field-title judul_simpul">        <div class="field-content judul_simpul"><a href="/home/node/170">Provinsi Nusa Tenggara Timur</a></div>  </div>  
  <div class="thumb_simpul">        <div class="thumb_simpul"><div class="simpul_on">Terhubung</div></div>  </div>            </div>
          </div>
              </div>
    
  </div>
    </div>
  
      <div class="text-center"><ul class="pagination"><li class="active"><span>1</span></li>
<li><a title="Go to page 2" href="/home/node?page=0%2C0%2C0%2C1">2</a></li>
<li><a title="Go to page 3" href="/home/node?page=0%2C0%2C0%2C2">3</a></li>
<li><a title="Go to page 4" href="/home/node?page=0%2C0%2C0%2C3">4</a></li>
<li><a title="Go to page 5" href="/home/node?page=0%2C0%2C0%2C4">5</a></li>
<li><a title="Go to page 6" href="/home/node?page=0%2C0%2C0%2C5">6</a></li>
<li><a title="Go to page 7" href="/home/node?page=0%2C0%2C0%2C6">7</a></li>
<li><a title="Go to page 8" href="/home/node?page=0%2C0%2C0%2C7">8</a></li>
<li><a title="Go to page 9" href="/home/node?page=0%2C0%2C0%2C8">9</a></li>
<li class="next"><a title="Go to next page" href="/home/node?page=0%2C0%2C0%2C1">next ›</a></li>
<li class="pager-last"><a href="/home/node?page=0%2C0%2C0%2C8">akhir »</a></li>
</ul></div>  
  
  
  
  
</div>
</section>
<section id="block-views-berita-block-1" class="block block-views clearfix">

        <h2 class="block-title">News</h2>
    
  <div class="view view-berita view-id-berita view-display-id-block_1 view-dom-id-5e4d049b3fdebe865f7b79ac75daf33a jquery-once-1-processed">
        
  
  
      <div class="view-content">
      
<div id="views-bootstrap-thumbnail-2" class="views-bootstrap-thumbnail-plugin-style">
  
          <div class="row">
                  <div class="col col-lg-3">
            <div class="thumbnail">
                
  <div class="views-field views-field-title">        <p class="field-content"><a href="/home/node/134">Peta NKRI versi terbaru kini dapat diunduh di Ina-Geoportal</a></p>  </div>  
  <div class="views-field views-field-field-foto">        <div class="field-content"><a href="/home/node/134"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/berita_block_view/public/nkri2.jpg?itok=kBMy1XoE" width="310" height="130" alt=""></a></div>  </div>  
  <div class="views-field views-field-body-et">        <div class="field-content"><p open="" sans="">Tegaskan Batas Wilayah NKRI, Pemerintah Terbitkan Peta Mutakhir</p>...</div>  </div>            </div>
          </div>
                  <div class="col col-lg-3">
            <div class="thumbnail">
                
  <div class="views-field views-field-title">        <p class="field-content"><a href="/home/node/102">BIG Selenggarakan Bimbingan Teknis Konseptor SNI, Editor SNI dan Asesor Akreditasi</a></p>  </div>  
  <div class="views-field views-field-field-foto">        <div class="field-content"><a href="/home/node/102"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/berita_block_view/public/bsn.gif?itok=fxn7F_A1" width="310" height="130" alt=""></a></div>  </div>  
  <div class="views-field views-field-body-et">        <div class="field-content"><p><strong>Depok, Berita Geospasial BIG -</strong>&nbsp;Dalam upaya mewujudkan penyelenggaraan...</p></div>  </div>            </div>
          </div>
                  <div class="col col-lg-3">
            <div class="thumbnail">
                
  <div class="views-field views-field-title">        <p class="field-content"><a href="/home/berita/informasi-geospasial-mendukung-provinsi-ntb-gemilang">Informasi Geospasial Mendukung Provinsi NTB Gemilang</a></p>  </div>  
  <div class="views-field views-field-field-foto">        <div class="field-content"><a href="/home/berita/informasi-geospasial-mendukung-provinsi-ntb-gemilang"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/berita_block_view/public/peta_ntb_0.jpg?itok=egzmQrla" width="310" height="130" alt=""></a></div>  </div>  
  <div class="views-field views-field-body-et">        <div class="field-content"><p>Mataram, Berita Geospasial BIG - Dalam rangka mendukung nawacita pemerintah, Badan Informasi...</p></div>  </div>            </div>
          </div>
                  <div class="col col-lg-3">
            <div class="thumbnail">
                
  <div class="views-field views-field-title">        <p class="field-content"><a href="/home/berita/one-map-policy-satu-peta-untuk-satu-indonesia">One Map Policy Satu Peta untuk Satu Indonesia</a></p>  </div>  
  <div class="views-field views-field-field-foto">        <div class="field-content"><a href="/home/berita/one-map-policy-satu-peta-untuk-satu-indonesia"><img typeof="foaf:Image" class="img-responsive" src="http://portal.ina-sdi.or.id/home/sites/default/files/styles/berita_block_view/public/animasi_big_mid1.gif?itok=XN1-jzU_" width="310" height="130" alt=""></a></div>  </div>  
  <div class="views-field views-field-body-et">        <div class="field-content"><p>(Berita Geospasial – Cibinong, 3 Januari 2017) Kebijakan Satu Peta (KSP) atau One Map Policy (...</p></div>  </div>            </div>
          </div>
              </div>
    
  </div>
    </div>
  
      <ul class="pager"><li class="pager-previous">&nbsp;</li>
<li class="pager-current">1 of 2</li>
<li class="pager-next"><a title="Go to next page" href="/home/node?page=1">next ›</a></li>
</ul>  
  
  
  
  
</div>
</section>
<section id="block-block-10" class="block block-block clearfix">

      
  <script src="https://www.w3counter.com/tracker.js?id=109318"></script>
</section>
  </div>
    </section>

    
  </div>
</div>

        
<div class="container_footer">	  
  <footer class="footer_big">
      <div class="region region-footer">
    <section id="block-block-2" class="block block-block clearfix">

      
  <div class="row"><div class="col-md-4"><h4 class="footer-title">INAGEOPORTAL</h4><p class="footer-text" style="font-size: 12px;">Ina-Geoportal merupakan portal Geospasial Indonesia yang resmi diluncurkan oleh Badan Informasi Geospasial (BIG) pada tanggal 17 oktober 2011. Ina-Geoportal dibangun dengan partisipasi berbagai kementrian, lembaga, serta pemerintah daerah di indonesia, sebagai bukti apresiasi terhadap kebutuhan data geospasial di tanah air Indonesia. Portal ini merupakan gerbang utama akses informasi geospasial yang menghubungkan berbagai Kementerian, Lembaga, Provinsi, dan Daerah yang menjadi mitra penghubung simpul Jaringan Informasi Geospasial Nasional (JIGN).</p></div><div class="col-md-4"><h4 class="footer-title">KONTAK KAMI</h4><p class="footer-text" style="font-size: 12px;">Pusat Pengelolaan dan Penyebarluasan Informasi Geospasial<br>Badan Informasi Geospasial (BIG)<br>Jl. Raya Jakarta - Bogor KM. 46<br>Cibinong 16911, INDONESIA</p><p class="footer-text" style="font-size: 12px;">Telp. 021-8753155 atau 021-8752062<br>ext.3608/3611/3103<br>Fax. 021-87908988/87916647</p><p class="footer-text" style="font-size: 12px;">E-mail : helpdesk.nsdi@big.go.id</p><div class="footer_big" style="text-align: center;">&nbsp;</div></div><div class="col-md-4"><h4 class="footer-title">PENGUNJUNG</h4><p><iframe height="150px" scrolling="no" src="https://portal.ina-sdi.or.id/statistic/visitor.html" width="250px"></iframe></p><span style="font-size:10px;"><a href="https://portal.ina-sdi.or.id/statistic/index.html"><span style="color:#ffffff;">Statistik Detail</span></a></span></div></div><div class="footer" style="background-color: black"><div class="row"><div><p style="text-align: center;">Copyright © 2016 - Badan Informasi Geospasial</p><p style="text-align: center;">All rights reserved</p><p style="text-align: center;"><a class="xfu" href="/home/faq" id="T:pt_g18">FAQ </a>| <a class="xfu" href="/home/contact" id="T:pt_g18">Kontak</a></p></div></div></div>
</section>
  </div>
  </footer>
  </div>

  <script src="http://portal.ina-sdi.or.id/home/sites/all/themes/bootstrap/js/bootstrap.js?p04mox"></script>


</body></html>