    @extends('frontend.layoutmap')

@section('content')


    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

<div class="row">
  <div class="col-sm-3"> <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

     <!--ITEMS LISTING
            =========================================================================================================-->

      <div  class="zink">

         <!--Display selector on the left-->
         <div align="center" class="">

             <a id="ts-display-list" class="btn">
                 Investment Incentives
             </a>
             <br>
                  <a href="{{ url('holiday') }}"> <div align="center"  style="margin-top:10px; color:orange; " class="border-bottom pb-2">Tax Holiday</div></a>
            <a href="{{ url('allow') }}"> <div align="center" style="margin-top:10px;  color:white;" class="border-bottom pb-2">Tax Allowance</div></a>
           <a href="{{ url('master') }}">  <div align="center" style="margin-top:10px; color:white; " class="border-bottom pb-2">Masterlist</div></a>
           <a href="{{ url('super') }}">  <div align="center" style="margin-top:10px; color:white;" class="border-bottom pb-2">Super Deduction</div></a>
                <div style="margin-top:10px; font-size:small;"> This page last updated at <br>01/01/2020, 10:30:30 AM (WIB)</div>
         </div>

         <!--Display selector on the right-->


     </div>
  
  
     <!--end container-->
 </section></div>
  <div class="col-sm-9">  <section id="tbody" class="ts-box p-1 long" style="margin-top:10px; margin-left:10px; margin-right:10px;">
  

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="clearfix zink hijau">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 TAX Holiday
             </a>
         </div>

    


     </div>
     <br>
     <div align="center">
    <img alt="" height="400" src="https://regionalinvestment.bkpm.go.id/gis/sharing/rest/content/items/626cc4fd1eb7428094ea089ca08cca10/resources/tax%20holiday__1569919489801__w604.png" width="350">
     </div>
     <br>
    <dt style="width:auto; margin-top:20px;"class="zink">Tax Holiday Procedure (1)</dt>
    <img alt=""  src="https://regionalinvestment.bkpm.go.id/gis/sharing/rest/content/items/626cc4fd1eb7428094ea089ca08cca10/resources/tax1__1569919934537__w1500.png" width="100%">

    <dt style="width:auto; margin-top:20px;"class="zink">Tax Holiday Procedure (2)</dt>
     <img alt=""  src="https://regionalinvestment.bkpm.go.id/gis/sharing/rest/content/items/626cc4fd1eb7428094ea089ca08cca10/resources/tax2__1569920028888__w1500.png" width="100%">

  <dt style="width:auto; margin-top:20px;"class="zink">Tax Holiday Procedure (3)</dt>
     <img alt=""  src="https://regionalinvestment.bkpm.go.id/gis/sharing/rest/content/items/626cc4fd1eb7428094ea089ca08cca10/resources/tax3__1569920075448__w1500.png" width="100%">

</div>

<div >




     <!--end container-->
 </section>

</div>
</div>
<script>
    const postDetails = document.querySelector(".long");
    const postSidebar = document.querySelector(".when");
    const postDetailsz = document.querySelector(".ts-boxzzs");
    const postSidebarContent = document.querySelector(".ts-bosxzz > div");


    const controller = new ScrollMagic.Controller();


    const scene = new ScrollMagic.Scene({
        triggerElement: postSidebar,
        triggerHook: 0,
        duration: getDuration
    }).addTo(controller);

    //3
    if (window.matchMedia("(min-width: 768px)").matches) {
        scene.setPin(postSidebar, {
            pushFollowers: false
        });
    }

    //4
    window.addEventListener("resize", () => {
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        } else {
            scene.removePin(postSidebar, true);
        }
    });

    function getDuration() {
        return postDetails.offsetHeight - postSidebar.offsetHeight;
    }
</script>

<script>
  var LeafIconz = L.Icon.extend({
    options: {
     
        iconSize:     [25, 36],
        
    }
});
var  office = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=industry&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
	var mymap = L.map('mapid').setView([-6.2268898,106.8160027,15], 17);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);

	L.marker([-6.2268898,106.8160027,15], {icon: office}).addTo(mymap);





</script>

 
    @endsection