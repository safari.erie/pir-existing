 @extends('frontend.layoutmap')

 @section('content')

 <div class="row">
     <div class="col-xs-7 col-sm-2 col-lg-8">
         <section id="ts-hero" style="margin-left:20px; margin-right:20px; margin-top:20px;" class="ts-hero-slider ts-bg-black mb-0 ">

             <div class="ts-min-h__70vh w-100">

                 <!--Owl Carousel-->
                 <div class="owl-carousel" data-owl-loop="1" data-owl-nav="1">

                     <!-- SLIDE
                =====================================================================================================-->
                     <div class="ts-slide" data-bg-image="assets/slider.jpg">
                         <div class="ts-slide-description h-100 ts-center__vertical pb-0">
                             <div align="center" class="container">

                                 <!--Title-->
                                 <h1 class="mb-3">Why Indonesia ?</h1>

                                 <!--Location-->
                                 <figure style="justify; margin-left:20px; margin-right:20px;" class="ts-opacity__70">

                                     Potensi Investasi Regional (PIR) PIR, an information system on regional investment
                                     potential of BKPM or Indonesian provisions of the regulations. Our mission is to
                                     create sustainable regional economic growth, with vibrant business and good job
                                     opportunities for regional in Indonesia.
                                 </figure>

                                 <!--Features-->


                                 <a href="detail-01.html" class="btn btn-primary ts-btn-arrow">Read More</a>

                             </div>
                         </div>
                     </div>
                     <!--end slide-->
                     <div class="ts-slide" data-bg-image="assets/slider.jpg">
                         <div class="ts-slide-description h-100 ts-center__vertical pb-0">
                             <div align="center" class="container">

                                 <!--Title-->
                                 <h1 class="mb-3">Why Indonesia ?</h1>

                                 <!--Location-->
                                 <figure style="justify; margin-left:10px; margin-right:10px;" class="ts-opacity__70">

                                     Potensi Investasi Regional (PIR) PIR, an information system on regional investment
                                     potential of BKPM or Indonesian provisions of the regulations. Our mission is to
                                     create sustainable regional economic growth, with vibrant business and good job
                                     opportunities for regional in Indonesia.
                                 </figure>

                                 <!--Features-->


                                 <a href="detail-01.html" class="btn btn-primary ts-btn-arrow">Read More</a>

                             </div>
                         </div>
                     </div>
                     <!--end slide-->


                 </div>
                 <!--end owl-carousel-->

                 <!--Hero slider control-->
                 <div class="ts-hero-slider-control">
                     <div class="container" id="owl-control"></div>
                 </div>

             </div>

         </section>
     </div>
     <div class="col-xs-5 col-sm-2 col-lg-4">
         <section id="tbody" class="ts-box p-1" style="margin-top:20px; margin-left:20px; margin-right:20px;">

             <!--ITEMS LISTING
            =========================================================================================================-->
             <section id="display-control zink">
                 <div class="container clearfix zink ">

                     <!--Display selector on the left-->
                     <div class="float-left">

                         <a id="ts-display-list" class="btn">
                             About PIR
                         </a>
                     </div>

                     <!--Display selector on the right-->


                 </div>
                 <br>
                 <figure style="text-align: justify; margin-left:10px; margin-right:10px;">PIR, an information system on
                     regional investment potential of BKPM or Indonesian Investment Coordinating Board is a
                     Non-Ministerial Government Agency, which in charge of implementing policy and service coordination
                     in investment in accordance with the provisions of the regulations. Our mission is to create
                     sustainable regional economic growth, with vibrant business and good job opportunities for regional
                     in Indonesia.
                 </figure>
                 <a style="margin-left:20px; margin-bottom:20px;" href="detail-01.html"
                     class="btn btn-primary ts-btn-arrow">Read More</a>
             </section>


     </div>
 </div>

 <section id="tbody" class="ts-box p-1" style="margin-top:20px; margin-left:20px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div class="clearfix zink ">

         <!--Display selector on the left-->
         <div class="">

             <a id="ts-display-list" class="btn">
                 Top Project Oppertunity
             </a>
         </div>

         <!--Display selector on the right-->


     </div>
     <br>
     <div class="owl-carousel ts-items-carousel" data-owl-items="4" data-owl-nav="1">

         @foreach ($propertiesx as $list)

         <!--Item-->
         <div style="justify; margin-left:22px; margin-right:22px;" class="slide ">

             <div class="card ts-item ts-card ">

                 <div class="ts-ribbon-corner">
                     <span>National</span>
                 </div>
                 <!--Card Image-->
                 <a href="detail-01.html" class="card-img ts-item__image"
                     data-bg-image="https://cdn.pixabay.com/photo/2018/04/30/08/00/industry-3362044_1280.jpg">
                     <div class="ts-item__info-badge">
                         <?php if ($list->nilai_investasi  < 1000000000) { ?>
                         Nilai Investasi : {{ $list->nilai_investasi / 1000000}} Juta
                         <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                         Nilai Investasi : {{$list->nilai_investasi / 1000000000}} Milyar

                         <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                         Nilai Investasi : {{$list->nilai_investasi / 1000000000000}} Triliyun
                         <?php } ?>
                     </div>
                     <figure  class="ts-item__info">
                         <h6>{{$list->judul}} </h6>
                         <aside>
                             <i class="fa fa-map-marker mr-2"></i>
                             {{$list->alamat}}
                         </aside>
                     </figure>
                 </a>


                 <div class="card-body ts-item__body">
                     <div class="ts-description-lists zink">

                         Rating : &nbsp;&nbsp;<i style=color:#FFD700; " class=" fa
                             fa-star "></i><i style=color:#FFD700; " class="fa fa-star "></i><i
                             style=color:#FFD700; " class=" fa fa-star "></i><i style=color:#FFD700; "
                             class="fa fa-star "></i><i style=color:#FFD700; " class=" fa fa-star "></i></dt>

                                                            
                                                                </div>
                                                             
                                                                </div>
                                                            
              

                        <!--Card Footer-->
                        <a href=" detail-01.html" class="card-footer">
                             <span class="ts-btn-arrow">Detail</span>
                             </a>


                     </div>
                     <!--end ts-item-->

                 </div>
                 <!--end slide-->

                 @endforeach

             </div>

             <!--end container-->
 </section>

 <section id="tbody" class="ts-box p-1" style="margin-top:20px; margin-left:20px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div class="clearfix zink ">

         <!--Display selector on the left-->
         <div class="">

             <a id="ts-display-list" class="btn">
                 Top Sectors for Indonesia Investment
             </a>

               <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                                   
                                    <select style="color:grey;"  class="custom-select" id="sorting" name="sorting">
                                        <option    value="">Foreign Direct Investment (USD $)</option>
                                            <option value="1">Domestic Direct Investment (Million Rupiah) </option>

                                       
                                        
                                    </select>
                                </div>
         </div>

         <!--Display selector on the right-->


     </div>
     <br>

     <div id="app"></div>

     <!--end container-->
 </section>

 <section id="tbody" class="ts-box p-1" style="margin-top:20px; margin-left:20px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div class="clearfix zink ">

         <!--Display selector on the left-->
         <div class="">

             <a id="ts-display-list" class="btn">
                 Top Province or Investement
             </a>
         </div>

         <!--Display selector on the right-->


     </div>
     <br>
     <section style="margin-left:10px; margin-right:10px;">

         <div class="row">
             <div class="col-sm-2">
                 <div class="zink hijau ">

                     <!--Display selector on the left-->
                     <div align="center" class="">

                         <a>
                             Central Java
                         </a>
                     </div>




                 </div>
                 <div align="center" style="margin-top:10px;">

                     <img src="https://4.bp.blogspot.com/-OIoIvDPOteo/UXuOeCSM9BI/AAAAAAAAARk/sfTXqchJMK0/s1600/Jawa+Tengah.png"
                         height="130px" width="130px">
                 </div>
                 <div align="center" style="margin-top:10px;">
                     <figure>Opportunities | 99</figure>
                 </div>
             </div>
             <div class="col-sm-2">
                 <div align="center" class="zink hijau">

                     <!--Display selector on the left-->
                     <div class="">

                         <a>
                             Central Kalimantan
                         </a>
                     </div>

                     <!--Display selector on the right-->


                 </div>
                 <div align="center" style="margin-top:10px;">

                     <img src="https://i.pinimg.com/originals/8f/f7/de/8ff7de83e1e71d3f3914186c482e798d.png"
                         height="130px" width="180px">
                 </div>
                 <div align="center" style="margin-top:10px;">
                     <figure>Opportunities | 99</figure>
                 </div>
             </div>
             <div class="col-sm-2">
                 <div class="zink hijau">

                     <!--Display selector on the left-->
                     <div align="center" class="">

                         <a>
                             Central Sulawesi
                         </a>
                     </div>

                     <!--Display selector on the right-->


                 </div>
                 <div align="center" style="margin-top:10px;">

                     <img src="https://3.bp.blogspot.com/-dgul5wRRNRM/Vvib0HgcfZI/AAAAAAAAD0c/jl5NUH0Nf6Ah-BEt02EafA0Nrdy-EIGMQ/s1600/Provinsi%2BSulawesi%2BTengah%2Bvector%2Blogo.png"
                         height="130px" width="180px">
                 </div>
                 <div align="center" style="margin-top:10px;">
                     <figure>Opportunities | 99</figure>
                 </div>
             </div>
             <div class="col-sm-2">
                 <div align="center" class="zink hijau ">

                     <!--Display selector on the left-->
                     <div class="">

                         <a>
                             DKI Jakarta
                         </a>
                     </div>

                     <!--Display selector on the right-->


                 </div>
                 <div align="center" style="margin-top:10px;">

                     <img src="https://s3.jakarta.go.id/jakgo/uploads/public/5a0/554/8c5/5a05548c578f9387024151.png"
                         height="130px" width="130px">
                 </div>
                 <div align="center" style="margin-top:10px;">
                     <figure>Opportunities | 99</figure>
                 </div>
             </div>
             <div class="col-sm-2">
                 <div class="zink hijau">

                     <!--Display selector on the left-->
                     <div align="center" class="">

                         <a>
                             East Java
                         </a>
                     </div>

                     <!--Display selector on the right-->


                 </div>
                 <div align="center" style="margin-top:10px;">

                     <img src="https://2.bp.blogspot.com/-S1hrqQnm0nc/XDCpZ6C7iSI/AAAAAAAABvI/MlNdKdkNBHQa0lU-1I0gyTT5FbFSR2K0QCLcBGAs/s1600/logo%2Bprovinsi%2Bjawa%2Btimur.png"
                         height="130px" width="130px">
                 </div>
                 <div align="center" style="margin-top:10px;">
                     <figure>Opportunities | 99</figure>
                 </div>
             </div>
             <div class="col-sm-2">
                 <div align="center" class="zink hijau ">

                     <!--Display selector on the left-->
                     <div class="">

                         <a>
                             Lampung
                         </a>
                     </div>

                     <!--Display selector on the right-->


                 </div>
                 <div align="center" style="margin-top:10px;">

                     <img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/Lampung_coa.png" height="130px"
                         width="130px">
                 </div>
                 <div align="center" style="margin-top:10px;">
                     <figure>Opportunities | 99</figure>
                 </div>
             </div>
         </div>

     </section>
 </section>

 <section id="tbody" class="ts-box p-1" style="margin-top:20px; margin-left:20px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div class="clearfix zink ">

         <!--Display selector on the left-->
         <div class="">

             <a id="ts-display-list" class="btn">
                 Investment Realization per Provinces
             </a>

               <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                                   
                                     <select style="color:grey;"  class="custom-select" id="sorting" name="sorting">
                                        <option    value="">Foreign Direct Investment (USD $)</option>
                                            <option value="1">Domestic Direct Investment (Million Rupiah) </option>

                                       
                                        
                                    </select>
                                </div>
         </div>

         <!--Display selector on the right-->


     </div>
     <br>

     <div id="app2"></div>

     <!--end container-->
 </section>

 <section id="tbody" class="ts-box p-1" style="margin-top:20px; margin-left:20px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div class="clearfix zink ">

         <!--Display selector on the left-->
         <div class="">

             <a id="ts-display-list" class="btn">
                 Online Investment Services
             </a>
         </div>

         <!--Display selector on the right-->


     </div>
     <br>

    
 <section id="tbody" class="ts-box p-1" style="margin-top:20px;  margin-left:20px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <br>
     <div class="row">




         <div style="left:80px; margin-right:22px;" class="col-sm-2 col-6">
             <div>
                 <img style="float: left; padding:10px; right:40px;" class="img-esicon"
                     src="//www.bkpm.go.id/assets/icon/i-track.png">
             </div>

             <h4>SPIPISE</h4>
             <p>
                 Track your application
             </p>



         </div>
         <div style="left:80px; margin-right:22px;" class="col-sm-2">



             <div>
                 <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                     src="//www.bkpm.go.id/images/uploads/icon/Frame_2.png">
             </div>
             <h4>OSS</h4>
             <p>
                 Online Single Submission
             </p>




         </div>
         <div style="left:80px; margin-right:22px;" class="col-sm-2">
             <div>
                 <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                     src="//www.bkpm.go.id/images/uploads/icon/NSWI.png">
             </div>
             <h4>NSWI</h4>
             <p>
                 National for Investment
             </p>
         </div>
         <div style="left:80px; margin-right:22px;" class="col-sm-2">
             <div>
                 <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                     src="//www.bkpm.go.id/images/uploads/icon/insreport.png">
             </div>
             <h4>EIBN</h4>
             <p>
                 Industrial Estate
             </p>
         </div>
         <div style="left:80px; margin-right:22px;" class="col-sm-2">
             <div>
             <a href="https://digisign.bkpm.go.id:2727/verifikasi" >
                 <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                     src="//www.bkpm.go.id/images/uploads/icon/license.png"">
								</div>
                                
									<h4>License</h4>
							<p >
						License Verification
							</p>
						</div>
</div>
			</section>
	
        </section>

 <main id=" ts-main">
                 </main>



                 <script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
                 <script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
                 <script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
                 <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script><br>
                 <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
                 <script src="https://cdn.jsdelivr.net/npm/react-apexcharts@1.3.6/dist/react-apexcharts.iife.min.js">
                 </script>
              
                 <script src="{{url('map/html/assets/js/owl.carousel.min.js')}}"></script>


                 <script>
                     (function () {

                         "use strict";

                         if (document.getElementById("ts-header").classList.contains("fixed-top")) {
                             if (!document.getElementsByClassName("ts-homepage")[0]) {
                                 document.getElementById("ts-main").style.marginTop = document.getElementById(
                                         "ts-header")
                                     .offsetHeight + "px";
                             }
                         }

                     })();

                     $(document).ready(function ($) {
                         "use strict";

                         $('.navbar-nav .nav-link:not([href="#"])').on('click', function () {
                             $('.navbar-collapse').collapse('hide');
                         });

                         if ($(window).width() < 600) {
                             $("#ts-header").addClass("fixed-top");
                             $("#mobile").hide();
                         } else {

                         }

                         $(".ts-img-into-bg").each(function () {
                             $(this).css("background-image", "url(" + $(this).find("img").attr("src") +
                                 ")");
                         });

                         $("#testimonial-slider").owlCarousel({
                             items: 3,
                             itemsDesktop: [1000, 3],
                             itemsDesktopSmall: [990, 2],
                             itemsTablet: [768, 2],
                             itemsMobile: [650, 1],
                             pagination: true,
                             navigation: false,
                             autoPlay: true
                         });

                         $("[data-bg-color], [data-bg-image], [data-bg-pattern]").each(function () {
                             var $this = $(this);

                             if ($this.hasClass("ts-separate-bg-element")) {
                                 $this.append('<div class="ts-background">');

                                 // Background Color

                                 if ($("[data-bg-color]")) {
                                     $this.find(".ts-background").css("background-color", $this.attr(
                                         "data-bg-color"));
                                 }

                                 // Background Image

                                 if ($this.attr("data-bg-image") !== undefined) {
                                     $this.find(".ts-background").append(
                                         '<div class="ts-background-image">');
                                     $this.find(".ts-background-image").css("background-image", "url(" +
                                         $this
                                         .attr("data-bg-image") + ")");
                                     $this.find(".ts-background-image").css("background-size", $this
                                         .attr(
                                             "data-bg-size"));
                                     $this.find(".ts-background-image").css("background-position", $this
                                         .attr(
                                             "data-bg-position"));
                                     $this.find(".ts-background-image").css("opacity", $this.attr(
                                         "data-bg-image-opacity"));

                                     $this.find(".ts-background-image").css("background-size", $this
                                         .attr(
                                             "data-bg-size"));
                                     $this.find(".ts-background-image").css("background-repeat", $this
                                         .attr(
                                             "data-bg-repeat"));
                                     $this.find(".ts-background-image").css("background-position", $this
                                         .attr(
                                             "data-bg-position"));
                                     $this.find(".ts-background-image").css("background-blend-mode",
                                         $this.attr(
                                             "data-bg-blend-mode"));
                                 }

                                 // Parallax effect

                                 if ($this.attr("data-bg-parallax") !== undefined) {
                                     $this.find(".ts-background-image").addClass("ts-parallax-element");
                                 }
                             } else {

                                 if ($this.attr("data-bg-color") !== undefined) {
                                     $this.css("background-color", $this.attr("data-bg-color"));
                                     if ($this.hasClass("btn")) {
                                         $this.css("border-color", $this.attr("data-bg-color"));
                                     }
                                 }

                                 if ($this.attr("data-bg-image") !== undefined) {
                                     $this.css("background-image", "url(" + $this.attr(
                                         "data-bg-image") + ")");

                                     $this.css("background-size", $this.attr("data-bg-size"));
                                     $this.css("background-repeat", $this.attr("data-bg-repeat"));
                                     $this.css("background-position", $this.attr("data-bg-position"));
                                     $this.css("background-blend-mode", $this.attr(
                                         "data-bg-blend-mode"));
                                 }

                                 if ($this.attr("data-bg-pattern") !== undefined) {
                                     $this.css("background-image", "url(" + $this.attr(
                                             "data-bg-pattern") +
                                         ")");
                                 }

                             }
                         });

                         $(".ts-password-toggle").on("click", function () {
                             var $parent = $(this).closest(".ts-has-password-toggle");
                             var $this = $(this);
                             var $password = $parent.find("input");
                             if ($password.attr("type") === "password") {
                                 $password.attr("type", "text");
                                 $this.find("i").removeClass("fa-eye").addClass("fa-eye-slash");
                             } else {
                                 $password.attr("type", "password");
                                 $this.find("i").removeClass("fa-eye-slash").addClass("fa-eye");
                             }
                         });


                         $("select").each(function () {
                             isSelected($(this));
                         }).on("change", function () {
                             isSelected($(this));
                         });

                         if ($(".ts-video").length > 0) {
                             $(this).fitVids();
                         }

                         // Owl Carousel

                         var $owlCarousel = $(".owl-carousel");

                         if ($owlCarousel.length) {
                             $owlCarousel.each(function () {

                                 var items = parseInt($(this).attr("data-owl-items"), 10);
                                 if (!items) items = 1;

                                 var nav = parseInt($(this).attr("data-owl-nav"), 2);
                                 if (!nav) nav = 0;

                                 var dots = parseInt($(this).attr("data-owl-dots"), 2);
                                 if (!dots) dots = 0;

                                 var center = parseInt($(this).attr("data-owl-center"), 2);
                                 if (!center) center = 0;

                                 var loop = parseInt($(this).attr("data-owl-loop"), 2);
                                 if (!loop) loop = 0;

                                 var margin = parseInt($(this).attr("data-owl-margin"), 2);
                                 if (!margin) margin = 0;

                                 var autoWidth = parseInt($(this).attr("data-owl-auto-width"), 2);
                                 if (!autoWidth) autoWidth = 0;

                                 var navContainer = $(this).attr("data-owl-nav-container");
                                 if (!navContainer) navContainer = 0;

                                 var autoplay = parseInt($(this).attr("data-owl-autoplay"), 2);
                                 if (!autoplay) autoplay = 0;

                                 var autoplayTimeOut = parseInt($(this).attr(
                                     "data-owl-autoplay-timeout"), 10);
                                 if (!autoplayTimeOut) autoplayTimeOut = 5000;

                                 var autoHeight = parseInt($(this).attr("data-owl-auto-height"), 2);
                                 if (!autoHeight) autoHeight = 0;

                                 var fadeOut = $(this).attr("data-owl-fadeout");
                                 if (!fadeOut) fadeOut = 0;
                                 else fadeOut = "fadeOut";

                                 if ($("body").hasClass("rtl")) var rtl = true;
                                 else rtl = false;

                                 if (items === 1) {
                                     $(this).owlCarousel({
                                         navContainer: navContainer,
                                         animateOut: fadeOut,
                                         autoplayTimeout: autoplayTimeOut,
                                         autoplay: 1,
                                         autoheight: autoHeight,
                                         center: center,
                                         loop: loop,
                                         margin: margin,
                                         autoWidth: autoWidth,
                                         items: 1,
                                         nav: nav,
                                         dots: dots,
                                         rtl: rtl,
                                         navText: []
                                     });
                                 } else {
                                     $(this).owlCarousel({
                                         navContainer: navContainer,
                                         animateOut: fadeOut,
                                         autoplayTimeout: autoplayTimeOut,
                                         autoplay: autoplay,
                                         autoheight: autoHeight,
                                         center: center,
                                         loop: loop,
                                         margin: margin,
                                         autoWidth: autoWidth,
                                         items: 1,
                                         nav: nav,
                                         dots: dots,
                                         rtl: rtl,
                                         navText: [],
                                         responsive: {
                                             1368: {
                                                 items: items
                                             },
                                             992: {
                                                 items: 3
                                             },
                                             450: {
                                                 items: 2
                                             },
                                             0: {
                                                 items: 1
                                             }
                                         }
                                     });
                                 }

                                 if ($(this).find(".owl-item").length === 1) {
                                     $(this).find(".owl-nav").css({
                                         "opacity": 0,
                                         "pointer-events": "none"
                                     });
                                 }

                             });
                         }

                         // Magnific Popup

                         var $popupImage = $(".popup-image");

                         if ($popupImage.length > 0) {
                             $popupImage.magnificPopup({
                                 type: 'image',
                                 fixedContentPos: false,
                                 gallery: {
                                     enabled: true
                                 },
                                 removalDelay: 300,
                                 mainClass: 'mfp-fade',
                                 callbacks: {
                                     // This prevents pushing the entire page to the right after opening Magnific popup image
                                     open: function () {
                                         $(".page-wrapper, .navbar-nav").css("margin-right",
                                             getScrollBarWidth());
                                     },
                                     close: function () {
                                         $(".page-wrapper, .navbar-nav").css("margin-right", 0);
                                     }
                                 }
                             });
                         }

                         var $videoPopup = $(".video-popup");

                         if ($videoPopup.length > 0) {
                             $videoPopup.magnificPopup({
                                 type: "iframe",
                                 removalDelay: 300,
                                 mainClass: "mfp-fade",
                                 overflowY: "hidden",
                                 iframe: {
                                     markup: '<div class="mfp-iframe-scaler">' +
                                         '<div class="mfp-close"></div>' +
                                         '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                                         '</div>',
                                     patterns: {
                                         youtube: {
                                             index: 'youtube.com/',
                                             id: 'v=',
                                             src: '//www.youtube.com/embed/%id%?autoplay=1'
                                         },
                                         vimeo: {
                                             index: 'vimeo.com/',
                                             id: '/',
                                             src: '//player.vimeo.com/video/%id%?autoplay=1'
                                         },
                                         gmaps: {
                                             index: '//maps.google.',
                                             src: '%id%&output=embed'
                                         }
                                     },
                                     srcAction: 'iframe_src'
                                 }
                             });
                         }

                         $(".ts-form-email [type='submit']").each(function () {
                             var text = $(this).text();
                             $(this).html("").append("<span>" + text + "</span>").prepend(
                                 "<div class='status'><i class='fas fa-circle-notch fa-spin spinner'></i></div>"
                             );
                         });

                         $(".ts-form-email .btn[type='submit']").on("click", function () {
                             var $button = $(this);
                             var $form = $(this).closest("form");
                             var pathToPhp = $(this).closest("form").attr("data-php-path");
                             $form.validate({
                                 submitHandler: function () {
                                     $button.addClass("processing");
                                     $.post(pathToPhp, $form.serialize(), function (
                                         response) {
                                         $button.addClass("done").find(".status")
                                             .append(
                                                 response).prop("disabled", true);
                                     });
                                     return false;
                                 }
                             });
                         });

                         if ($("input[type=file].with-preview").length) {
                             $("input.file-upload-input").MultiFile({
                                 list: ".file-upload-previews"
                             });
                         }

                         if ($(".ts-has-bokeh-bg").length) {

                             $("#ts-main").prepend(
                                 "<div class='ts-bokeh-background'><canvas id='ts-canvas'></canvas></div>");
                             var canvas = document.getElementById("ts-canvas");
                             var context = canvas.getContext("2d");
                             var maxRadius = 50;
                             var minRadius = 3;
                             var colors = ["#5c81f9", "#66d3f7"];
                             var numColors = colors.length;

                             for (var i = 0; i < 50; i++) {
                                 var xPos = Math.random() * canvas.width;
                                 var yPos = Math.random() * 10;
                                 var radius = minRadius + (Math.random() * (maxRadius - minRadius));
                                 var colorIndex = Math.random() * (numColors - 1);
                                 colorIndex = Math.round(colorIndex);
                                 var color = colors[colorIndex];
                                 drawCircle(context, xPos, yPos, radius, color);
                             }
                         }

                         function drawCircle(context, xPos, yPos, radius, color) {
                             context.beginPath();
                             context.arc(xPos, yPos, radius, 0, 360, false);
                             context.fillStyle = color;
                             context.fill();
                         }

                         heroPadding();

                         var $scrollBar = $(".scrollbar-inner");
                         if ($scrollBar.length) {
                             $scrollBar.scrollbar();
                         }

                         initializeSly();
                         hideCollapseOnMobile();

                     });

                     ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                     // Functions
                     ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                     // On RESIZE actions

                     var resizeId;
                     $(window).on("resize", function () {
                         clearTimeout(resizeId);
                         resizeId = setTimeout(doneResizing, 250);
                     });

                     // Do after resize

                     function doneResizing() {
                         //heroPadding();
                         hideCollapseOnMobile();
                     }

                     function isSelected($this) {
                         if ($this.val() !== "") $this.addClass("ts-selected");
                         else $this.removeClass("ts-selected");
                     }

                     function initializeSly() {
                         $(".ts-sly-frame").each(function () {

                             var horizontal = parseInt($(this).attr("data-ts-sly-horizontal"), 2);
                             if (!horizontal) horizontal = 0;

                             var scrollbar = $(this).attr("data-ts-sly-scrollbar");
                             if (!scrollbar) scrollbar = 0;

                             $(this).sly({
                                 horizontal: horizontal,
                                 smart: 1,
                                 elasticBounds: 1,
                                 speed: 300,
                                 itemNav: 'basic',
                                 mouseDragging: 1,
                                 touchDragging: 1,
                                 releaseSwing: 1,
                                 scrollBar: $(scrollbar),
                                 dragHandle: 1,
                                 scrollTrap: 1,
                                 clickBar: 1,
                                 scrollBy: 1,
                                 dynamicHandle: 1
                             }, {
                                 load: function () {
                                     $(this.frame).addClass("ts-loaded");
                                 }
                             });
                         });
                     }

                     function heroPadding() {
                         var $header = $("#ts-header");
                         var $hero = $("#ts-hero");

                         if ($header.hasClass("fixed-top")) {
                             if ($hero.find(".ts-full-screen").length) {
                                 $hero.find(".ts-full-screen").css("padding-top", $(".fixed-top").height());
                             } else {
                                 $hero.css("padding-top", $(".fixed-top").height());
                             }
                         } else {
                             if ($hero.find(".ts-full-screen").length) {
                                 $hero.find(".ts-full-screen").css("min-height", "calc( 100vh - " + $header.height() +
                                     "px");
                             }
                         }
                     }

                     // Smooth Scroll

                     $(".ts-scroll").on("click", function (event) {
                         if (
                             location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') &&
                             location.hostname === this.hostname
                         ) {
                             var target = $(this.hash);
                             var headerHeight = 0;
                             var $fixedTop = $(".fixed-top");
                             target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                             if ($fixedTop.length) {
                                 headerHeight = $fixedTop.height();
                             }
                             if (target.length) {
                                 event.preventDefault();
                                 $('html, body').animate({
                                     scrollTop: target.offset().top - headerHeight
                                 }, 1000, function () {
                                     var $target = $(target);
                                     $target.focus();
                                     if ($target.is(":focus")) {
                                         return false;
                                     } else {
                                         $target.attr('tabindex', '-1');
                                         $target.focus();
                                     }
                                 });
                             }
                         }
                     });

                     function hideCollapseOnMobile() {
                         if ($(window).width() < 575) {
                             $(".ts-xs-hide-collapse.collapse").removeClass("show");
                         }
                     }
                 </script>
                 <script>
                     jQuery(document).ready(function ($) {
                         "use strict";
                         //  TESTIMONIALS CAROUSEL HOOK
                         $('#customers-testimonials').owlCarousel({
                             loop: true,
                             center: true,
                             items: 3,
                             margin: 0,
                             autoplay: true,
                             dots: true,
                             autoplayTimeout: 8500,
                             smartSpeed: 450,
                             responsive: {
                                 0: {
                                     items: 1
                                 },
                                 768: {
                                     items: 2
                                 },
                                 1170: {
                                     items: 3
                                 }
                             }
                         });
                     });
                 </script>
                 <script type="text/babel">
                     class ApexChart extends React.Component {
			constructor(props) {
				super(props);
	
				this.state = {
				
				series: [{
					data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
				}],
				options: {
					chart: {
					type: 'bar',
					height: 100
					},
					plotOptions: {
					bar: {
						horizontal: true,
					}
					},
					dataLabels: {
					enabled: false
					},
					xaxis: {
					categories: ['Sector A', 'Sector B', 'Sector C', 'Sector D', 'Sector E', 'Sector F', 'Sector G',
						'Sector H', 'Sector I', 'Sector J'
					],
					}
				},
				
				
				};
			}
	
			
	
			render() {
				return (
				<div>
					<div id="chart">
					<ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={350} />
					</div>
					<div id="html-dist"></div>
				</div>
				);
			}
			}
	
			const domContainer = document.querySelector('#app');
			ReactDOM.render(React.createElement(ApexChart), domContainer);
		</script>
                 <script type="text/babel">
                     class ApexChart extends React.Component {
			constructor(props) {
				super(props);
	
				this.state = {
				
				series: [{
					data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
				}],
				options: {
					chart: {
					type: 'bar',
					height: 200
					},
					plotOptions: {
					bar: {
						horizontal: true,
					}
					},
					dataLabels: {
					enabled: false
					},
					xaxis: {
					categories: ['Sector A', 'Sector B', 'Sector C', 'Sector D', 'Sector E', 'Sector F', 'Sector G',
						'Sector H', 'Sector I', 'Sector J'
					],
					}
				},
				
				
				};
			}
	
			
	
			render() {
				return (
				<div>
					<div id="chart">
					<ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={350} />
					</div>
					<div id="html-dist"></div>
				</div>
				);
			}
			}
	
			const domContainer = document.querySelector('#app');
			ReactDOM.render(React.createElement(ApexChart), domContainer);
		</script>
                 <script type="text/babel">
                     class ApexChart extends React.Component {
		constructor(props) {
			super(props);

			this.state = {
			
			series: [{
				data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380, 400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380, 400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380 , 690, 1100, 1200, 1380]
			}],
			options: {
				chart: {
				type: 'bar',
				height: 1000
				},
				plotOptions: {
				bar: {
					horizontal: false,
				}
				},
				dataLabels: {
				enabled: false
				},
				xaxis: {
				categories: ['Aceh',
                      'Bali',
                      'Bangka-Belitung',
                      'Banten',
                      'Bengkulu',
                      'Daerah Istimewa Yogyakarta',
                      'DKI Jakarta',
                      'Gorontalo',
                      'Jambi',
                      'Jawa Barat',
                      'Jawa Tengah',
                      'Jawa Timur',
                      'Kalimantan Barat',
                      'Kalimantan Selatan',
                      'Kalimantan Tengah',
                      'Kalimantan Timur',
                      'Kalimantan Utara',
                      'Kepulauan Riau',
                      'Lampung',
                      'Maluku',
                      'Maluku Utara',
                      'Nusa Tenggara Barat',
                      'Nusa Tenggara Timur',
                      'Papua',
                      'Papua Barat',
                      'Riau',
                      'Sulawesi Barat',
                      'Sulawesi Selatan',
                      'Sulawesi Tengah',
                      'Sulawesi Tenggara',
                      'Sulawesi Utara',
                      'Sumatera Barat',
                      'Sumatera Selatan',
                      'Sumatera Utara'
                         
					],
				}
			},
			
			
			};
		}

		

		render() {
			return (
			<div>
				<div id="chart">
				<ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={350} />
				</div>
				<div id="html-dist"></div>
			</div>
			);
		}
		}

		const domContainer = document.querySelector('#app2');
		ReactDOM.render(React.createElement(ApexChart), domContainer);
	</script>

                 @endsection