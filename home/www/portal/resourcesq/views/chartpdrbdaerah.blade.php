       <!-- Include fusioncharts core library file -->
   
       

 <div id="ssssss">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
      FusionCharts.ready(function(){
                    var chr22zz = new FusionCharts({
            type: 'scrollcombidy2d',
            renderAt: 'ssssss',
            width: '100%',
            height: '430',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fusion",
                    "caption": "Gross Regional Domestic Product Last year",
                    "subCaption": "(2015 to 2020)",
                    "showDivLineSecondaryValue":"0",
                    "showSecondaryLimits":"0",
                    "numberScaleValue": "1,1,1",
                    "numberScaleUnit": " , , ",
                    "showAxisValue":"0",
                    "palettecolors": "5d62b5,29c3be,f2726f",
                        "valuePadding": "5",
                    "numberPrefix": "Rp ",
                    "labelPadding":'20',
                   " plotSpacePercent":'170',
                  
                   
                    "numVisiblePlot": "12",
                    "flatScrollBars": "1",
                    "scrollheight": "10"
                },
                "categories": [{
                    "category": [@foreach($investasi as $chart){ 
                            "label": "Sektor {{$chart->sektor}}"
                        }, @endforeach
                       
                    ]
                }],
                "dataset": [{
                        "color":"0088ff",
                        "seriesName": "PDRB",
                        "labelStep": "2",
                        "data": [@foreach($investasi as $chart){
                      
                              
                                "value": "{{$chart->jumlah}}"
                              
                            }, @endforeach
                           
                        ]
                    },
                    {
                        
                        
                        "data": [@foreach($investasi as $chart){
                      
                            "color":"0088ff",
                                "value": "{{$chart->jumlah}}",
                                
                            }, @endforeach
                           
                        ]
                    },
                
                  
                ]
            }
        });
                    chr22zz.render();
                });
            </script>