

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<nav id="mobilez" class="nav ts-z-index__2">
  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">dashboard</i>
    <span class="nav__text">Dashboard</span>
  </a>

  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">devices</i>
    <span class="nav__text">Devices</span>
  </a>
  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">lock</i>
    <span class="nav__text">Privacy</span>
  </a>
  <a href="#" class="nav__link">
    <i class="material-icons nav__icon">settings</i>
    <span class="nav__text">Settings</span>
  </a>
</nav>

<div class="container-fluid footer">
			<div class="foot-content">
			<div id="mobile">
				
<div class="row">
  <div class="col-sm-3"><h4>
							<b>&nbsp;About PIR</b>
						</h4>
						<ul>
							<li>
								<h4 href="en/about-bkpm/profile-of-institution/index.htm">
									<b>&nbsp;Regional Opportunity</b>
								</h4>
							</li>
							<li>
								<h4 href="en/about-bkpm/timeline/index.htm">
									<b>&nbsp;Contact us</b>
								</h4>
							</li>
						</ul></div>
  <div class="col-sm-2"><h4><b>Project Opportunity</b></h4>
						<ul>
							<li>
								<a href="en/ppid/profile-ppid/index.htm">· Project Offering</a>
							</li>
							<li>
								<a href="en/ppid/statment/index.htm">· Project Portfolio</a>
							</li>
							<BR>
							<H4>
								<B href="en/ppid/periodical-information/index.htm">Infrastructure</B>
							</H4>
							<li>
								<a href="en/ppid/general-information/index.htm">· Infrastructure Highlight</a>
							</li>
							<li>
								<a href="en/ppid/information-service-mechanism/index.htm">· Industrial Estate</a>
							</li>
							
						</ul></div>
  <div class="col-sm-2"><h4><b>Investment Incentive</b></h4>
						<ul class="mhf">
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/procedure\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/procedure'"
									target="_blank">· Tax Holiday</a>
							</li>
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/incentives\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/incentives'"
									target="_blank">· Tax Allowance</a>
							</li>
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service'"
									target="_blank">· Masterlist</a>
							</li>
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service'"
									target="_blank">· Super Deduction</a>
							</li>
							<br>
						
						
						</ul></div>
  <div class="col-sm-3"><h4><b>Online Investment Services</b></h4>
						<ul class="mhf">
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/procedure\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/procedure'"
									target="_blank">· SPIPISE Tracking</a>
							</li>
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/incentives\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/incentives'"
									target="_blank">· Online Single Submission</a>
							</li>
							<li>
								<a  CLASS="justify" href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service'"
									target="_blank">· National Single Window for <BR> &nbsp;&nbsp;Investment</a>
							</li>
							<li>
								<a href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service'"
									target="_blank">· License Verification</a>
							</li>
							<li>
								<a align="justify"  href="javascript:if(confirm('https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service\n\nThis file was not retrieved because it was filtered out by your project settings.\n\nWould you like to open it from the server?'))window.location='https://www.investindonesia.go.id/en/how-we-can-help/one-stop-service'"
									target="_blank">· Indonesia Industrial Estate &nbsp;&nbsp;Directory</a>
							</li>
							<br>
						
						
						</ul></div>
  <div class="col-sm-2"><h4><b>Partnership</b></h4>
						<ul class="mhf">
							<li>
								<a href="javascript:if(confirm('http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpma'"
									target="_blank">· PMA</a>
							</li>
							<li>
								<a href="javascript:if(confirm('http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpmdn'"
									target="_blank">· PMDN</a>
							</li>
							<li>
								<a href="javascript:if(confirm('http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fumkm'"
									target="_blank">· UMKM</a>
							</li>
						
							<br>
						
						
						</ul>
                        
                        </div>
						</div>
                       
</div>
<hr>
<div class="row">
  <div class="col-sm-4"><strong>© 2020 -All rights reserved</strong>
						<br>
						INDONESIA INVESTMENT COORDINATING BOARD
						<br>
						Jl. Jend. Gatot Subroto No. 44, Jakarta 12190
						<br>
						P.O. Box 3186, Indonesia
						<br></div>
  <div class="col-sm-2"><ul>
							<li>
								<span class="green">P.</span> +6221 525 2008 (Hunting)
								<br>
							</li>
							<li>
								<span class="green">F.</span> +6221 525 4945

							</li>
							<li>
								<span class="green" >C.</span> 0807 100 2576 (Contact
								Center)

							</li>
						</ul></div>
  <div class="col-sm-2"><h5>Follow BKPM on :</h5>
					
</div>
	</footer>

	</div>

	<script>


$(document).ready(function(event, state){
	 $("#mobilez").hide();
     if ($(window).width() < 600) {
                             $("#ts-header").addClass("fixed-top");
                             $("#mobile").hide();
							 $("#mobilez").show();
                         } else {

                         }
						 });
						 </script>