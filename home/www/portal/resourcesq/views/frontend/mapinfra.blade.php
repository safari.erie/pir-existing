
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<title>FeatureLayer</title>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

    <script> 
        var map;
        require([
          "esri/map",
           "esri/dijit/PopupTemplate",
             "esri/layers/FeatureLayer",
            "dojo/_base/array",
            "esri/layers/ArcGISDynamicMapServiceLayer",
            "esri/geometry/Geometry",
            "esri/geometry/Point",
            "esri/geometry/webMercatorUtils",
            "esri/graphic",
            "esri/symbols/SimpleMarkerSymbol",
            "esri/symbols/SimpleLineSymbol",
            "esri/symbols/SimpleFillSymbol",
            "esri/symbols/PictureMarkerSymbol",
            "esri/Color",
            "esri/InfoTemplate",
            "esri/dijit/Popup",
           "esri/dijit/BasemapGallery",
            "esri/arcgis/utils",
          "dojo/parser",
    
          "dijit/layout/BorderContainer", 
          "dijit/layout/ContentPane", 
          "dijit/TitlePane",
          "dojo/domReady!"
        ], function(
          Map,
                PopupTemplate, 
             FeatureLayer, 
             arrayUtils, 
             ArcGISDynamicMapServiceLayer, 
             Geometry, 
             Point, 
             webMercatorUtils,  
             Graphic, 
             SimpleMarkerSymbol, 
             SimpleLineSymbol, 
             SimpleFillSymbol,
             PictureMarkerSymbol,  
             Color, 
             InfoTemplate,
             Popup,
           BasemapGallery,
            arcgisUtils,
          parser
        ) {
          parser.parse();
    
         
@foreach($propertiesx as $list){
      map = new Map("map", {
          basemap: "hybrid",
          center: [{{$list->y}}, {{$list->x}}],
          zoom: 7
      });
}
@endforeach
    
          //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
          var basemapGallery = new BasemapGallery({
            showArcGISBasemaps: true,
            map: map
          }, "basemapGallery");
          basemapGallery.startup();
          
          basemapGallery.on("error", function(msg) {
            console.log("basemap gallery error:  ", msg);
          });
    
          
          var title ='<div align="center">RTRW Kabupaten</div>';
    var isi ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'
    
               var popupTemplate = new PopupTemplate({
                title: title,
              description: isi
              
            });
    
              $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken",
        {
          username: 'bkpm-sipd',
          password: 'Bkpm_S1pd@atrbpn123'
         
          
          
          
        },
   
        function as(data,status){
          
            var key = data;
            var token='?token='+key+'';
            @foreach($bentuk as $list)
                  var linkz='{{$list->rtrw}}'                     
                          @endforeach              
          var rtrw = new FeatureLayer(linkz+ token,{
          
              outFields: ["*"],
              opacity:0.5,
        infoTemplate: popupTemplate
         });
         map.addLayer(rtrw);
         rtrw.hide();
         
         $('input:checkbox[name=dadan]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      rtrw.show();
                      
                  
                      // map.graphics.hide();  
                  } else {
                    rtrw.hide();
                  }
    
                });
          

         
    var featureLayer = '';
 
    var ay =  '';
   
    
                  
    
    
    @foreach($pgis as $list)
    var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Name :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Category:   </div><dd style="margin-left:70px;  "class="">{{$list->
               kategori}}</dd> '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail University</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/College-University.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer2 = new esri.layers.GraphicsLayer();
                  layer2.add(graphic);
                  map.addLayer(layer2);
                
                  $('#openmap{{$list->id_pendidikan}}').on('click', function () {
              var center{{$list->id_pendidikan}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_pendidikan}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_pendidikan}} ,17);
                  
      });
                @endforeach
                @foreach($pbngis as $list)
                
    var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Name :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Category :   </div><dd style="margin-left:70px;  "class="">{{$list->
               kelas}}</dd> '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail University</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Marina.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer2 = new esri.layers.GraphicsLayer();
                  layer2.add(graphic);
                  map.addLayer(layer2);

                  
           
                @endforeach
                @foreach($rmgis as $list)
    var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Name :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Category :   </div><dd style="margin-left:70px;  "class="">{{$list->
               kategori}}</dd> '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail University</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/First-Aid.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer2 = new esri.layers.GraphicsLayer();
                  layer2.add(graphic);
                  map.addLayer(layer2);

                  
                  $('#openmaprumahsakit{{$list->id_rumah_sakit}}').on('click', function () {
              var center{{$list->id_rumah_sakit}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_rumah_sakit}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_rumah_sakit}} ,17);
                  
      });
                @endforeach
    @foreach($hotel as $list)
    var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Hotel Name :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Hotel Class :   </div><dd style="margin-left:70px;  "class="">{{$list->
               kelas}}</dd> '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/OCHA/Infrastructure/infrastructure_hotel_bluebox.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer1 = new esri.layers.GraphicsLayer();
                  layer1.add(graphic);
                  map.addLayer(layer1);

                  
                  $('#openmap{{$list->id_hotel}}').on('click', function () {
              var center{{$list->id_hotel}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
              map.centerAt(center{{$list->id_hotel}});
              document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
       map.centerAndZoom(center{{$list->id_hotel}} ,17);
                  
      });

@endforeach
   
        
                <?php foreach ($wilayah as $list): ?>
                      @foreach($bentuk as $lisa)
                      
               @if($list->kategori == 'Internasional Airport')
               var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Kategori Bandara :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               kategori}}</dd> <div style="float:left;"  >Nama Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Kelas Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               kelas}}</dd> <div style="float:left;"  >Jam Operasional :   </div><dd style="margin-left:100px;  "class="">{{$list->jam_operasional}}</dd>   '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Transportation/esriDefaultMarker_113_Blue.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer1 = new esri.layers.GraphicsLayer();
                  layer1.add(graphic);
                  map.addLayer(layer1);
          @elseif($list->kategori == 'Domestik Airport')

          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Kategori Bandara :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               kategori}}</dd> <div style="float:left;"  >Nama Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Kelas Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               kelas}}</dd> <div style="float:left;"  >Jam Operasional :   </div><dd style="margin-left:100px;  "class="">{{$list->jam_operasional}}</dd>   '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Transportation/esriDefaultMarker_113_Red.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer1 = new esri.layers.GraphicsLayer();
                  layer1.add(graphic);
                  map.addLayer(layer1);
                  
        
        
            
       
          
             
                  @elseif($list->kategori == 'Internasional Airport, Embarkas Haji')

                  var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
               var content ='<div style="float:left;"  >Kategori Bandara :   </div><dd style="margin-left:60px; text-align:justify; "class="">{{$list->
               kategori}}</dd> <div style="float:left;"  >Nama Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               nama}}</dd> <div style="float:left;"  >Kelas Bandara :   </div><dd style="margin-left:100px;  "class="">{{$list->
               kelas}}</dd> <div style="float:left;"  >Jam Operasional :   </div><dd style="margin-left:100px;  "class="">{{$list->jam_operasional}}</dd>   '
         
                  point = esri.geometry.geographicToWebMercator(point);
                  var title ='<div align="center">Detail Project</div>'
                  var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Transportation/esriDefaultMarker_113_Blue.png", 21, 21);
                  pointInfoTemplate = new InfoTemplate(); 
                  pointInfoTemplate.setTitle(title);
                  pointInfoTemplate.setContent(content);
                  var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
                  layer1 = new esri.layers.GraphicsLayer();
                  layer1.add(graphic);
                  map.addLayer(layer1);
                
            
    

            @endif
           
            $('#openmap{{$list->id_bandara}}').on('click', function () {
              var center{{$list->id_bandara}} = new esri.geometry.Point({{$list->y}}, {{$list->x}});
      
       map.centerAndZoom(center{{$list->id_bandara}} ,17);
                  
      });

              @endforeach
               @endforeach
    
              });
              $('input:checkbox[name=dadans]').change(function () {
                  if ($(this).is(':checked')) {
                      //map.graphics(layer1).hide();
                      featureLayer.hide();
                      
                  
                      // map.graphics.hide();  
                  } else {
                      featureLayer.show();
                  }
    
                  function hideLayer1() {
    
                      dd.push(layer1);
                      
                  }
    
                  function hideLayer2() {}
              });
 
    
          });
        
      </script> 
</head>

<body>
<div data-dojo-type="dijit/layout/BorderContainer" 
                                data-dojo-props="design:'headline', gutters:false" 
                                style="width:100%;height:100%;margin:0;">
                                <div id="map" 
                                data-dojo-type="dijit/layout/ContentPane" 
                                data-dojo-props="region:'center'" 
                                style="padding:0;">
                               
                             
                
                </div>
            </div>

</body>

</html>
