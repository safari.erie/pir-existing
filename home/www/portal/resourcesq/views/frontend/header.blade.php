  <!--*********************************************************************************************************-->
    <!--HEADER **************************************************************************************************-->
    <!--*********************************************************************************************************-->
    <!DOCTYPE html>
    
  <style>
  .img {
right:10px;
align:left;
  }

  .asu {
      color: rgba(0, 0, 0, 0.747);
  }
  </style>
  
    <header id="ts-header" class="head">

        <!-- SECONDARY NAVIGATION
        ================""=============================================================================================-->
     <nav id="ts-secondary-navigation" class="navbar p-0">
            <div class="container justify-content-end justify-content-sm-between">

                <!--Left Side-->
                <div class="navbar-nav asu d-none d-sm-block">
                    <!--Phone-->
                    <span class="asu mr-4">
                        " Welcome to Indonesia Investement Coordinating Board"
                          
                        </span>
                    <!--Email-->
                  
                </div>
                <div>
                   
               <a >
                   
                </a></div>
               
                <!--Right Side-->
                <div class="navbar-nav flex-row">

                 
               
                    <!--Language Select-->
                    <select class="custom-select asu bg-transparent ts-text-small border-left border-right ts-selected" id="language" name="language">
                        <option value="1"> Select Language : English</option>
                        <option value="2"> Select Language : Indonesia</option>
                        
                    </select>
                   
 <li class="asu">
   
                            <a class="btn asu btn-outline-primary btn-sm m-1 px-3" href="submit.html">
                                <i class="fa fa-lock small asu mr-2"></i>
                                Login Admin
                            </a>
                        </li>
                </div>
                <!--end navbar-nav-->
            </div>
            <!--end container-->
        </nav>
        <!--PRIMARY NAVIGATION
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
            <div class="container">

                <!--Brand Logo-->
                <a  href="index-map-leaflet-fullscreen.html">
                    <img src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg" class=""  width="160px" alt="">
                </a>

                <!--Responsive Collapse Button-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Collapsing Navigation-->
                <div class="collapse navbar-collapse" id="navbarPrimary">

                    <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav">

                        <!--HOME (Main level)
                        =============================================================================================-->
                        <li class="nav-item ">

                            <!--Main level link-->
                            <a style="margin-left:11px;"class="nav-link active" href="{{config::get('app.url')}}/homes">
                                Home
                            
                            </a>

                      
                        <li class="nav-item ">

                            <!--Main level link-->
                            <a style="margin-left:11px;"class="nav-link" href="{{config::get('app.url')}}/profil">Regional Profile</a>

                            <!-- List (1st level) -->
                           
                        <li class="nav-item ">

                            <!--Main level link-->
                            <a style="margin-left:11px;" class="nav-link" href="{{config::get('app.url')}}/peluang">Projects Opportunity</a>

                            <!-- List (1st level) -->
                         
                        <!--end PAGES nav-item-->

                        <!--ABOUT US (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a style="margin-left:11px;"class="nav-link" href="{{config::get('app.url')}}/peluangdaerah">Regional Opportunity</a>
                        </li>
                        <!--end ABOUT US nav-item-->

                        <!--CONTACT (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a style="margin-left:11px;" class="nav-link mr-2" href="{{config::get('app.url')}}/infranasional">Infrastructure</a>
                        </li>
                        <!--end CONTACT nav-item-->
<li class="nav-item">
                            <a style="margin-left:11px;"class="nav-link mr-2" href="contact.html">Investment Incentive</a>
                        </li>
                        <li class="nav-item">
                            <a style="margin-left:11px;"class="nav-link mr-2" href="contact.html">UMKM</a>
                            <ul class="ts-child">

                                <!-- AGENCY (1st level)
                                =====================================================================================-->
                                <li class="nav-item ts-has-child">

                                    <a href="#" class="nav-link">UMKM</a>
                                    <a href="#" class="nav-link">PMA</a>
                                    <a href="#" class="nav-link">PMDN</a>


                                </li>
                            </ul>
                        </li>
                       
                    </ul>
                    <!--end Left navigation main level-->

                    <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                  
                    <!--end Right navigation-->

                </div>
                <!--end navbar-collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end #ts-primary-navigation.navbar-->

    </header>
    <!--end Header-->