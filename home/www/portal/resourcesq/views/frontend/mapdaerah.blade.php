 <!-- MAIN ***************************************************************************************************-->




<!--*********************************************************************************************************-->


 <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

<!--*********************************************************************************************************-->


			<div class="loader-section section-left"></div>
			<div class="loader-section section-right"></div>
		</div>
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css">
    <link rel="stylesheet" href="https://raw.githubusercontent.com/lvoogdt/Leaflet.awesome-markers/2.0/develop/dist/leaflet.awesome-markers.css">
    <script src="https://raw.githubusercontent.com/lvoogdt/Leaflet.awesome-markers/2.0/develop/dist/leaflet.awesome-markers.js"></script>
<div id="map" style="width: 100%; height:440px;   "></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
<script src="https://torfsen.github.io/leaflet.zoomhome/dist/leaflet.zoomhome.min.js"></script>

<script>


$(document).ready(function(event, state){
    
  

          setTimeout(function(){
		$('body').addClass('loaded');
		$('h1').css('color','')
	},1000);
      
   if(state){
           map.addLayer(bandara);

            
        }
        else {
             map.removeLayer(bandara);
        }
     
         if(state){
           map.addLayer(marker);

            
        }
        else {
             map.removeLayer(marker);
        }
  
});

 

</script>
<script>
var cities = L.layerGroup();
var pangans = L.layerGroup();
var indus = L.layerGroup();
var jasas = L.layerGroup();
var LeafIcon = L.Icon.extend({
    options: {
     
        iconSize:     [22, 21],
        
    }
});

  var LeafIconz = L.Icon.extend({
    options: {
     
        iconSize:     [25, 36],
        
    }
});



var  factory = new LeafIcon({iconUrl: 'https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png'});
var Infra = new LeafIcon({iconUrl: 'https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png'}),
    jasa = new LeafIcon({iconUrl: 'http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png'}),
    pangan = new LeafIcon({iconUrl: 'https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png'});
   

<?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Infrastruktur') { ?>
	var marker1 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: Infra}).bindPopup('<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->id_parent)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Daerah :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->bentuk_daerah}} {{$list->
             nama}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>').addTo(cities);
             
         
     marker1.on('click', function(e){
      map.setView(new L.LatLng([<?php echo $list->x ?>, <?php echo $list->y  ?>]), 5);
 markers.addLayer(marker1);
 map.addLayer(markers);
 });
 <?php } ?>
 
<?php endforeach; ?>
<?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Industri') { ?>
	var marker2 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: factory}).bindPopup('<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->id_parent)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Daerah :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->bentuk_daerah}} {{$list->
             nama}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>').addTo(indus);
 marker2.on('click', function(e){
  map.setView(new L.LatLng([<?php echo $list->x ?>, <?php echo $list->y  ?>]), 5);
 markers.addLayer(marker2);
 map.addLayer(markers);
 });
 <?php } ?>
 
<?php endforeach; ?>
<?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Pangan dan Pertanian') { ?>
	var marker3 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: pangan}).bindPopup('<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->id_parent)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Daerah :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->bentuk_daerah}} {{$list->
             nama}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>').addTo(pangans);
 marker3.on('click', function(e){
 map.setView(new L.LatLng([<?php echo $list->x ?>, <?php echo $list->y  ?>]), 5);
 markers.addLayer(marker3);
 map.addLayer(markers);
 });
 <?php } ?>
 
<?php endforeach; ?>
<?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Jasa') { ?>
	var marker4 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: jasa}).bindPopup('<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->id_parent)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>').addTo(jasas);
 marker4.on('click', function(e){
  map.setView(new L.LatLng([<?php echo $list->x ?>, <?php echo $list->y  ?>]), 5);
 markers.addLayer(marker4);
 map.addLayer(markers);
 });
 <?php } ?>




 <?php endforeach; ?>
	var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

	var grayscale   = L.esri.basemapLayer('Imagery'),
		streets  =  L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

var googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

	var map = L.map('map', {
		center: [-2.3270756,114.4842481,5],
		zoom: 5,
		layers: [grayscale, cities, pangans, indus, jasas]
	});

  map.zoomControl.remove();
 var zoomHome = L.Control.zoomHome();
                zoomHome.addTo(map);
  var icon = L.icon({
    iconUrl: 'https://cdn4.iconfinder.com/data/icons/map-pins-2/256/1-512.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11]
  });

  
	var baseLayers = {
		"Grayscale": grayscale,
		"GoogleStreets ": googleStreets ,
      "Google":googleHybrid 
	};

	var overlays = {
	
      
      
      
	};

 

	L.control.layers(baseLayers, overlays).addTo(map);
var wildfireRisk = new L.esri.featureLayer({
   url: 'https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/0',
   style: function () {
      return {
         color: "#70ca49",
         weight: 0.5,
         fillOpacity: -0.1
         
      }
   }
}).addTo(map);




var popupTemplate = "<h3>{provinsi}</h3>";

wildfireRisk.bindPopup(function (layer){
   return L.Util.template(popupTemplate, layer.feature.properties)
});

   var featureGroup = L.featureGroup();
  var icon = L.icon({
    iconUrl: 'https://cdn4.iconfinder.com/data/icons/map-pins-2/256/1-512.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11]
  });
    var bandara = new L.esri.featureLayer({
   url: 'https://gis.bnpb.go.id/server/rest/services/SIAGA_MENTAWAI/SEBARAN_INFRASTRUKTUR/MapServer/4',
   where: "RuleID = 3",
  
    pointToLayer: function (geojson, latlng) {
      return L.marker(latlng, {
        icon: icon
      });
   }
}).addTo(map)

var popupbandara = '<p>Nama Bandara :{NMBDR}<p>';

bandara.bindPopup(function (layer){
   return L.Util.template(popupbandara, layer.feature.properties)
});


</script>
