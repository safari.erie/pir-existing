 <!-- MAIN ***************************************************************************************************-->




<!--*********************************************************************************************************-->


 <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
  
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin="" />
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
<script src="https://torfsen.github.io/leaflet.zoomhome/dist/leaflet.zoomhome.min.js"></script>


<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />


<!-- Load Esri Leaflet from CDN -->
<script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
    crossorigin=""></script>

			<div class="loader-section section-left"></div>
			<div class="loader-section section-right"></div>
		</div>
      
<div id="map" style="width: 100%; height:780px;   "></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
<script src="https://torfsen.github.io/leaflet.zoomhome/dist/leaflet.zoomhome.min.js"></script>

<script>


$(document).ready(function(event, state){
marker.remove();

          setTimeout(function(){
		$('body').addClass('loaded');
		$('h1').css('color','')
	},1000);
      
   if(state){
           map.addLayer(bandara);

            
        }
        else {
             map.removeLayer(bandara);
        }
       if(state){
           map.addLayer(wildfireRisk );

            
        }
        else {
             map.removeLayer(wildfireRisk );
        }
         if(state){
           map.addLayer(marker);

            
        }
        else {
             map.removeLayer(marker);
        }
  
});

 

</script>
<script>



    var map = L.map('map', {
                                      center: [-8.8402817,113.912959,5],
                                      zoom: 5,
                                      zoomControl: false,
                                   

                   scrollWheelZoom: true,
                });   
                
  L.esri.basemapLayer('Imagery').addTo(map);
   L.esri.basemapLayer('ImageryLabels').addTo(map);
   var zoomHome = L.Control.zoomHome();
                zoomHome.addTo(map);
var wildfireRisk = new L.esri.featureLayer({
   url: 'https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/0',
   style: function () {
      return {
         color: "#70ca49",
         weight: 0.5,
         fillOpacity: -0.1
         
      }
   }
}).addTo(map);




var popupTemplate = "<h3>{provinsi}</h3>";

wildfireRisk.bindPopup(function (layer){
   return L.Util.template(popupTemplate, layer.feature.properties)
});

var featureGroup = L.featureGroup();
  var icon = L.icon({
    iconUrl: 'https://cdn4.iconfinder.com/data/icons/map-pins-2/256/1-512.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11]
  });
var bandara = new L.esri.featureLayer({
   url: 'https://gis.bnpb.go.id/server/rest/services/SIAGA_MENTAWAI/SEBARAN_INFRASTRUKTUR/MapServer/4',
   where: "RuleID = 3",
  
    pointToLayer: function (geojson, latlng) {
      return L.marker(latlng, {
        icon: icon
      });
   }
}).addTo(map)

var popupbandara = '<p>Nama Bandara :{NMBDR}<p>';

bandara.bindPopup(function (layer){
   return L.Util.template(popupbandara, layer.feature.properties)
});

  var LeafIcon = L.Icon.extend({
    options: {
     
        iconSize:     [25, 37],
        
    }
});

  var LeafIconz = L.Icon.extend({
    options: {
     
        iconSize:     [25, 36],
        
    }
});
var pop = ''
var  factory = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=industry&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
var Infra = new LeafIcon({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=building&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'}),
    jasa = new LeafIcon({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=handshake&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'}),
    pangan = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=seedling&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
   
<?php foreach ($propertiesz as $list): ?>
var myid = "thismarker"; // or whatever

      var marker = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: Infra});

      marker.addTo(map);
      <?php if($list->sektor == 'Infrastruktur') { ?>
         marker.bindPopup(
             '<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->judul)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>');
           marker.on('click', function(e){
              
 map.setView([<?php echo $list->x ?>, <?php echo $list->y  ?>], 17);



});
<?php } ?>
      <?php endforeach; ?>
     
     <?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Jasa') { ?>
      var marker2 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: jasa}).addTo(map);
         marker2.bindPopup(
             '<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->judul)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>');
           marker2.on('click', function(e){
 map.setView([<?php echo $list->x ?>, <?php echo $list->y  ?>], 17);
 markers.addLayer(marker2);
 map.addLayer(markers);


});
<?php } ?>
      <?php endforeach; ?>
     
   <?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Pangan dan Pertanian') { ?>
      var marker2 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: pangan}).addTo(map);
         marker2.bindPopup(
             '<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->judul)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>');
           marker2.on('click', function(e){
 map.setView([<?php echo $list->x ?>, <?php echo $list->y  ?>], 17);
 markers.addLayer(marker2);
 map.addLayer(markers);


});
<?php } ?>
      <?php endforeach; ?>

   <?php foreach ($propertiesz as $list): ?>
<?php if($list->sektor == 'Industri') { ?>
      var marker2 = L.marker([<?php echo $list->x ?>, <?php echo $list->y  ?>], {icon: factory}).addTo(map);
         marker2.bindPopup(
             '<div class="zink " align="center"><i class="fa fa-search"> Peluang Investasi</i></div><div class="zink"><a href="<?php echo url("property/" . $list->id_info_peluang_da . "/" . 
             clean($list->judul)); ?>"> <dd style="color:white; text-align:justify; "class=""><div style="float:left;"  >Sektor :</div> <dd style="margin-left:75px;   color:white;  " class="border-bottom pb-2" >{{$list->
             sektor}}</dd><div style="float:left; color:white;"  >Judul :</div> <dd style="margin-left:75px; margin-bottom:10px; color:white;  "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left; color:white;"  >Tahun :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" >{{$list->
             tahun}}</dd><div style="float:left; color:white;"  >Nilai :</div> <dd style="margin-left:75px;  color:white;  "class="border-bottom pb-2" > <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd><button class="zink hijau">Read More</button></dd></div>');
           marker2.on('click', function(e){
 map.setView([<?php echo $list->x ?>, <?php echo $list->y  ?>], 17);
 markers.addLayer(marker2);
 map.addLayer(markers);


});
<?php } ?>
      <?php endforeach; ?>
  var states = L.esri.featureLayer({
    url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer/3',
    style: function (feature) {
      return { color: '#bada55', weight: 2 };
    }
  });

var cities = L.layerGroup();
	var baseLayers = {
		"Grayscale": grayscale,
		"Streets": streets
	};

	var overlays = {
		"Cities": cities
	};

	L.control.layers(baseLayers, overlays).addTo(map);
  
</script>
