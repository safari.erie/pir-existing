
   <!-- Include fusioncharts core library file -->


 <div id="chart-container">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
                FusionCharts.ready(function(){
                    var chartObj = new FusionCharts({
            type: 'scrollcombidy2d',
            renderAt: 'chart-container',
            width: '100%',
            height: '430',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fusion",
                    "caption": "Investment Realization per Provinces in 5 years",
                    "subCaption": "(2015 to 2020)",
                 
                    "numberScaleValue": "10,100,1000",
                    "numberScaleUnit": " juta, Milyar, Trillyun",
                        
                        "valuePadding": "5",
                    "numberPrefix": "Rp ",
                   
                  
                   
                    "numVisiblePlot": "12",
                    "flatScrollBars": "1",
                    "scrollheight": "10"
                },
                "categories": [{
                    "category": [@foreach($investasi as $chart){ 
                            "label": "Province {{$chart->nama}}"
                        }, @endforeach
                       
                    ]
                }],
                "dataset": [{
                        "seriesName": "PMA",
                        
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pma}}"
                            }, @endforeach
                           
                        ]
                    },
                    {
                        "seriesName": "PMDN",
                      
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pmdn}}"
                            },@endforeach
                          
                        ]
                    },
                
                  
                ]
            }
        });
                    chartObj.render();
                });
            </script>