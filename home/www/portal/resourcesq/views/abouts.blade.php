@extends('frontend.layoutmap')

@section('content')

<div class="row">
  <div class="col-sm-3"> <section id="tbody" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="zink">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 About (PIR)
             </a>
             <br>
                <div style="margin-top:10px; font-size:small;"> This page last updated at <br>01/01/2020, 10:30:30 AM (WIB)</div>
         </div>

         <!--Display selector on the right-->


     </div>
     <br>
  
     <!--end container-->
 </section></div>
  <div class="col-sm-9">  <section id="tbody" class="ts-box p-1" style="margin-top:10px; margin-left:5px; margin-right:5px;">
  

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="clearfix zink hijau">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 About Potensi Investasi Regional (PIR)
             </a>
         </div>

    


     </div>
     <br>
<div ">
     <img class="images" src="https://www.bkpm.go.id/images/uploads/lembaga/bkpm-build.jpg" class="p0" title="Institution"  style="width: 100%;" alt="Institution"/>
     <div style="width:250px; bottom:50px;" class="zink hijau">BKPM Head Office Jakarta</div>
</div>
<div style="text-align:justify; bottom:30px;" class="tulisan">
BKPM or Indonesian Investment Coordinating Board is a Non-Ministerial Government Agency, which in charge of implementing policy and service coordination in investment in accordance with the provisions of the regulations. As the primary interface between business and government, BKPM is mandated to boost domestic and foreign direct investment through creating a conducive investment climate. Restored to ministry level status in 2009, and reporting directly to the President of the Republic of Indonesia, this investment promotion agency’s goal is not only to seek more domestic and foreign investment but also seek quality investments that may drive the Indonesian economy and absorb a lot of manpower.

Established in 1973, BKPM replaced the functions that previously undertaken by the Investment Technical Committee, a Government Institution which was established in 1968. In its organizational structure, BKPM is led by a Chairman, in accordance with the Chairman of BKPM Regulation no. 90/2007. Since October 2019, BKPM is chaired by Bahlil Lahadalia.
</div>

 <div  style="margin-left:5px; margin-right:5px;" class="row ts-box p-1">
  <div  class="col-sm-6"><div id="chart-container">FusionCharts will render here</div></div>
  <div  class="col-sm-6"><div id="chart-container2">FusionCharts will render here</div></div>
</div>




     <!--end container-->
 </section>

</div>
</div>
 	<script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
		<script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
		<script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
		<script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
		
<script>
	FusionCharts.ready(function() {
  var revenueChart = new FusionCharts({
    type: 'doughnut2d',
    renderAt: 'chart-container',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Foreign Direct Investment (Thousand US $)",
        "subCaption": "Last year",
        "numberPrefix": "$",
        "bgColor": "#ffffff",
        "startingAngle": "310",
        "showLegend": "1",
        "defaultCenterLabel": "Total revenue: $64.08K",
        "centerLabel": "Revenue from $label: $value",
        "centerLabelBold": "1",
        "showTooltip": "0",
        "decimals": "0",
        "theme": "fusion"
      },
      "data": [{
          "label": "Food",
          "value": "28504"
        },
        {
          "label": "Apparels",
          "value": "14633"
        },
        {
          "label": "Electronics",
          "value": "10507"
        },
        {
          "label": "Household",
          "value": "4910"
        }
      ]
    }
  }).render();
});
	</script>
 <script>
	FusionCharts.ready(function() {
  var revenueChart = new FusionCharts({
    type: 'doughnut2d',
    renderAt: 'chart-container2',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Domestic Direct Investment (Thousand US $)",
        "subCaption": "Last year",
        "numberPrefix": "$",
        "bgColor": "#ffffff",
        "startingAngle": "310",
        "showLegend": "1",
        "defaultCenterLabel": "Total revenue: $64.08K",
        "centerLabel": "Revenue from $label: $value",
        "centerLabelBold": "1",
        "showTooltip": "0",
        "decimals": "0",
        "theme": "fusion"
      },
      "data": [{
          "label": "Food",
          "value": "28504"
        },
        {
          "label": "Apparels",
          "value": "14633"
        },
        {
          "label": "Electronics",
          "value": "10507"
        },
        {
          "label": "Household",
          "value": "4910"
        }
      ]
    }
  }).render();
});
	</script>


    @endsection