    @extends('frontend.layoutmap')

    @section('content')


    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>

    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="zink">

                    <!--Display selector on the left-->
                    <div align="center" class="">

                        <a id="ts-display-list" class="btn">
                            Investment Incentives
                        </a>
                        <br>
                        <a href="{{ url('holiday') }}">
                            <div align="center" style="margin-top:10px; color:white ; " class="border-bottom pb-2">Tax
                                Holiday</div>
                        </a>
                        <a href="{{ url('allow') }}">
                            <div align="center" style="margin-top:10px;  color:orange;" class="border-bottom pb-2">Tax
                                Allowance</div>
                        </a>
                        <a href="{{ url('master') }}">
                            <div align="center" style="margin-top:10px; color:white; " class="border-bottom pb-2">
                                Masterlist</div>
                        </a>
                        <a href="{{ url('super') }}">
                            <div align="center" style="margin-top:10px; color:white;" class="border-bottom pb-2">Super
                                Deduction</div>
                        </a>
                        <div style="margin-top:10px; font-size:small;"> This page last updated at <br>01/01/2020,
                            10:30:30 AM (WIB)</div>
                    </div>

                    <!--Display selector on the right-->


                </div>


                <!--end container-->
            </section>
        </div>
        <div class="col-sm-9">
            <section id="tbody" class="ts-box p-1 long" style="margin-top:10px; margin-left:10px; margin-right:10px;">


                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix zink hijau">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            TAX Allowance
                        </a>
                    </div>




                </div>
                <br>
                <div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div style="height:172px;" class="zink">
                                <div align="center" style="margin-top:50px;">
                                    <h5 style="color:orange;">TAX ALLOWANCE</h5>
                                    (Government Regulation No.9/2016)
                                </div>

                            </div>
                           
                        </div>
                        <div class="col-sm-6">
                            <div style="margin-top:5px;" class="zink hijau">
                                <h5 style="">30% of Investment Value</h5>
                                Reduction of corporate net income tax
                                TAX ALLOWANCE<br> for 6 years, 5% each year
                            </div>
                            <div style="margin-top:5px;" class="zink orange">
                                <h5 style="">145 Business Fields</h5>
                                eligible for Tax Allowance
                            </div>
                              
                        </div>
                        
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-sm-6">
                           
                            <br>
                            <dt style="color:blue; margin-left:10px;">Agriculture</dt>
                            <div style="margin-left:20px;">
                                <li>Livestock</li>
                                <li>Corn Plantation</li>
                                <li>Soybean Plantation</li>
                                <li>ice Farming</li>
                                <li>Tropical Fruit Plantation</li>
                            </div>
                            <br>
                            <dt style="color:blue; margin-left:10px;">Power Plants</dt>
                            <div style="margin-left:20px;">
                                <li>Geothermal</li>
                                <li>Alternative/Renewable Energy</li>

                            </div>
                            <br>
                            <dt style="color:blue; margin-left:10px;">Oil and Gas Industry</dt>
                            <div style="margin-left:20px;">
                                <li>Oil Refineries</li>
                                <li>Liquefied Natural & Petroleum Gas</li>
                                <li>Soybean Plantation</li>
                                <li>Lubricant</li>
                               
                            </div>
                            </div>
                           
                        
                        <div class="col-sm-6">
                       <br>
                            <dt style="color:blue; margin-left:10px;">Manufacturing Industry</dt>
                            <div style="margin-left:20px;">
                                <li>Iron and Steel</li>
                                <li>Clothes</li>
                                <li>Semi Conductors</li>
                                <li>Electronic Component</li>
                                <li>Computer</li>
                                 <li>Communication Tools</li>
                                <li>Television</li>
                                <li>Tire</li>
                                <li>Pharmacy</li>
                                <li>Cosmetics</li>
                                 <li>Processed Fish and Shrimp</li>
                               
                            </div>
                    </div>
                </div>
                <br>
                <dt style="width:auto; margin-top:20px;" class="zink">For Accelerated reduction of tangible assets</dt>
                <div style="margin-top:10px;" align="center">
                <img  alt=""
                    src="https://regionalinvestment.bkpm.go.id/gis/sharing/rest/content/items/6f169e9df0a2437da8752c06de165437/resources/allo3__1569920404114__w973.png"
                    width="80%">
                    <br>
                    <img style="margin-top:20px;" alt=""
                    src="https://regionalinvestment.bkpm.go.id/gis/sharing/rest/content/items/6f169e9df0a2437da8752c06de165437/resources/allo4__1569920467510__w879.png"
                    width="80%">
</div>
             
        </div>

        <div>




            <!--end container-->
            </section>

        </div>
    </div>
    <script>
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: getDuration
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>

    <script>
        var LeafIconz = L.Icon.extend({
            options: {

                iconSize: [25, 36],

            }
        });
        var office = new LeafIconz({
            iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=industry&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'
        });
        var mymap = L.map('mapid').setView([-6.2268898, 106.8160027, 15], 17);

        L.tileLayer(
            'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
            }).addTo(mymap);

        L.marker([-6.2268898, 106.8160027, 15], {
            icon: office
        }).addTo(mymap);
    </script>


    @endsection