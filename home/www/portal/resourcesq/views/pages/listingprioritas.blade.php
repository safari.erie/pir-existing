

<section  >




 <div class="container">

                                    <!--Featured Items-->
                                    <div  class="row">

                                        @foreach ($propertiesz as $list)
                                           
                                        <?php if($list->id_sektor_peluang == $infras ) { ?>
                                                    <!--Item-->
                                                    <!--Item-->
                                               
                                                    <div  class="col-sm-8 col-lg-4">
                                                        <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card ">
            
                                                            <!--Ribbon-->
                                                      
                                                             <div class="ts-ribbon-corner">
                                                               <span>Prioritas</span>
                                                                      </div>
                                                               
                                                               
                
            
                                                            <!--Card Image-->
                                                            <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                                                                class="card-img ts-item__image"
                                                               >
                                                               <img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                        <figure class="ts-item__info">
                                                                    <h6>{{$list->judul}}</h6>
                                                                  
                                                                </figure>
                                                                <div style="background-color:#e70d0de1; border-radius: 10px; color:white;" class="ts-item__info-badge">Infrastruktur
            </div>
                                                            </a>
            
            
                                                            <div class="card-body ts-item__body">
                                                             <div class="atbd_listing_meta">
                                                                 <span style="background-color:royalblue; border-radius: 10px"  class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                                     @currency($list->nilai_investasi / 1000000) Juta
                                                                      <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                                     @currency($list->nilai_investasi / 1000000000) Milyar
                                      
                                                                      <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                                     @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                                      <?php } ?></span>
                                                                 <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                                 <div class="atbd_listing_data_list">
                                                                     <ul>
                                                                         <li>
                                                                             <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                                         </li>
                                                                         <li>
                                                                         
                                                                         </li>
                                                                     </ul>
                                                                 </div>
                                                             </div>
                                                            </div>
            
                                                            <div class="atbd_listing_bottom_content">
                                                             <div class="atbd_content_left">
                                                                 <div class="atbd_listing_category">
                                                                     <a   style="color:dimgrey" href=""><span   style="color:dimgrey" class="fa fa-eye"></span>Read More</a>
                                                                 </div>
                                                             </div>
                                                             <ul class="atbd_content_right">
                                                                 <li class="atbd_count"></span></li>
                                                                 <li class="atbd_save">
                                                                   
                                                                 </li>
                                                             </ul>
         
                                                         </div>
            
                                                        </div>
                                                        <!--end ts-item ts-card-->
                                                    </div>
            
                                               
                                                  
                                                 <a>
            
                                                     <?php } else if($list->id_sektor_peluang == $jasas) { ?>
                                                    <!--Item-->
                                                    <!--Item-->
                                                    <div id="jasaz" class="col-sm-8 col-lg-4">
                                                        <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card">
            
                                                            <!--Ribbon-->
                                                           
                                                             <div class="ts-ribbon-corner">
                                                               <span>Prioritas</span>
                                                                      </div>
                                                               
                
            
                                                            <!--Card Image-->
                                                            <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                                                                class="card-img ts-item__image">
                                                                <img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                        <figure class="ts-item__info">
                                                                 <h6>{{$list->judul}}</h6>
                                                               
                                                             </figure>
                                                             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Jasa
         </div>
                                                         </a>
         
         
                                                         <div class="card-body ts-item__body">
                                                          <div class="atbd_listing_meta">
                                                              <span style="background-color:royalblue; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000) Juta
                                                                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000000) Milyar
                                   
                                                                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                                   <?php } ?></span>
                                                              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                              <div class="atbd_listing_data_list">
                                                                  <ul>
                                                                      <li>
                                                                          <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                                      </li>
                                                                      <li>
                                                                      
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                         </div>
         
                                                         <div class="atbd_listing_bottom_content">
                                                          <div class="atbd_content_left">
                                                              <div class="atbd_listing_category">
                                                                 <a   style="color:dimgrey" href=""><span   style="color:dimgrey" class="fa fa-eye"></span>Read More</a>
                                                              </div>
                                                          </div>
                                                          <ul class="atbd_content_right">
                                                              <li class="atbd_count"></span></li>
                                                              <li class="atbd_save">
                                                                
                                                              </li>
                                                          </ul>
         
                                                      </div>
         
                                                     </div>
                                                     <!--end ts-item ts-card-->
                                                 </div>
         
                                            
            
                                                     <?php } else if($list->id_sektor_peluang == $pangans) { ?>
                                                    <!--Item-->
                                                    <!--Item-->
                                                    <div id="jasaz" class="col-sm-8 col-lg-4">
                                                        <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card">
            
                                                            <!--Ribbon-->
                                                          
                                                             <div class="ts-ribbon-corner">
                                                               <span>Prioritas</span>
                                                                      </div>
                                                               
                                                                  
                
            
                                                            <!--Card Image-->
                                                            <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                                                                class="card-img ts-item__image" >
                                                                <img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                        <figure class="ts-item__info">
                                                                 <h6>{{$list->judul}}</h6>
                                                               
                                                             </figure>
                                                             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Pangan & Pertanian
         </div>
                                                         </a>
         
         
                                                         <div class="card-body ts-item__body">
                                                          <div class="atbd_listing_meta">
                                                              <span style="background-color:royalblue; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000) Juta
                                                                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000000) Milyar
                                   
                                                                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                                   <?php } ?></span>
                                                              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                              <div class="atbd_listing_data_list">
                                                                  <ul>
                                                                      <li>
                                                                          <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                                      </li>
                                                                      <li>
                                                                      
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                         </div>
         
                                                         <div class="atbd_listing_bottom_content">
                                                          <div class="atbd_content_left">
                                                              <div class="atbd_listing_category">
                                                                 <a   style="color:dimgrey"><span   style="color:dimgrey" class="fa fa-eye"></span>Read More</a>
                                                              </div>
                                                          </div>
                                                          <ul class="atbd_content_right">
                                                              <li class="atbd_count"></span></li>
                                                              <li class="atbd_save">
                                                                
                                                              </li>
                                                          </ul>
         
                                                      </div>
         
                                                     </div>
                                                     <!--end ts-item ts-card-->
                                                 </div>
         
                                            
                                                
                                                     <?php } else if($list->id_sektor_peluang == $indus)  { ?>
                                                       
                                                    <!--Item-->
                                                    <!--Item-->
                                                    <div id="jasaz" class="col-sm-8 col-lg-4">
                                                        <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card">
            
                                                      <div class="ts-ribbon-corner">
                                                        <span>Prioritas</span>
                                                               </div>
                                                        
                                                            
         
            
                                                            <!--Card Image-->
                                                            <a href="{{ Config::get('app.url') }}/propertypro/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                                                                class="card-img ts-item__image"
                                                                >
                                                                <img src="{{ Config::get('app.download') }}/{{$list->id_daerah}}/peluang/{{$list->tahun}}/{{$list->prioritas}}/{{$list->gambar}}" style='max-height: 100%; height: 99999px; width: 100%; '>
                                                        <figure class="ts-item__info">
                                                                 <h6>{{$list->judul}}</h6>
                                                               
                                                             </figure>
                                                             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Industri
         </div>
                                                         </a>
         
         
                                                         <div class="card-body ts-item__body">
                                                          <div class="atbd_listing_meta">
                                                              <span style="background-color:royalblue; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000) Juta
                                                                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000000) Milyar
                                   
                                                                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                                                                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                                                                   <?php } ?></span>
                                                              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                                                              <div class="atbd_listing_data_list">
                                                                  <ul>
                                                                      <li>
                                                                          <p><span class="fa fa-map-marker"></span>{{$list->bentuk_daerah}} {{$list->nama}} </p>
                                                                      </li>
                                                                      <li>
                                                                      
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                         </div>
         
                                                         <div class="atbd_listing_bottom_content">
                                                          <div class="atbd_content_left">
                                                              <div class="atbd_listing_category">
                                                                 <a   style="color:dimgrey" href=""><span  style="color:dimgrey" class="fa fa-eye"></span>Read More</a>
                                                              </div>
                                                          </div>
                                                          <ul class="atbd_content_right">
                                                              <li class="atbd_count"></span></li>
                                                              <li class="atbd_save">
                                                                
                                                              </li>
                                                          </ul>
         
                                                      </div>
         
                                                     </div>
                                                     <!--end ts-item ts-card-->
                                                 </div>
         
                                            
                                                   <?php }?>
            
                                                   
                                                    <!--end Item col-md-4-->
                                                
            
                                                    @endforeach

                                    </div>
                                    <!--Item-->
                                  
                            </section>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<script src="assets/js/custom.js"></script>
                            <!--PAGINATION
            =========================================================================================================-->
                         