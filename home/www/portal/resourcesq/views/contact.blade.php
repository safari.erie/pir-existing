@extends('frontend.layoutmap')

@section('content')


    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

<div class="row">
  <div class="col-sm-3"> <section id="tbody" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="zink">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 Contact Us
             </a>
             <br>
                <div style="margin-top:10px; font-size:small;"> This page last updated at <br>01/01/2020, 10:30:30 AM (WIB)</div>
         </div>

         <!--Display selector on the right-->


     </div>
  
  
     <!--end container-->
 </section></div>
  <div class="col-sm-9">  <section id="tbody" class="ts-box p-1" style="margin-top:10px; margin-left:10px; margin-right:10px;">
  

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="clearfix zink hijau">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 Contact & Location
             </a>
         </div>

    


     </div>
     <br>
     <div id="mapid" class="ts-z-index__1" style="width: 100%; height: 400px;"></div>
     <br>
     <div style="text-align:justify; bottom:30px;" class="tulisan"><dt style="width:auto; margin-top:20px;"class="zink"> Office</dt>
     <dd style="margin-top:10px; margin-left:10px; text-align:justify;" class="border-top">
     Jl. Jend. Gatot subroto No.44, Jakarta 12190<br>
P.O. Box 3186, Indonesia<br><br>
P. +62 21 5252 008 (Hunting)<br>
P. +62 8071002576 atau 1500765 (Contact Center)<br>
F. +62 21 5202050<br>
E. info@bkpm.go.id (Read the email sending instrutions)<br>
E. satgasnasional@bkpm.go.id (Read the email sending instrutions)</dd>
<dt style="width:auto; margin-top:20px;"class="zink">Office Hours (GMT +7)</dt>
     <dd style="margin-top:10px; width:auto; text-align:justify;" class="border-top">
    <dd style="float:left; margin-left:10px">Monday to Thursday</dd><dd style="margin-left:150px; text-align:justify; "> : 07.30 am to 04.00 pm</dd>
    <dd style="float:left; margin-left:10px">Monday to Thursday</dd><dd style="margin-left:150px; text-align:justify; "> : 07.30 am to 04.30 pm</dd>
    <dd style="float:left; margin-left:10px">Monday to Thursday</dd><dd style="margin-left:150px; text-align:justify; "> : Closed </dd>
    </dd>
    <dt style="width:auto; margin-top:20px;"class="zink">Licensing and Non Licensing Service Hours (GMT +7)</dt>
     <dd style="margin-top:10px; text-align:justify;" class="border-top">
    <dd style="float:left; margin-left:10px">Monday to Thursday</dd><dd style="margin-left:150px; text-align:justify; "> : 07.30 am to 04.00 pm</dd>
    <dd style="float:left; margin-left:10px">Monday to Thursday</dd><dd style="margin-left:150px; text-align:justify; "> : 07.30 am to 04.30 pm</dd>
    <dd style="float:left; margin-left:10px">Monday to Thursday</dd><dd style="margin-left:150px; text-align:justify; "> : Closed </dd>
    </dd>

</div>

<div >




     <!--end container-->
 </section>

</div>
</div>

<script>
  var LeafIconz = L.Icon.extend({
    options: {
     
        iconSize:     [25, 36],
        
    }
});
var  factory = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=industry&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
var Infra = new LeafIcon({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=building&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'}),
    jasa = new LeafIcon({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=handshake&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'}),
    pangan = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=seedling&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
   
var  office = new LeafIconz({iconUrl: 'https://api.geoapify.com/v1/icon/?type=material&color=%23594fe0&icon=industry&iconType=awesome&textSize=large&apiKey=3d7d01dae8534e2a87765e5188918c0c'});
	var mymap = L.map('mapid').setView([-6.2268898,106.8160027,15], 17);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);

	L.marker([-6.2268898,106.8160027,15], {icon: office}).addTo(mymap);





</script>

 
    @endsection