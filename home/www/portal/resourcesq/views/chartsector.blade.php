       <!-- Include fusioncharts core library file -->
   



 <div id="chart-container2">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
      FusionCharts.ready(function(){
                    var chartObjz = new FusionCharts({
            type: 'scrollcombidy2d',
            renderAt: 'chart-container2',
            width: '100%',
            height: '430',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fusion",
                    "caption": "Investment Realization per Provinces in 5 years",
                    "subCaption": "(2015 to 2020)",
                 
                    "numberScaleValue": "10,100,1000",
                    "numberScaleUnit": " juta, Milyar, Trillyun",
                        
                        "valuePadding": "5",
                    "numberPrefix": "Rp ",
                   
                  
                   
                    "numVisiblePlot": "12",
                    "flatScrollBars": "1",
                    "scrollheight": "10"
                },
                "categories": [{
                    "category": [@foreach($investasi as $chart){ 
                            "label": "Sektor {{$chart->sektor}}"
                        }, @endforeach
                       
                    ]
                }],
                "dataset": [{
                        "seriesName": "PMA",
                        
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pma}}"
                            }, @endforeach
                           
                        ]
                    },
                    {
                        "seriesName": "PMDN",
                      
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pmdn}}"
                            },@endforeach
                          
                        ]
                    },
                
                  
                ]
            }
        });
                    chartObjz.render();
                });
            </script>