    @extends('frontend.layoutmap')

    @section('content')

   
    <style type="text/css">
        g[class^='raphael-group-'][class$='-creditgroup'] {
            display: none !important;
        }
    </style>
    <style type="text/css">
        g[class$='creditgroup'] {
            display: none !important;
        }
    </style>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

    <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
    <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />


    <!-- Load Esri Leaflet from CDN -->


    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

                <div id="mydatas"></div>

                <!--end container-->
            </section>
        </div>
        <br>
        <div class="col-sm-9">
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix zink hijau">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            INVESTMENT REALIZATION
                        </a>
                    </div>




                </div>
               
            
                            <div style="margin-top:10px; margin-right:5px; margin-left:5px;">
                                <div class="clearfix zink">

                                    <!--Display selector on the left-->
                                    <div class="">

                                        <a id="ts-display-list" class="btn">
                                            Regional Investment Opportunity Maps
                                        </a>
                                    </div>




                                </div>
                                <div id="map">
                                </div>
                  
                    </div>

            </section>

           
         
            <section class="ts-box p-1 long" style=" margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix zink ">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">

                            Province Minimum Wages (Last 5 Years)
                        </a>

                     
                    </div>






                </div>
                
                <br>

                <div id="chart-container3">FusionCharts will render here</div>

                <!--end container-->
            </section>

          

</div>

        </div>

        <div>




            <!--end container-->
            </section>

        </div>
    </div>
    <script src="{{ url('assets/js/custom.js') }}"></script>
    <script src="https://127.0.0.1/map/html/assets/js/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script><br>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-apexcharts@1.3.6/dist/react-apexcharts.iife.min.js">
    </script>

    <script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Include fusioncharts core library file -->
    <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
    <!-- Include fusion theme file -->
    <script type="text/javascript"
        src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
    <!-- Include fusioncharts jquery plugin -->
    <script type="text/javascript"
        src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
    </script>
       <script>
        $(document).ready(function () {
@foreach($bentuk as $list)
            $.get("{{ url('/chart') }}/{{$pages}}/{{$list->id_daerah}}", function (data) {
                @endforeach
                $("#mydatas").append(data);




            });

        });
    </script>

    <body>
        
        <script>
            const postDetails = document.querySelector(".long");
            const postSidebar = document.querySelector(".when");
            const postDetailsz = document.querySelector(".ts-boxzzs");
            const postSidebarContent = document.querySelector(".ts-bosxzz > div");


            const controller = new ScrollMagic.Controller();


            const scene = new ScrollMagic.Scene({
                triggerElement: postSidebar,
                triggerHook: 0,
                duration: 400
            }).addTo(controller);

            //3
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            }

            //4
            window.addEventListener("resize", () => {
                if (window.matchMedia("(min-width: 768px)").matches) {
                    scene.setPin(postSidebar, {
                        pushFollowers: false
                    });
                } else {
                    scene.removePin(postSidebar, true);
                }
            });

            function getDuration() {
                return postDetails.offsetHeight - postSidebar.offsetHeight;
            }
        </script>



        <script src="https://js.arcgis.com/3.34/"></script>

        <script>
            require([
                    "esri/map",
                    "esri/layers/FeatureLayer",
                    "dojo/domReady!"
                ],
                function (
                    Map,
                    FeatureLayer
                ) {

                    var map = new Map("map", {
                        basemap: "hybrid",
                        center: [117.62527, -1.4509444],
                        zoom: 5
                    });

                    /****************************************************************
                     * Add feature layer - A FeatureLayer at minimum should point
                     * to a URL to a feature service or point to a feature collection 
                     * object.
                     ***************************************************************/

                    // Carbon storage of trees in Warren Wilson College.
                    var featureLayer = new FeatureLayer(
                        "https://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/Landscape_Trees/FeatureServer/0"
                    );

                    map.addLayer(featureLayer);

                });
        </script>
    
        <script>
            FusionCharts.ready(function () {


                var visitChart = new FusionCharts({
                            type: "line",
                            renderAt: "chart-container3",
                            id: "demoChart",
                            width: "100%",
                            height: "400",
                            dataFormat: "json",
                            dataSource: {
                                "chart": {
                                    "theme": "fusion",
                                    "caption": "Province Minimum Wages (Last 5 Years)",
                                    "subcaption": "2015-2020",
                                    "xaxisname": "Quarter",
                                    "yaxisname": "(IDR per Month)",
                                    "numberPrefix": "Rp.",
                                    "rotateValues": "0",
                                    "placeValuesInside": "0",
                                    "valueFontColor": "#000000",
                                    "valueBgColor": "#FFFFFF",
                                    "valueBgAlpha": "50",
                                    //Disabling number scale compression
                                    "formatNumberScale": "0",
                                    //Defining custom decimal separator
                                    "decimalSeparator": ",",
                                    //Defining custom thousand separator
                                    "thousandSeparator": ".",

                                    "showValues": "1"
                                },
                                "data": [{
                                        "label": "2015",
                                        "value": "2400000"
                                    },
                                    {
                                        "label": "2016",
                                        "value": "2800000"
                                    },
                                    {
                                        "label": "2017",
                                        "value": "3100000"
                                    }, {
                                        "label": "2018",
                                        "value": "3350000"
                                    }, {
                                        "label": "2019",
                                        "value": "3800000"
                                    }, {
                                        "label": "2020",
                                        "value": "4100000"
                                    }
                                ]
                            }
                        }

                    )
                    .render();

                const chartObj = FusionCharts.items['demoChart'];

                document.getElementById('option-controller').onchange = changeEventHandler;

                function changeEventHandler(event) {
                    switch (parseInt(event.target.value)) {
                        case 0:
                            chartObj.setChartAttribute("formatNumberScale", "1");
                            break;
                        case 1:
                            chartObj.setChartAttribute("formatNumberScale", "0");
                            break;
                        default:
                            console.log("Unexpected Input")
                            break;
                    }
                }
            });
        </script>
        
        @endsection