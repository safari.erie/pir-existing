        <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="wings when">

                    <!--Display selector on the left-->
                    <div align="center" class="">

                        <a style="color:white;"id="ts-display-list" class="btn titlez">
                            Regional Profile
                        </a>
                        <br>
                      
                        <br>
                           @foreach($bentuk as $list)
                        <a href="{{ url('profil/detail') }}/{{$list->id_daerah}}" 
                     @if($pages == 'profil')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-home fa-fw mr-3"></span>
                                <span class="menu-collapsed">Profile </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        @foreach($bentuk as $list)
                        <a href="{{ url('invest/detail') }}/{{$list->id_daerah}}" 
                     @if($pages == 'invest')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-money fa-fw mr-3"></span>
                                <span class="menu-collapsed">Investment Realization</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                          @foreach($bentuk as $list)
                        <a href="{{ url('potensial/detail') }}/{{$list->id_daerah}}" 
                     @if($pages == 'potensial')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-bar-chart fa-fw mr-3"></span>
                                <span class="menu-collapsed">Regional Potential </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                        @foreach($bentuk as $list)
                        <a href="{{ url('oppo/detail') }}/{{$list->id_daerah}}" 
                     @if($pages == 'oppo')
                     style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                              @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-dollar fa-fw mr-3"></span>
                                <span >Investment Opportunity</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        @foreach($bentuk as $list)
                        <a href="{{ url('industri/detail')}}/{{$list->id_daerah}}"    @if($pages == 'industri')
                        style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                          @endforeach
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-industry fa-fw mr-3"></span>
                                <span class="menu-collapsed">Industrial Estate</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                        @foreach($bentuk as $list)
                        <a href="{{ url('infra/detail')}}/{{$list->id_daerah}}"    @if($pages == 'infra')
                        style="border-bottom-left-radius: 20px;"  class="zink hijau list-group-item-action list-group-item  flex-column align-items-start titlez">
                            @else
                            style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                            @endif
                          @endforeach  <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-road fa-fw mr-3"></span>
                                <span class="menu-collapsed">Infrastructure</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <a href="#submenu7" data-toggle="collapse" aria-expanded="false"
                            class="zink list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-start align-items-center">
                                <span class="fa fa-id-card-o fa-fw mr-3"></span>
                                <span class="menu-collapsed">Contact</span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>





                        <div style="margin-top:10px; color:white; font-size:small;"> This page last updated at <br>01/01/2020,
                            10:30:30 AM (WIB)</div>
                    </div>

                    <!--Display selector on the right-->


                </div>