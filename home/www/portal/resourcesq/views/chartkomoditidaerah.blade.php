       <!-- Include fusioncharts core library file -->
   


  
 <div id="asd">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
      FusionCharts.ready(function() {
        var myChartzssss = new FusionCharts({
          type: "scrollbar2d",
          renderAt: "asd",
          width: "100%",
          height: "350",
          dataFormat: "json",
          dataSource: {
                    chart: {
                        caption: "Top Commodities by Last Year",
                        subcaption: "2016-2020 ",
                        plottooltext: "$dataValue Production Results",
                        yaxisname: "Total Production Results",
                        xaxisname: "Commodities",
                        theme: "fusion",
                        numberScaleValue: "1,1,1",
                    numberScaleUnit: " Kg, Kwintal, Ton",
                    
                    },
                    categories: [{
                        category: [@foreach($investasi as $r)

                            {
                                label: "({{$r->sektor}}) Commodities {{$r->nama}}"
                            }, @endforeach
                        
                        ]
                    }],
                    dataset: [{
                        data: [@foreach($investasi as $r){
                                value: "{{$r->hasil}}"
                            }, @endforeach
                          


                        ]
                    }],
                    trendlines: [{
                    line: [{
                            startvalue: "16200",
                            color: "#5D62B5",
                            thickness: "1.5",
                            displayvalue: " ."
                        }
                     
                    ]
                }]
            
                }
        }).render();
      });

            </script>