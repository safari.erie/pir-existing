@extends('frontend.layoutmap')

@section('content')
<?PHP
header('Access-Control-Allow-Origin: *');
?>
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<script type="text/javascript"
    src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript"
    src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
</script>
<script>
          
          $.get("{{ Config::get('app.url') }}/chartinvestasi/2017", function (data) {
                $("#chart2").html(data);
               
              
            });
            
            
            </script>
<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
<div class="preloader">
  <div align="center" class="loading">
    <img src="{{Config::get('app.url')}}/gif/loading.gif" height="100">
    <div align="center" style="margin-bottom:100x;">
    <br>
   <a style="color:grey">" Welcome to Indonesia Investement Coordinating Board "</a>
</div>
    
  </div>
</div>
    
<script> $(document).ready(function () {



$(".preloader").delay(1500).fadeOut("slow");
                $('#submit_100').hide();
             
                $('.has-spinner').click(function () {

                    var btn = $(this);
                    $(btn).buttonLoader('start');
                    setTimeout(function () {
                        $(btn).buttonLoader('stop');
                    }, 5000);
                    var nameform = $('#identity').val();

                });

 $("#layerclick").click(function(){
        
        $('#layerhidden').toggle();
        $('#layeron').toggleClass("hijau");

    
});
$("#baseclick").click(function(){
        
        $('#base').toggle();
        $('#baseon').toggleClass("hijau");

    
});
$('#layerhidden').hide();
$('#base').hide();

});</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/css/ion.rangeSlider.min.css" />




<script src="https://js.arcgis.com/3.34/"></script>

<script>
    var map;
    require([
        "esri/map",
        "esri/dijit/PopupTemplate",
        "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
        "esri/InfoTemplate",
        "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
        "dojo/parser",

        "dijit/layout/BorderContainer",
        "dijit/layout/ContentPane",
        "dijit/TitlePane",
        "dojo/domReady!"
    ], function (
        Map,
        PopupTemplate,
        FeatureLayer,
        arrayUtils,
        ArcGISDynamicMapServiceLayer,
        Geometry,
        Point,
        webMercatorUtils,
        Graphic,
        SimpleMarkerSymbol,
        SimpleLineSymbol,
        SimpleFillSymbol,
        PictureMarkerSymbol,
        Color,
        InfoTemplate,
        BasemapGallery,
        arcgisUtils,
        parser
    ) {
        parser.parse();


        var map = new Map("map", {
            basemap: "hybrid",
            center: [117.62527, -1.4509444],
            zoom: 5
        });


        //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
        var basemapGallery = new BasemapGallery({
            showArcGISBasemaps: true,
            map: map
        }, "basemapGallery");
        basemapGallery.startup();

        basemapGallery.on("error", function (msg) {
            console.log("basemap gallery error:  ", msg);
        });



        var title1 = '<div align="center">Batas Wilayah Provinsi</div>';
        var isi1 =
            '<div style="float:left;"  >Nama Provinsi :  </div><dd style="margin-left:103px;  ; "class="border-bottom pb-2"> {Provinsi}</dd>'
        var title2 = '<div align="center">Batas Wilayah Kecamatan</div>';
        var isi2 =
            '<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
        var popupprov = new PopupTemplate({
            title: title1,
            description: isi1

        });

        var popupkota = new PopupTemplate({
            title: title2,
            description: isi2

        });


        var title3 = '<div align="center">RTRW Kabupaten</div>';
        var isi3 =
            '<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

        var popuprtrw = new PopupTemplate({
            title: title3,
            description: isi3

        });


        $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken", {
                username: 'bkpm-sipd',
                password: 'Bkpm_S1pd@atrbpn123'




            },


            function (data, status) {



                var key = data;
                var token = '?token=' + key + '';
                var featureLayerz = new FeatureLayer(
                    "https://gistaru.atrbpn.go.id/arcgis/rest/services/013_RTR_KABUPATEN_KOTA_PROVINSI_RIAU/_1400_RIAU_PR_PERDA/MapServer/6" +
                    token, {
                        outFields: ["*"],
                        opacity: 0.5,
                        infoTemplate: popupTemplate
                    });
                map.addLayer(featureLayerz);
            });
        var featureLayerprovinsi = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/0", {
                outFields: ["*"],

                infoTemplate: popupprov
            });
        map.addLayer(featureLayerprovinsi);
        var featurekota = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/2", {
                outFields: ["*"],

                infoTemplate: popupkota
            });
        map.addLayer(featurekota);

        // Layer Infrastruktur
        var title4 = '<div align="center">Infrastruktur Rumah Sakit</div>';
        var isi4 =
            '<div style="float:left;"  >Rumah Sakit :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Alamat :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {alamat}</dd>'
        var Popup_rumah_sakit = new PopupTemplate({
            title: title4,
            description: isi4
        });
        var Rumah_sakit = new FeatureLayer(
            "https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0", {
                outFields: ["*"],

                infoTemplate: Popup_rumah_sakit
            });
        map.addLayer(Rumah_sakit);

        // Akhir layer infrastruktur




        Rumah_sakit.hide();
        $('input:checkbox[name=rumah]').change(function () {
            if ($(this).is(':checked')) {
                //map.graphics(layer1).hide();

                Rumah_sakit.show();

                // map.graphics.hide();  
            } else {
                Rumah_sakit.hide();
            }


            function hideLayer1() {

                dd.push(layer1);

            }

            function hideLayer2() {}
        });

    });
</script>

<body class="claro">
<div class="row">
    <div class="col-sm-3">
        <section id="tbody" class="when" style=" margin-top:10px; margin-left:10px; margin-right:5px; ">

            <!--ITEMS LISTING
        =========================================================================================================-->

            <div class="wings zink" style="border-radius: 22px;">

                <!--Display selector on the left-->
                <div align="center" class="">

                    <a id="ts-display-list" class="btn">
                        Regional Profile
                    </a>
                    <br>
                    <input class="custom-select" type="text"
                        style="margin-top:10px; margin-bottom:10px; height:38px; " id="keywords"
                        placeholder=" Searching" name="keywords">
                    <br>
                    <a href="#submenu2" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Java <i class="fa fa-caret-down"
                                    style="margin-left:105px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu2' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Jawa')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <a href="#submenu3" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Sumatra <i class="fa fa-caret-down"
                                    style="margin-left:80px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu3' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Sumatera')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <a href="#submenu4" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Kalimantan <i class="fa fa-caret-down"
                                    style="margin-left:61px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu4' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Kalimantan')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <a href="#submenu5" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Sulawesi<i class="fa fa-caret-down"
                                    style="margin-left:80px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu5' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Sulawesi')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <a href="#submenu6" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Maluku<i class="fa fa-caret-down"
                                    style="margin-left:90px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu6' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Maluku')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach

                    </div>
                    <a href="#submenu7" data-toggle="collapse" aria-expanded="false"
                    style="border-bottom-left-radius: 20px;"  class="zink list-group-item-action list-group-item   flex-column align-items-start titlez">
                        <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="fa fa-user fa-fw mr-3"></span>
                            <span class="menu-collapsed">Papua<i class="fa fa-caret-down"
                                    style="margin-left:96px; font-size:18px"></i></span>
                            <span class="submenu-icon ml-auto"></span>
                        </div>
                    </a>
                    <div id='submenu7' class="collapse sidebar-submenu">
                        @foreach($bentuk as $list)
                        @if($list->wilayah_daerah == 'Papua')
                        <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"
                            style="width:230px; font-size:14px;"
                            class="list-group-item list-group-item-action zink ">
                            <span class="menu-collapsed">{{$list->nama}}</span>
                        </a>
                        @endif
                        @endforeach

                    </div>




                    <div style="margin-top:10px; font-size:small;"> This page last updated at <br>01/01/2020,
                        10:30:30 AM (WIB)</div>
                </div>

                <!--Display selector on the right-->


            </div>


            <!--end container-->
        </section>
    </div>
    <br>
    <div class="col-sm-9">
        <section class="single-pricing-wrap Efek5 p-1 long" style="margin-top:10px; padding:10px; margin-left:5px; margin-right:5px; ">


            <!--ITEMS LISTING
        =========================================================================================================-->

          

                <!--Display selector on the left-->
                <div style="margin-top:10px;" class="clearfix  btn-pro titlez  ">

                
                       <div style="margin-top:10px; margin-bottom:10px;"> Peta Profil Daerah</div>
                       
                  
                </div>

                <div  style="width:35px;  right:14px; top:125px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="layerclick"  class="">

                        <i class="fa fa-map-o" aria-hidden="true"></i>

                    </div>
                </div> <div  style="width:35px;  right:14px; top:165px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                    <div id="baseclick"  class="">

                        <i class="fa fa-th-large" aria-hidden="true"></i>

                    </div>
                </div>

            <br>
            <div id="layerhidden" style="right:40px; top:125px width:300px" class="ts-form__map-search ts-z-index__2">
                <form>
                    <div align="center" class="zink">
                        <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                            <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Infrastruktur
                        </div>
                    </div>
                    <div style="align-center;" class="zink "
                        id="collapseExample">
                        <div style="float:left;"  > Rumah Sakit : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                name="rumah" /><span class="toogle "></span></div></dd><br>
            </div>
            <div style="align-center;" class="zink "
                        id="collapseExample">
                        <div style="float:left;"  >Pendidikan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                name="dadan" /><span class="toogle"></span></div></dd><br>
            </div>
            </form>
            </div>
            <div data-dojo-type="dijit/layout/BorderContainer" 
            data-dojo-props="design:'headline', gutters:false" 
            style="width:100%;height:100%;margin:0;">
            <div class="" id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:2px;">
            
                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                    <div data-dojo-type="dijit/TitlePane" 
                         data-dojo-props="title:'Switch Basemap', open:true">
                      <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                        <div id="basemapGallery"></div>
                    </div>
                </div>
              </div>
            </div>
        </div>
   
       

      

            <!--ITEMS LISTING
        =========================================================================================================-->

            <div class="clearfix  btn-pro titlez ">

                <!--Display selector on the left-->
                <div class="">

               
                        Investment Realization per Provinces in 5 years
                    
                        <div class="float-none float-sm-right pl-2 ts-center__vertical ">

<a>Pilih Tahun</a>&nbsp;
    <select style="color:grey; width:100px;" class="custom-select" id="sectorz" name="sorting">
    <option value="2020">2020</option>
        <option value="2019">2019</option>
        <option value="2018">2018</option>
        <option value="2017">2017</option>
        <option value="2016">2016 </option>



    </select>
</div>
                </div>






            </div>
            <br>

            <div align="center" id="chart2">Data Belum Terdownload silahkan Refresh Kembali</div>

            <!--end container-->
        
        <br>
       
            <div style="" class="">

                <!--Display selector on the left-->
                <div style="margin-top:10px;" class="clearfix  btn-pro titlez ">

                
                    <div style="margin-top:10px; margin-bottom:10px;"> Province List</div>
                    
               
             </div>
            </div>
            <br>
            <section style="margin-left:5px; margin-right:5px;">

                <div class="row">
                    @foreach($bentuk as $list)
                    <div class="col-sm-3">
                    <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card ">

                <!--Ribbon-->
                              
                   
           
                          <div align="center" style="max-height:200px; margin-top:10px; font-size:14px; border-radius: 8px; border-bottom-left-radius: 20px;"
                              class="ts-card zink hijau titlez ">

                              <!--Display selector on the left-->
                              <div class="">

                                  <a>
                                      {{$list->nama}}
                                  </a>
                              </div>




                          </div>
       
                          <div align="center" class="" style="padding:20px; border-radius: 11px;">
                            @if($list->id_daerah == 11)
                            <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}"  data-toggle="tooltip" data-placement="top" title="For Detail Province {{$list->nama}}">
                            <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/{{$list->logo}}" height="110px"
                                width="110px">

                            @elseif($list->id_daerah == 14)
                            <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}" data-toggle="tooltip" data-placement="top" title="Detail Province {{$list->nama}}">
                            <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/riau.jpg"
                                height="110px" width="110px">
                            @else
                            <a href="{{ Config::get('app.url')}}/profil/detail/{{$list->id_daerah}}" data-toggle="tooltip" data-placement="top" title="For Detail Province {{$list->nama}}">
                            <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/{{$list->logo}}"
                                height="110px" width="110px">
                            @endif
                        </div>
               


        
                 <div style="margin-left:40px;" class="atbd_listing_meta">
                
                 </div>
                

               

            </div>
                  
                  
                        
                    </div>
                    @endforeach


            </section>

    </div>

    <div>
        <div id="map" 
        data-dojo-type="dijit/layout/ContentPane" 
        data-dojo-props="region:'center'" 
        style="padding:0;"></div>



        <!--end container-->
        </section>

    </div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script><br>

</script>

<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->

<body>

    <script>
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 3800
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>




    <script>
        
        $(document).ready(function () {
     
            
           

       
            $('#layerhidden').hide();
      
                    $('#provinsi').change(function (e) {
                        $.ajax({
                            url: "{{Config::get('app.url')}}/getkota/" + $(this).val(),
                            method: 'GET',
                            success: function (data) {
                                console.log(data);

                                $('#kota').children('option:not(:first)').remove().end();

                                $.each(data, function (index, kotaObj) {

                                    $('#kota').append('<option value="' + kotaObj
                                        .id_daerah + '">  ' + kotaObj
                                        .bentuk_daerah +
                                        ' ' + kotaObj.nama + ' </option>')
                                });
                            }
                        });
                    });

                    $('.aku').on('click', function () {
                        $('.collapse').collapse();
                    })
                    $.get("{{Config::get('app.url')}}/cari?keywords=}", function (data) {
                        $("#mydata").html(data);

                    });
                    $.get("{{Config::get('app.url')}}('/map-home?keywords=", function (data) {
                        $("#mydatas").html(data);

                    });

                    $.get("{{Config::get('app.url')}}/daerah?industri=Industri", function (data) {


                        $("#daerah").append(data);
                        const postDetails = document.querySelector(".hasil");

                        const postSidebar = document.querySelector(".pencarian");
                        const postDetailsz = document.querySelector(".ts-boxzzs");
                        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


                        const controller = new ScrollMagic.Controller();


                        const scene = new ScrollMagic.Scene({
                            triggerElement: postSidebar,
                            triggerHook: 0,
                            duration: getDuration
                        }).addTo(controller);

                        //3
                        if (window.matchMedia("(min-width: 768px)").matches) {
                            scene.setPin(postSidebar, {
                                pushFollowers: false
                            });
                        }

                        //4
                        window.addEventListener("resize", () => {
                            if (window.matchMedia("(min-width: 768px)").matches) {
                                scene.setPin(postSidebar, {
                                    pushFollowers: false
                                });
                            } else {
                                scene.removePin(postSidebar, true);
                            }
                        });

                        function getDuration() {




                            return (postDetails.offsetHeight - postSidebar.offsetHeight) + 500;

                        }
                    });
    </script>
    <script> $('#sectorz').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartinvestasi/"+tahun+"", function (data) {
            $("#chart2").html(data);
           
          
        });
});</script>
    @endsection