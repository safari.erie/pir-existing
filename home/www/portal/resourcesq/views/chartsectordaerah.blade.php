       <!-- Include fusioncharts core library file -->
   



 <div id="sssss">FusionCharts will render here</div>
  <!-- Include fusioncharts core library file -->

<script type="text/javascript">
      FusionCharts.ready(function(){
                    var chr22zz = new FusionCharts({
            type: 'scrollcombidy2d',
            renderAt: 'sssss',
            width: '100%',
            height: '430',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fusion",
                    "caption": "Investment Realization per Provinces in 5 years",
                    "subCaption": "(2015 to 2020)",
                    "showDivLineSecondaryValue":"0",
                    "showSecondaryLimits":"0",
                    "numberScaleValue": "10,100,1000",
                    "numberScaleUnit": " juta, Milyar, Trillyun",
                    "showAxisValue":"0",
                        "valuePadding": "5",
                    "numberPrefix": "Rp ",
                   
                  
                   
                    "numVisiblePlot": "12",
                    "flatScrollBars": "1",
                    "scrollheight": "10"
                },
                "categories": [{
                    "category": [@foreach($investasi as $chart){ 
                            "label": "Sektor {{$chart->sektor}}"
                        }, @endforeach
                       
                    ]
                }],
                "dataset": [{
                        "seriesName": "PMA",
                        
                        "data": [@foreach($investasi as $chart){
                      
                              
                                "value": "{{$chart->investasi_pma}}"
                              
                            }, @endforeach
                           
                        ]
                    },
                    {
                        "seriesName": "PMDN",
                      
                        "data": [@foreach($investasi as $chart){
                                "value": "{{$chart->investasi_pmdn}}"
                            },@endforeach
                          
                        ]
                    },
                
                  
                ]
            }
        });
                    chr22zz.render();
                });
            </script>