 @extends('frontend.layoutmap')

 @section('content')
 <?php 
 function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

 <div class="preloader">
  <div align="center" class="loading">
    <img src="{{ Config::get('app.url') }}/gif/loading.gif" height="100">
    <div align="center" style="margin-bottom:100x;">
    <br>
   <a style="color:grey">" Welcome to Indonesia Investement Coordinating Board "</a>
</div>
    
  </div>
</div>
 <div class="video-background-holder">
     <div class="video-background-overlay"></div>
     <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
         <source src="{{Config::get('app.url')}}/Indonesiamaju.mp4" type="video/mp4">
     </video>
     <div class="video-background-content container h-100">
         <div class="d-flex h-100 text-center align-items-center">
       
             <div align="right"  class="w-100 text-white">
                
               
                <header style="bottom:150px;" class="ribbon-container">
           
                    <h1></h1>
                    <h2 style="right:0px; bottom:-6px; width:310px; position: absolute;" class="ribbon">
                      <a class="ribbon-content">About PIR</a>
                    </h2>
                  
                    <div style="right:0px; position: absolute;  width:300px;"" class="underpage">
                        <div align="center"  ">
                            <p style=" margin-top:20px; padding:10px; color:black; text-align: justify">PIR, an information system on
                                regional investment potential of BKPM or Indonesian Investment Coordinating Board is a
                                Non-Ministerial Government Agency, which in charge of implementing policy and service coordination
                                in investment in accordance with the provisions of the regulations. Our mission is to create
                                sustainable regional economic growth, with vibrant business and good job opportunities for regional
                                in Indonesia.</p>
                                <div style="margin-right:60px;">
                                <a href="#"   class="btn btn-basic">Read More</a>
                            </div>
                        </div>
                      
               
                    </div>
                  </header>
             
                 </section>
           
                 <div style="margin-right:20px;"align="center" >
                    <h2>Indonesia Investement Coordinating Board</h2>
                    <h4 class="offer-text">Let’s explore the amazing offers</h4>
                    
                    
                    
                   
                    
                 </div>
                 

             </div>


         </div>

     </div>
 </div>
 <!-- End -->
 </div>



 <section id="tbody" class="single-pricing-wrap" style="margin-top:20px; margin-left:22px; margin-right:20px;">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div style="margin-top:5px;" class="clearfix  btn-pro titlez">

         <!--Display selector on the left-->
         <div class="">

             <a id="ts-display-list" class="">
                 Top Project Oppertunity
             </a>
         </div>

         <!--Display selector on the right-->


     </div>
<br>
     <div id="owl" class="owl-carousel ts-items-carousel" data-owl-items="4" data-owl-nav="1">
        @foreach ($propertiesx as $list)
        @if($list->sektor == 'Infrastruktur' ) 
            <!--Item-->
            <!--Item-->
       
       
                <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card ">

                    <!--Ribbon-->
                    <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
                     <div class="ts-ribbon-corner">
                       <span>Prioritas</span>
                              </div>
                       
                           <?php }; ?>


                    <!--Card Image-->
                    <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                        class="card-img ts-item__image"
                        data-bg-image="https://1.bp.blogspot.com/-N31gDhrSYZ0/Xggmzk8qkNI/AAAAAAAABLc/ERh9sQlbewkRVOOazZswKEZXbUdT_GHwQCLcBGAsYHQ/s1600/Infrastruktur.jpg">
                        
                        <figure class="ts-item__info">
                            <h6>{{$list->judul}}</h6>
                          
                        </figure>
                        <div style="background-color:#e70d0de1; border-radius: 10px; color:white;" class="ts-item__info-badge">Infrastruktur
</div>
                    </a>


                    <div class="card-body ts-item__body">
                     <div class="atbd_listing_meta">
                         <span style="background-color:#0517bbc5; border-radius: 10px"  class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                             @currency($list->nilai_investasi / 1000000) Juta
                              <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                             @currency($list->nilai_investasi / 1000000000) Milyar

                              <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                             @currency($list->nilai_investasi / 1000000000000) Triliyun
                              <?php } ?></span>
                         <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                         <div class="atbd_listing_data_list">
                           
                                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
                            
                         </div>
                     </div>
                    </div>

                    <div class="atbd_listing_bottom_content">
                     <div class="atbd_content_left">
                         <div class="atbd_listing_category">
                             <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
                         </div>
                     </div>
                     <ul class="atbd_content_right">
                         <li class="atbd_count"></span></li>
                         <li class="atbd_save">
                           
                         </li>
                     </ul>

                 </div>

                </div>
        

@elseif($list->sektor == 'Jasa') 
    <!--Item-->
    <!--Item-->
   
        <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

            <!--Ribbon-->
            <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
             <div class="ts-ribbon-corner">
               <span>Prioritas</span>
                      </div>
               
                   <?php }; ?>


            <!--Card Image-->
            <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                class="card-img ts-item__image"
                data-bg-image="https://i1.wp.com/www.maxmanroe.com/vid/wp-content/uploads/2019/11/Pengertian-Jasa-Adalah.jpg?fit=700%2C387&ssl=1">
                <figure class="ts-item__info">
                 <h6>{{$list->judul}}</h6>
               
             </figure>
             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Jasa
</div>
         </a>


         <div class="card-body ts-item__body">
          <div class="atbd_listing_meta">
              <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                  @currency($list->nilai_investasi / 1000000) Juta
                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000) Milyar

                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                   <?php } ?></span>
              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
              <div class="atbd_listing_data_list">
                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
              </div>
          </div>
         </div>

         <div class="atbd_listing_bottom_content">
          <div class="atbd_content_left">
              <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
              </div>
          </div>
          <ul class="atbd_content_right">
              <li class="atbd_count"></span></li>
              <li class="atbd_save">
                
              </li>
          </ul>

      </div>

     </div>
     <!--end ts-item ts-card-->




     @elseif($list->sektor == 'Pangan dan Pertanian')
    <!--Item-->
    <!--Item-->
    
        <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

            <!--Ribbon-->
            <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
             <div class="ts-ribbon-corner">
               <span>Prioritas</span>
                      </div>
               
                   <?php }; ?>


            <!--Card Image-->
            <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                class="card-img ts-item__image"
                data-bg-image="https://img2.pngdownload.id/20180326/bqe/kisspng-kentucky-agriculture-natural-resource-agricultural-agriculture-5ab96fc040fc11.1686302015221022082662.jpg">
                <figure class="ts-item__info">
                 <h6>{{$list->judul}}</h6>
               
             </figure>
             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Pangan & Pertanian
</div>
         </a>


         <div class="card-body ts-item__body">
          <div class="atbd_listing_meta">
              <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                  @currency($list->nilai_investasi / 1000000) Juta
                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000) Milyar

                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                   <?php } ?></span>
              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
              <div class="atbd_listing_data_list">
                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
              </div>
          </div>
         </div>

         <div class="atbd_listing_bottom_content">
          <div class="atbd_content_left">
              <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
              </div>
          </div>
          <ul class="atbd_content_right">
              <li class="atbd_count"></span></li>
              <li class="atbd_save">
                
              </li>
          </ul>

      </div>

     </div>




     @elseif($list->sektor == 'Industri') 
       
    <!--Item-->
    <!--Item-->
  
        <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

         <?php  if($list->nilai_investasi > 1000000000000 ) { ?>
      <div class="ts-ribbon-corner">
        <span>Prioritas</span>
               </div>
        
            <?php }; ?>


            <!--Card Image-->
            <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                class="card-img ts-item__image"
                data-bg-image="https://eljohnnews.com/wp-content/uploads/2019/01/agap2_technologies-5-1920-1080.jpg">
                <figure class="ts-item__info">
                 <h6>{{$list->judul}}</h6>
               
             </figure>
             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Industri
</div>
         </a>


         <div class="card-body ts-item__body">
          <div class="atbd_listing_meta">
              <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                  @currency($list->nilai_investasi / 1000000) Juta
                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000) Milyar

                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                   <?php } ?></span>
              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
              <div class="atbd_listing_data_list">
                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
              </div>
          </div>
         </div>

         <div class="atbd_listing_bottom_content">
          <div class="atbd_content_left">
              <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
              </div>
          </div>
          <ul class="atbd_content_right">
              <li class="atbd_count"></span></li>
              <li class="atbd_save">
                
              </li>
          </ul>

      </div>

     </div>
     <!--end ts-item ts-card-->



  @endif

   
    <!--end Item col-md-4-->


 

        @endforeach
      

             </div>

             <!--end container-->




             <!--ITEMS LISTING
            =========================================================================================================-->

             <div class="clearfix  btn-pro titlez">

                 <!--Display selector on the left-->
                 <div class="">

                     <a id="ts-display-list" class="">
                         Top Sectors for Indonesia Investment
                         
                     </a>

                     <div class="float-none float-sm-right pl-2 ts-center__vertical ">

                     <a>Pilih Tahun</a>&nbsp;
                         <select style="color:grey; width:100px;" class="custom-select" id="sectorz" name="sorting">
                         <option value="2020">2020</option>
                             <option value="2019">2019</option>
                             <option value="2018">2018</option>
                             <option value="2017">2017</option>
                             <option value="2016">2016 </option>



                         </select>
                     </div>
                 </div>

                 <!--Display selector on the right-->


             </div>
             <br>

             <div id="sector"></div>

             <!--end container-->



             <!--ITEMS LISTING
            =========================================================================================================-->

             <div class="clearfix  btn-pro titlez ">

                 <!--Display selector on the left-->
                 <div class="">

                     <a id="ts-display-list" class="">
                         Top Province or Investement
                     </a>
                 </div>

                 <!--Display selector on the right-->


             </div>
             <div id="owl2" class="owl-carousel ts-items-carousel" data-owl-items="" data-owl-nav="1">

                @foreach ($bentuk as $list)
       
                   
                <div style="background-color:white; border-radius: 10px" class="card ts-item ts-card ">

<!--Ribbon-->
              
   
       
<div align="center" style="margin-top:10px; font-size:14px; border-radius: 8px; border-bottom-left-radius: 20px;"
                              class="ts-card zink hijau titlez ">

              <!--Display selector on the left-->
              <div class="">

                  <a>
                      {{$list->nama}}
                  </a>
              </div>




          </div>

          <div align="center" class="" style="margin-top:10px; border-radius: 11px;">
            @if($list->id_daerah == 11)
            <a href="{{ Config::get('app.url') }}/profil/detail/{{$list->id_daerah}}"  data-toggle="tooltip" data-placement="top" title="For Detail Province {{$list->nama}}">
            <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/{{$list->logo}}" style="padding:10px " height="140px" width="10px">
            @elseif($list->id_daerah == 14)
            <a href="{{ Config::get('app.url') }}/profil/detail/{{$list->id_daerah}}" data-toggle="tooltip" data-placement="top" title="Detail Province {{$list->nama}}">
            <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/riau.jpg"
            style="padding:10px " height="140px" width="10px">
            @else
            <a href="{{ Config::get('app.url') }}/profil/detail/{{$list->id_daerah}}" data-toggle="tooltip" data-placement="top" title="For Detail Province {{$list->nama}}">
            <img  src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/{{$list->logo}}"
               style="padding:10px " height="140px" width="10px">
            @endif
        </div>




 <div style="margin-left:50px; bottom:50px;" class="atbd_listing_meta">
    
     <span id="openmap"  class="atbd_meta atbd_badge_open">Click For Detail</span>
  
 </div>


<div style="background-color:#0517bbc5;" class="atbd_listing_bottom_content">
 <div class="atbd_content_left">
     <div  class="atbd_listing_category">
         <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Province</a>
     </div>
 </div>
 <ul class="atbd_content_right">
     <li class="atbd_count"></li>
     <li class="atbd_save">
       
     </li>
 </ul>

</div>

</div>
  
                  </header>
       
                        @endforeach
       
                    </div>
             <!--ITEMS LISTING
            =========================================================================================================-->

             <div class="clearfix  btn-pro titlez">

                 <!--Display selector on the left-->
                 <div class="">

                     <a id="ts-display-list" class="">
                         Investment Realization per Provinces
                     </a>

                     <div class="float-none float-sm-right pl-2 ts-center__vertical ">
<a>Pilih Tahun</a>&nbsp;
                         <select style="color:grey; width:100px;" class="custom-select" id="sortingz" name="sorting">
                         <option value="2020">2020</option>
                             <option value="2019">2019</option>
                             <option value="2018">2018</option>
                             <option value="2017">2017</option>
                             <option value="2016">2016 </option>



                         </select>
                     </div>
                 </div>

                 <!--Display selector on the right-->

                 

             </div>
             <br>

             <div id="chart2"></div>


             <!--end container-->




             <!--ITEMS LISTING
            =========================================================================================================-->

             <div class="clearfix  btn-pro titlez ">

                 <!--Display selector on the left-->
                 <div class="">

                     <a id="ts-display-list" class="">
                         Online Investment Services
                     </a>
                 </div>

                 <!--Display selector on the right-->


             </div>
             <br>




             <!--ITEMS LISTING
            =========================================================================================================-->

             <br>
             <div class="row">




                 <div style="margin-left:22px; margin-right:22px;" class="col-sm-2 col-6">
                     <div>
                         <img style="float: left; padding:10px; right:40px;" class="img-esicon"
                             src="//www.bkpm.go.id/assets/icon/i-track.png">
                     </div>

                     <h4>SPIPISE</h4>
                     <p>
                         Track your application
                     </p>



                 </div>
                 <div style="margin-left:22px; margin-right:22px;" class="col-sm-2">



                     <div>
                         <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                             src="//www.bkpm.go.id/images/uploads/icon/Frame_2.png">
                     </div>
                     <h4>OSS</h4>
                     <p>
                         Online Single Submission
                     </p>




                 </div>
                 <div style="margin-left:22px; margin-right:22px;" class="col-sm-2">
                     <div>
                         <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                             src="//www.bkpm.go.id/images/uploads/icon/NSWI.png">
                     </div>
                     <h4>NSWI</h4>
                     <p>
                         National for Investment
                     </p>
                 </div>
                 <div style="margin-left:22px; margin-right:22px;" class="col-sm-2">
                     <div>
                         <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                             src="//www.bkpm.go.id/images/uploads/icon/insreport.png">
                     </div>
                     <h4>EIBN</h4>
                     <p>
                         Industrial Estate
                     </p>
                 </div>
                 <div style="margin-left:22px; margin-right:22px;" class="col-sm-2">
                     <div>
                         <a href="https://digisign.bkpm.go.id:2727/verifikasi">
                             <img style="float: left; padding:7px; right:40px;" class="img-esicon"
                                 src="//www.bkpm.go.id/images/uploads/icon/license.png"">
								</div>
                                
									<h4>License</h4>
							<p >
						License Verification
							</p>
						</div>
</div>
			</section>
	
  

    </section>

 <main id=" ts-main">
                             </main>


       <!-- Include fusioncharts core library file -->
       <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
       <!-- Include fusion theme file -->
       <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
       <!-- Include fusioncharts jquery plugin -->
       <script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>
   

                             <script src="{{ Config::get('app.url') }}/map/html/assets/js/owl.carousel.min.js"></script>


                             <script>
                                 (function () {

                                     "use strict";

                                     if (document.getElementById("ts-header").classList.contains("fixed-top")) {
                                         if (!document.getElementsByClassName("ts-homepage")[0]) {
                                             document.getElementById("ts-main").style.marginTop = document
                                                 .getElementById(
                                                     "ts-header")
                                                 .offsetHeight + "px";
                                         }
                                     }

                                 })();

                                 $(document).ready(function ($) {
                                     "use strict";

                                     $('.navbar-nav .nav-link:not([href="#"])').on('click', function () {
                                         $('.navbar-collapse').collapse('hide');
                                     });

                                     if ($(window).width() < 600) {
                                         $("#ts-header").addClass("fixed-top");
                                         $("#mobile").hide();
                                     } else {

                                     }

                                     $(".ts-img-into-bg").each(function () {
                                         $(this).css("background-image", "url(" + $(this).find("img")
                                             .attr("src") +
                                             ")");
                                     });

                                     $("#owl").owlCarousel({
                                    
                                         itemsDesktop: [1000, 3],
                                         itemsDesktopSmall: [990, 2],
                                         itemsTablet: [768, 2],
                                         itemsMobile: [650, 1],
                                         pagination: true,
                                         navigation: false,
                                         autoPlay: true,
                                         responsive: {
                                             0: {
                                                 items: 1
                                             },
                                             768: {
                                                 items: 2
                                             },
                                             1170: {
                                                 items: 4
                                             }
                                         }
                                     });

                                     
                                     $("#owl2").owlCarousel({
                                         
                                         itemsDesktop: [1000, 3],
                                         itemsDesktopSmall: [990, 2],
                                         itemsTablet: [768, 2],
                                         itemsMobile: [650, 1],
                                         pagination: true,
                                         navigation: false,
                                         autoPlay: true,
                                         responsive: {
                                             0: {
                                                 items: 1
                                             },
                                             768: {
                                                 items: 2
                                             },
                                             1170: {
                                                 items: 5
                                             }
                                         }
                                     });

                                     $("[data-bg-color], [data-bg-image], [data-bg-pattern]").each(function () {
                                         var $this = $(this);

                                         if ($this.hasClass("ts-separate-bg-element")) {
                                             $this.append('<div class="ts-background">');

                                             // Background Color

                                             if ($("[data-bg-color]")) {
                                                 $this.find(".ts-background").css("background-color",
                                                     $this.attr(
                                                         "data-bg-color"));
                                             }

                                             // Background Image

                                             if ($this.attr("data-bg-image") !== undefined) {
                                                 $this.find(".ts-background").append(
                                                     '<div class="ts-background-image">');
                                                 $this.find(".ts-background-image").css(
                                                     "background-image", "url(" +
                                                     $this
                                                     .attr("data-bg-image") + ")");
                                                 $this.find(".ts-background-image").css(
                                                     "background-size", $this
                                                     .attr(
                                                         "data-bg-size"));
                                                 $this.find(".ts-background-image").css(
                                                     "background-position", $this
                                                     .attr(
                                                         "data-bg-position"));
                                                 $this.find(".ts-background-image").css("opacity", $this
                                                     .attr(
                                                         "data-bg-image-opacity"));

                                                 $this.find(".ts-background-image").css(
                                                     "background-size", $this
                                                     .attr(
                                                         "data-bg-size"));
                                                 $this.find(".ts-background-image").css(
                                                     "background-repeat", $this
                                                     .attr(
                                                         "data-bg-repeat"));
                                                 $this.find(".ts-background-image").css(
                                                     "background-position", $this
                                                     .attr(
                                                         "data-bg-position"));
                                                 $this.find(".ts-background-image").css(
                                                     "background-blend-mode",
                                                     $this.attr(
                                                         "data-bg-blend-mode"));
                                             }

                                             // Parallax effect

                                             if ($this.attr("data-bg-parallax") !== undefined) {
                                                 $this.find(".ts-background-image").addClass(
                                                     "ts-parallax-element");
                                             }
                                         } else {

                                             if ($this.attr("data-bg-color") !== undefined) {
                                                 $this.css("background-color", $this.attr(
                                                     "data-bg-color"));
                                                 if ($this.hasClass("btn")) {
                                                     $this.css("border-color", $this.attr(
                                                         "data-bg-color"));
                                                 }
                                             }

                                             if ($this.attr("data-bg-image") !== undefined) {
                                                 $this.css("background-image", "url(" + $this.attr(
                                                     "data-bg-image") + ")");

                                                 $this.css("background-size", $this.attr(
                                                     "data-bg-size"));
                                                 $this.css("background-repeat", $this.attr(
                                                     "data-bg-repeat"));
                                                 $this.css("background-position", $this.attr(
                                                     "data-bg-position"));
                                                 $this.css("background-blend-mode", $this.attr(
                                                     "data-bg-blend-mode"));
                                             }

                                             if ($this.attr("data-bg-pattern") !== undefined) {
                                                 $this.css("background-image", "url(" + $this.attr(
                                                         "data-bg-pattern") +
                                                     ")");
                                             }

                                         }
                                     });

                                     $(".ts-password-toggle").on("click", function () {
                                         var $parent = $(this).closest(".ts-has-password-toggle");
                                         var $this = $(this);
                                         var $password = $parent.find("input");
                                         if ($password.attr("type") === "password") {
                                             $password.attr("type", "text");
                                             $this.find("i").removeClass("fa-eye").addClass(
                                                 "fa-eye-slash");
                                         } else {
                                             $password.attr("type", "password");
                                             $this.find("i").removeClass("fa-eye-slash").addClass(
                                                 "fa-eye");
                                         }
                                     });


                                     $("select").each(function () {
                                         isSelected($(this));
                                     }).on("change", function () {
                                         isSelected($(this));
                                     });

                                     if ($(".ts-video").length > 0) {
                                         $(this).fitVids();
                                     }

                                     // Owl Carousel
                                   
                                     // Magnific Popup

                                     var $popupImage = $(".popup-image");

                                     if ($popupImage.length > 0) {
                                         $popupImage.magnificPopup({
                                             type: 'image',
                                             fixedContentPos: false,
                                             gallery: {
                                                 enabled: true
                                             },
                                             removalDelay: 300,
                                             mainClass: 'mfp-fade',
                                             callbacks: {
                                                 // This prevents pushing the entire page to the right after opening Magnific popup image
                                                 open: function () {
                                                     $(".page-wrapper, .navbar-nav").css(
                                                         "margin-right",
                                                         getScrollBarWidth());
                                                 },
                                                 close: function () {
                                                     $(".page-wrapper, .navbar-nav").css(
                                                         "margin-right", 0);
                                                 }
                                             }
                                         });
                                     }

                                     var $videoPopup = $(".video-popup");

                                     if ($videoPopup.length > 0) {
                                         $videoPopup.magnificPopup({
                                             type: "iframe",
                                             removalDelay: 300,
                                             mainClass: "mfp-fade",
                                             overflowY: "hidden",
                                             iframe: {
                                                 markup: '<div class="mfp-iframe-scaler">' +
                                                     '<div class="mfp-close"></div>' +
                                                     '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                                                     '</div>',
                                                 patterns: {
                                                     youtube: {
                                                         index: 'youtube.com/',
                                                         id: 'v=',
                                                         src: '//www.youtube.com/embed/%id%?autoplay=1'
                                                     },
                                                     vimeo: {
                                                         index: 'vimeo.com/',
                                                         id: '/',
                                                         src: '//player.vimeo.com/video/%id%?autoplay=1'
                                                     },
                                                     gmaps: {
                                                         index: '//maps.google.',
                                                         src: '%id%&output=embed'
                                                     }
                                                 },
                                                 srcAction: 'iframe_src'
                                             }
                                         });
                                     }

                                     $(".ts-form-email [type='submit']").each(function () {
                                         var text = $(this).text();
                                         $(this).html("").append("<span>" + text + "</span>").prepend(
                                             "<div class='status'><i class='fas fa-circle-notch fa-spin spinner'></i></div>"
                                         );
                                     });

                                     $(".ts-form-email .btn[type='submit']").on("click", function () {
                                         var $button = $(this);
                                         var $form = $(this).closest("form");
                                         var pathToPhp = $(this).closest("form").attr("data-php-path");
                                         $form.validate({
                                             submitHandler: function () {
                                                 $button.addClass("processing");
                                                 $.post(pathToPhp, $form.serialize(),
                                                     function (
                                                         response) {
                                                         $button.addClass("done").find(
                                                                 ".status")
                                                             .append(
                                                                 response).prop(
                                                                 "disabled", true);
                                                     });
                                                 return false;
                                             }
                                         });
                                     });

                                     if ($("input[type=file].with-preview").length) {
                                         $("input.file-upload-input").MultiFile({
                                             list: ".file-upload-previews"
                                         });
                                     }

                                     if ($(".ts-has-bokeh-bg").length) {

                                         $("#ts-main").prepend(
                                             "<div class='ts-bokeh-background'><canvas id='ts-canvas'></canvas></div>"
                                             );
                                         var canvas = document.getElementById("ts-canvas");
                                         var context = canvas.getContext("2d");
                                         var maxRadius = 50;
                                         var minRadius = 3;
                                         var colors = ["#5c81f9", "#66d3f7"];
                                         var numColors = colors.length;

                                         for (var i = 0; i < 50; i++) {
                                             var xPos = Math.random() * canvas.width;
                                             var yPos = Math.random() * 10;
                                             var radius = minRadius + (Math.random() * (maxRadius - minRadius));
                                             var colorIndex = Math.random() * (numColors - 1);
                                             colorIndex = Math.round(colorIndex);
                                             var color = colors[colorIndex];
                                             drawCircle(context, xPos, yPos, radius, color);
                                         }
                                     }

                                     function drawCircle(context, xPos, yPos, radius, color) {
                                         context.beginPath();
                                         context.arc(xPos, yPos, radius, 0, 360, false);
                                         context.fillStyle = color;
                                         context.fill();
                                     }

                                     heroPadding();

                                     var $scrollBar = $(".scrollbar-inner");
                                     if ($scrollBar.length) {
                                         $scrollBar.scrollbar();
                                     }

                                     initializeSly();
                                     hideCollapseOnMobile();

                                 });

                                 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                 // Functions
                                 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                                 // On RESIZE actions

                                 var resizeId;
                                 $(window).on("resize", function () {
                                     clearTimeout(resizeId);
                                     resizeId = setTimeout(doneResizing, 250);
                                 });

                                 // Do after resize

                                 function doneResizing() {
                                     //heroPadding();
                                     hideCollapseOnMobile();
                                 }

                                 function isSelected($this) {
                                     if ($this.val() !== "") $this.addClass("ts-selected");
                                     else $this.removeClass("ts-selected");
                                 }

                                 function initializeSly() {
                                     $(".ts-sly-frame").each(function () {

                                         var horizontal = parseInt($(this).attr("data-ts-sly-horizontal"), 2);
                                         if (!horizontal) horizontal = 0;

                                         var scrollbar = $(this).attr("data-ts-sly-scrollbar");
                                         if (!scrollbar) scrollbar = 0;

                                         $(this).sly({
                                             horizontal: horizontal,
                                             smart: 1,
                                             elasticBounds: 1,
                                             speed: 300,
                                             itemNav: 'basic',
                                             mouseDragging: 1,
                                             touchDragging: 1,
                                             releaseSwing: 1,
                                             scrollBar: $(scrollbar),
                                             dragHandle: 1,
                                             scrollTrap: 1,
                                             clickBar: 1,
                                             scrollBy: 1,
                                             dynamicHandle: 1
                                         }, {
                                             load: function () {
                                                 $(this.frame).addClass("ts-loaded");
                                             }
                                         });
                                     });
                                 }

                                 function heroPadding() {
                                     var $header = $("#ts-header");
                                     var $hero = $("#ts-hero");

                                     if ($header.hasClass("fixed-top")) {
                                         if ($hero.find(".ts-full-screen").length) {
                                             $hero.find(".ts-full-screen").css("padding-top", $(".fixed-top").height());
                                         } else {
                                             $hero.css("padding-top", $(".fixed-top").height());
                                         }
                                     } else {
                                         if ($hero.find(".ts-full-screen").length) {
                                             $hero.find(".ts-full-screen").css("min-height", "calc( 100vh - " + $header
                                                 .height() +
                                                 "px");
                                         }
                                     }
                                 }

                                 // Smooth Scroll

                                 $(".ts-scroll").on("click", function (event) {
                                     if (
                                         location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//,
                                             '') &&
                                         location.hostname === this.hostname
                                     ) {
                                         var target = $(this.hash);
                                         var headerHeight = 0;
                                         var $fixedTop = $(".fixed-top");
                                         target = target.length ? target : $('[name=' + this.hash.slice(1) +
                                             ']');
                                         if ($fixedTop.length) {
                                             headerHeight = $fixedTop.height();
                                         }
                                         if (target.length) {
                                             event.preventDefault();
                                             $('html, body').animate({
                                                 scrollTop: target.offset().top - headerHeight
                                             }, 1000, function () {
                                                 var $target = $(target);
                                                 $target.focus();
                                                 if ($target.is(":focus")) {
                                                     return false;
                                                 } else {
                                                     $target.attr('tabindex', '-1');
                                                     $target.focus();
                                                 }
                                             });
                                         }
                                     }
                                 });

                                 function hideCollapseOnMobile() {
                                     if ($(window).width() < 575) {
                                         $(".ts-xs-hide-collapse.collapse").removeClass("show");
                                     }
                                 }
                             </script>
                             <script>
                                 jQuery(document).ready(function ($) {

                                    $(".preloader").delay(1500).fadeOut("slow");
                $('#submit_100').hide();
             
                $('.has-spinner').click(function () {

                    var btn = $(this);
                    $(btn).buttonLoader('start');
                    setTimeout(function () {
                        $(btn).buttonLoader('stop');
                    }, 5000);
                    var nameform = $('#identity').val();

                });
                                     "use strict";
                                     //  TESTIMONIALS CAROUSEL HOOK
                                     $('#customers-testimonials').owlCarousel({
                                         loop: true,
                                         center: true,
                                         items: 3,
                                         margin: 0,
                                         autoplay: true,
                                         dots: true,
                                         autoplayTimeout: 8500,
                                         smartSpeed: 450,
                                         responsive: {
                                             0: {
                                                 items: 1
                                             },
                                             768: {
                                                 items: 2
                                             },
                                             1170: {
                                                 items: 3
                                             }
                                         }
                                     });
                                 });
                             </script>
                              <script type="text/javascript">
                                const dataSource = {
                                    chart: {
                                        caption: "Investment Realization by Sector Last Year 2020",
                                        placevaluesinside: "1",
                                        showvalues: "0",
                                        plottooltext: "<b>$ $dataValue</b> $seriesName",
                                        theme: "fusion"
                                    },
                                    categories: [{
                                        category: [{
                                                label: "Sector A"
                                            },
                                            {
                                                label: "Sector B"
                                            },
                                            {
                                                label: "Sector C"
                                            },
                                            {
                                                label: "Sector D"
                                            },
                                            {
                                                label: "Sector E"
                                            }
                                        ]
                                    }],
                                    dataset: [{
                                            seriesname: "Foreign Direct Investment (USD $)",
                                            color: " #5D62B5",
                    
                                            data: [{
                                                    value: "17000"
                    
                                                },
                                                {
                                                    value: "19500"
                                                },
                                                {
                                                    value: "12500"
                                                },
                                                {
                                                    value: "14500"
                                                },
                                                {
                                                    value: "17500"
                                                }
                                            ]
                                        },
                                        {
                                            seriesname: "Domestic Direct Investment (Million Rupiah)",
                                            color: "#29C3BE",
                                            data: [{
                                                    value: "25400"
                                                },
                                                {
                                                    value: "29800"
                                                },
                                                {
                                                    value: "21800"
                                                },
                                                {
                                                    value: "19500"
                                                },
                                                {
                                                    value: "21200"
                                                }
                                            ]
                                        }
                                    ],
                                    trendlines: [{
                                        line: [{
                                                startvalue: "16200",
                                                color: "#5D62B5",
                                                thickness: "1.5",
                                                displayvalue: "2017's Avg."
                                            },
                                            {
                                                startvalue: "23500",
                                                color: "#29C3BE",
                                                thickness: "1.5",
                                                displayvalue: "2018's Avg."
                                            }
                                        ]
                                    }]
                                };
                    
                                FusionCharts.ready(function () {
                                    var myChart = new FusionCharts({
                                        type: "msbar2d",
                                        renderAt: "chart-container2",
                                        width: "100%",
                                        height: "350",
                                        dataFormat: "json",
                                        dataSource
                                    }).render();
                                });
                            </script>
       
                             <script>
                                 (function ($) {
                                     // writes the string
                                     // @param jQuery $target
                                     // @param String str
                                     // @param Numeric cursor
                                     // @param Numeric delay
                                     // @param Function cb
                                     // @return void
                                     function typeString($target, str, cursor, delay, cb) {
                                         $target.html(function (_, html) {
                                             return html + str[cursor];
                                         });

                                         if (cursor < str.length - 1) {
                                             setTimeout(function () {
                                                 typeString($target, str, cursor + 1, delay, cb);
                                             }, delay);
                                         } else {
                                             cb();
                                         }
                                     }

                                     // clears the string
                                     //
                                     // @param jQuery $target
                                     // @param Numeric delay
                                     // @param Function cb
                                     // @return void
                                     function deleteString($target, delay, cb) {
                                         var length;

                                         $target.html(function (_, html) {
                                             length = html.length;
                                             return html.substr(0, length - 1);
                                         });

                                         if (length > 1) {
                                             setTimeout(function () {
                                                 deleteString($target, delay, cb);
                                             }, delay);
                                         } else {
                                             cb();
                                         }
                                     }

                                     // jQuery hook
                                     $.fn.extend({
                                         teletype: function (opts) {
                                             var settings = $.extend({}, $.teletype.defaults, opts);

                                             return $(this).each(function () {
                                                 (function loop($tar, idx) {
                                                     // type
                                                     typeString($tar, settings.text[idx], 0,
                                                         settings.delay,
                                                         function () {
                                                             // delete
                                                             setTimeout(function () {
                                                                 deleteString(
                                                                     $tar,
                                                                     settings
                                                                     .delay,
                                                                     function () {
                                                                         loop($tar,
                                                                             (idx +
                                                                                 1
                                                                                 ) %
                                                                             settings
                                                                             .text
                                                                             .length
                                                                             );
                                                                     });
                                                             }, settings.pause);
                                                         });

                                                 }($(this), 0));
                                             });
                                         }
                                     });

                                     // plugin defaults  
                                     $.extend({
                                         teletype: {
                                             defaults: {
                                                 delay: 50,
                                                 pause: 5000,
                                                 text: []
                                             }
                                         }
                                     });
                                 }(jQuery));

                                 $('#target').teletype({
                                     text: [
                                         'Welcome to Indonesia',
                                         'Potential investement regional',

                                     ]
                                 });

                                 $('#cursor').teletytoppe({
                                     text: ['|', ' '],
                                     delay: 0,
                                     pause: 500
                                 });
                             </script>
                             <script>
                                      $.get("{{ Config::get('app.url') }}/chartsector/2017", function (data) {
                $("#sector").html(data);
               
              
            });
                             $.get("{{ Config::get('app.url') }}/chartinvestasi/2017", function (data) {
                $("#chart2").html(data);
               
              
            });
            
            $('#sortingz').on('change', function() {

    var tahun = $(this).find(":selected").val() ;
    $.get("{{ Config::get('app.url') }}/chartinvestasi/"+tahun+"", function (data) {
                $("#chart2").html(data);
               
              
            });
});

$('#sectorz').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartsector/"+tahun+"", function (data) {
            $("#sector").html(data);
           
          
        });
});
            </script>

                             @endsection