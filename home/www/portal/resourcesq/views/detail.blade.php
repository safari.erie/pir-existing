

@extends('frontend.layoutmap')

@section('content')
<script src="{{ url('assets/js/custom.js') }}"></script>

<html>  
<head> 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
  <title>Basemap gallery</title>
  <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
  <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">
  <style> 
    html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
    #map{
      padding:0;
    }
  </style> 
  
  <script src="https://js.arcgis.com/3.34/"></script>
  <script> 
var map;
require([
  "esri/map",
   "esri/dijit/PopupTemplate",
     "esri/layers/FeatureLayer",
    "dojo/_base/array",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/geometry/Geometry",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
    "esri/InfoTemplate",
   "esri/dijit/BasemapGallery",
    "esri/arcgis/utils",
  "dojo/parser",

  "dijit/layout/BorderContainer", 
  "dijit/layout/ContentPane", 
  "dijit/TitlePane",
  "dojo/domReady!"
], function(
  Map,
        PopupTemplate, 
     FeatureLayer, 
     arrayUtils, 
     ArcGISDynamicMapServiceLayer, 
     Geometry, 
     Point, 
     webMercatorUtils,  
     Graphic, 
     SimpleMarkerSymbol, 
     SimpleLineSymbol, 
     SimpleFillSymbol,
     PictureMarkerSymbol,  
     Color, 
     InfoTemplate,
   BasemapGallery,
    arcgisUtils,
  parser
) {
  parser.parse();

 
@foreach($propertiesx as $list){
map = new Map("map", {
  basemap: "hybrid",
  center: [{{$list->y}}, {{$list->x}}],
  zoom: 6
});
}
@endforeach

  //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
  var basemapGallery = new BasemapGallery({
    showArcGISBasemaps: true,
    map: map
  }, "basemapGallery");
  basemapGallery.startup();
  
  basemapGallery.on("error", function(msg) {
    console.log("basemap gallery error:  ", msg);
  });

  

   var title1 ='<div align="center">Batas Wilayah Provinsi</div>';
var isi1 ='<div style="float:left;"  >Nama Provinsi :  </div><dd style="margin-left:103px;  ; "class="border-bottom pb-2"> {Provinsi}</dd>'
var title2 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi2 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
var title4 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi4 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
       var popupprov = new PopupTemplate({
        title: title1,
      description: isi1
      
    });

    var popupkota = new PopupTemplate({
        title: title2,
      description: isi2
      
    });

    var popupkabupaten = new PopupTemplate({
        title: title4,
      description: isi4
      
    });
  

    var title3 ='<div align="center">RTRW Kabupaten</div>';
var isi3 ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popuprtrw = new PopupTemplate({
            title: title3,
          description: isi3
          
        });
      

          $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken",
    {
      username: 'bkpm-sipd',
      password: 'Bkpm_S1pd@atrbpn123'
     
      
      
      
    },
   
        
    function(data,status){
  


    });
    var featureLayerprovinsi = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0",{
      outFields: ["*"],
      
infoTemplate: popupprov
 });
map.addLayer(featureLayerprovinsi);
var featurekota = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/2",{
      outFields: ["*"],
      
infoTemplate: popupkota
 });
map.addLayer(featurekota);

var featurekabupaten = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_kabupaten/MapServer/0",{
    opacity: 0.5,
      outFields: ["*"],
      
infoTemplate: popupkabupaten
 });
map.addLayer(featurekabupaten);

                    // Layer Infrastruktur
                    var title4 ='<div align="center">Infrastruktur Rumah Sakit</div>';
                         var isi4 ='<div style="float:left;"  >Rumah Sakit :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Alamat :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {alamat}</dd>'
                            var Popup_rumah_sakit = new PopupTemplate({
                                title: title4,
                                    description: isi4
                                            });
                                     var Rumah_sakit = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0",{
                        outFields: ["*"],
                        
                    infoTemplate: Popup_rumah_sakit
                    });
                    map.addLayer(Rumah_sakit);

                    // Akhir layer infrastruktur


        <?php foreach ($propertiesz as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
         
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Infrastruktur') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("openmap").innerHTML = "{{$list->y}}, {{$list->x}}";
           alert('as');
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Jasa') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($propertiesz  as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Industri') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       Rumah_sakit.hide();
      $('input:checkbox[name=rumah]').change(function () {
          if ($(this).is(':checked')) {
              //map.graphics(layer1).hide();
             
              Rumah_sakit.show();
          
              // map.graphics.hide();  
          } else {
            Rumah_sakit.hide();
          }


          function hideLayer1() {

              dd.push(layer1);
              
          }

          function hideLayer2() {}
      });

});


</script> 
</head> 

<body class="claro"> 
  <div class=" row">

            <div class="col-md-4">
                <div class="widget">
                    <section style="top:19px;" class="ts-box p-1 pencarian">
                        <div class="zink ">

                            <!--Display selector on the left-->
                            <div class="zink " align="center">

                                <i class="fa fa-search"> Info Peluang</i>


                            </div>
                        
                        <br>
                       
@foreach($propertiesz as $list)
 @foreach($bentuk as $lisa)

               
                           
                        <div style="float:left;"  >Sektor  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->sektor}}</dd>
                        <div style="float:left;"  >Proyek   </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->judul}}</dd>

@endforeach
                       
                      <div style="float:left;"  >Luas Area </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
 
<div style="float:left;"  >Tahun  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->tahun}}</dd>
<div style="float:left;"  ><div style="float:left;">Nilai</div> <br> Investasi  </div><dd style="margin-left:75px; margin-top:20px; text-align:justify; "class="border-bottom pb-2"> <?php if ($list->nilai_investasi  < 1000000000) { ?>
                               @currency($list->nilai_investasi / 1000000) Juta
                                <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000) Milyar

                                <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000000) Triliyun
                                <?php } ?></dd>
                                 <div style="float:left;"  >IRR </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                 <div style="float:left;"  >NPV </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                  <div style="float:left;"  >PP </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                               <div style="float:left;"  >Penghubung</div><dd style="margin-left:90px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                               <div style="float:left;"  >No Telepon </div><dd style="margin-left:90px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                               <div style="float:left;"  >Jabatan </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
<div style="float:left;"  >Sumber  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">{{$list->instansi}}</dd>
<div style="float:left;"  ></div><dd style="margin-left:75px; text-align:justify; "class=""> <button data-toggle="modal" data-target="#myModal" class="btn zink hijau">Interested</button></dd>
@endforeach
                        
                        </form>

                    </section>
                    </aside>
                </div>
            </div>
            

            <div class="col-sm-8">
                <div class="post-details">
             
          

  




<div id="layerclick"  style="width:35px;  right:30px; top:140px"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
    <div id="layeron" class="">

        <i class="fa fa-map-o" aria-hidden="true"></i>

    </div>
</div>

<div id="basemapclick"  style="width:35px;  right:30px; top:180px"  align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
    <div id="basemapon" class="">

        <i class="fa fa-th-large" aria-hidden="true"></i>

    </div>
</div>


</div>
        
            <div id="layer" style=" width:300px; right:55px; top:120px" class="ts-form__map-search ts-z-index__2">


              
                   

                     <div style ="align-center;"class="zink" id="collapseExample">
                        <div  style="float:left; "><i class="fa fa-road" aria-hidden="true"></i> RTRW &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;</div>
                        &nbsp;&nbsp;<div style="float:right; margin-right:20px;"class="box-1"><input type='checkbox' name="dadan"/><span class="toogle"></span></div>

                    </div>
                   

            </div>


        </section>
          <section id="tbody" class="ts-boxz p-1" style="margin-top:20px;">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Map
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                             

                            </div>
                            </section>
                            <br>
                   <div data-dojo-type="dijit/layout/BorderContainer" 
       data-dojo-props="design:'headline', gutters:false" 
       style="width:100%;height:100%;margin:0;">

    <div id="map" 
         data-dojo-type="dijit/layout/ContentPane" 
         data-dojo-props="region:'center'" 
         style="padding:0;">

      <div id="hide"style="position:absolute; right:55px; top:10px; z-Index:999;">
        <div data-dojo-type="dijit/TitlePane" 
             data-dojo-props="title:'Switch Basemap', open:true">
          <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
            <div id="basemapGallery"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
                
                            </section>
          <section id="tbody" class="ts-boxz p-1" style="margin-top:20px;">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Image & Video
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                             

                            </div>
                            </section>
                            <br>
                    
                     <div class="row">
  <div class="col-sm-6"> <div class="owl-carousel ts-gallery-carousel" data-owl-auto-height="1" data-owl-dots="1" data-owl-loop="1">

                                <!--Slide-->
                                <div class="slide">
                                    <div class="ts-image" data-bg-image="https://kek.go.id/assets/images/map/20170823112037_HEADER_KAWASAN-02.png">
                                        <a href="https://kek.go.id/assets/images/map/20170823112037_HEADER_KAWASAN-02.png" class="ts-zoom"><i class="fa fa-search-plus"></i>Zoom</a>
                                    </div>
                                </div>

                              

                            </div></div>
  <div class="col-sm-6"><section style="bottom:20px; "id="video">

                       <br>

                            <div class="embed-responsive embed-responsive-16by9 rounded ts-shadow__md">
                          @if (count($propertiesx))   
                          
    <video controls="true" class="embed-responsive-item">
      <source src="/portal/video/{{$list->id_daerah}}/{{$list->video}}" type="video/mp4" />
    </video>

                                       @else
                          <p>Kosong</p>
                          @endif
                            </div>

                      

                        </section>
                        </div>
</div>
</section>
        
               
                   <section  class="ts-boxz p-1 hasil" style="margin-top:20px;">

            
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Deskripsi
                                    </a>
                                </div>

                               
                               

                            </div>
                       <?php foreach ($propertiesz as $list) { ?>
  
                           <div style="margin-left:20px; margin-right:20px; margin-top:20px;"><?php echo $list->deskripsi ?> </div>
                         
                       <?php } ?>
                          
                             
                            </section>
                           
                    
                    
                     
                           
                            
                            
                  
                         <div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->
    <div class="modal-content">

      <!--Modal cascading tabs-->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            <br>
            <div  align="center">
            <label>Send To email Download Data</label> <div style ="margin-left:20px; margin-right:20px; float:right;" class="form-group">
                    <button type="submit" class="btn btn-md btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
            <br>
            @if(\Session::has('alert-failed'))
                <div class="alert alert-failed">
                    <div>{{Session::get('alert-failed')}}</div>
                </div>
            @endif
            @if(\Session::has('alert-success'))
                <div class="alert alert-success">
                    <div>{{Session::get('alert-success')}}</div>
                </div>
            @endif
            @foreach($propertiesz as $list)
            <form action="{{ url('/sendEmail/833') }}" method="post">
            @endforeach
                {{ csrf_field() }}
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="nama">Nama:</label>
                    <input type="text" class="form-control" id="name" name="nama"/>
                </div>
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="judul">Asal Negara:</label>
                    <input type="text" class="form-control" id="judul" name="judul"/>
                </div>
                 <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <label for="judul">Phone:</label>
                    <input type="text" class="form-control" id="judul" name="phone"/ >
                </div>

                 <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    @foreach($propertiesz as $list)
                    <input type="hidden" class="form-control" id="judul" name="daerah" Value="{{$list->id_daerah}}"/>
                    @endforeach
                </div>

              
                <div style ="margin-left:40px; margin-right:40px;" class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Send Email</button>
                </div>
                
            </form>
        </div>
        <!-- /.content -->
    </section>
          
         

          </div>
          <!--/.Panel 7-->

      </div>
    </div>
    <!--/.Content-->
  </div>
  </div>   
                            

 

    </main>

    <!--end #ts-main-->
    </div>
    </div>
    <br>
                           

    <div id="map" 
         data-dojo-type="dijit/layout/ContentPane" 
         data-dojo-props="region:'center'" 
         style="padding:0;"></div>

</body> 

</html>
<script src="{{url('map/html/assets/js/owl.carousel.min.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="{{url('map/html/assets/js/popper.min.js')}}"></script>

  <script src="{{url('map/html/assets/js/jquery.magnific-popup.min.js')}}"></script>
  
 
   <script>  
   $(document).ready(function () {
         
   

         // LegendLayer
  
         

            $("#layerclick").click(function(){
            
            $('#layer').toggle();
            $('#layeron').toggleClass("hijau");

        
});


$("#basemapclick").click(function(){
            
            $('#hide').toggle();
            $('#basemapon').toggleClass("hijau");

        
});

$('#layer').hide();
$('#hide').hide();
            $('#legendlayer').hide();
        
});

</script>

    @endsection