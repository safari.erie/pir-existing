@extends('frontend.layoutmap')

@section('content')
<html>
    <head>
<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
	
        <script type="text/javascript">
		FusionCharts.ready(function(){
			var chartObj = new FusionCharts({
    type: 'pie2d',
    renderAt: 'chart-container',
    width: '450',
    height: '450',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Status Block Site Plan",
            "subCaption": "Last year",
            "numberPrefix": "$",
            "showPercentInTooltip": "0",
            "decimals": "1",
            "useDataPlotColorForLabels": "1",
            //Theme
            "theme": "fusion"
        },
        "data": [{
            "label": "Available",
            "value": "285040"
        }, {
            "label": "Future Development",
            "value": "146330"
        }, {
            "label": "Sold",
            "value": "105070"
        }, {
            "label": "Not for Sale",
            "value": "49100"
        }]
    }
}
);
			chartObj.render();
		});
	</script>
<script src="https://js.arcgis.com/3.34/"></script>
<script> 
    var map;
    require([
      "esri/map",
       "esri/dijit/PopupTemplate",
         "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
        "esri/InfoTemplate",
       "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
      "dojo/parser",

      "dijit/layout/BorderContainer", 
      "dijit/layout/ContentPane", 
      "dijit/TitlePane",
      "dojo/domReady!"
    ], function(
      Map,
            PopupTemplate, 
         FeatureLayer, 
         arrayUtils, 
         ArcGISDynamicMapServiceLayer, 
         Geometry, 
         Point, 
         webMercatorUtils,  
         Graphic, 
         SimpleMarkerSymbol, 
         SimpleLineSymbol, 
         SimpleFillSymbol,
         PictureMarkerSymbol,  
         Color, 
         InfoTemplate,
       BasemapGallery,
        arcgisUtils,
      parser
    ) {
      parser.parse();

     
@foreach($kota as $list){
  map = new Map("map", {
      basemap: "hybrid",
      center: [{{$list->y}}, {{$list->x}}],
      zoom: 7
  });
}
@endforeach

      //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
      var basemapGallery = new BasemapGallery({
        showArcGISBasemaps: true,
        map: map
      }, "basemapGallery");
      basemapGallery.startup();
      
      basemapGallery.on("error", function(msg) {
        console.log("basemap gallery error:  ", msg);
      });

      

       var title ='<div align="center">RTRW Kabupaten</div>';
var isi ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popupTemplate = new PopupTemplate({
            title: title,
          description: isi
          
        });
      

          $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken",
    {
      username: 'bkpm-sipd',
      password: 'Bkpm_S1pd@atrbpn123'
     
      
      
      
    },
    function(data,status){
        
        var key = data;
        var token='?token='+key+'';
      var featureLayer = new FeatureLayer("https://gistaru.atrbpn.go.id/arcgis/rest/services/013_RTR_KABUPATEN_KOTA_PROVINSI_RIAU/_1400_RIAU_PR_PERDA/MapServer/6"+ token,{
          outFields: ["*"],
          opacity:0.5,
    infoTemplate: popupTemplate
     });
    map.addLayer(featureLayer);
});
    
            <?php foreach ($projecs as $list): ?>
                  @foreach($bentuk as $lisa)
            
             
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div >sas</dd>';
             
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

              layer1.on('click', function () {
                document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
            })
        
          @endforeach
           @endforeach

              

  
          $('input:checkbox[name=dadan]').change(function () {
              if ($(this).is(':checked')) {
                  //map.graphics(layer1).hide();
                  featureLayer.hide();
                  
              
                  // map.graphics.hide();  
              } else {
                  featureLayer.show();
              }

              function hideLayer1() {

                  dd.push(layer1);
                  
              }

              function hideLayer2() {}
          });

    });

  
    
  </script> 

<?php

function clean($string) {
$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>


<style> 
html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
#map{
  padding:0;
}
</style> 



<!-- Load Esri Leaflet from CDN -->
<body class="claro">

<div class="row">
    <div class="col-sm-3">
        <section id="tbody" class="when" style=" margin-top:10px; ">

            <div id="mydatas"></div>

            <!--end container-->
        </section>
    </div>
    <br>
    <div class="col-sm-9">
        <section class="single-pricing-wrap p-1" style="margin-top:10px; ">

            <!--ITEMS LISTING
        =========================================================================================================-->

        <section id="tbody" class="single-pricing-wrap p-1" style="">
           
        
                        <div style="margin-top:10px; ">
                            <div class="btn-pro">

                                <!--Display selector on the left-->
                                <div class="">

                                    <a id="ts-display-list" class="">
                                      Regional Industrial Estate Maps
                                    </a>
                                </div>




                            </div>
                            
                            <br>
                            <div id="dadan">s</div>
                            <div data-dojo-type="dijit/layout/BorderContainer" 
                            data-dojo-props="design:'headline', gutters:false" 
                            style="width:100%;height:100%;margin:0;">
                     <div class="row"><div class="col-sm-5"><div id="chart-container">FusionCharts XT will load here!</div></div><div class="col-sm-7">  <div id="map" 
                              data-dojo-type="dijit/layout/ContentPane" 
                              data-dojo-props="region:'center'" 
                              style="padding:0;">
                     
                           <div style="position:absolute; right:20px; top:10px; z-Index:999;">
                             <div data-dojo-type="dijit/TitlePane" 
                                  data-dojo-props="title:'Switch Basemap', open:false">
                               <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                                 <div id="basemapGallery"></div>
                               </div>
                             </div>
                           </div>
                     
                         </div></div></div>
                       
                                     
                            <br>
         
            <div class="clearfix btn-pro ">

                <!--Display selector on the left-->
                <div class="">

                    <a id="ts-display-list" class="">
                        Industrial Estate
                    </a>
                </div>

                <!--Display selector on the right-->


            </div>
            <br>
            {{-- Memanggil project --}}
            <div class="owl-carousel ts-items-carousel" data-owl-items="4" data-owl-nav="1">
           
                @foreach ($projecs as $list)
                


                <!--Item-->
                <div style="justify;  -webkit-box-shadow: 0px 0px 30px 0px rgba(211, 220, 255, 0.78);
                box-shadow: 0px 0px 30px 0px rgba(211, 220, 255, 0.78);" class="slide ">

                    <div class="card ts-item ts-card">

                        <!--Ribbon-->


                        <!--Card Image-->
                        <a href=""
                            class="card-img ts-item__image"
                            data-bg-image="https://cdn.pixabay.com/photo/2018/04/30/08/00/industry-3362044_1280.jpg">
                            <figure class="ts-item__info">
                                <h4>{{$list->kawasan}} </h4>
                                <aside>
                                    <i class="fa fa-map-marker mr-2"></i>

                                 

                                </aside>
                            </figure>
                            <div class="ts-item__info-badge">Nilai Investasi
                              </div>
                        </a>


                        <div class="card-body ts-item__body">

                        </div>

               
                    </div>

                </div>
                <!--end slide-->
@endforeach
             

            </div>
        </section>

      

</div>

    </div>

    <div>




        <!--end container-->
        </section>

    </div>
</div>
<div id="map" 
     data-dojo-type="dijit/layout/ContentPane" 
     data-dojo-props="region:'center'" 
     style="padding:0;"></div>
</body>
<script src="{{ url('assets/js/custom.js') }}"></script>
<script src="https://127.0.0.1/map/html/assets/js/owl.carousel.min.js"></script>


<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<script type="text/javascript"
    src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript"
    src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
</script>
   <script>
    $(document).ready(function () {
@foreach($bentuk as $list)
        $.get("{{ url('/chart') }}/{{$pages}}/{{$list->id_daerah}}", function (data) {
            @endforeach
            $("#mydatas").append(data);




        });

    });
</script>

<body>
    
    <script>
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 400
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>

    
    
    @endsection