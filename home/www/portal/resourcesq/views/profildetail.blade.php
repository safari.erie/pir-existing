    @extends('frontend.layoutmap')

    @section('content')
<html>
<script>
          $.get("{{ Config::get('app.url') }}/chartsectordaerah/{{ Request::segment(3) }}/2017", function (data) {
                $("#sector").html(data);
               
              
            });
            $.get("{{ Config::get('app.url') }}/chartkomoditidaerah/{{ Request::segment(3) }}/2017", function (data) {
                $("#sector2").html(data);
               
              
            });

            $.get("{{ Config::get('app.url') }}/chartpdrbdaerah/{{ Request::segment(3) }}/2017", function (data) {
                $("#sector4").html(data);
               
              
            });
       </script>
   
    <style type="text/css">
        g[class^='raphael-group-'][class$='-creditgroup'] {
            display: none !important;
        }
    </style>
    <style type="text/css">
        g[class$='creditgroup'] {
            display: none !important;
        }
    </style>

    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

<link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">    
<link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
   <script>
     $(document).ready(function () {

        $('#sectorz2').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartkomoditidaerah/{{ Request::segment(3) }}/"+tahun+"", function (data) {
            $("#sector2").html(data);
           
          
        });
});

        $('#sectorz').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartsectordaerah/{{ Request::segment(3) }}/"+tahun+"", function (data) {
            $("#sector").html(data);
           
          
        });
});
$('#sectorz4').on('change', function() {

var tahun = $(this).find(":selected").val() ;
$.get("{{ Config::get('app.url') }}/chartpdrbdaerah/{{ Request::segment(3) }}/"+tahun+"", function (data) {
            $("#sector4").html(data);
           
          
        });
});
           
  
  $("#layerclick").click(function(){
         
         $('#layerhidden').toggle();
         $('#layeron').toggleClass("hijau");

     
});
$("#baseclick").click(function(){
         
         $('#base').toggle();
         $('#baseon').toggleClass("hijau");

     
});
 $('#layerhidden').hide();
 $('#base').hide();

});
   
     
     </script>
 <script>
    $(document).ready(function () {
        
@foreach($bentuk as $list)
        $.get("{{ Config::get('app.url') }}/chart/{{$pages}}/{{$list->id_daerah}}", function (data) {
            @endforeach
            $("#mydatas").append(data);




        });

    });
</script>

<body>
    
    <script>
        const postDetails = document.querySelector(".long");
        const postSidebar = document.querySelector(".when");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration: 400
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>





<script src="https://js.arcgis.com/3.34/"></script>

<script> 
var map;
require([
  "esri/map",
   "esri/dijit/PopupTemplate",
     "esri/layers/FeatureLayer",
    "dojo/_base/array",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/geometry/Geometry",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
    "esri/InfoTemplate",
   "esri/dijit/BasemapGallery",
    "esri/arcgis/utils",
  "dojo/parser",

  "dijit/layout/BorderContainer", 
  "dijit/layout/ContentPane", 
  "dijit/TitlePane",
  "dojo/domReady!"
], function(
  Map,
        PopupTemplate, 
     FeatureLayer, 
     arrayUtils, 
     ArcGISDynamicMapServiceLayer, 
     Geometry, 
     Point, 
     webMercatorUtils,  
     Graphic, 
     SimpleMarkerSymbol, 
     SimpleLineSymbol, 
     SimpleFillSymbol,
     PictureMarkerSymbol,  
     Color, 
     InfoTemplate,
   BasemapGallery,
    arcgisUtils,
  parser
) {
  parser.parse();

 
@foreach($propertiesx as $list){
map = new Map("map", {
  basemap: "hybrid",
  center: [{{$list->y}}, {{$list->x}}],
  zoom: 6
});
}
@endforeach

  //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
  var basemapGallery = new BasemapGallery({
    showArcGISBasemaps: true,
    map: map
  }, "basemapGallery");
  basemapGallery.startup();
  
  basemapGallery.on("error", function(msg) {
    console.log("basemap gallery error:  ", msg);
  });

  

   var title1 ='<div align="center">Batas Wilayah Provinsi</div>';
var isi1 ='<div style="float:left;"  >Nama Provinsi :  </div><dd style="margin-left:103px;  ; "class="border-bottom pb-2"> {Provinsi}</dd>'
var title2 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi2 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
var title4 ='<div align="center">Batas Wilayah Kecamatan</div>';
var isi4 ='<div style="float:left;"  >Nama Kecamatan :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {WA}</dd>'
       var popupprov = new PopupTemplate({
        title: title1,
      description: isi1
      
    });

    var popupkota = new PopupTemplate({
        title: title2,
      description: isi2
      
    });

    var popupkabupaten = new PopupTemplate({
        title: title4,
      description: isi4
      
    });
  

    var title3 ='<div align="center">RTRW Kabupaten</div>';
var isi3 ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popuprtrw = new PopupTemplate({
            title: title3,
          description: isi3
          
        });
      

          $.post("https://gistaru.atrbpn.go.id/arcgis/tokens/generateToken",
    {
      username: 'bkpm-sipd',
      password: 'Bkpm_S1pd@atrbpn123'
     
      
      
      
    },
   
        
    function(data,status){
  


    });
    var featureLayerprovinsi = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0",{
      outFields: ["*"],
      
infoTemplate: popupprov
 });
map.addLayer(featureLayerprovinsi);
var featurekota = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/2017/Batas_Admin/MapServer/2",{
      outFields: ["*"],
      
infoTemplate: popupkota
 });
map.addLayer(featurekota);

var featurekabupaten = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_kabupaten/MapServer/0",{
    opacity: 0.5,
      outFields: ["*"],
      
infoTemplate: popupkabupaten
 });
map.addLayer(featurekabupaten);

                    // Layer Infrastruktur
                    var title4 ='<div align="center">Infrastruktur Rumah Sakit</div>';
                         var isi4 ='<div style="float:left;"  >Rumah Sakit :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {nama}</dd><div style="float:left;"  >Alamat :</div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {alamat}</dd>'
                            var Popup_rumah_sakit = new PopupTemplate({
                                title: title4,
                                    description: isi4
                                            });
                                     var Rumah_sakit = new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/sarana_prasarana/rumah_sakit_id/MapServer/0",{
                        outFields: ["*"],
                        
                    infoTemplate: Popup_rumah_sakit
                    });
                    map.addLayer(Rumah_sakit);

                    // Akhir layer infrastruktur


        <?php foreach ($projecs as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Pangan dan Pertanian') { ?>
         
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($projecs as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Infrastruktur') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("openmap").innerHTML = "{{$list->y}}, {{$list->x}}";
           alert('as');
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($projecs as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Jasa') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       <?php foreach ($projecs as $list): ?>
              @foreach($bentuk as $lisa)
        
              <?php if($list->sektor == 'Industri') { ?>
          
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         
        
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);
              layer1.on('click', function () {
            document.getElementById("dadan").innerHTML = "{{$list->y}}, {{$list->x}}";
        })
          
        
        <?php } ?>

      
    
      @endforeach
       @endforeach

       Rumah_sakit.hide();
      $('input:checkbox[name=rumah]').change(function () {
          if ($(this).is(':checked')) {
              //map.graphics(layer1).hide();
             
              Rumah_sakit.show();
          
              // map.graphics.hide();  
          } else {
            Rumah_sakit.hide();
          }


          function hideLayer1() {

              dd.push(layer1);
              
          }

          function hideLayer2() {}
      });

});


</script> 

    <script>
        FusionCharts.ready(function () {


            var visitChart = new FusionCharts({
                        type: "line",
                        renderAt: "chart-container3",
                        id: "demoChart",
                        width: "100%",
                        height: "400",
                        dataFormat: "json",
                        dataSource: {
                            "chart": {
                                "theme": "fusion",
                                "caption": "Province Umr (Last 5 Years)",
                                "subcaption": "2015-2020",
                                "xaxisname": "Quarter",
                                "yaxisname": "(IDR per Month)",
                                "numberPrefix": "Rp.",
                                "rotateValues": "0",
                                "yAxisMinValue":'2000000',
                                "placeValuesInside": "0",
                                "valueFontColor": "#000000",
                                "valueBgColor": "#FFFFFF",
                                "valueBgAlpha": "50",
                                //Disabling number scale compression
                                "formatNumberScale": "0",
                                //Defining custom decimal separator
                                "decimalSeparator": ",",
                                //Defining custom thousand separator
                                "thousandSeparator": ".",

                                "showValues": "1"
                            },
                            "data": [@foreach($umr as $umr){
                                @if ($umr->tahun == '2016')
                                    "label": "2016",
                                    "value": "{{$umr->umr_2016}}"
                                    @elseif ($umr->tahun == '2017')
                                    "label": "2017",
                                    "value": "{{$umr->umr_2017}}"
                                    @elseif ($umr->tahun == '2018')
                                    "label": "2018",
                                    "value": "{{$umr->umr_2018}}"
                                    @elseif ($umr->tahun == '2019')
                                    "label": "2019",
                                    "value": "{{$umr->umr_2019}}"
                                    @elseif ($umr->tahun == '2020')
                                    "label": "2020",
                                    "value": "{{$umr->umr_2020}}"
                                    @endif
                                }, @endforeach
                            
                            ]
                        }
                    }

                )
                .render();

        
        });
    </script>
    <script>FusionCharts.ready(function() {
var myChart1 = new FusionCharts({
type: "msline",
renderAt: "chart-container4",
width: "100%",
height: "400",
dataFormat: "json",
dataSource: {
                chart: {
                    "theme": "fusion",
                    "caption": "Yearly Growth Population in last 5 Years",
                    "subcaption": "2015-2020",
                    "yaxisname": "(IDR per Month)",
                    "numberPrefix": "Rp.",
                    "rotateValues": "0",
                    "placeValuesInside": "0",
                    "valueFontColor": "#000000",
                    "valueBgColor": "#FFFFFF",
                    "valueBgAlpha": "50",
                    //Disabling number scale compression
                    "formatNumberScale": "0",
                    //Defining custom decimal separator
                    "decimalSeparator": ",",
                    //Defining custom thousand separator
                    "thousandSeparator": ".",


                },
                categories: [{
                    category: [{
                            label: "2012"
                        },
             
                        {
                            label: "2013"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
         
            "dashed": "1"
        },
                        {
                            label: "2014"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
        
            "dashed": "1"
        },
                        {
                            label: "2015"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
         
            "dashed": "1"
        },
                        {
                            label: "2016"
                        },
                        {
            "vline": "true",
            "lineposition": "0",
            "color": "#62B58F",
            "labelHAlign": "center",
            "labelPosition": "0",
       
            "dashed": "1"
        },
                    ]
                }],
                dataset: [{
                        seriesname: "Agriculture",
                        data: [{
                                value: "1800000"
                            },
                            {
                                value: "2200000"
                            },
                            {
                                value: "2400000"
                            },
                            {
                                value: "2600000"
                            },
                            {
                                value: "3400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Animal Husbandry",
                        data: [{
                                value: "3800000"
                            },
                            {
                                value: "1200000"
                            },
                            {
                                value: "2900000"
                            },
                            {
                                value: "3600000"
                            },
                            {
                                value: "3400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Plantation",
                        data: [{
                                value: "1800000"
                            },
                            {
                                value: "1200000"
                            },
                            {
                                value: "1900000"
                            },
                            {
                                value: "1600000"
                            },
                            {
                                value: "2400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Fishery",
                        
                        data: [{
                                value: "1300000"
                            },
                            {
                                value: "1200000"
                            },
                            {
                                value: "1500000"
                            },
                            {
                                value: "1600000"
                            },
                            {
                                value: "2400000"
                            }
                        ]
                    }
                ],
            }
}).render();
});</script>
  
  <script>
    FusionCharts.ready(function() {
var myChart2 = new FusionCharts({
type: "msline",
renderAt: "chart-container6",
width: "100%",
height: "430",
dataFormat: "json",
dataSource: {
                chart: {
                    "theme": "fusion",
                    "caption": "Yearly Growth Population in last 5 Years",
                    "subcaption": "2015-2020",
                    "rotateValues": "0",
                    "placeValuesInside": "0",
                    "valueFontColor": "#000000",
                    "valueBgColor": "#FFFFFF",
                    "decimals": "4",
                    "yAxisNamePadding": "22",
                
                    "yAxisMinValue":'2500000',
                
                    "numDivLines": "2",
                    "adjustDiv": "222",
                    "valueBgAlpha": "50",
                    //Disabling number scale compression
                    "formatNumberScale": "0",
                    //Defining custom decimal separator
                    "decimalSeparator": ",",
                    //Defining custom thousand separator
                    "thousandSeparator": ".",
"showValues": "1"

                },
                categories: [{
                    category: [@foreach($demografi as $demo){
                            label: "{{$demo->tahun}}"
                        }, @endforeach
             
                     
                    ]
                }],
                dataset: [{
                        seriesname: "Woman",
                        data: [@foreach($demografi as $demo)
                        
                        { 
                            @if($demo->tahun == 2016)
                                value: "{{$demo->wanita_2016}}"
                                @elseif($demo->tahun == 2016)
                                value: "{{$demo->wanita_2016}}"
                                @elseif($demo->tahun == 2017)
                                value: "{{$demo->wanita_2017}}"
                                @elseif($demo->tahun == 2018)
                                value: "{{$demo->wanita_2018}}"
                                @elseif($demo->tahun == 2019)
                                value: "{{$demo->wanita_2019}}"
                                @elseif($demo->tahun == 2020)
                                value: "{{$demo->wanita_2020}}"
                                @endif
                            },
                             @endforeach
                         
                        ]
                    },
                    {
                        seriesname: "Man",
                        data: [@foreach($demografi as $demo)
                        
                        { 
                            @if($demo->tahun == 2016)
                                value: "{{$demo->pria_2016}}"
                                @elseif($demo->tahun == 2016)
                                value: "{{$demo->pria_2016}}"
                                @elseif($demo->tahun == 2017)
                                value: "{{$demo->pria_2017}}"
                                @elseif($demo->tahun == 2018)
                                value: "{{$demo->pria_2018}}"
                                @elseif($demo->tahun == 2019)
                                value: "{{$demo->pria_2019}}"
                                @elseif($demo->tahun == 2020)
                                value: "{{$demo->pria_2020}}"
                                @endif
                            },
                             @endforeach
                        ]
                    },
                 
                ],
            }
}).render();
});
  </script>
     <body class="claro">
    <div class="row">
        <div class="col-sm-3">
            <section id="tbody" class="when" style=" margin-top:10px; margin-left:5px; margin-right:5px; ">

                <div id="mydatas"></div>

                <!--end container-->
            </section>
        </div>
        <br>
        
        <div class="col-sm-9">
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">


                <div  class="clearfix  btn-pro titlez">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            Profile 
                        </a>
                    </div>




                </div>
                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <div style="margin-right:2px; margin-left:2px;">
                            <div class="clearfix  btn-pro titlez">

                                <!--Display selector on the left-->
                                <div class="">
 @foreach($bentuk as $list)
                          <a id="ts-display-list"  class="btn">
                                      Provinsi {{$list->nama}}
                                    </a>
                             @endforeach
                                   
                                </div>


                            </div>

                            <br>
                            {{-- Memanggil Logo daerah  --}}
                            @foreach($bentuk as $list)
                            <div align="center" style=" font-size:14px" class=" ">
                            
                                @if($list->id_daerah == 11)

                                <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/{{$list->logo}}" height="110px"
                                    width="110px">

                                @elseif($list->id_daerah == 14)
                                <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/riau.jpg"
                                    height="110px" width="110px">
                                @else
                                <img src="{{ Config::get('app.url') }}/attachment/{{$list->id_daerah}}/attachment/{{$list->logo}}"
                                    height="110px" width="110px">
                                @endif
                            </div>
                            @endforeach
                             @foreach($bentuk as $list)
                             <div align="center">{{$list->nama}}</div>
                             @endforeach
                             <br>
                           
                            <div style="margin-right:2px; margin-left:2px;">
                                <div class="clearfix  btn-pro titlez">

                                    <!--Display selector on the left-->
                                    <div class="">

                                        <a id="ts-display-list" class="btn">
                                            Administrative Cities
                                        </a>
                                    </div>


                                </div>
                                <br>
<select style="color:rgba(49, 18, 18, 0.795)" class="custom-select titlez" id="kota" name="status">
<option value="">Kabupaten/kota</option>
                                {{-- Memanggil Kota --}}
                                @foreach($kota as $list)
                              
                                @if($list->nama == "sasadsd")

                                <div align="center" style="margin-top:5px; margin-left:20px; margin-right:20px;"
                                    class="">

                                    @else
                    
                                <option value="">{{$list->bentuk_daerah}} {{$list->nama}}</option>

                                        <br>
                                        @endif
                                    </div>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                            <div style="margin-right:5px; margin-left:5px;">
                                <div class="clearfix  btn-pro titlez">

                                    <!--Display selector on the left-->
                                    <div class="">

                                        <a id="ts-display-list" class="btn">
                                            Profile
                                        </a>
                                    </div>




                                </div>
                                <div data-dojo-type="dijit/layout/BorderContainer" 
                                data-dojo-props="design:'headline', gutters:false" 
                                style="width:100%;height:100%;margin:0;">
                                <div id="map" 
                                data-dojo-type="dijit/layout/ContentPane" 
                                data-dojo-props="region:'center'" 
                                style="padding:0;">
                                <div style=" width:200px;  top:200px;" class="ts-form__map-search ts-z-index__2">

                            <!--Form-->
                            <form>
                                <!-- <div align="center" class="zink">
                                    <div href=".ts-form-collapse" data-toggle="collapse" class="">

                                        <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Legenda

                                    </div>

                                </div>

                                <div class="ts-box p-1">
                                    <div style="margin-top:5px;"><img
                                            src="https://static.arcgis.com/images/Symbols/Government/Tax-Reverted-Property-Yes.png"
                                            width="23px;">&nbsp;Industri</div>
                                    <div style="margin-top:5px;"><img
                                            src="https://static.arcgis.com/images/Symbols/Government/Demolition-Contracted.png"
                                            width="23px;">&nbsp;Infrastruktur</div>
                                    <div style="margin-top:5px;"><img
                                            src="https://static.arcgis.com/images/Symbols/Government/Invasive-Pest-Species-Highly-Suspected.png"
                                            width="23px;">&nbsp;Pangan & Pertanian</div>
                                    <div style="margin-top:5px;"><img
                                            src="http://static.arcgis.com/images/Symbols/Government/HabiliationFacility.png"
                                            width="23px;">&nbsp;Jasa</div>


                                </div>


 -->


                                <!--end ts-form-collapse-->

                            </form>
                            <!--end ts-form-->
                            </div>
                            <div  style="width:35px;  right:14px; top:70px" id="layeron" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                        <div id="layerclick"  class="">

                            <i class="fa fa-map-o" aria-hidden="true"></i>

                        </div>
                    </div>
                    <div  style="width:35px;  right:14px; top:110px" id="baseon" align="center" class="ts-box p-1 ts-form__map-search ts-z-index__2">
                        <div id="baseclick"  class="">

                            <i class="fa fa-th-large" aria-hidden="true"></i>

                        </div>
                    </div>
                     <div id="layerhidden" style="right:40px; top:125px width:300px" class="ts-form__map-search ts-z-index__2">
                    <form>
                        <div align="center" class="zink">
                            <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">
                                <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer Infrastruktur
                            </div>
                        </div>
                        <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  > Rumah Sakit : </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1 checked"><input type='checkbox' value="false"
                                    name="rumah" /><span class="toogle "></span></div></dd><br>
                </div>
                <div style="align-center;" class="zink "
                            id="collapseExample">
                            <div style="float:left;"  >Pendidikan :  </div><dd style="margin-top:1px;"class=""> <div style="float:right; margin-right:20px;" class="box-1"><input type='checkbox'
                                    name="dadan" /><span class="toogle"></span></div></dd><br>
                </div>
                </form>
                </div>

                             
                <div id="base" style="position:absolute; right:60px; top:10px; z-Index:999;">
                        <div data-dojo-type="dijit/TitlePane" 
                             data-dojo-props="title:'Switch Basemap', open:true">
                          <div data-dojo-type="dijit/layout/ContentPane" align="center" style="margin-left:1px; width:150px; height:280px; overflow:auto;">
                            <div id="basemapGallery"></div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>

            </section>
         
            <section id="tbody" class="ts-box p-1" style="margin-top:10px; margin-left:10px; margin-right:10px;">

              

                <div class="clearfix  btn-pro titlez">

          
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            Top Project Oppertunity
                         
                        </a>
                    </div>



                </div>
                <br>
                {{-- Memanggil project --}}
                <div id="owl" class="owl-carousel ts-items-carousel" data-owl-items="4" data-owl-nav="1">
        @foreach ($projecs as $list)
        @if($list->sektor == 'Infrastruktur' ) 
            <!--Item-->
            <!--Item-->
       
       
                <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card ">

                    <!--Ribbon-->
                    <?php  if($list->prioritas == '1'  ) { ?>
                     <div class="ts-ribbon-corner">
                       <span>Prioritas</span>
                              </div>
                       
                           <?php }; ?>


                    <!--Card Image-->
                    <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                        class="card-img ts-item__image"
                        data-bg-image="https://1.bp.blogspot.com/-N31gDhrSYZ0/Xggmzk8qkNI/AAAAAAAABLc/ERh9sQlbewkRVOOazZswKEZXbUdT_GHwQCLcBGAsYHQ/s1600/Infrastruktur.jpg">
                        
                        <figure class="ts-item__info">
                            <h6>{{$list->judul}}</h6>
                          
                        </figure>
                        <div style="background-color:#e70d0de1; border-radius: 10px; color:white;" class="ts-item__info-badge">Infrastruktur
</div>
                    </a>


                    <div class="card-body ts-item__body">
                     <div class="atbd_listing_meta">
                         <span style="background-color:#0517bbc5; border-radius: 10px"  class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                             @currency($list->nilai_investasi / 1000000) Juta
                              <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                             @currency($list->nilai_investasi / 1000000000) Milyar

                              <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                             @currency($list->nilai_investasi / 1000000000000) Triliyun
                              <?php } ?></span>
                         <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
                         <div class="atbd_listing_data_list">
                           
                                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
                            
                         </div>
                     </div>
                    </div>

                    <div class="atbd_listing_bottom_content">
                     <div class="atbd_content_left">
                         <div class="atbd_listing_category">
                             <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
                         </div>
                     </div>
                     <ul class="atbd_content_right">
                         <li class="atbd_count"></span></li>
                         <li class="atbd_save">
                           
                         </li>
                     </ul>

                 </div>

                </div>
        

@elseif($list->sektor == 'Jasa') 
    <!--Item-->
    <!--Item-->
   
        <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

            <!--Ribbon-->
            <?php  if($list->prioritas == '1' ) { ?>
             <div class="ts-ribbon-corner">
               <span>Prioritas</span>
                      </div>
               
                   <?php }; ?>


            <!--Card Image-->
            <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                class="card-img ts-item__image"
                data-bg-image="https://i1.wp.com/www.maxmanroe.com/vid/wp-content/uploads/2019/11/Pengertian-Jasa-Adalah.jpg?fit=700%2C387&ssl=1">
                <figure class="ts-item__info">
                 <h6>{{$list->judul}}</h6>
               
             </figure>
             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Jasa
</div>
         </a>


         <div class="card-body ts-item__body">
          <div class="atbd_listing_meta">
              <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                  @currency($list->nilai_investasi / 1000000) Juta
                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000) Milyar

                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                   <?php } ?></span>
              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
              <div class="atbd_listing_data_list">
                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
              </div>
          </div>
         </div>

         <div class="atbd_listing_bottom_content">
          <div class="atbd_content_left">
              <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
              </div>
          </div>
          <ul class="atbd_content_right">
              <li class="atbd_count"></span></li>
              <li class="atbd_save">
                
              </li>
          </ul>

      </div>

     </div>
     <!--end ts-item ts-card-->




     @elseif($list->sektor == 'Pangan dan Pertanian')
    <!--Item-->
    <!--Item-->
    
        <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

            <!--Ribbon-->
            <?php  if($list->prioritas == '1' ) { ?>
             <div class="ts-ribbon-corner">
               <span>Prioritas</span>
                      </div>
               
                   <?php }; ?>


            <!--Card Image-->
            <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                class="card-img ts-item__image"
                data-bg-image="https://img2.pngdownload.id/20180326/bqe/kisspng-kentucky-agriculture-natural-resource-agricultural-agriculture-5ab96fc040fc11.1686302015221022082662.jpg">
                <figure class="ts-item__info">
                 <h6>{{$list->judul}}</h6>
               
             </figure>
             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Pangan & Pertanian
</div>
         </a>


         <div class="card-body ts-item__body">
          <div class="atbd_listing_meta">
              <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                  @currency($list->nilai_investasi / 1000000) Juta
                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000) Milyar

                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                   <?php } ?></span>
              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
              <div class="atbd_listing_data_list">
                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
              </div>
          </div>
         </div>

         <div class="atbd_listing_bottom_content">
          <div class="atbd_content_left">
              <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
              </div>
          </div>
          <ul class="atbd_content_right">
              <li class="atbd_count"></span></li>
              <li class="atbd_save">
                
              </li>
          </ul>

      </div>

     </div>




     @elseif($list->sektor == 'Industri') 
       
    <!--Item-->
    <!--Item-->
  
        <div style="background-color:#0517bbc5; border-radius: 10px" class="card ts-item ts-card">

         <?php  if($list->prioritas == '1' ) { ?>
      <div class="ts-ribbon-corner">
        <span>Prioritas</span>
               </div>
        
            <?php }; ?>


            <!--Card Image-->
            <a href="{{ Config::get('app.url') }}/property/{{$list->id_info_peluang_da}}/{{clean($list->id_parent)}}"
                class="card-img ts-item__image"
                data-bg-image="https://eljohnnews.com/wp-content/uploads/2019/01/agap2_technologies-5-1920-1080.jpg">
                <figure class="ts-item__info">
                 <h6>{{$list->judul}}</h6>
               
             </figure>
             <div style="background-color:#e70d0de1; border-radius: 10px; color:white;"  class="ts-item__info-badge">Industri
</div>
         </a>


         <div class="card-body ts-item__body">
          <div class="atbd_listing_meta">
              <span style="background-color:#0517bbc5; border-radius: 10px" class="atbd_meta atbd_listing_price"><?php if ($list->nilai_investasi  < 1000000000) { ?>
                  @currency($list->nilai_investasi / 1000000) Juta
                   <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000) Milyar

                   <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                  @currency($list->nilai_investasi / 1000000000000) Triliyun
                   <?php } ?></span>
              <span id="openmap" class="atbd_meta atbd_badge_open">Invest Now</span>
              <div class="atbd_listing_data_list">
                <p><span class="fa fa-map-marker"></span>&nbsp;{{$list->bentuk_daerah}} {{$list->nama}} </p>
              </div>
          </div>
         </div>

         <div class="atbd_listing_bottom_content">
          <div class="atbd_content_left">
              <div class="atbd_listing_category">
                 <a style="color:white;" href=""><span style="color:white;" class="fa fa-eye"></span>Read More</a>
              </div>
          </div>
          <ul class="atbd_content_right">
              <li class="atbd_count"></span></li>
              <li class="atbd_save">
                
              </li>
          </ul>

      </div>

     </div>
     <!--end ts-item ts-card-->



  @endif

   
    <!--end Item col-md-4-->


 

        @endforeach
      

             </div>
            </section>
            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez ">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            Investment Realization by Sector Last Year
                        </a>
                        <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                        <a>Pilih Tahun</a>&nbsp;
                         <select style="color:grey; width:100px;" class="custom-select" id="sectorz" name="sorting">
                         <option value="2020">2020</option>
                             <option value="2019">2019</option>
                             <option value="2018">2018</option>
                             <option value="2017">2017</option>
                             <option value="2016">2016 </option>



                         </select>
                         </div>
                    </div>






                </div>
                <br>

                <div id="sector">FusionCharts will render here</div>

                <!--end container-->
            </section>

            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez ">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">

                        Top Commodities by Last Year
                        </a>

                        <div class="float-none float-sm-right pl-2 ts-center__vertical ">
                        <a>Pilih Tahun</a>&nbsp;
                         <select style="color:grey; width:100px;" class="custom-select" id="sectorz2" name="sorting">
                         <option value="2020">2020</option>
                             <option value="2019">2019</option>
                             <option value="2018">2018</option>
                             <option value="2017">2017</option>
                             <option value="2016">2016 </option>



                         </select>
                         </div>
                    </div>






                </div>
                <br>

                <div id="sector2">FusionCharts will render here</div>

                <!--end container-->
            </section>
             <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">
                            Yearly Growth Population in last 5 Years
                        </a>

                    </div>






                </div>
                <br>

                <div id="chart-container6">FusionCharts will render here</div>

                <!--end container-->
            </section>
            <section class="ts-box p-1 long" style=" margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

                <div class="clearfix  btn-pro titlez">

                    <!--Display selector on the left-->
                    <div class="">

                        <a id="ts-display-list" class="btn">

                            Province Minimum Wages (Last 5 Years)
                        </a>

                     
                    </div>






                </div>
                
                <br>

                <div id="chart-container3">FusionCharts will render here</div>

                <!--end container-->
            </section>

            <section class="ts-box p-1 long" style="margin-top:10px; margin-left:5px; margin-right:5px;">

                <!--ITEMS LISTING
            =========================================================================================================-->

            <div class="clearfix  btn-pro titlez ">

<!--Display selector on the left-->
<div class="">

    <a id="ts-display-list" class="btn">

        Gross Regional Domestic Product
    </a>
    <div class="float-none float-sm-right pl-2 ts-center__vertical ">
    <a>Pilih Tahun</a>&nbsp;
     <select style="color:grey; width:100px;" class="custom-select" id="sectorz4" name="sorting">
     <option value="2020">2020</option>
         <option value="2019">2019</option>
         <option value="2018">2018</option>
         <option value="2017">2017</option>
         <option value="2016">2016 </option>



     </select>
     </div>
 
</div>



                </div>
                <br>

                <div id="sector4">FusionCharts will render here</div>

                <!--end container-->
            </section>



        </div>

        <div>


            <div id="map" 
            data-dojo-type="dijit/layout/ContentPane" 
            data-dojo-props="region:'center'" 
            style="padding:0;"></div>

            <!--end container-->
            </section>

        </div>
    </div>
</body>
    <script src="{{Config::get('app.url') }}/assets/js/custom.js"></script>
     <script src="{{Config::get('app.url')}}/map/html/assets/js/owl.carousel.min.js"></script>

 

    <script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Include fusioncharts core library file -->
    <!-- Include fusioncharts jquery plugin -->
    <script type="text/javascript"
        src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js">
    </script>
     
        @endsection