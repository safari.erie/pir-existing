

<!-- JavaScript Libraries -->
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/jquery/jquery.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/jquery/jquery-migrate.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/easing/easing.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/superfish/hoverIntent.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/superfish/superfish.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/wow/wow.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/magnific-popup/magnific-popup.min.js"></script>
<script src="{{ $_ENV['APP_URL'] }}/public/assets/lib/sticky/sticky.js"></script>
<!-- Contact Form JavaScript File -->
<!--script src="{{ $_ENV['APP_URL'] }}/public/assets/contactform/contactform.js"></script-->

<!-- Template Main Javascript File -->
<script type="text/javascript" src="{{ $_ENV['APP_URL'] }}/public/assets/lib/elastislide/jquerypp.custom.js"></script>
<script type="text/javascript" src="{{ $_ENV['APP_URL'] }}/public/assets/lib/elastislide/jquery.elastislide.js"></script>

<script src="{{ $_ENV['APP_URL'] }}/public/assets/js/main.js"></script>

<script type="text/javascript" src="{{ $_ENV['APP_URL'] }}/vendor/starrr/dist/starrr.js"></script>
<script>
	
	@if ($ratingList)
		@foreach ($ratingList as $rate)
			$('#star{{ $rate->rating_id }}').starrr({
			  change: function(e, value){
				if (value) {
					$('#txt_star{{ $rate->rating_id }}').val(value);
				} else {
					$('#txt_star{{ $rate->rating_id }}').val('0');
				}
			  }
			});
		@endforeach
	@endif
	
	/*
	@if ($postRate)
		@foreach ($postRate as $pr)
			@if ($postRateDt[$pr->rating_post_id])
			@foreach ($postRateDt[$pr->rating_post_id] as $dt)
				$('#star_txt{{ $dt->rating_post_dt_id }}').starrr({
					rating : {{ $dt->rate }},
					readOnly : true
				});
			@endforeach
			@endif
		@endforeach
	@endif
	*/
	
	<!-- captcha -->
	$(".btn-refresh").click(function(){
		$.ajax({
			type:'GET',
			url:'{{ Config::get("app.url") }}/refresh_captcha',
			success:function(data){
				$(".captcha span").html(data.captcha);
			}
		});
	});
</script>
  