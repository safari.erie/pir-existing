@extends('frontend.layoutmap')

@section('content')


  <div > <section id="tbody" class="tx-box p-1"style=" margin-top:10px; margin-left:20px; margin-right:20px; ">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="zink hijau">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 Contact
             </a>
           
         </div>

         <!--Display selector on the right-->


     </div>
     <div class="row">
  <div class="col-sm-4"><section class="ts-box p-1" id="tbody" style="  ">

     <!--ITEMS LISTING
            =========================================================================================================-->

     <div  class="zink">

         <!--Display selector on the left-->
         <div  class="">

             <a id="ts-display-list" class="btn">
                 DPM PTSP DKI JAKARTA
             </a>
           
         </div>
       <br>
       
         <div style="float:left;"  >Address : </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">Jl. Epicentrum Selatan No.
Kav. 22, RT 2 RW 5, Karet
Kuningan, Kecamatan
Setiabudi, Kota Jakarta
Selatan, DKI Jakarta 12940</dd></div>

 <div style="float:left;"  >Address : </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">Jl. Epicentrum Selatan No.
Kav. 22, RT 2 RW 5, Karet
Kuningan, Kecamatan
Setiabudi, Kota Jakarta
Selatan, DKI Jakarta 12940</dd></div>
        
         </section>
       
         
         </div>
  <div class="col-sm-8">col-sm-3</div>
</div>
  
     <!--end container-->
 </section>
 </DIV>
 	<script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
		<script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
		<script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
		<script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
		
<script>
	FusionCharts.ready(function() {
  var revenueChart = new FusionCharts({
    type: 'doughnut2d',
    renderAt: 'chart-container',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Foreign Direct Investment (Thousand US $)",
        "subCaption": "Last year",
        "numberPrefix": "$",
        "bgColor": "#ffffff",
        "startingAngle": "310",
        "showLegend": "1",
        "defaultCenterLabel": "Total revenue: $64.08K",
        "centerLabel": "Revenue from $label: $value",
        "centerLabelBold": "1",
        "showTooltip": "0",
        "decimals": "0",
        "theme": "fusion"
      },
      "data": [{
          "label": "Food",
          "value": "28504"
        },
        {
          "label": "Apparels",
          "value": "14633"
        },
        {
          "label": "Electronics",
          "value": "10507"
        },
        {
          "label": "Household",
          "value": "4910"
        }
      ]
    }
  }).render();
});
	</script>
 <script>
	FusionCharts.ready(function() {
  var revenueChart = new FusionCharts({
    type: 'doughnut2d',
    renderAt: 'chart-container2',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Domestic Direct Investment (Thousand US $)",
        "subCaption": "Last year",
        "numberPrefix": "$",
        "bgColor": "#ffffff",
        "startingAngle": "310",
        "showLegend": "1",
        "defaultCenterLabel": "Total revenue: $64.08K",
        "centerLabel": "Revenue from $label: $value",
        "centerLabelBold": "1",
        "showTooltip": "0",
        "decimals": "0",
        "theme": "fusion"
      },
      "data": [{
          "label": "Food",
          "value": "28504"
        },
        {
          "label": "Apparels",
          "value": "14633"
        },
        {
          "label": "Electronics",
          "value": "10507"
        },
        {
          "label": "Household",
          "value": "4910"
        }
      ]
    }
  }).render();
});
	</script>


    @endsection