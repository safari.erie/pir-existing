@extends('frontend.layoutmap')

@section('content')






  <link rel="stylesheet" href="https://js.arcgis.com/3.34/dijit/themes/claro/claro.css">
  <link rel="stylesheet" href="https://js.arcgis.com/3.34/esri/css/esri.css">

  

<style>
    .wew {
        background-color: rgb(68, 38, 201);
        margin-top: 3px;
    }

    .checked {
        display: flex;
        color: orange;
    }
</style>
 <main id="ts-main">
<body class="demo">
    <div id="loader-wrapper" style="background:#fffff;">
        <div id="loader"></div>


        <div style="background:#fffff;" class="loader-section section-left"></div>
        <div style="background:#fffff;" class="loader-section section-right"></div>

    </div>
    


        <!--end form-row-->

    
        <!-- filter-horizontal -->

        <div class=" row">

            <div class="col-md-4">
                <div class="widget">
                    <section style="top:19px;" class="ts-box p-1">
                        <div class="zink ">

                            <!--Display selector on the left-->
                            <div class="zink " align="center">

                                <i class="fa fa-search"> Info Peluang</i>


                            </div>
                        
                        <br>
                       
@foreach($propertiesz as $list)
 @foreach($bentuk as $lisa)
                        <div style="float:left;"  >Sektor  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->sektor}}</dd>
                        <div style="float:left;"  >Proyek   </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->judul}}</dd>
<div style="float:left;"  >Lokasi   </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->nama}}, Provinsi {{$lisa->nama}}</dd>
@endforeach
                       
                      <div style="float:left;"  >Luas Area </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
 
<div style="float:left;"  >Tahun  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"> {{$list->tahun}}</dd>
<div style="float:left;"  ><div style="float:left;">Nilai</div> <br> Investasi  </div><dd style="margin-left:75px; margin-top:20px; text-align:justify; "class="border-bottom pb-2"> <?php if ($list->nilai_investasi  < 1000000000) { ?>
                               @currency($list->nilai_investasi / 1000000) Juta
                                <?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000) Milyar

                                <?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>
                               @currency($list->nilai_investasi / 1000000000000) Triliyun
                                <?php } ?></dd>
                                 <div style="float:left;"  >IRR </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                 <div style="float:left;"  >NPV </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                  <div style="float:left;"  >PP </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                               <div style="float:left;"  >Penghubung</div><dd style="margin-left:90px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                               <div style="float:left;"  >No Telepon </div><dd style="margin-left:90px; text-align:justify; "class="border-bottom pb-2"><br></dd>
                                               <div style="float:left;"  >Jabatan </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2"><br></dd>
<div style="float:left;"  >Sumber  </div><dd style="margin-left:75px; text-align:justify; "class="border-bottom pb-2">{{$list->instansi}}</dd>
<div style="float:left;"  ></div><dd style="margin-left:75px; text-align:justify; "class=""> <button data-toggle="modal" data-target="#myModal" class="btn zink hijau">Interested</button></dd>
@endforeach
                        
                        </form>

                    </section>
                    </aside>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="post-details">
             
            <div style=" width:300px; left:100px; top:100px" class="ts-form__map-search ts-z-index__2">


                <!--Form-->
                <form>
                    <div  align="center" class="zink">
                        <div href=".ts-form-collapse" data-toggle="collapse" class="zink aku">

                            <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;Layer

                        </div>
                    </div>
            
                   

                     <div style ="align-center;"class="zink ts-form-collapse ts-xs-hide-collapse collapse" id="collapseExample">
                        <div  style="float:left; "><i class="fa fa-road" aria-hidden="true"></i> RTRW &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;</div>
                        &nbsp;&nbsp;<div style="float:right; margin-right:20px;"class="box-1"><input type='checkbox' name="dadan"/><span class="toogle"></span></div>

                    </div>
                   

            </div>
            <!--end ts-form-collapse-->

            </form>
            <!--end ts-form-->
            </div>


        </section>
          <section id="tbody" class="ts-boxz p-1" style="margin-top:20px;">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Map
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                             

                            </div>
                            </section>
                            <br>
                     <div style="position:absolute; right:20px; top:10px; z-Index:999;">
        <div data-dojo-type="dijit/TitlePane" 
             data-dojo-props="title:'Switch Basemap', open:true">
          <div data-dojo-type="dijit/layout/ContentPane" style="width:380px; height:280px; overflow:auto;">
            <div id="basemapGallery"></div>
          </div>
        </div>
      </div>
                     <div id="map" class="ts-map w-100 ts-min-h__50 vh ts-z-index__1" style="width: 100%; height:400px;   "></div>
                     
                            </section>
          <section id="tbody" class="ts-boxz p-1" style="margin-top:20px;">

                        <!--ITEMS LISTING
            =========================================================================================================-->
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Image & Video
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                             

                            </div>
                            </section>
                            <br>
                    
                     <div class="row">
  <div class="col-sm-6"> <div class="owl-carousel ts-gallery-carousel" data-owl-auto-height="1" data-owl-dots="1" data-owl-loop="1">

                                <!--Slide-->
                                <div class="slide">
                                    <div class="ts-image" data-bg-image="https://kwriu.kemdikbud.go.id/wp-content/uploads/2017/12/Istana-Siak-featured-742x440.jpg">
                                        <a href="https://kwriu.kemdikbud.go.id/wp-content/uploads/2017/12/Istana-Siak-featured-742x440.jpg" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                                    </div>
                                </div>

                              

                            </div></div>
  <div class="col-sm-6"><section style="bottom:20px; "id="video">

                       <br>

                            <div class="embed-responsive embed-responsive-16by9 rounded ts-shadow__md">
                          @if (count($propertiesx))   
                                <iframe src="http://127.0.0.1/easy/video/{{$list->id_daerah}}/{{$list->video}}" width="640" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                          @else
                          <p>Kosong</p>
                          @endif
                            </div>

                      

                        </section>
                        </div>
</div>
</section>

                           
                        
               
                   <section  class="ts-boxz p-1" style="margin-top:20px;">

            
                        <section id="display-control zink">
                            <div class="container clearfix zink ">

                                <!--Display selector on the left-->
                                <div class="float-left">
                                
                                    <a id="ts-display-list" class="btn">
                                        Deskripsi
                                    </a>
                                </div>

                                <!--Display selector on the right-->
                             

                            </div>
                            </section>
                           
                    
                    
                     
                           
                            
                            
                  
                         <div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->
    <div class="modal-content">

      <!--Modal cascading tabs-->
      <div class="modal-c-tabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
              Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
              Register</a>
          </li>
        </ul>

        <!-- Tab panels -->
        <div class="tab-content">
          <!--Panel 7-->
          <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

            <!--Body-->
            <div class="modal-body mb-1">
              <div class="md-form form-sm mb-5">
                <input type="email" id="modalLRInput10" class="form-control form-control-sm validate">
                <label data-error="wrong" data-success="right" for="modalLRInput10">Your email</label>
              </div>

              <div class="md-form form-sm mb-4">
                <i class="fas fa-lock prefix"></i>
                <input type="password" id="modalLRInput11" class="form-control form-control-sm validate">
                <label data-error="wrong" data-success="right" for="modalLRInput11">Your password</label>
              </div>
              <div class="text-center mt-2">
                <button class="btn btn-info">Log in <i class="fas fa-sign-in ml-1"></i></button>
              </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
              <div class="options text-center text-md-right mt-1">
                <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p>
                <p>Forgot <a href="#" class="blue-text">Password?</a></p>
              </div>
              <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
            </div>

          </div>
          <!--/.Panel 7-->

          <!--Panel 8-->
          <div class="tab-pane fade" id="panel8" role="tabpanel">

            <!--Body-->
            <div class="modal-body">
              <div class="md-form form-sm mb-5">
                <i class="fas fa-envelope prefix"></i>
                <input type="email" id="modalLRInput12" class="form-control form-control-sm validate">
                <label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>
              </div>

              <div class="md-form form-sm mb-5">
                <i class="fas fa-lock prefix"></i>
                <input type="password" id="modalLRInput13" class="form-control form-control-sm validate">
                <label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>
              </div>

              <div class="md-form form-sm mb-4">
                <i class="fas fa-lock prefix"></i>
                <input type="password" id="modalLRInput14" class="form-control form-control-sm validate">
                <label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>
              </div>

              <div class="text-center form-sm mt-2">
                <button class="btn btn-info">Sign up <i class="fas fa-sign-in ml-1"></i></button>
              </div>

            </div>
            <!--Footer-->
            <div class="modal-footer">
              <div class="options text-right">
                <p class="pt-1">Already have an account? <a href="#" class="blue-text">Log In</a></p>
              </div>
              <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!--/.Panel 8-->
        </div>

      </div>
    </div>
    <!--/.Content-->
  </div>
  </div>   
                            

 

    </main>

    <!--end #ts-main-->
    </div>
    </div>
    <br>
     
    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->


    <!-- Footer area - footer_down -->

    <!-- End Footer area- footer_down -->


    <!--end page-->
    <!--ITEMS LISTING

    <!--end #ts-main-->

    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->


    <?php

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<script src="https://cdn.jsdelivr.net/npm/ion-rangeslider@2.3.0/js/ion.rangeSlider.min.js"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
<script>
var $range = $(".js-range-slider"),
    $inputFrom = $(".js-input-from"),
    $inputTo = $(".js-input-to"),
    instance,
    min = 1,
    max = 100,
    from = 0,
    to = 0;

$range.ionRangeSlider({
	skin: "round",
    type: "double",
    min: min,
    max: max,
    from: 1,
    to: 100,
    prefix:"Rp.",
     postfix: " M",
    onStart: updateInputs,
    onChange: updateInputs
});
instance = $range.data("ionRangeSlider");

function updateInputs (data) {
	from = data.from * 10000000;
    to = data.to * 1000000000;
    
    $inputFrom.prop("value", from);
    $inputTo.prop("value", to);	
}

$inputFrom.on("input", function () {
    var val = $(this).prop("value");
    
    // validate
    if (val < min) {
        val = min;
    } else if (val > to) {
        val = to;
    }
    
    instance.update({
        from: val
    });
});

$inputTo.on("input", function () {
    var val = $(this).prop("value");
    
    // validate
    if (val < from) {
        val = from;
    } else if (val > max) {
        val = max;
    }
    
    instance.update({
        to: val
    });
});</script>
<script src="https://js.arcgis.com/3.34/"></script>
    <script>
        $(document).ready(function () {

            setTimeout(function () {
                $('body').addClass('loaded');
                $('h1').css('color', '#FFFFF');
            }, 500);
           

        
         
     
     

      
 var map;

  require([
        "esri/map",
        "esri/dijit/PopupTemplate",
         "esri/layers/FeatureLayer",
        "dojo/_base/array",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/geometry/Geometry",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/Color",
        "esri/InfoTemplate",
        "dijit/layout/BorderContainer", 
        "dijit/layout/ContentPane",
        "esri/dijit/BasemapGallery",
        "esri/arcgis/utils",
      "dojo/parser",
      "esri/InfoTemplate",
        "dojo/domReady!"
        ], function(
            Map,
            PopupTemplate, 
         FeatureLayer, 
         arrayUtils, 
         ArcGISDynamicMapServiceLayer, 
         Geometry, 
         Point, 
         webMercatorUtils,  
         Graphic, 
         SimpleMarkerSymbol, 
         SimpleLineSymbol, 
         SimpleFillSymbol,
         PictureMarkerSymbol,  
         Color, 
         InfoTemplate,
         BasemapGallery, 
         arcgisUtils,
      parser,
         GraphicsLayer
         ) { 
             
          var layer1, layer2;
      var pointInfoTemplate, pointInfoTemplate2;
@foreach($propertiesx as $list){
          map = new Map("map", {
              basemap: "hybrid",
              center: [{{$list->y}}, {{$list->x}}],
              zoom: 17
          });
}
 @endforeach
       var basemapGallery = new BasemapGallery({
        showArcGISBasemaps: true,
        map: map
      }, "basemapGallery");
      basemapGallery.startup();
      
      basemapGallery.on("error", function(msg) {
        console.log("basemap gallery error:  ", msg);
      });
         var title ='<div align="center">RTRW Kabupaten</div>';
var isi ='<div style="float:left;"  >Nama Zona :  </div><dd style="margin-left:90px;  ; "class="border-bottom pb-2"> {NAMOBJ}</dd>'

           var popupTemplate = new PopupTemplate({
            title: title,
          description: isi,
          
        });
        <?php {?>
        //hide the popup if its outside the map's extent
        var key = 'fhIfqsbSxL-sLyqTMFAIwIpekJ35eLCbwkCk3fIC4JoG5uc4ce9Ap1DZ110cWX6G';
        var token='?token='+key+'';
      var featureLayer = new FeatureLayer("https://gistaru.atrbpn.go.id/arcgis/rest/services/013_RTR_KABUPATEN_KOTA_PROVINSI_RIAU/_1400_RIAU_PR_PERDA/MapServer/6"+ token,{
          outFields: ["*"],
          opacity:0.5,
    infoTemplate: popupTemplate
     });
    map.addLayer(featureLayer);
     <?php } ?>
  
          function drawSeries1() {
                 <?php foreach ($propertiesz as $list): ?>
                  @foreach($bentuk as $lisa)
            <?php if($list->sektor == 'Infrastruktur') { ?>
          var point = new esri.geometry.Point({{$list->y}}, {{$list->x}});
           var content ='<div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;"  >Proyek :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->
             judul}}</dd><div style="float:left;"  >Lokasi :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2">{{$list->bentuk_daerah}} {{$list->
             nama}}, Provinsi {{$lisa->nama}}</dd> <div style="float:left;"  >Luas Area :  </div><dd style="margin-left:60px; "margin-right:1px; text-align:justify; "class="border-bottom pb-2"> 898</dd><div style="float:left;"  >Sektor :  </div><dd style="margin-left:60px; text-align:justify; "class="border-bottom pb-2">{{$list->
             sektor}}</dd><div style="float:left;">Nilai</div>   </div><dd style="margin-left:40px; margin-bottom:20px; text-align:justify; "class="border-bottom pb-2"> : <?php if ($list->nilai_investasi  < 1000000000) { ?>@currency($list->nilai_investasi / 1000000) Juta<?php	} else if ($list->nilai_investasi  < 1000000000000) { ?>@currency($list->nilai_investasi / 1000000000) Milyar<?php	} else if ($list->nilai_investasi  < 1000000000000000) { ?>@currency($list->nilai_investasi / 1000000000000) Triliyun<?php } ?></dd>';
             
         <?php } ?>
          @endforeach
           @endforeach
              point = esri.geometry.geographicToWebMercator(point);
              var title ='<div align="center">Detail Project</div>'
              var symbol = new esri.symbol.PictureMarkerSymbol("http://static.arcgis.com/images/Symbols/OCHA/Infrastructure/infrastructure_building_bluebox.png", 25, 25);
              pointInfoTemplate = new InfoTemplate(); 
              pointInfoTemplate.setTitle(title);
              pointInfoTemplate.setContent(content);
              var graphic = new esri.Graphic(point, symbol).setInfoTemplate(pointInfoTemplate);
              layer1 = new esri.layers.GraphicsLayer();
              layer1.add(graphic);
              map.addLayer(layer1);

          }
          drawSeries1();
        
  
          $('input:checkbox[name=dadan]').change(function () {
              if ($(this).is(':checked')) {
                  //map.graphics(layer1).hide();
                  featureLayer.hide();
                  
              
                  // map.graphics.hide();  
              } else {
                  featureLayer.show();
              }

              function hideLayer1() {}

              function hideLayer2() {}
          });

      });
  $("[name='dadanh']").bootstrapSwitch({
            onSwitchChange: function (event, state) {

                if (state) {
                    map.graphics(layer1).hide();
                 


                } else {
                    map.graphics(layer1).hide();
                }
            }
        });

        layer1.hide();
        });
        
    </script>
    <script>

   
        const postDetails = document.querySelector(".ts-box");
        const postSidebar = document.querySelector(".ts-box");
        const postDetailsz = document.querySelector(".ts-boxzzs");
        const postSidebarContent = document.querySelector(".ts-bosxzz > div");


        const controller = new ScrollMagic.Controller();


        const scene = new ScrollMagic.Scene({
            triggerElement: postSidebar,
            triggerHook: 0,
            duration:100    
        }).addTo(controller);

        //3
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {
                pushFollowers: false
            });
        }

        //4
        window.addEventListener("resize", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {
                scene.setPin(postSidebar, {
                    pushFollowers: false
                });
            } else {
                scene.removePin(postSidebar, true);
            }
        });

        function getDuration() {
            return postDetails.offsetHeight - postSidebar.offsetHeight;
        }
    </script>
  <script src="http://localhost/map/html/assets/js/owl.carousel.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="http://localhost/map/html/assets/js/popper.min.js"></script>

  <script src="http://localhost/map/html/assets/js/jquery.magnific-popup.min.js"></script>
<script>
(function () {

    "use strict";

    if ( document.getElementById("ts-header").classList.contains("fixed-top") ){
        if( !document.getElementsByClassName("ts-homepage")[0] ) {
            document.getElementById("ts-main").style.marginTop = document.getElementById("ts-header").offsetHeight + "px";
        }
    }

})();

$(document).ready(function($) {
    
    "use strict";

	$('.navbar-nav .nav-link:not([href="#"])').on('click', function(){
		$('.navbar-collapse').collapse('hide');
	});

    $(".ts-img-into-bg").each(function() {
        $(this).css("background-image", "url("+ $(this).find("img").attr("src") +")" );
    });

//  Background

    $("[data-bg-color], [data-bg-image], [data-bg-pattern]").each(function() {
        var $this = $(this);

        if( $this.hasClass("ts-separate-bg-element") ){
            $this.append('<div class="ts-background">');

            // Background Color

            if( $("[data-bg-color]") ){
                $this.find(".ts-background").css("background-color", $this.attr("data-bg-color") );
            }

            // Background Image

            if( $this.attr("data-bg-image") !== undefined ){
                $this.find(".ts-background").append('<div class="ts-background-image">');
                $this.find(".ts-background-image").css("background-image", "url("+ $this.attr("data-bg-image") +")" );
                $this.find(".ts-background-image").css("background-size", $this.attr("data-bg-size") );
                $this.find(".ts-background-image").css("background-position", $this.attr("data-bg-position") );
                $this.find(".ts-background-image").css("opacity", $this.attr("data-bg-image-opacity") );

                $this.find(".ts-background-image").css("background-size", $this.attr("data-bg-size") );
                $this.find(".ts-background-image").css("background-repeat", $this.attr("data-bg-repeat") );
                $this.find(".ts-background-image").css("background-position", $this.attr("data-bg-position") );
                $this.find(".ts-background-image").css("background-blend-mode", $this.attr("data-bg-blend-mode") );
            }

            // Parallax effect

            if( $this.attr("data-bg-parallax") !== undefined ){
                $this.find(".ts-background-image").addClass("ts-parallax-element");
            }
        }
        else {

            if(  $this.attr("data-bg-color") !== undefined ){
                $this.css("background-color", $this.attr("data-bg-color") );
                if( $this.hasClass("btn") ) {
                    $this.css("border-color", $this.attr("data-bg-color"));
                }
            }

            if( $this.attr("data-bg-image") !== undefined ){
                $this.css("background-image", "url("+ $this.attr("data-bg-image") +")" );

                $this.css("background-size", $this.attr("data-bg-size") );
                $this.css("background-repeat", $this.attr("data-bg-repeat") );
                $this.css("background-position", $this.attr("data-bg-position") );
                $this.css("background-blend-mode", $this.attr("data-bg-blend-mode") );
            }

            if( $this.attr("data-bg-pattern") !== undefined ){
                $this.css("background-image", "url("+ $this.attr("data-bg-pattern") +")" );
            }

        }
    });

    $(".ts-password-toggle").on("click",function() {
        var $parent = $(this).closest(".ts-has-password-toggle");
        var $this = $(this);
        var $password = $parent.find("input");
        if ($password.attr("type") === "password") {
            $password.attr("type", "text");
            $this.find("i").removeClass("fa-eye").addClass("fa-eye-slash");
        } else {
            $password.attr("type", "password");
            $this.find("i").removeClass("fa-eye-slash").addClass("fa-eye");
        }
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $("select").each(function () {
        isSelected( $(this) );
    }).on("change", function () {
        isSelected( $(this) );
    });

    if ($(".ts-video").length > 0) {
        $(this).fitVids();
    }

    // Owl Carousel

    var $owlCarousel = $(".owl-carousel");

    if( $owlCarousel.length ){
        $owlCarousel.each(function() {

            var items = parseInt( $(this).attr("data-owl-items"), 10);
            if( !items ) items = 1;

            var nav = parseInt( $(this).attr("data-owl-nav"), 2);
            if( !nav ) nav = 0;

            var dots = parseInt( $(this).attr("data-owl-dots"), 2);
            if( !dots ) dots = 0;

            var center = parseInt( $(this).attr("data-owl-center"), 2);
            if( !center ) center = 0;

            var loop = parseInt( $(this).attr("data-owl-loop"), 2);
            if( !loop ) loop = 0;

            var margin = parseInt( $(this).attr("data-owl-margin"), 2);
            if( !margin ) margin = 0;

            var autoWidth = parseInt( $(this).attr("data-owl-auto-width"), 2);
            if( !autoWidth ) autoWidth = 0;

            var navContainer = $(this).attr("data-owl-nav-container");
            if( !navContainer ) navContainer = 0;

            var autoplay = parseInt( $(this).attr("data-owl-autoplay"), 2);
            if( !autoplay ) autoplay = 0;

            var autoplayTimeOut = parseInt( $(this).attr("data-owl-autoplay-timeout"), 10);
            if( !autoplayTimeOut ) autoplayTimeOut = 5000;

            var autoHeight = parseInt( $(this).attr("data-owl-auto-height"), 2);
            if( !autoHeight ) autoHeight = 0;

            var fadeOut = $(this).attr("data-owl-fadeout");
            if( !fadeOut ) fadeOut = 0;
            else fadeOut = "fadeOut";

            if( $("body").hasClass("rtl") ) var rtl = true;
            else rtl = false;

            if( items === 1 ){
                $(this).owlCarousel({
                    navContainer: navContainer,
                    animateOut: fadeOut,
                    autoplayTimeout: autoplayTimeOut,
                    autoplay: 1,
                    autoheight: autoHeight,
                    center: center,
                    loop: loop,
                    margin: margin,
                    autoWidth: autoWidth,
                    items: 1,
                    nav: nav,
                    dots: dots,
                    rtl: rtl,
                    navText: []
                });
            }
            else {
                $(this).owlCarousel({
                    navContainer: navContainer,
                    animateOut: fadeOut,
                    autoplayTimeout: autoplayTimeOut,
                    autoplay: autoplay,
                    autoheight: autoHeight,
                    center: center,
                    loop: loop,
                    margin: margin,
                    autoWidth: autoWidth,
                    items: 1,
                    nav: nav,
                    dots: dots,
                    rtl: rtl,
                    navText: [],
                    responsive: {
                        1368: {
                            items: items
                        },
                        992: {
                            items: 3
                        },
                        450: {
                            items: 2
                        },
                        0: {
                            items: 1
                        }
                    }
                });
            }

            if( $(this).find(".owl-item").length === 1 ){
                $(this).find(".owl-nav").css( { "opacity": 0,"pointer-events": "none"} );
            }

        });
    }

    // Magnific Popup

    var $popupImage = $(".popup-image");

    if ( $popupImage.length > 0 ) {
        $popupImage.magnificPopup({
            type:'image',
            fixedContentPos: false,
            gallery: { enabled:true },
            removalDelay: 300,
            mainClass: 'mfp-fade',
            callbacks: {
                // This prevents pushing the entire page to the right after opening Magnific popup image
                open: function() {
                    $(".page-wrapper, .navbar-nav").css("margin-right", getScrollBarWidth());
                },
                close: function() {
                    $(".page-wrapper, .navbar-nav").css("margin-right", 0);
                }
            }
        });
    }

    var $videoPopup = $(".video-popup");

    if ( $videoPopup.length > 0 ) {
        $videoPopup.magnificPopup({
            type: "iframe",
            removalDelay: 300,
            mainClass: "mfp-fade",
            overflowY: "hidden",
            iframe: {
                markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div>',
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: '//www.youtube.com/embed/%id%?autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    },
                    gmaps: {
                        index: '//maps.google.',
                        src: '%id%&output=embed'
                    }
                },
                srcAction: 'iframe_src'
            }
        });
    }

    $(".ts-form-email [type='submit']").each(function(){
        var text = $(this).text();
        $(this).html("").append("<span>"+ text +"</span>").prepend("<div class='status'><i class='fas fa-circle-notch fa-spin spinner'></i></div>");
    });

    $(".ts-form-email .btn[type='submit']").on("click", function(){
        var $button = $(this);
        var $form = $(this).closest("form");
        var pathToPhp = $(this).closest("form").attr("data-php-path");
        $form.validate({
            submitHandler: function() {
                $button.addClass("processing");
                $.post( pathToPhp, $form.serialize(),  function(response) {
                    $button.addClass("done").find(".status").append(response).prop("disabled", true);
                });
                return false;
            }
        });
    });

    if( $("input[type=file].with-preview").length ){
        $("input.file-upload-input").MultiFile({
            list: ".file-upload-previews"
        });
    }

    if( $(".ts-has-bokeh-bg").length ){

        $("#ts-main").prepend("<div class='ts-bokeh-background'><canvas id='ts-canvas'></canvas></div>");
        var canvas = document.getElementById("ts-canvas");
        var context = canvas.getContext("2d");
        var maxRadius  = 50;
        var minRadius  = 3;
        var colors = ["#5c81f9",  "#66d3f7"];
        var numColors  =  colors.length;

        for(var i=0;i<50;i++){
            var xPos       =  Math.random()*canvas.width;
            var yPos       =  Math.random()*10;
            var radius     =  minRadius+(Math.random()*(maxRadius-minRadius));
            var colorIndex =  Math.random()*(numColors-1);
            colorIndex     =  Math.round(colorIndex);
            var color      =  colors[colorIndex];
            drawCircle(context, xPos, yPos, radius, color);
        }
    }

    function drawCircle(context, xPos, yPos, radius, color)
    {
        context.beginPath();
        context.arc(xPos, yPos, radius, 0, 360, false);
        context.fillStyle = color;
        context.fill();
    }

    heroPadding();

    var $scrollBar = $(".scrollbar-inner");
    if( $scrollBar.length ) {
        $scrollBar.scrollbar();
    }

    initializeSly();
    hideCollapseOnMobile();

});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// On RESIZE actions

var resizeId;
$(window).on("resize", function(){
    clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 250);
});

// Do after resize

function doneResizing(){
    //heroPadding();
    hideCollapseOnMobile();
}

function isSelected($this){
    if( $this.val() !== "" ) $this.addClass("ts-selected");
    else $this.removeClass("ts-selected");
}

function initializeSly() {
    $(".ts-sly-frame").each(function () {

        var horizontal = parseInt( $(this).attr("data-ts-sly-horizontal"), 2);
        if( !horizontal ) horizontal = 0;

        var scrollbar = $(this).attr("data-ts-sly-scrollbar");
        if( !scrollbar ) scrollbar = 0;

        $(this).sly({
            horizontal: horizontal,
            smart: 1,
            elasticBounds: 1,
            speed: 300,
            itemNav: 'basic',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            scrollBar: $(scrollbar),
            dragHandle: 1,
            scrollTrap: 1,
            clickBar: 1,
            scrollBy: 1,
            dynamicHandle: 1
        }, {
            load: function () {
                $(this.frame).addClass("ts-loaded");
            }
        });
    });
}

function heroPadding() {
    var $header = $("#ts-header");
    var $hero = $("#ts-hero");

    if( $header.hasClass("fixed-top") ){
        if( $hero.find(".ts-full-screen").length ) {
            $hero.find(".ts-full-screen").css("padding-top", $(".fixed-top").height() );
        }
        else {
            $hero.css("padding-top", $(".fixed-top").height() );
        }
    }
    else {
        if( $hero.find(".ts-full-screen").length ) {
            $hero.find(".ts-full-screen").css("min-height", "calc( 100vh - " + $header.height() + "px" );
        }
    }
}

// Smooth Scroll

$(".ts-scroll").on("click", function(event) {
    if (
        location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
        &&
        location.hostname === this.hostname
    ) {
        var target = $(this.hash);
        var headerHeight = 0;
        var $fixedTop = $(".fixed-top");
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if( $fixedTop.length ){
            headerHeight = $fixedTop.height();
        }
        if (target.length) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top - headerHeight
            }, 1000, function() {
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) {
                    return false;
                } else {
                    $target.attr('tabindex','-1');
                    $target.focus();
                }
            });
        }
    }
});

function hideCollapseOnMobile() {
    if ($(window).width() < 575) {
        $(".ts-xs-hide-collapse.collapse").removeClass("show");
    }
}</script>
 
     <script>var dojoConfig = { parseOnLoad: true };</script>
    
   
    <script>
      
    </script>


    @endsection