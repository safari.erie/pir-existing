<?php

return [
	'txtHome' => 'Beranda',
	'txtMap' => 'Peta',
	'txtNews' => 'Berita',
	'txtGuide' => 'Petunjuk',
	'txtRating' => 'Rating',
	'txtContact' => 'Kontak Kami',
	'txtProfilDaerah' => 'Profil Daerah',
	'txtKawasanIndustri' => 'Kawasan Industri',
	'txtPeluangInvestasi' => 'Peluang Investasi',
	'txtRealisasiInvestasi' => 'Realisasi Investasi',
	'txtRatingDesc' => 'Berikan penilaian Anda terhadap Sistem Informasi Potensi Investasi Daerah',
	'txtSendMessage' => 'Kirim Pesan',
	'txtAddress' => 'Alamat',
	'txtPhone' => 'Telepon'
];
