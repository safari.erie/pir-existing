<?php

return [
	'txtHome' => 'Home',
	'txtMap' => 'Map',
	'txtNews' => 'News',
	'txtGuide' => 'Guide',
	'txtRating' => 'Rating',
	'txtContact' => 'Contact Us',
	'txtProfilDaerah' => 'Regional Profile',
	'txtKawasanIndustri' => 'Industrial Estate',
	'txtPeluangInvestasi' => 'Investment Opportunity',
	'txtRealisasiInvestasi' => 'Investment Realization',
	'txtRatingDesc' => 'Give your rate for the Regional Investment Potential Information System',
	'txtSendMessage' => 'Send Message',
	'txtAddress' => 'Address',
	'txtPhone' => 'Phone'
];
