<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Daerah extends Model
{
    protected $table="daerah";
	protected $primaryKey = "id_daerah";
	public $timestamps = false;

    protected $fillable = [
        'id_daerah', 'id_parent', 'id_bentuk_daerah', 'id_sumber', 'ibukota', 'logo'
    ];
	
	public function nswi() {
		return $this->hasMany('\App\Nswi', 'id_nswi', 'id_nswi');
	}
	
}
