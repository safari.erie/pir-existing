<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;

use App\Nswi;
use App\Nswi_new;
use App\Daerah;

class NswiController extends Controller
{
    public function __construct() {
	}
	
	public function genData($tahun) {
		$tahun_awal = '2017';
		$nswi = Nswi::whereHas('daerah', function($q) {
						$q->where('id_bentuk_daerah', 2);
					})
					->where('tahun', $tahun_awal)
					->orderBy('id_daerah', 'asc')->get();
		$i=0; $a=0; $x=0;
		foreach ($nswi as $n) {
			$j=0; $k=0;
			$investasi = number_format((random_int(0, 5000 - 1) + (random_int(0, PHP_INT_MAX - 1) / PHP_INT_MAX )),'2','.','');
			echo $n->id_daerah." - ".$n->id_sektor." - ".$n->jenis." - ".$n->proyek." - ".$n->investasi." - ".$n->jenis_data." - ".$tahun." - ".$n->status." - ".$n->id_sumber."<br/>";
			
			$nswi_new2 = new Nswi_new;
			$nswi_new2->id_daerah = $n->id_daerah;
			$nswi_new2->id_sektor = $n->id_sektor;
			$nswi_new2->jenis = $n->jenis;
			$nswi_new2->proyek = rand(0,50);
			$nswi_new2->investasi = $investasi;
			$nswi_new2->jenis_data = $n->jenis_data;
			$nswi_new2->tahun = $tahun;
			$nswi_new2->status = $n->status;
			$nswi_new2->id_sumber = $n->id_sumber;
			
			if ($nswi_new2->save()) {
				$i++; $x++;
				echo "-> SUKSES<br/>";
			}
			else {
				$a++;
				echo "-> FAILED<br/>";
			}
			
			//get data kabupaten 
			$daerah = Daerah::where('id_parent', $n->id_daerah)->whereIn('id_bentuk_daerah', [3,4])->get();
			foreach ($daerah as $d) {
				//echo "---".$d->id_daerah." - ".$n->id_sektor." - ".$n->jenis." - ".$n->proyek." - ".$n->investasi." - ".$n->jenis_data." - ".$n->tahun." - ".$n->status." - ".$n->id_sumber."<br/>";
				$investasi = number_format((random_int(0, 5000 - 1) + (random_int(0, PHP_INT_MAX - 1) / PHP_INT_MAX )),'2','.','');
				//echo "INSERT INTO nswi_new (id_daerah, id_sektor, jenis, proyek, investasi, jenis_data, tahun, status, id_sumber) VALUES (".$d->id_daerah.", ".$n->id_sektor.", ".$n->jenis.", ".rand(0,50).", ".$investasi.", ".$n->jenis_data.", ".$tahun.", ".$n->status.", ".$n->id_sumber.")<br />";
				$nswi_new = new Nswi_new;
				$nswi_new->id_daerah = $d->id_daerah;
				$nswi_new->id_sektor = $n->id_sektor;
				$nswi_new->jenis = $n->jenis;
				$nswi_new->proyek = rand(0,50);
				$nswi_new->investasi = $investasi;
				$nswi_new->jenis_data = $n->jenis_data;
				$nswi_new->tahun = $tahun;
				$nswi_new->status = $n->status;
				$nswi_new->id_sumber = $n->id_sumber;
				
				if ($nswi_new->save()) {
					$j++; $x++;
					echo "--> SUKSES<br/>";
				}
				else {
					$k++;
					echo "--> FAILED<br/>";
				}
				
				usleep(500);
			}
			echo "Total Insert Kab/Kota : $j | Failed : $k<br/>================<br/>";
		}
		
		echo "Total Insert PROP : $i | NSWI NEW : $x<br/>";
	}
}
