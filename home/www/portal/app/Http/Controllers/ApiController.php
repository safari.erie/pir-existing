<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;
use DB;

class ApiController extends Controller
{
    public function __construct() {
	}
	
	public function listDemografi($id_daerah = 0) {
		$list = DB::table('daerah')
				->select('daerah.id_daerah', 'daerah_tr.nama')
				->leftJoin('daerah_tr', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
				->leftJoin(DB::raw(
									"(select demografi.id_daerah, kateg_demografi_tr.kategori as kategori_demografi, demografi.tahun, demografi.jumlah as jumlah_penduduk
										from 
										(
											select *
											from demografi where tahun='2016'
										) demografi
										left join
										(
											select kateg_demografi_tr.id_kateg_demografi, kategori
											from kateg_demografi_tr
											where kode_bahasa='id'
										) kateg_demografi_tr on kateg_demografi_tr.id_kateg_demografi = demografi.id_kateg_demografi
									) demografi 
									"
									), 
									function($join)
									{
									   $join->on('demografi.id_daerah', '=', 'daerah.id_daerah');
									}
							)
				->where('daerah_tr.kode_bahasa', '=', 'en')
				->where('daerah.id_bentuk_daerah', 2)
				->orderBy('daerah.nama', 'asc')
				->get();
				
		if ($list->count() > 0) {
			
		}
		
		$result = array('status' => TRUE, 
						'data' => $list, 
						'message' => ''
		);
		
        return response()->json($result, 200);
	}
}
