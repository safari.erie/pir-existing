<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Rating_post extends Model
{
    protected $table="rating_post";
	protected $primaryKey = "rating_post_id";
	public $timestamps = false;

    protected $fillable = [
        'date_inserted', 'name', 'email', 'session_id', 'ip_user', 'message'
    ];
	
	public function rating() {
		return $this->belongsTo('\App\Rating', 'rating_id', 'rating_id');
	}
	
	public function rating_post_dt() {
		return $this->hasMany('\App\Rating_post_dt', 'rating_post_id', 'rating_post_id');
	}
}
