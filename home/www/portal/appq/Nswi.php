<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Nswi extends Model
{
    protected $table="nswi";
	protected $primaryKey = "id_nswi";
	public $timestamps = false;

    protected $fillable = [
        'id_daerah', 'id_sektor', 'jenis', 'proyek', 'investasi', 'jenis_data', 'tahun', 'status', 'id_sumber'
    ];
	
	public function daerah() {
		return $this->hasOne('\App\Daerah', 'id_daerah', 'id_daerah');
	}
}
