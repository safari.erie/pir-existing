<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table="rating";
	protected $primaryKey = "rating_id";
	public $timestamps = false;

    protected $fillable = [
        'name', 'sort'
    ];
	
	public function rating_post() {
		return $this->hasMany('\App\Rating_post', 'rating_id', 'rating_id');
	}
	
	public function rating_post_dt() {
		return $this->hasMany('\App\Rating_post_dt', 'rating_id', 'rating_id');
	}
}
