<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;
use DB;
use Response;
use App\Agent;
use App\Rating;
use App\Rating_post;
use App\Rating_post_dt;
use App\Property;
use Yajra\Datatables\Datatables;
use Auth;
use App\Category;
use App\Slider;
use App;
use Mail;
use Session;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $locale = Session::get("locale");
        App::setLocale($locale);
       
    }

    /**
     * Show the application dashboard.
     *
     */
   public function index() {
		if (session('lang') == '') {
			session(['lang' => 'id']);
		}
		\App::setLocale(session('lang'));
		
		$sess_id = session()->getId();
		$req_ip = request()->ip();
		$ratingList = Rating::orderBy('rating_id', 'asc')->get();
		$data['ratingList'] = $ratingList;
		$data['session_id'] = $sess_id;
		$data['client_ip'] = $req_ip;
		$data['hasRate'] = Rating_post::where('session_id', $sess_id)->where('ip_user', $req_ip)->count();
		
		$postRate = Rating_post::orderBy('date_inserted', 'desc')->limit(3)->with('rating_post_dt')->get();
		$data['postRate'] = $postRate;
		
		$postRateDt = null;
		$i=0;
		foreach ($postRate as $pr) {
			foreach ($pr->rating_post_dt as $dt) {
				$id = $dt->rating_post_dt_id;
				$rating = Rating::where('rating_id',$dt->rating_id)->limit(1)->get();
				$postRateDt[$pr->rating_post_id][$id] = $dt;
				foreach ($rating as $rt) {
					$postRateDt[$pr->rating_post_id][$id]->rating_name = $rt->name;
					$postRateDt[$pr->rating_post_id][$id]->icon = $rt->icon;
				}
			}
			
		}
		
		$data['postRateDt'] = $postRateDt;
		//dd($postRateDt);
		//get average rating
		$avgRating = Rating_post_dt::groupBy('rating_id')->selectRaw('count(rating_id) as jml, sum(rate)/count(rating_id) as avg_rate, rating_id')->orderBy('rating_id', 'asc')->get();
		$arr_avg = array();
		$i=0;
		foreach ($avgRating as $avg) {
			$arr_avg[$avg->rating_id]['avg'] = $avg['avg_rate'];
			$arr_avg[$avg->rating_id]['jml'] = $avg['jml'];
			$arr_avg[$avg->rating_id]['div_class'] = ($i <= 2 ? 'col-lg-4' : 'col-lg-6');
			
			$i++;
		}
		//dd($arr_avg);
		$listRating = array();
		foreach ($ratingList as $rt) {
			$listRating[$rt->rating_id]['name'] = $rt->name;
			$listRating[$rt->rating_id]['avg'] = $arr_avg[$rt->rating_id];
			$listRating[$rt->rating_id]['icon'] = $rt->icon;
		}
		
		$data['avgRating'] = $listRating;
		
		//get rss news
        $arr = array();
		$news = array();
		/*
		$feeds = array(
			"http://rss.detik.com/index.php/detikcom"
        );
        
        //Read each feed's items
		
        foreach($feeds as $feed) {
            if (isset($feed)) {
				$xml = simplexml_load_file($feed);
				$arr = array_merge($arr, $xml->xpath("//item"));
			}
        }
		
        
		if (count($arr) > 0) {
			//Sort feed news by pubDate
			usort($arr, function ($feed1, $feed2) {
				return strtotime($feed2->pubDate) - strtotime($feed1->pubDate);
			});
			//dd($news);
		}
		
		if (count($arr) > 0) {
			$limit = 5;$i=1;
			foreach ($arr as $dt) {
				$news[] = $dt;
				
				if ($i >= $limit) break;
				$i++;
			}
		}
		*/
		$data['news'] = $news;
		
		return view('home', $data);
    }    


    public function mapdaerah() {
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $infra = "";
        if (!empty(Input::get("infra"))) {
            $infra = Input::get("infra");
        }

        $pangan = "";
        if (!empty(Input::get("pangan"))) {
            $pangan = Input::get("pangan");
        }

        $jasa = "";
        if (!empty(Input::get("jasa"))) {
            $jasa = Input::get("jasa");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
       
              
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
     
        if (!empty($type)) {
            $query->where('daerah.id_parent', $type);
          
    }
   
        
        if (!empty($category)) {
            $query->where('daerah.id_daerah', $category);
        }

     

        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
           
            
          
        }
       

     
      

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }


     
        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }
        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', 14)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();
    
        $propertiesz = $query->get();
        $bentuk = $bentukz->get();
        $propertiesx = $query->paginate(12);
        $categories = Category::get();
       
        return view('frontend.mapdaerah', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"bentuk" => $bentuk]);
        return response()->json($propertiesx);

        
    }
    
    public function map() {
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $infra = "";
        if (!empty(Input::get("infra"))) {
            $infra = Input::get("infra");
        }

        $pangan = "";
        if (!empty(Input::get("pangan"))) {
            $pangan = Input::get("pangan");
        }

        $jasa = "";
        if (!empty(Input::get("jasa"))) {
            $jasa = Input::get("jasa");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
       
              
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da.prioritas', '=', 1 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
     
        if (!empty($type)) {
            $query->where('daerah.id_parent', $type);
          
    }
   
        
        if (!empty($category)) {
            $query->where('daerah.id_daerah', $category);
        }

     

        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
           
            
          
        }
       

     
      

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }


     
        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }
        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', 14)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();
    
        $propertiesz = $query->get();
        $bentuk = $bentukz->get();
        $propertiesx = $query->paginate(12);
        $categories = Category::get();
       
        return view('frontend.map', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"bentuk" => $bentuk]);
        return response()->json($propertiesx);

        
    }

    public function mapinfra() {
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $infra = "";
        if (!empty(Input::get("infra"))) {
            $infra = Input::get("infra");
        }

        $pangan = "";
        if (!empty(Input::get("pangan"))) {
            $pangan = Input::get("pangan");
        }

        $jasa = "";
        if (!empty(Input::get("jasa"))) {
            $jasa = Input::get("jasa");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $daerah = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.bandara', 'daerah.id_daerah', '=', 'bandara.id_daerah')
        ->join('sde.bandara_tr', 'bandara_tr.id_bandara', '=', 'bandara.id_bandara')
        ->join('sde.lokasi', 'bandara.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('sde.daerah.*','sde.daerah_tr.*','sde.bandara.*','sde.bandara_tr.*','sde.lokasi.*')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.bandara_tr.kode_bahasa', '=', 'id' )
         ->where('sde.bandara.status','=', 2)
         ->orderBy('daerah.id_parent','ASC')
         ->orderBy('daerah_tr.nama','ASC')->distinct();
       
      $query =  DB::table('daerah_tr')
      ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
      ->join('sde.bandara', 'daerah.id_daerah', '=', 'bandara.id_daerah')
      ->join('sde.bandara_tr', 'bandara_tr.id_bandara', '=', 'bandara.id_bandara')
      ->join('sde.lokasi', 'bandara.id_lokasi', '=', 'lokasi.id_lokasi')
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bandara.*','sde.bandara_tr.*','sde.lokasi.*')
       ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
       ->where('sde.bandara_tr.kode_bahasa', '=', 'id' )
       ->where('sde.bandara.status','=', 2)
       ->orderBy('daerah.id_parent','ASC')
       ->orderBy('daerah_tr.nama','ASC')->distinct();

       $hotelzz =  DB::table('daerah_tr')
       ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
       ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
       ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
       ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
       ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*')
        ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
       
        ->where('sde.hotel.status','=', 2);

        $pendidikanGIS =  DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.pendidikan', 'daerah.id_daerah', '=', 'pendidikan.id_daerah')
        ->join('sde.pendidikan_tr', 'pendidikan.id_pendidikan', '=', 'pendidikan_tr.id_pendidikan')
        ->join('sde.lokasi', 'pendidikan.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('sde.daerah.*','sde.daerah_tr.*','sde.pendidikan.*','sde.pendidikan_tr.*','sde.lokasi.*')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.pendidikan_tr.kode_bahasa', '=', 'id' )
         
         ->where('sde.pendidikan.status','=', 2)->distinct();

         $pelabuhanGIS =  DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
         ->join('sde.pelabuhan', 'daerah.id_daerah', '=', 'pelabuhan.id_daerah')
         ->join('sde.pelabuhan_tr', 'pelabuhan.id_pelabuhan', '=', 'pelabuhan_tr.id_pelabuhan')
         ->join('sde.lokasi', 'pelabuhan.id_lokasi', '=', 'lokasi.id_lokasi')
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.pelabuhan.*','sde.pelabuhan_tr.*','sde.lokasi.*')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sde.pelabuhan_tr.kode_bahasa', '=', 'id' )
      
          ->where('sde.pelabuhan.status','=', 2)->distinct();

          $rumahsakitGIS =  DB::table('daerah_tr')
          ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.rumah_sakit', 'daerah.id_daerah', '=', 'rumah_sakit.id_daerah')
          ->join('sde.rumah_sakit_tr', 'rumah_sakit.id_rumah_sakit', '=', 'rumah_sakit_tr.id_rumah_sakit')
          ->join('sde.lokasi', 'rumah_sakit.id_lokasi', '=', 'lokasi.id_lokasi')
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.rumah_sakit.*','sde.rumah_sakit_tr.*','sde.lokasi.*')
           ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
           ->where('sde.rumah_sakit_tr.kode_bahasa', '=', 'id' )
           ->where('sde.rumah_sakit.status','=', 2)->distinct();

           $bentukz = DB::table('daerah_tr')
           ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
           ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
            ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
            ->where('sde.daerah.id_bentuk_daerah', '=', 2)
          
            ->select('sde.daerah.*','sde.daerah_tr.*','sde.tata_ruang.*')
            ->orderBy('daerah_tr.nama','ASC')->distinct();
       
           if (!empty($type)) {
            $pendidikanGIS->where('daerah.id_parent', $type);
          
         }
         if (!empty($type)) {
            $daerah->where('daerah.id_parent', $type);
          
         }
         
         if (!empty($type)) {
            $bentukz->where('daerah.id_daerah', $type);
          
         }
           if (!empty($type)) {
            $pelabuhanGIS->where('daerah.id_parent', $type);
          
         }
         if (!empty($type)) {
            $rumahsakitGIS->where('daerah.id_parent', $type);
          
         }
               if (!empty($type)) {
            $hotelzz->where('daerah.id_parent', $type);
          
         }
    

    if (!empty($type)) {
        $query->where('daerah.id_parent', $type);
      
}

   
        
        if (!empty($category)) {
            $query->where('daerah.id_daerah', $category);
        }

     

        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
           
            
          
        }
       

     
      

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }


     
        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }
  
        $propertiesz = $query->get();
        $bentuk = $bentukz->get();
        $hotel = $hotelzz->get();
        $pgis = $pendidikanGIS->get();
        $pbngis = $pelabuhanGIS->get();
        $rmgis = $rumahsakitGIS->get();
        $wilayah = $daerah->get();
        $propertiesx = $query->paginate(1);
        $categories = Category::get();
       
        return view('frontend.mapinfra', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"bentuk" => $bentuk,"wilayah" => $wilayah,"hotel" => $hotel
        ,"pgis" => $pgis ,"pbngis" => $pbngis ,"rmgis" => $rmgis]);
        return response()->json($propertiesx);

        
    }

    public function mapinfranasional() {
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $infra = "";
        if (!empty(Input::get("infra"))) {
            $infra = Input::get("infra");
        }

        $pangan = "";
        if (!empty(Input::get("pangan"))) {
            $pangan = Input::get("pangan");
        }

        $jasa = "";
        if (!empty(Input::get("jasa"))) {
            $jasa = Input::get("jasa");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $daerah = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.bandara', 'daerah.id_daerah', '=', 'bandara.id_daerah')
        ->join('sde.bandara_tr', 'bandara_tr.id_bandara', '=', 'bandara.id_bandara')
        ->join('sde.lokasi', 'bandara.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('sde.daerah.*','sde.daerah_tr.*','sde.bandara.*','sde.bandara_tr.*','sde.lokasi.*')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.bandara_tr.kode_bahasa', '=', 'id' )
         ->where('sde.bandara.status','=', 2)
         ->orderBy('daerah.id_parent','ASC')
         ->orderBy('daerah_tr.nama','ASC')->distinct();
       
      $query =  DB::table('daerah_tr')
      ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
      ->join('sde.bandara', 'daerah.id_daerah', '=', 'bandara.id_daerah')
      ->join('sde.bandara_tr', 'bandara_tr.id_bandara', '=', 'bandara.id_bandara')
      ->join('sde.lokasi', 'bandara.id_lokasi', '=', 'lokasi.id_lokasi')
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bandara.*','sde.bandara_tr.*','sde.lokasi.*')
       ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
       ->where('sde.bandara_tr.kode_bahasa', '=', 'id' )
       ->where('sde.bandara.status','=', 2)
       ->orderBy('daerah.id_parent','ASC')
       ->orderBy('daerah_tr.nama','ASC')->distinct();

       $hotelzz =  DB::table('daerah_tr')
       ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
       ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
       ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
       ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
       ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*')
        ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
       
        ->where('sde.hotel.status','=', 2);

        $pendidikanGIS =  DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.pendidikan', 'daerah.id_daerah', '=', 'pendidikan.id_daerah')
        ->join('sde.pendidikan_tr', 'pendidikan.id_pendidikan', '=', 'pendidikan_tr.id_pendidikan')
        ->join('sde.lokasi', 'pendidikan.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('sde.daerah.*','sde.daerah_tr.*','sde.pendidikan.*','sde.pendidikan_tr.*','sde.lokasi.*')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.pendidikan_tr.kode_bahasa', '=', 'id' )
         
         ->where('sde.pendidikan.status','=', 2)->distinct();

         $pelabuhanGIS =  DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
         ->join('sde.pelabuhan', 'daerah.id_daerah', '=', 'pelabuhan.id_daerah')
         ->join('sde.pelabuhan_tr', 'pelabuhan.id_pelabuhan', '=', 'pelabuhan_tr.id_pelabuhan')
         ->join('sde.lokasi', 'pelabuhan.id_lokasi', '=', 'lokasi.id_lokasi')
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.pelabuhan.*','sde.pelabuhan_tr.*','sde.lokasi.*')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sde.pelabuhan_tr.kode_bahasa', '=', 'id' )
      
          ->where('sde.pelabuhan.status','=', 2)->distinct();

          $rumahsakitGIS =  DB::table('daerah_tr')
          ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.rumah_sakit', 'daerah.id_daerah', '=', 'rumah_sakit.id_daerah')
          ->join('sde.rumah_sakit_tr', 'rumah_sakit.id_rumah_sakit', '=', 'rumah_sakit_tr.id_rumah_sakit')
          ->join('sde.lokasi', 'rumah_sakit.id_lokasi', '=', 'lokasi.id_lokasi')
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.rumah_sakit.*','sde.rumah_sakit_tr.*','sde.lokasi.*')
           ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
           ->where('sde.rumah_sakit_tr.kode_bahasa', '=', 'id' )
           ->where('sde.rumah_sakit.status','=', 2)->distinct();

           $bentukz = DB::table('daerah_tr')
           ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
           ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
            ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
            ->where('sde.daerah.id_bentuk_daerah', '=', 2)
          
            ->select('sde.daerah.*','sde.daerah_tr.*','sde.tata_ruang.*')
            ->orderBy('daerah_tr.nama','ASC')->distinct();
       
      
         
         if (!empty($type)) {
            $bentukz->where('daerah.id_daerah', $type);
          
         }
      
    if (!empty($type)) {
        $query->where('daerah.id_parent', $type);
      
}

   
        
        if (!empty($category)) {
            $query->where('daerah.id_daerah', $category);
        }

     

        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
           
            
          
        }
       

     
      

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }


     
        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }
  
        $propertiesz = $query->get();
        $bentuk = $bentukz->get();
        $hotel = $hotelzz->paginate(1);
        $pgis = $pendidikanGIS->paginate(1);;
        $pbngis = $pelabuhanGIS->paginate(1);
        $rmgis = $rumahsakitGIS->paginate(1);
        $wilayah = $daerah->paginate(1);
        $propertiesx = $query->paginate(1);
        $categories = Category::get();
       
        return view('frontend.mapinfranasional', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"bentuk" => $bentuk,"wilayah" => $wilayah,"hotel" => $hotel
        ,"pgis" => $pgis ,"pbngis" => $pbngis ,"rmgis" => $rmgis]);
        return response()->json($propertiesx);

        
    }

  public function download_admin($id, $file_name) {
		#echo "$id = $file_name";exit;
                $path = '/var/www/html/sipid/public/daerah/'.$id.'/attachment/'.$file_name;
                return response()->download($path);
        }

    /**
     * Show the Grid Listing of Properties.
     *
     */
    public function listing() {
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }

        $query = Property::where("is_delete", 0);

        if (!empty($keywords)) {
            $query->where('title', 'like', "%$keywords%");
            $query->orWhere('address', 'like', "%$keywords%");
            $query->orWhere('zip', 'like', "%$keywords%");
            $query->orWhere('city', 'like', "%$keywords%");
            $query->orWhere('state', 'like', "%$keywords%");
        }
		
		

        if (!empty($min) and ! empty($max)) {
            $query->where('price', '>=', $min);
            $query->where('price', '<=', $max);
        }


        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }

        $forms = array(
            "keywords" => $keywords,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $query->orderBy($orderby, $ordertype);
        $properties = $query->paginate(12);
		$categories = Category::get();
        $agents = Agent::limit(5)->where("AccessLevel" , 1)->where("is_delete",0)->get();
        return view('box_listing', ["categories" => $categories,"forms" => $forms, "order" => $order, 'agents' => $agents, 'properties' => $properties]);
    }
	
	
	 /**
     * Show the Listing of Properties .
     *
     */
    public function list_properties() {
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }

        $query = Property::where("is_delete", 0);

        if (!empty($keywords)) {
            $query->where('title', 'like', "%$keywords%");
            $query->orWhere('address', 'like', "%$keywords%");
            $query->orWhere('zip', 'like', "%$keywords%");
            $query->orWhere('city', 'like', "%$keywords%");
            $query->orWhere('state', 'like', "%$keywords%");
        }
		
		

        if (!empty($min) and ! empty($max)) {
            $query->where('price', '>=', $min);
            $query->where('price', '<=', $max);
        }


        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }

        $forms = array(
            "keywords" => $keywords,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $query->orderBy($orderby, $ordertype);
        $properties = $query->paginate(12);
		$categories = Category::get();
        $agents = Agent::limit(5)->where("AccessLevel" , 1)->where("is_delete",0)->get();
        return view('list_listing', ["categories" => $categories,"forms" => $forms, "order" => $order, 'agents' => $agents, 'properties' => $properties]);
    }
	
public function rate_post(Request $request) {
		$validator = Validator::make($request->all(), 
			[
				'session_id' => 'required',
				'ip' => 'required',
				'name' => 'required',
				'email' => 'required|email',
				'message' => 'required'
			],
			[
				'session_id.required' => 'Session tidak valid.',
				'ip.required' => 'IP Address tidak valid.',
				'name.required' => 'Nama harus diisi.',
				'email.required' => 'Email harus diisi.',
				'email.email' => 'Email tidak valid.',
				'message.required' => 'Pesan harus diisi'
			]
		);
		
		if ($validator->passes()) {
			//save rating
			$rate_post = new Rating_post;
			$rate_post->date_inserted = date('Y-m-d H:i:s');
			$rate_post->name = $request->name;
			$rate_post->email = $request->email;
			$rate_post->message = $request->message;
			$rate_post->session_id = $request->session_id;
			$rate_post->ip_user = $request->ip;
			if ($rate_post->save()) {
				//save rating detail
				$rate = Rating::all();
				foreach ($rate as $rt) {
					$nm = "txt_star".$rt->rating_id;
					$rate_post_dt = new Rating_post_dt;
					$rate_post_dt->rating_post_id = $rate_post->rating_post_id;
					$rate_post_dt->rating_id = $rt->rating_id;
					$rate_post_dt->rate = $request->$nm;
					$rate_post_dt->save();
					
				}
			}

			return redirect('/');
		
		}
		else {
			$data = [
				'session_id' => 'required',
				'ip' => 'required',
				'name' => 'required',
				'email' => 'required|email',
				'message' => 'required'
			];
			
			return redirect()->back()
						->withErrors($validator)
						->withInput();
		}
	}
	
	public function download_guide($id) {
		$path = $_ENV['APP_STORAGE_PATH'].'/public/';

		$name = $id.'.pdf';
		$download_path = $path.$name;
		return response()->download($download_path);
	}

	public function setLang($lang) {
		session(['lang' => $lang]);
		return redirect('/');
	}

        public function download_kawasan($id_kawasan, $file_name) {
                $path = '/var/www/html/sipid/public/daerah/0/kawasan/'.$id_kawasan.'/'.$file_name;
                return response()->download($path);
        }

	public function view_pdf($id_kawasan, $file_name) {
                $path = '/var/www/html/sipid/public/daerah/0/kawasan/'.$id_kawasan.'/'.$file_name;
		return Response::make(file_get_contents($path), 200, [
    			'Content-Type' => 'application/pdf',
			'Content-Disposition' => 'inline; filename="attachment.pdf"'
		]);
        }





    public function peluang()
    { 
        
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();
        
         $query2 = DB::table('sde.info_peluang_da')
       
         ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
         ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
         ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
         ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
         ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
         ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
         ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
         ->where('info_peluang_da.status', '=', 2 )
         ->where('sde.info_peluang_da.prioritas', '=', 1 )
         ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
         ->where('peluang_tr.kode_bahasa', '=', 'id' )
         ->where('daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )
         ->orderBy('info_peluang_da_tr.nilai_investasi','DSC')->distinct();
        
  
     
        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'like', "%$keywords%");
            $query->where('info_peluang_da_tr.judul', 'like', "%$keywords%");
            
            
          
        }
		
  


        if (!empty($min) and ! empty($max)) {
            $query->where('price', '>=', $min);
            $query->where('price', '<=', $max);
        }


        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }
        
        if (!empty($category)) {
            $query->where('id_sektor_peluang', $category);
        }

        if (!empty($industri)) {
            $query->where('id_sektor_peluang', $industri);
        }

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $propertiesx = $query->get();
      
        
        $propertiesz = $query2->get();
        $categories = Category::get();
       
        return view('maplisting', ['propertiesx' => $propertiesx,'propertiesz' =>$propertiesz,'categories' => $categories,"forms" => $forms]);
    


        
    }


    public function peluangdaerah()
    { 
        
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();
        
         $query2 = DB::table('sde.info_peluang_da')
       
         ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
         ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
         ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
         ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
         ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
         ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
         ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
         ->where('info_peluang_da.status', '=', 2 )
         ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
         ->where('peluang_tr.kode_bahasa', '=', 'id' )
         ->where('daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )
         ->orderBy('info_peluang_da_tr.nilai_investasi','DSC')->distinct();
        
  
     
        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'like', "%$keywords%");
            $query->where('info_peluang_da_tr.judul', 'like', "%$keywords%");
            
            
          
        }
		
  


        if (!empty($min) and ! empty($max)) {
            $query->where('price', '>=', $min);
            $query->where('price', '<=', $max);
        }


        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }
        
        if (!empty($category)) {
            $query->where('id_sektor_peluang', $category);
        }

        if (!empty($industri)) {
            $query->where('id_sektor_peluang', $industri);
        }

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $propertiesx = $query->get();
      
        
        $propertiesz = $query2->get();
        $categories = Category::get();
       
        return view('peluangdaerah', ['propertiesx' => $propertiesx,'propertiesz' =>$propertiesz,'categories' => $categories,"forms" => $forms]);
    


        
    }
    public function detaildaerah($id,$prov) {
       
        
        $query = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*','sde.tata_ruang.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da.id_info_peluang_da', '=', $id )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
        $daerah = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.sektor','peluang_tr.*','sde.lokasi.*','bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('info_peluang_da.id_info_peluang_da', '=', $id )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
     
        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
    
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $prov)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct()->limit(1);

        $propertiesz = $query->get();
        
        $daerahs = $daerah->get();
        $bentuk = $bentukz->paginate(1);
        
        $propertiesx = $query->paginate(1);
        $categories = Category::get();

     
       
        return view('detail', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'daerahs' => $daerahs,'bentuk' => $bentuk]);
        return response()->json($propertiesx);
    }


    public function detailprioritas($id,$prov) {
       
        
        $query = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.nama AS nama_kabkot','sde.peluang.status','sde.bentuk_daerah_tr.*','sde.tata_ruang.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da.id_info_peluang_da', '=', $id )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
        $daerah = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.sektor','peluang_tr.*','sde.lokasi.*','bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('info_peluang_da.id_info_peluang_da', '=', $id )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.tata_ruang.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $prov)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct()->limit(1);

        $propertiesz = $query->get();
        
        $daerahs = $daerah->get();
        $bentuk = $bentukz->paginate(1);
        
        $propertiesx = $query->paginate(1);
        $categories = Category::get();

     
       
        return view('detailpro', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'daerahs' => $daerahs,'bentuk' => $bentuk]);
        return response()->json($propertiesx);
    }

    public function emailz($id) {
       
        
        $query = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da.id_info_peluang_da', '=', $id )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
        
       

        $propertiesz = $query->get();
   
        
        $propertiesx = $query->paginate(12);
        $categories = Category::get();

     
       
        return view('email', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz]);
      
    }


    /**
     * Show the Agents.
     *
     */
    public function agents() {
        $agents = Agent::where("AccessLevel" , 1)->where("is_delete",0)->get();
        return view('pages.agent', ['agents' => $agents]);
    }

    /**
     * Show the Contact Page.
     *
     */
    public function contact() {
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang', 'peluang.id_sektor_peluang', '=', 'sektor_peluang.id_sektor_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*','peluang_tr.*','peluang.status','peluang.*','sektor_peluang_tr.*','daerah.*','daerah_tr.*')
        ->where('peluang.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )->distinct();
     
        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
            
          
        }
		
 

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }

		

    

        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }
        
        if (!empty($category)) {
            $query->where('id_sektor_peluang', $category);
        }

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $propertiesz = $query->get();
        
        $propertiesx = $query->paginate(12);
        $categories = Category::get();
       
        return view('pages.gallery', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms]);
      

    }

    /**
     * Show the Loan Calculator Page.
     *
     */
    public function listingprioritas() {
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $infra = "";
        if (!empty(Input::get("infra"))) {
            $infra = Input::get("infra");
        }

        $pangan = "";
        if (!empty(Input::get("pangan"))) {
            $pangan = Input::get("pangan");
        }

        $jasa = "";
        if (!empty(Input::get("jasa"))) {
            $jasa = Input::get("jasa");
        }
        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
       
          
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da.prioritas', '=', 1 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )
        ->orderBy('info_peluang_da_tr.nilai_investasi','DSC')->distinct();
       
        if (!empty($type)) {
            $query->where('daerah.id_parent', $type);
          
    }
   
        
        if (!empty($category)) {
            $query->where('daerah.id_daerah', $category);
        }

     
        if (!empty($keywords)) {
           
         $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
         
            
          
        }
		
		

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }



        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

   

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();


        $bentuk = $bentukz->get();
        $propertiesz = $query->get();
        
        $propertiesx = $query->paginate(24);
        $categories = Category::get();
        $infras = $infra;
        $jasas = $jasa;
        $indus = $industri;
        $pangans = $pangan;
       
        return view('pages.listingprioritas', compact('infras','jasas','indus','pangans'), ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"infra" => $infra, "bentuk" => $bentuk]);
      
    }




    public function listingdaerah() {
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $industri = "";
        if (!empty(Input::get("industri"))) {
            $industri = Input::get("industri");
        }

        $infra = "";
        if (!empty(Input::get("infra"))) {
            $infra = Input::get("infra");
        }

        $pangan = "";
        if (!empty(Input::get("pangan"))) {
            $pangan = Input::get("pangan");
        }

        $jasa = "";
        if (!empty(Input::get("jasa"))) {
            $jasa = Input::get("jasa");
        }
        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
       
          
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )
        ->orderBy('info_peluang_da_tr.nilai_investasi','DSC')->distinct();
       
        if (!empty($type)) {
            $query->where('daerah.id_parent', $type);
          
    }
   
        
        if (!empty($category)) {
            $query->where('daerah.id_daerah', $category);
        }

     
        if (!empty($keywords)) {
           
         $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
         
            
          
        }
		
		

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }



        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

   

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();


        $bentuk = $bentukz->get();
        $propertiesz = $query->get();
        
        $propertiesx = $query->paginate(24);
        $categories = Category::get();
        $infras = $infra;
        $jasas = $jasa;
        $indus = $industri;
        $pangans = $pangan;
       
        return view('pages.about', compact('infras','jasas','indus','pangans'), ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"infra" => $infra, "bentuk" => $bentuk]);
      
    }

    /**
     * About Us Page.
     *
     */
    public function home() {
	
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )
        ->orderBy('info_peluang_da_tr.nilai_investasi','DSC')->distinct();
     
        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'Ilike', "%$keywords%");
            
          
        }
		
 

        if (!empty($min) and ! empty($max)) {
            $query->where('info_peluang_da_tr.nilai_investasi', '>=', $min);
            $query->where('info_peluang_da_tr.nilai_investasi', '<=', $max);
        }

		

    

        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }
        
        if (!empty($category)) {
            $query->where('id_sektor_peluang', $category);
        }

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

      
         
       
         
        $bentuk = $bentukz->get();

        $propertiesz = $query->get();
        
        $propertiesx = $query->paginate(12);
        $categories = Category::get();
       
        return view('beranda', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms,"bentuk" => $bentuk]);
      

    }
    

    /**
     * Agent Profile Page.
     *
     */

    //  Memanggil Key
    public function key() {
        $user = '98347343883748';
        if ( $user == '98347343883748' ) return view('key');
       
    }

    public function keyanalis($token) {
        if (!empty($token)) {
        $pages = 'kosong';
        } else {
$pages = '$token';

        }
        return view('detail', compact('pages'));
    }


   

   

    public function abouts() {
      
        return view('abouts', [ 'title', "My Profile"]);
    }
    
    public function contactz() {
      
        return view('contact', [ 'title', "My Profile"]);
    }

    public function holiday() {
      
        return view('holiday', [ 'title', "My Profile"]);
    }

    public function master() {
        $pages = 'master';
        return view('master', compact('pages'));
    }
// Coding Halaman Profil 
    public function profil() {

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

       
        
        $bentuk = $bentukz->get();
        $pages = 'master';
        return view('profil', compact('pages'), ["bentuk" => $bentuk ]);
    }

    // Halaman profil Detail
    public function profildetail($id) {

        $daerah = DB::table('sde.info_peluang_da')
    ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
    ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
    ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
    ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
    ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
    ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
    
    ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
    ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
    ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
    
    ->where('sde.daerah.id_parent', '=', $id )
    
    ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
    ->where('peluang_tr.kode_bahasa', '=', 'id' )
    ->where('daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

         $kotaz = DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         
          ->where('sde.daerah.id_parent', '=', $id )
          ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
          ->orderBy('daerah.id_parent','ASC')
          ->orderBy('daerah_tr.nama','ASC')->distinct();

          $kabupatenz = DB::table('daerah_tr')
          ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
           ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
           ->where('sde.daerah.id_bentuk_daerah', '=', 4)
           ->where('sde.daerah.id_parent', '=', $id )
           ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
           ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
           ->orderBy('daerah.id_parent','ASC')
           ->orderBy('daerah_tr.nama','ASC')->distinct();
 
      
    
           
          $queryz = DB::table('sde.info_peluang_da')
          ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
          ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
          ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
          ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
          ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
          ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
          ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
          ->where('info_peluang_da.status', '=', 2 )
          ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
          ->where('peluang_tr.kode_bahasa', '=', 'id' )
          ->where('daerah_tr.kode_bahasa', '=', 'id' )
          ->where('daerah.id_parent', '=', "$id" )
          ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )
          ->orderBy('sde.info_peluang_da.prioritas', 'ASC')->distinct();

          $umrz =    DB::select("SELECT DISTINCT 
            sde.daerah.id_daerah,
            umr.tahun,
            sde.daerah.id_parent,
               sum(CASE WHEN(((umr.tahun) ::text = '2016' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2016,
             sum(CASE WHEN(((umr.tahun) ::text = '2017' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2017,
             sum(CASE WHEN(((umr.tahun) ::text = '2018' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2018,
             sum(CASE WHEN(((umr.tahun) ::text = '2019' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2019,
             sum(CASE WHEN(((umr.tahun) ::text = '2020' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2020,
            sde.daerah_tr.nama
          
          FROM
            sde.daerah
            INNER JOIN sde.daerah_tr ON (sde.daerah.id_daerah = sde.daerah_tr.id_daerah)
            INNER JOIN sde.umr ON (sde.daerah_tr.id_daerah = sde.umr.id_daerah)
          WHERE
            daerah_tr.kode_bahasa = 'id' ::bpchar AND
            daerah.id_daerah = '$id' AND
            umr.tahun >= '2016'
            GROUP BY
            sde.daerah.id_daerah,
            umr.tahun,
            sde.daerah.id_parent,
            sde.daerah_tr.nama
            
          ORDER BY
            daerah.id_daerah
          
          
          ");
            
        
            $demografiz =    DB::select("SELECT 
            daerah.id_daerah,
            daerah_tr.nama AS nama_daerah,
            demografi.tahun,
            sum(CASE WHEN(((demografi.tahun) ::text = '2016' ::text)) THEN demografi.pria ELSE NULL ::numeric END) AS pria_2016,
            sum(CASE WHEN(((demografi.tahun) ::text = '2016' ::text)) THEN demografi.wanita ELSE NULL ::numeric END) AS wanita_2016,
            sum(CASE WHEN(((demografi.tahun) ::text = '2017' ::text)) THEN demografi.pria ELSE NULL ::numeric END) AS pria_2017,
            sum(CASE WHEN(((demografi.tahun) ::text = '2017' ::text)) THEN demografi.wanita ELSE NULL ::numeric END) AS wanita_2017,
            sum(CASE WHEN(((demografi.tahun) ::text = '2018' ::text)) THEN demografi.pria ELSE NULL ::numeric END) AS pria_2018,
            sum(CASE WHEN(((demografi.tahun) ::text = '2018' ::text)) THEN demografi.wanita ELSE NULL ::numeric END) AS wanita_2018,
            sum(CASE WHEN(((demografi.tahun) ::text = '2019' ::text)) THEN demografi.pria ELSE NULL ::numeric END) AS pria_2019,
            sum(CASE WHEN(((demografi.tahun) ::text = '2019' ::text)) THEN demografi.wanita ELSE NULL ::numeric END) AS wanita_2019,
            sum(CASE WHEN(((demografi.tahun) ::text = '2020' ::text)) THEN demografi.pria ELSE NULL ::numeric END) AS pria_2020,
            sum(CASE WHEN(((demografi.tahun) ::text = '2020' ::text)) THEN demografi.wanita ELSE NULL ::numeric END) AS wanita_2020
          FROM
            demografi
            INNER JOIN daerah ON (demografi.id_daerah = daerah.id_daerah)
            INNER JOIN daerah_tr ON (demografi.id_daerah = daerah_tr.id_daerah)
            INNER JOIN kateg_demografi_tr ON (demografi.id_kateg_demografi = kateg_demografi_tr.id_kateg_demografi)
            INNER JOIN sumber_tr ON (demografi.id_sumber = sumber_tr.id_sumber)
            INNER JOIN lokasi ON (daerah.id_lokasi = lokasi.id_lokasi)
          WHERE
            daerah_tr.kode_bahasa = 'id' ::bpchar AND 
            kateg_demografi_tr.kode_bahasa = 'id' ::bpchar AND 
            sumber_tr.kode_bahasa = 'id' ::bpchar AND 
            daerah.id_daerah = '11' AND 
            demografi.tahun > '2015'
          GROUP BY
            daerah.id_daerah,
            daerah_tr.nama,
            demografi.tahun          
            
          
          
          ");
        $bentuk = $bentukz->get();
        $kota = $kotaz->get();
        $kabupaten = $kabupatenz->get();
        $prov = 'as';
        $projecs = $queryz->get();
        $pages = 'profil';
        $propertiesx = $daerah->get();
        return view('profildetail', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs, "propertiesx" => $propertiesx, "prov" => $prov , "kabupaten" => $kabupaten, "umr" => $umrz , "demografi" => $demografiz ]);
    }


    public function potensialdetail($id) {

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

         $kotaz = DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'en' )
          ->where('sde.daerah.id_bentuk_daerah', '=', 3)
          ->where('sde.daerah.id_parent', '=', $id )
          ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'en' )
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
          ->orderBy('daerah.id_parent','ASC')
          ->orderBy('daerah_tr.nama','ASC')->distinct();
 
           
          $queryz = DB::table('sde.info_peluang_da')
          ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
          ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
          ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
          ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
          ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
          ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
          ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
          ->where('info_peluang_da.status', '=', 2 )
          ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
          ->where('peluang_tr.kode_bahasa', '=', 'id' )
          ->where('daerah_tr.kode_bahasa', '=', 'id' )
          ->where('daerah.id_parent', '=', $id )
          ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
          $umrz =    DB::select("SELECT DISTINCT 
          sde.daerah.id_daerah,
          umr.tahun,
          sde.daerah.id_parent,
             sum(CASE WHEN(((umr.tahun) ::text = '2016' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2016,
           sum(CASE WHEN(((umr.tahun) ::text = '2017' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2017,
           sum(CASE WHEN(((umr.tahun) ::text = '2018' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2018,
           sum(CASE WHEN(((umr.tahun) ::text = '2019' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2019,
           sum(CASE WHEN(((umr.tahun) ::text = '2020' ::text)) THEN umr.umr ELSE NULL ::integer END) AS umr_2020,
          sde.daerah_tr.nama
        
        FROM
          sde.daerah
          INNER JOIN sde.daerah_tr ON (sde.daerah.id_daerah = sde.daerah_tr.id_daerah)
          INNER JOIN sde.umr ON (sde.daerah_tr.id_daerah = sde.umr.id_daerah)
        WHERE
          daerah_tr.kode_bahasa = 'id' ::bpchar AND
          daerah.id_daerah = '$id' AND
          umr.tahun >= '2016'
          GROUP BY
          sde.daerah.id_daerah,
          umr.tahun,
          sde.daerah.id_parent,
          sde.daerah_tr.nama
          
        ORDER BY
          daerah.id_daerah
        
        
        ");
        $bentuk = $bentukz->get();
        $kota = $kotaz->get();
        $projecs = $queryz->get();
        $pages = 'potensial';
        return view('potensialdetail', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs, "umr" => $umrz  ]);
    }


    public function investdetail($id) {

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

         $kotaz = DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'en' )
          ->where('sde.daerah.id_bentuk_daerah', '=', 3)
          ->where('sde.daerah.id_parent', '=', $id )
          ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'en' )
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
          ->orderBy('daerah.id_parent','ASC')
          ->orderBy('daerah_tr.nama','ASC')->distinct();
 
           
          $queryz = DB::table('sde.info_peluang_da')
          ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
          ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
          ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
          ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
          ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
          ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
          ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
          ->where('info_peluang_da.status', '=', 2 )
          ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
          ->where('peluang_tr.kode_bahasa', '=', 'id' )
          ->where('daerah_tr.kode_bahasa', '=', 'id' )
          ->where('daerah.id_parent', '=', $id )
          ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
        
        $bentuk = $bentukz->get();
        $kota = $kotaz->get();
        $projecs = $queryz->get();
        $pages = 'invest';
        return view('investdetail', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs ]);
    }
    public function Realisasiinvestasi($tahun) {

        $investasiz =    DB::select("SELECT 
        sde.nswi.id_daerah,
        sde.daerah_tr.nama,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMA' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.proyek ELSE NULL ::integer END) AS proyek_pma,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMA' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.investasi ELSE NULL ::numeric END) AS investasi_pma,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMDN' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.investasi ELSE NULL ::numeric END) AS investasi_pmdn,
        sde.nswi.jenis_data
      FROM
        sde.nswi
        INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.nswi.id_daerah)
        INNER JOIN sde.daerah_tr ON (sde.daerah_tr.id_daerah = sde.daerah.id_daerah)
      WHERE
        nswi.id_daerah >= 11 AND 
        nswi.id_daerah <= 92 AND
        sde.daerah_tr.kode_bahasa = 'id'
      GROUP BY
        sde.nswi.id_daerah,
        sde.daerah_tr.nama,
        sde.nswi.jenis_data
        ORDER BY  investasi_pmdn desc
      ");
        
        $bentuk = $investasiz;
   
        return view('chartinvestasi', ["investasi" => $bentuk ]);
    }


    public function Realisasisector($tahun) {

        $investasiz =    DB::select("SELECT 
        nswi.id_sektor,
        
        sektor_nswi.sektor,
          sum(CASE WHEN(((nswi.jenis) ::text = 'PMDN' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.proyek ELSE NULL ::integer END) AS investasi_proyek,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMDN' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.investasi ELSE NULL ::integer END) AS investasi_pmdn,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMA' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.investasi ELSE NULL ::numeric END) AS investasi_pma,
        nswi.jenis_data
      FROM
        sde.nswi
        INNER JOIN sektor_nswi ON (nswi.id_sektor = sektor_nswi.id_sektor)
        
        WHERE nswi.status = 2
      GROUP BY
      
      
        nswi.id_sektor,
        sektor_nswi.sektor,
        nswi.jenis_data
      ORDER BY
        investasi_pmdn DESC
      
      ");
        
        $bentuk = $investasiz;
   
        return view('chartsector', ["investasi" => $bentuk ]);
    }

    public function RealisasisectorDaerah($daerah,$tahun) {

        $investasiz =    DB::select("SELECT 
        
        sde.nswi.id_daerah,
        sektor_nswi.sektor,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMDN' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.proyek ELSE '0' ::integer END) AS investasi_proyek,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMDN' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.investasi ELSE '0' ::integer END) AS investasi_pmdn,
        sum(CASE WHEN(((nswi.jenis) ::text = 'PMA' ::text) AND((nswi.tahun) ::text = $tahun ::text)) THEN nswi.investasi ELSE '0' ::numeric END) AS investasi_pma,
        nswi.jenis_data
      FROM
        sde.nswi
        INNER JOIN sektor_nswi ON (nswi.id_sektor = sektor_nswi.id_sektor)
        
        WHERE nswi.status = 2 AND
        sde.nswi.id_daerah = '$daerah' 
       
      GROUP BY
      sde.nswi.id_daerah,
        
        sektor_nswi.sektor,
        nswi.jenis_data
      ORDER BY
        investasi_pmdn  DESC, investasi_pma
      
      ");
        
        $bentuk = $investasiz;
   
        return view('chartsectordaerah', ["investasi" => $bentuk ]);
    }

    public function pdrbDaerah($daerah,$tahun) {

        $investasiz =    DB::select("SELECT DISTINCT 
        daerah.id_parent,
        daerah.id_daerah,
        daerah_tr.nama AS daerah,
        pdrb.*,
        sektor_pdrb.*,
        sektor_pdrb_tr.*
       
      FROM
        pdrb
          INNER JOIN daerah ON (pdrb.id_daerah = daerah.id_daerah)
        INNER JOIN daerah_tr ON (daerah_tr.id_daerah = daerah.id_daerah)
        INNER JOIN sektor_pdrb ON (sektor_pdrb.id_sektor_pdrb = pdrb.id_sektor_pdrb)
        INNER JOIN sektor_pdrb_tr ON (sektor_pdrb_tr.id_sektor_pdrb = sektor_pdrb.id_sektor_pdrb)
        
        WHERE daerah.id_daerah = '$daerah' and pdrb.tahun = '$tahun' and
         daerah_tr.kode_bahasa = 'en' and sektor_pdrb_tr.kode_bahasa = 'en'
      
      ORDER BY
      pdrb.jumlah desc
      
      ");
        
        $bentuk = $investasiz;
   
        return view('chartpdrbdaerah', ["investasi" => $bentuk ]);
    }
    public function komoditiDaerah($daerah,$tahun) {

        $investasiz =    DB::select("SELECT DISTINCT 
        daerah.id_parent,
        daerah.id_daerah,
        daerah_tr.nama AS daerah,
        sektor_komoditi_tr.sektor,
        komoditi_tr.nama,
        komoditi_daerah.status,
        komoditi_daerah.tahun,
        sum(komoditi_daerah.nilai_produksi) AS hasil
      FROM
        komoditi_daerah
        INNER JOIN daerah ON (daerah.id_daerah = komoditi_daerah.id_daerah)
        INNER JOIN daerah_tr ON (daerah_tr.id_daerah = daerah.id_daerah)
        INNER JOIN komoditi_tr ON (komoditi_tr.id_komoditi = komoditi_daerah.id_komoditi)
        INNER JOIN komoditi ON (komoditi.id_komoditi = komoditi_daerah.id_komoditi)
        INNER JOIN sektor_komoditi_tr ON (sektor_komoditi_tr.id_sektor_komoditi = komoditi.id_sektor_komoditi)
        INNER JOIN sumber_tr ON (sumber_tr.id_sumber = komoditi_daerah.id_sumber)
      WHERE
        daerah_tr.kode_bahasa = 'en' ::bpchar AND 
        komoditi_tr.kode_bahasa = 'en' ::bpchar AND 
        sektor_komoditi_tr.kode_bahasa = 'en' ::bpchar AND 
        sumber_tr.kode_bahasa = 'en' ::bpchar AND 
        komoditi_daerah.tahun = '$tahun' AND 
        daerah.id_daerah = '$daerah'
      GROUP BY
        daerah.id_parent,
        daerah.id_daerah,
        daerah_tr.nama,
        sektor_komoditi_tr.sektor,
        komoditi_tr.nama,
        komoditi_daerah.status,
        komoditi_daerah.tahun
      ORDER BY
        hasil DESC
      
      ");
        
        $bentuk = $investasiz;
   
        return view('chartkomoditidaerah', ["investasi" => $bentuk ]);
    }
    public function chart($text, $id) {

        $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
        
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

       
        
        $bentuk = $bentukz->get();
        $pages = $text;
        return view('chart', compact('pages'), ["bentuk" => $bentuk ]);
    }
//    Akhir Coding 

public function oppodetail($id) {

    $daerah = DB::table('sde.info_peluang_da')
    ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
    ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
    ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
    ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
    ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
    ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
    ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
    ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
    ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
    ->where('info_peluang_da.status', '=', 2 )
    ->where('sde.daerah.id_parent', '=', $id )
    
    ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
    ->where('peluang_tr.kode_bahasa', '=', 'id' )
    ->where('daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
    
    $bentukz = DB::table('daerah_tr')
    ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
     ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
     
     ->where('sde.daerah.id_daerah', '=', 11)
     ->select('sde.daerah.*','sde.daerah_tr.*')
     ->orderBy('daerah_tr.nama','ASC')->distinct();

     $kotaz = DB::table('daerah_tr')
     ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
     ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
      ->where('sde.daerah_tr.kode_bahasa', '=', 'en' )
      ->where('sde.daerah.id_bentuk_daerah', '=', 3)
      ->where('sde.daerah.id_parent', '=', $id )
      ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'en' )
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
      ->orderBy('daerah.id_parent','ASC')
      ->orderBy('daerah_tr.nama','ASC')->distinct();

       
      $queryz = DB::table('sde.info_peluang_da')
      ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
      ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
      ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
      ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
      ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
      ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
      ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
      ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
      ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
      ->where('info_peluang_da.status', '=', 2 )
      ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
      ->where('peluang_tr.kode_bahasa', '=', 'id' )
      ->where('daerah_tr.kode_bahasa', '=', 'id' )
      ->where('daerah.id_parent', '=', $id )
      ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
      ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
    
    $bentuk = $bentukz->get();
    $kota = $kotaz->get();
    $projecs = $queryz->get();
    $propertiesx = $daerah->get();
    $pages = 'oppo';
    return view('oppodetail', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs, "propertiesx" => $propertiesx ]);
}




public function industridetail($id) {

    $daerah = DB::table('sde.info_peluang_da')
    ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
    ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
    ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
    ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
    ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
    ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
    
    ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
    ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
    ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
    
    ->where('sde.daerah.id_parent', '=', $id )
    
    ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
    ->where('peluang_tr.kode_bahasa', '=', 'id' )
    ->where('daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
    
    $bentukz = DB::table('daerah_tr')
    ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
     ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
     
     ->where('sde.daerah.id_daerah', '=', 11)
     ->select('sde.daerah.*','sde.daerah_tr.*')
     ->orderBy('daerah_tr.nama','ASC')->distinct();

     $kotaz = DB::table('sde.industrial_site_new')
     ->join('sde.daerah', 'daerah.id_daerah', '=', 'industrial_site_new.id_daerah')
     ->select('sde.daerah.*','sde.industrial_site_new.*')
     
           ->where('sde.industrial_site_new.id_ibukota_provinsi', '=', $id )
       ->distinct()->limit(1);
         
       
      $queryz = DB::table('industrial_site_new')
      ->join('sde.daerah', 'daerah.id_daerah', '=', 'industrial_site_new.id_daerah')
      ->join('sde.lokasi', 'daerah.id_lokasi', '=', 'lokasi.id_lokasi')   
      ->select('sde.daerah.*','sde.industrial_site_new.*','sde.lokasi.*')
      ->where('sde.industrial_site_new.id_ibukota_provinsi', '=', $id )
  
   ->distinct();

        
      $queryzz = DB::table('sde.industrial_site_new')
->join('sde.daerah', 'daerah.id_daerah', '=', 'industrial_site_new.id_daerah')
      ->join('sde.kawasan_industri_tr', 'industrial_site_new.id_kawasan_industri', '=', 'kawasan_industri_tr.id_kawasan_industri')
     
      ->join('sde.lokasi', 'daerah.id_lokasi', '=', 'lokasi.id_lokasi')      ->join('sde.daerah_tr', 'daerah.id_daerah', '=', 'daerah_tr.id_daerah')
      ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
      ->select('sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.*','sde.industrial_site_new.*','sde.kawasan_industri_tr.*')

      
  ->distinct();
    
    
    $bentuk = $bentukz->get();
    $kota = $kotaz->get();
    $kabupaten = $queryz->get();
    $projecs = $queryz->get();
    $propertiesx = $daerah->get();
    $pages = 'industri';
    return view('industridetail', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs, "propertiesx" => $propertiesx , "kabupaten" => $kabupaten ]);
}
//    Akhir Coding 

public function infrastrukturdetail($id) {

    $daerah = DB::table('sde.info_peluang_da')
    ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
    ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
    ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
    ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
    ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
    ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
    ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
    ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
    ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
    ->where('info_peluang_da.status', '=', 2 )
    ->where('sde.daerah.id_parent', '=', $id )
    
    ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
    ->where('peluang_tr.kode_bahasa', '=', 'id' )
    ->where('daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
    
    $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.tata_ruang.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

     $kotaz = DB::table('daerah_tr')
     ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
     ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
     
      ->where('sde.daerah_tr.kode_bahasa', '=', 'en' )
      ->where('sde.daerah.id_bentuk_daerah', '=', 3)
      ->where('sde.daerah.id_parent', '=', $id )
      ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'en' )
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
      ->orderBy('daerah.id_parent','ASC')
      ->orderBy('daerah_tr.nama','ASC')->distinct()->limit(1);

       
      $queryz =  DB::table('daerah_tr')
      ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
      ->join('sde.bandara', 'daerah.id_daerah', '=', 'bandara.id_daerah')
      ->join('sde.bandara_tr', 'bandara_tr.id_bandara', '=', 'bandara.id_bandara')
      ->join('sde.sumber', 'bandara.id_sumber', '=', 'sumber.id_sumber')
      ->join('sde.sumber_tr', 'sumber.id_sumber', '=', 'sumber_tr.id_sumber')
      ->join('sde.lokasi', 'bandara.id_lokasi', '=', 'lokasi.id_lokasi')
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bandara.*','sde.bandara_tr.*','sde.lokasi.*','sde.sumber.*','sde.sumber_tr.instansi')
       ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
       ->where('sde.bandara_tr.kode_bahasa', '=', 'id' )
       ->where('sde.sumber_tr.kode_bahasa', '=', 'id' )
       ->where('sde.daerah.id_parent', '=', $id )
       ->where('sde.bandara.status','=', 2)
       ->orderBy('daerah.id_parent','ASC')
       ->orderBy('daerah_tr.nama','ASC')->distinct();
       $queryOrder = "CASE WHEN sde.hotel.kelas = 'Bintang 5' THEN 1 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 4' THEN 2 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 3' THEN 3 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 2' THEN 4 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 1' THEN 5 ";
       $queryOrder .= "ELSE 6 END";
       $hotelz =  DB::table('daerah_tr')
       ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
       ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
       ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
       ->join('sde.sumber', 'hotel.id_sumber', '=', 'sumber.id_sumber')
       ->join('sde.sumber_tr', 'sumber.id_sumber', '=', 'sumber_tr.id_sumber')
       ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
       ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*','sde.sumber.*','sde.sumber_tr.instansi')
        ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
         ->where('sde.sumber_tr.kode_bahasa', '=', 'id' )
        ->where('sde.daerah.id_parent', '=', $id )
        ->where('sde.hotel.status','=', 2)
    
        ->orderByRaw($queryOrder);

        $hotelzz =  DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
        ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
        ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_parent', '=', $id )
         ->where('sde.hotel.status','=', 2);
     
         
         $pendidikanz =  DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.pendidikan', 'daerah.id_daerah', '=', 'pendidikan.id_daerah')
         ->join('sde.pendidikan_tr', 'pendidikan.id_pendidikan', '=', 'pendidikan_tr.id_pendidikan')
         ->join('sde.sumber', 'pendidikan.id_sumber', '=', 'sumber.id_sumber')
         ->join('sde.sumber_tr', 'sumber.id_sumber', '=', 'sumber_tr.id_sumber')
         ->join('sde.lokasi', 'pendidikan.id_lokasi', '=', 'lokasi.id_lokasi')
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.pendidikan.*','sde.pendidikan_tr.*','sde.lokasi.*','sde.sumber.*','sde.sumber_tr.instansi')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sde.sumber_tr.kode_bahasa', '=', 'id' )
          ->where('sde.pendidikan_tr.kode_bahasa', '=', 'id' )
          ->where('sde.daerah.id_parent', '=', $id )
          ->where('sde.pendidikan.status','=', 2)->distinct();
          

          $pelabuhanz =  DB::table('daerah_tr')
          ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->join('sde.pelabuhan', 'daerah.id_daerah', '=', 'pelabuhan.id_daerah')
          ->join('sde.pelabuhan_tr', 'pelabuhan.id_pelabuhan', '=', 'pelabuhan_tr.id_pelabuhan')
          ->join('sde.sumber', 'pelabuhan.id_sumber', '=', 'sumber.id_sumber')
          ->join('sde.sumber_tr', 'sumber.id_sumber', '=', 'sumber_tr.id_sumber')
          ->join('sde.lokasi', 'pelabuhan.id_lokasi', '=', 'lokasi.id_lokasi')
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.pelabuhan.*','sde.pelabuhan_tr.*','sde.lokasi.*','sde.sumber.*','sde.sumber_tr.instansi')
           ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
           ->where('sde.pelabuhan_tr.kode_bahasa', '=', 'id' )
           ->where('sde.sumber_tr.kode_bahasa', '=', 'id' )
           ->where('sde.daerah.id_parent', '=', $id )
           ->where('sde.pelabuhan.status','=', 2)->distinct();

           $rumahsakitz =  DB::table('daerah_tr')
           ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
           ->join('sde.rumah_sakit', 'daerah.id_daerah', '=', 'rumah_sakit.id_daerah')
           ->join('sde.rumah_sakit_tr', 'rumah_sakit.id_rumah_sakit', '=', 'rumah_sakit_tr.id_rumah_sakit')
           ->join('sde.sumber', 'rumah_sakit.id_sumber', '=', 'sumber.id_sumber')
          ->join('sde.sumber_tr', 'sumber.id_sumber', '=', 'sumber_tr.id_sumber')
           ->join('sde.lokasi', 'rumah_sakit.id_lokasi', '=', 'lokasi.id_lokasi')
           ->select('sde.daerah.*','sde.daerah_tr.*','sde.rumah_sakit.*','sde.rumah_sakit_tr.*','sde.lokasi.*','sde.sumber_tr.instansi','sde.sumber.*')
            ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
            ->where('sde.rumah_sakit_tr.kode_bahasa', '=', 'id' )
            ->where('sde.sumber_tr.kode_bahasa', '=', 'id' )
            ->where('sde.daerah.id_parent', '=', $id )
            ->where('sde.rumah_sakit.status','=', 2)->distinct();

            
            
    $bentuk = $bentukz->get();
    $kota = $kotaz->get();
    $projecs = $queryz->get();
    $hotel = $hotelz->get();
    $pendidikan = $pendidikanz->get();
    $pelabuhan = $pelabuhanz->get();
    $rumahsakit = $rumahsakitz->get();
    $hotel2 = $hotelzz->get();
    $propertiesx = $daerah->get();
    $pages = 'infra';
    return view('infradetail', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs, "propertiesx" => $propertiesx, "hotel" => $hotel , "hotel2" => $hotel2 
    , "pendidikan" => $pendidikan , "pelabuhan" => $pelabuhan  , "rumahsakit" => $rumahsakit]);
    
}

public function databandara()
{

    $rumahsakitz =  DB::Select("SELECT 
    bandara.id_bandara AS bes,
    daerah.id_parent,
    bandara.id_daerah,
    bandara.id_lokasi,
    bandara.id_sumber,
    daerah_tr.nama AS nama_kabupaten,
    bandara_tr.nama,
    bandara_tr.alamat,
    bandara.telepon,
    bandara.fax,
    bandara.kelas,
    bandara.jam_operasional,
    bandara.iata,
    bandara.link,
    bandara.status,
    bandara.jenis_pesawat,
    bandara.maskapai,
    bandara.kategori,
    bandara.jarak_dari_ibukota_prov,
    lokasi.x,
    lokasi.y,
    daerah_tr.kode_bahasa
  FROM
    bandara
    INNER JOIN lokasi ON (lokasi.id_lokasi = bandara.id_lokasi)
    INNER JOIN daerah_tr ON (daerah_tr.id_daerah = bandara.id_daerah)
    INNER JOIN daerah ON (daerah.id_daerah = daerah_tr.id_daerah)
    INNER JOIN bandara_tr ON (bandara_tr.id_bandara = bandara.id_bandara)
  WHERE
    daerah_tr.kode_bahasa = 'id' ::bpchar AND 
    bandara_tr.kode_bahasa = 'id' ::bpchar 
    
  
  ");
  
    return Datatables::of($rumahsakitz)
    ->editColumn('action', function ($rumahsakitz) {
        $btn = '<button class="zink" id="banz'.$rumahsakitz->bes.'" onclick="bandara'.$rumahsakitz->bes.'()">Zoom Map</button>';
    
        return $btn;
})

    ->make(true);
}

public function datapendidikan()
{

    $pendidikan =  DB::Select("SELECT *
    FROM
      sde.pendidikan_tr
      INNER JOIN sde.pendidikan ON (sde.pendidikan_tr.id_pendidikan = sde.pendidikan.id_pendidikan)
      INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.pendidikan.id_daerah)
      INNER JOIN sde.sumber ON (sde.pendidikan.id_sumber = sde.sumber.id_sumber)
      INNER JOIN sde.lokasi ON (sde.lokasi.id_lokasi = sde.pendidikan.id_lokasi)
    WHERE
      pendidikan_tr.kode_bahasa = 'id' ::bpchar
    ORDER BY
      (CASE WHEN pendidikan.kategori = 'Negeri' THEN 1 WHEN pendidikan.kategori = 'Swasta' THEN 2 ELSE 3 END)
   
  
  ");
  
    return Datatables::of($pendidikan)
    ->editColumn('action', function ($pendidikan) {
        $btn = '<button class="zink" id="banz'.$pendidikan->id_pendidikan.'" onclick="pendidikanz'.$pendidikan->id_pendidikan.'()">Zoom Map</button>';
    
        return $btn;
})

    ->make(true);
}

public function datarumahsakit()
{

    $rumahsakitz =  DB::Select("SELECT *
    FROM
      sde.rumah_sakit_tr
      INNER JOIN sde.rumah_sakit ON (sde.rumah_sakit_tr.id_rumah_sakit = sde.rumah_sakit.id_rumah_sakit)
      INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.rumah_sakit.id_daerah)
      INNER JOIN sde.lokasi ON (sde.lokasi.id_lokasi = sde.rumah_sakit.id_lokasi)
    WHERE
      sde.rumah_sakit_tr.kode_bahasa = 'id'
      ORDER BY
      (CASE WHEN rumah_sakit.kategori = 'Negeri' THEN 1 WHEN rumah_sakit.kategori = 'Swasta' THEN 2 ELSE 3 END)
    
  
    
  
  ");
  
    return Datatables::of($rumahsakitz)
    ->editColumn('action', function ($rumahsakitz) {
        $btn = '<button class="zink" id="banz'.$rumahsakitz->id_rumah_sakit.'" onclick="rumahsakit'.$rumahsakitz->id_rumah_sakit.'()">Zoom Map</button>';
    
        return $btn;
})

    ->make(true);
}

public function datapelabuhan()
{

    $pelabuhan =  DB::Select("SELECT 
    pelabuhan.id_pelabuhan AS bes,
    pelabuhan.id_lokasi,
    daerah.id_parent,
    pelabuhan.id_daerah,
    pelabuhan.id_sumber,
    daerah_tr.nama AS nama_kabupaten,
    pelabuhan_tr.nama,
    pelabuhan_tr.alamat,
    pelabuhan.panjang_dermaga,
    pelabuhan.kedalaman,
    pelabuhan.link,
    pelabuhan.telepon,
    pelabuhan.fax,
    pelabuhan.status,
    pelabuhan.kelas,
    pelabuhan.fungsi,
    pelabuhan_tr.keterangan,
    lokasi.x,
    lokasi.y,
    daerah_tr.kode_bahasa
  FROM
    pelabuhan
    INNER JOIN pelabuhan_tr ON (pelabuhan_tr.id_pelabuhan = pelabuhan.id_pelabuhan)
    INNER JOIN lokasi ON (lokasi.id_lokasi = pelabuhan.id_lokasi)
    INNER JOIN daerah_tr ON (daerah_tr.id_daerah = pelabuhan.id_daerah)
    INNER JOIN daerah ON (daerah.id_daerah = pelabuhan.id_daerah)
  WHERE
    daerah_tr.kode_bahasa = 'id' ::bpchar AND 
    pelabuhan_tr.kode_bahasa = 'id' ::bpchar

    
  
  ");
  
    return Datatables::of($pelabuhan)
    ->editColumn('action', function ($pelabuhan) {
        $btn = '<button class="zink" id="banz'.$pelabuhan->bes.'" onclick="pelabuhan'.$pelabuhan->bes.'()">Zoom Map</button>';
    
        return $btn;
})

    ->make(true);
}


public function datahotel()
{

   
    $hotel =  DB::Select("SELECT 

    hotel_tr.nama,
    hotel.kelas AS kelas,
    hotel_tr.alamat,
    hotel.telp,
    hotel.fax,
    hotel_tr.kode_bahasa,
    hotel.email,
    hotel.status,
    hotel_tr.keterangan,
    hotel.id_hotel AS bes,
    hotel.id_daerah,
    hotel.id_sumber,
    hotel.operator_entri,
    hotel.tgl_entri,
    lokasi.x AS lat,
    lokasi.y AS long
 
  FROM
    hotel
    INNER JOIN lokasi ON (hotel.id_lokasi = lokasi.id_lokasi)
    INNER JOIN hotel_tr ON (hotel.id_hotel = hotel_tr.id_hotel)

    WHERE hotel_tr.kode_bahasa = 'id'
    ORDER BY 
    (CASE WHEN sde.hotel.kelas = 'Bintang 5' THEN 1
    WHEN sde.hotel.kelas = 'Bintang 4' THEN 2
    WHEN sde.hotel.kelas = 'Bintang 3' THEN 3
    WHEN sde.hotel.kelas = 'Bintang 2' THEN 4
    WHEN sde.hotel.kelas = 'Bintang 1' THEN 5
    ELSE 6 END)
  
  
    
  
  ");
  
    return Datatables::of($hotel)
    ->editColumn('action', function ($hotel) {
        $btn = '<button class="zink" id="banz'.$hotel->bes.'" onclick="hotel'.$hotel->bes.'()">Zoom Map</button>';
     
        return $btn;
})

    ->make(true);
}
public function Apifunction() {


    
      $queryz =  DB::Select("SELECT 
      bandara.id_bandara,
      daerah.id_parent,
      bandara.id_daerah,
      bandara.id_lokasi,
      bandara.id_sumber,
      daerah_tr.nama AS nama_kabupaten,
      bandara_tr.nama,
      bandara_tr.alamat,
      bandara.telepon,
      bandara.fax,
      bandara.kelas,
      bandara.jam_operasional,
      bandara.iata,
      bandara.link,
      bandara.status,
      bandara.jenis_pesawat,
      bandara.maskapai,
      bandara.kategori,
      bandara.jarak_dari_ibukota_prov,
      lokasi.x,
      lokasi.y,
      daerah_tr.kode_bahasa
    FROM
      bandara
      INNER JOIN lokasi ON (lokasi.id_lokasi = bandara.id_lokasi)
      INNER JOIN daerah_tr ON (daerah_tr.id_daerah = bandara.id_daerah)
      INNER JOIN daerah ON (daerah.id_daerah = daerah_tr.id_daerah)
      INNER JOIN bandara_tr ON (bandara_tr.id_bandara = bandara.id_bandara)
    WHERE
      daerah_tr.kode_bahasa = 'id' ::bpchar AND 
      bandara_tr.kode_bahasa = 'id' ::bpchar 
      
    
    ");
     

        $hotelzz =  DB::Select("SELECT 
        hotel_tr.nama,
        hotel.kelas AS kelas,
        hotel_tr.alamat,
        hotel.telp,
        hotel.fax,
        hotel.email,
        hotel.status,
        hotel_tr.keterangan,
        hotel.id_hotel,
        hotel.id_daerah,
        hotel.id_sumber,
        hotel.operator_entri,
        hotel.tgl_entri,
        lokasi.x ,
        lokasi.y 
      FROM
        hotel
        INNER JOIN lokasi ON (hotel.id_lokasi = lokasi.id_lokasi)
        INNER JOIN hotel_tr ON (hotel.id_hotel = hotel_tr.id_hotel) 
         
      ");

     
         
         $pendidikanz =  DB::Select("SELECT *
         FROM
           sde.pendidikan_tr
           INNER JOIN sde.pendidikan ON (sde.pendidikan_tr.id_pendidikan = sde.pendidikan.id_pendidikan)
           INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.pendidikan.id_daerah)
           INNER JOIN sde.sumber ON (sde.pendidikan.id_sumber = sde.sumber.id_sumber)
           INNER JOIN sde.lokasi ON (sde.lokasi.id_lokasi = sde.pendidikan.id_lokasi)
         WHERE
           pendidikan_tr.kode_bahasa = 'id' ::bpchar
         ORDER BY
           (CASE WHEN pendidikan.kategori = 'Negeri' THEN 1 WHEN pendidikan.kategori = 'Swasta' THEN 2 ELSE 3 END)
            
            
             ");
       
          

          $pelabuhanz =  DB::Select("SELECT 
          pelabuhan.id_pelabuhan,
          pelabuhan.id_lokasi,
          daerah.id_parent,
          pelabuhan.id_daerah,
          pelabuhan.id_sumber,
          daerah_tr.nama AS nama_kabupaten,
          pelabuhan_tr.nama,
          pelabuhan_tr.alamat,
          pelabuhan.panjang_dermaga,
          pelabuhan.kedalaman,
          pelabuhan.link,
          pelabuhan.telepon,
          pelabuhan.fax,
          pelabuhan.status,
          pelabuhan.kelas,
          pelabuhan.fungsi,
          pelabuhan_tr.keterangan,
          lokasi.x,
          lokasi.y,
          daerah_tr.kode_bahasa
        FROM
          pelabuhan
          INNER JOIN pelabuhan_tr ON (pelabuhan_tr.id_pelabuhan = pelabuhan.id_pelabuhan)
          INNER JOIN lokasi ON (lokasi.id_lokasi = pelabuhan.id_lokasi)
          INNER JOIN daerah_tr ON (daerah_tr.id_daerah = pelabuhan.id_daerah)
          INNER JOIN daerah ON (daerah.id_daerah = pelabuhan.id_daerah)
        WHERE
          daerah_tr.kode_bahasa = 'id' ::bpchar AND 
          pelabuhan_tr.kode_bahasa = 'id' ::bpchar
        ");


          

           $rumahsakitz =  DB::Select("SELECT *
           FROM
             sde.rumah_sakit_tr
             INNER JOIN sde.rumah_sakit ON (sde.rumah_sakit_tr.id_rumah_sakit = sde.rumah_sakit.id_rumah_sakit)
             INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.rumah_sakit.id_daerah)
             INNER JOIN sde.lokasi ON (sde.lokasi.id_lokasi = sde.rumah_sakit.id_lokasi)
           WHERE
             sde.rumah_sakit_tr.kode_bahasa = 'id'
             ORDER BY
             (CASE WHEN rumah_sakit.kategori = 'Negeri' THEN 1 WHEN rumah_sakit.kategori = 'Swasta' THEN 2 ELSE 3 END)
           ");
           
            
 
    $projecs = $queryz;
    $pendidikan = $pendidikanz;
    $pelabuhan = $pelabuhanz;
    $rumahsakit = $rumahsakitz;
    $hotel = $hotelzz;
  
    return view('apikeyinfra',  ["projecs" => $projecs,"hotel" => $hotel ,
     "pendidikan" => $pendidikan , "pelabuhan" => $pelabuhan  , "rumahsakit" => $rumahsakit]);
    
}

public function infrastrukturnasional() {

    $daerah = DB::table('sde.info_peluang_da')
    ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
    ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
    ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
    ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
    ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
    ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
    ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
    ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
    ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
    ->where('info_peluang_da.status', '=', 2 )
    
    
    ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
    ->where('peluang_tr.kode_bahasa', '=', 'id' )
    ->where('daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
    
    $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
        
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.tata_ruang.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

     $kotaz = DB::table('daerah_tr')
     ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
     ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
     
      ->where('sde.daerah_tr.kode_bahasa', '=', 'en' )
      ->where('sde.daerah.id_bentuk_daerah', '=', 3)

      ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'en' )
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
      ->orderBy('daerah.id_parent','ASC')
      ->orderBy('daerah_tr.nama','ASC')->distinct()->limit(1);

    
      $queryz =  DB::Select("SELECT 
      bandara.id_bandara,
      daerah.id_parent,
      bandara.id_daerah,
      bandara.id_lokasi,
      bandara.id_sumber,
      daerah_tr.nama AS nama_kabupaten,
      bandara_tr.nama,
      bandara_tr.alamat,
      bandara.telepon,
      bandara.fax,
      bandara.kelas,
      bandara.jam_operasional,
      bandara.iata,
      bandara.link,
      bandara.status,
      bandara.jenis_pesawat,
      bandara.maskapai,
      bandara.kategori,
      bandara.jarak_dari_ibukota_prov,
      lokasi.x,
      lokasi.y,
      daerah_tr.kode_bahasa
    FROM
      bandara
      INNER JOIN lokasi ON (lokasi.id_lokasi = bandara.id_lokasi)
      INNER JOIN daerah_tr ON (daerah_tr.id_daerah = bandara.id_daerah)
      INNER JOIN daerah ON (daerah.id_daerah = daerah_tr.id_daerah)
      INNER JOIN bandara_tr ON (bandara_tr.id_bandara = bandara.id_bandara)
    WHERE
      daerah_tr.kode_bahasa = 'id' ::bpchar AND 
      bandara_tr.kode_bahasa = 'id' ::bpchar 
      
    
    ");
       $queryOrder = "CASE WHEN sde.hotel.kelas = 'Bintang 5' THEN 1 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 4' THEN 2 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 3' THEN 3 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 2' THEN 4 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 1' THEN 5 ";
       $queryOrder .= "ELSE 6 END";
       $hotelz =  DB::table('daerah_tr')
       ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
       ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
       ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
       ->join('sde.sumber', 'hotel.id_sumber', '=', 'sumber.id_sumber')
       ->join('sde.sumber_tr', 'sumber.id_sumber', '=', 'sumber_tr.id_sumber')
       ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
       ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*','sde.sumber.*','sde.sumber_tr.instansi')
        ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
         ->where('sde.sumber_tr.kode_bahasa', '=', 'id' )
      
        ->where('sde.hotel.status','=', 2)
    
        ->orderByRaw($queryOrder);

        $hotelzz =  DB::Select("SELECT 
        hotel_tr.nama,
        hotel.kelas AS kelas,
        hotel_tr.alamat,
        hotel.telp,
        hotel.fax,
        hotel.email,
        hotel.status,
        hotel_tr.keterangan,
        hotel.id_hotel,
        hotel.id_daerah,
        hotel.id_sumber,
        hotel.operator_entri,
        hotel.tgl_entri,
        lokasi.x ,
        lokasi.y 
      FROM
        hotel
        INNER JOIN lokasi ON (hotel.id_lokasi = lokasi.id_lokasi)
        INNER JOIN hotel_tr ON (hotel.id_hotel = hotel_tr.id_hotel) 
         
      ");

     
         
         $pendidikanz =  DB::Select("SELECT *
         FROM
           sde.pendidikan_tr
           INNER JOIN sde.pendidikan ON (sde.pendidikan_tr.id_pendidikan = sde.pendidikan.id_pendidikan)
           INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.pendidikan.id_daerah)
           INNER JOIN sde.sumber ON (sde.pendidikan.id_sumber = sde.sumber.id_sumber)
           INNER JOIN sde.lokasi ON (sde.lokasi.id_lokasi = sde.pendidikan.id_lokasi)
         WHERE
           pendidikan_tr.kode_bahasa = 'id' ::bpchar
         ORDER BY
           (CASE WHEN pendidikan.kategori = 'Negeri' THEN 1 WHEN pendidikan.kategori = 'Swasta' THEN 2 ELSE 3 END)
            
            
             ");
       
          

          $pelabuhanz =  DB::Select("SELECT 
          pelabuhan.id_pelabuhan,
          pelabuhan.id_lokasi,
          daerah.id_parent,
          pelabuhan.id_daerah,
          pelabuhan.id_sumber,
          daerah_tr.nama AS nama_kabupaten,
          pelabuhan_tr.nama,
          pelabuhan_tr.alamat,
          pelabuhan.panjang_dermaga,
          pelabuhan.kedalaman,
          pelabuhan.link,
          pelabuhan.telepon,
          pelabuhan.fax,
          pelabuhan.status,
          pelabuhan.kelas,
          pelabuhan.fungsi,
          pelabuhan_tr.keterangan,
          lokasi.x,
          lokasi.y,
          daerah_tr.kode_bahasa
        FROM
          pelabuhan
          INNER JOIN pelabuhan_tr ON (pelabuhan_tr.id_pelabuhan = pelabuhan.id_pelabuhan)
          INNER JOIN lokasi ON (lokasi.id_lokasi = pelabuhan.id_lokasi)
          INNER JOIN daerah_tr ON (daerah_tr.id_daerah = pelabuhan.id_daerah)
          INNER JOIN daerah ON (daerah.id_daerah = pelabuhan.id_daerah)
        WHERE
          daerah_tr.kode_bahasa = 'id' ::bpchar AND 
          pelabuhan_tr.kode_bahasa = 'id' ::bpchar
        ");


          

           $rumahsakitz =  DB::Select("SELECT *
           FROM
             sde.rumah_sakit_tr
             INNER JOIN sde.rumah_sakit ON (sde.rumah_sakit_tr.id_rumah_sakit = sde.rumah_sakit.id_rumah_sakit)
             INNER JOIN sde.daerah ON (sde.daerah.id_daerah = sde.rumah_sakit.id_daerah)
             INNER JOIN sde.lokasi ON (sde.lokasi.id_lokasi = sde.rumah_sakit.id_lokasi)
           WHERE
             sde.rumah_sakit_tr.kode_bahasa = 'id'
             ORDER BY
             (CASE WHEN rumah_sakit.kategori = 'Negeri' THEN 1 WHEN rumah_sakit.kategori = 'Swasta' THEN 2 ELSE 3 END)
           ");
           
            
    $bentuk = $bentukz->paginate(1);
    $kota = $kotaz->paginate(1);
    $projecs = $queryz;
    $hotel2 = $hotelz->paginate(1);
    $pendidikan = $pendidikanz;
    $pelabuhan = $pelabuhanz;
    $rumahsakit = $rumahsakitz;
    $hotel = $hotelzz;
    $propertiesx = 'aa';
    $pages = 'infra';
    return view('infranasional', compact('pages'), ["bentuk" => $bentuk, "kota" => $kota , "projecs" => $projecs, "propertiesx" => $propertiesx, "hotel" => $hotel , "hotel2" => $hotel2 
    , "pendidikan" => $pendidikan , "pelabuhan" => $pelabuhan  , "rumahsakit" => $rumahsakit]);
    
}


public function infraAPI($id) {

    $daerah = DB::table('sde.info_peluang_da')
    ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
    ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
    ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
    ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
    ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
    ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
    ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
    ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
    ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
    ->where('info_peluang_da.status', '=', 2 )
    ->where('sde.daerah.id_parent', '=', $id )
    
    ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
    ->where('peluang_tr.kode_bahasa', '=', 'id' )
    ->where('daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
    ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct()->limit(1);
    
    $bentukz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.tata_ruang', 'tata_ruang.id_daerah', '=', 'daerah.id_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_bentuk_daerah', '=', 2)
         ->where('sde.daerah.id_daerah', '=', $id)
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.tata_ruang.*')
         ->orderBy('daerah_tr.nama','ASC')->distinct();

     $kotaz = DB::table('daerah_tr')
     ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
     ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
     
      ->where('sde.daerah_tr.kode_bahasa', '=', 'en' )
      ->where('sde.daerah.id_bentuk_daerah', '=', 3)
      ->where('sde.daerah.id_parent', '=', $id )
      ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'en' )
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
      ->orderBy('daerah.id_parent','ASC')
      ->orderBy('daerah_tr.nama','ASC')->distinct()->limit(1);

       
      $queryz =  DB::table('daerah_tr')
      ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
      ->join('sde.bandara', 'daerah.id_daerah', '=', 'bandara.id_daerah')
      ->join('sde.bandara_tr', 'bandara_tr.id_bandara', '=', 'bandara.id_bandara')
      ->join('sde.lokasi', 'bandara.id_lokasi', '=', 'lokasi.id_lokasi')
      ->select('sde.daerah.*','sde.daerah_tr.*','sde.bandara.*','sde.bandara_tr.*','sde.lokasi.*')
       ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
       ->where('sde.bandara_tr.kode_bahasa', '=', 'id' )
       ->where('sde.daerah.id_parent', '=', $id )
       ->where('sde.bandara.status','=', 2)
       ->orderBy('daerah.id_parent','ASC')
       ->orderBy('daerah_tr.nama','ASC')->distinct();
       $queryOrder = "CASE WHEN sde.hotel.kelas = 'Bintang 5' THEN 1 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 4' THEN 2 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 3' THEN 3 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 2' THEN 4 ";
       $queryOrder .= "WHEN sde.hotel.kelas = 'Bintang 1' THEN 5 ";
       $queryOrder .= "ELSE 6 END";
       $hotelz =  DB::table('daerah_tr')
       ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
       ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
       ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
       ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
       ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*')
        ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
        ->where('sde.daerah.id_parent', '=', $id )
        ->where('sde.hotel.status','=', 2)
    
        ->orderByRaw($queryOrder);

        $hotelzz =  DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.hotel', 'daerah.id_daerah', '=', 'hotel.id_daerah')
        ->join('sde.hotel_tr', 'hotel.id_hotel', '=', 'hotel_tr.id_hotel')
        ->join('sde.lokasi', 'hotel.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('sde.daerah.*','sde.daerah_tr.*','sde.hotel.*','sde.hotel_tr.*','sde.lokasi.*')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.hotel_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_parent', '=', $id )
         ->where('sde.hotel.status','=', 2);
     
         
         $pendidikanz =  DB::table('daerah_tr')
         ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
         ->join('sde.pendidikan', 'daerah.id_daerah', '=', 'pendidikan.id_daerah')
         ->join('sde.pendidikan_tr', 'pendidikan.id_pendidikan', '=', 'pendidikan_tr.id_pendidikan')
         ->join('sde.lokasi', 'pendidikan.id_lokasi', '=', 'lokasi.id_lokasi')
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.pendidikan.*','sde.pendidikan_tr.*','sde.lokasi.*')
          ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
          ->where('sde.pendidikan_tr.kode_bahasa', '=', 'id' )
          ->where('sde.daerah.id_parent', '=', $id )
          ->where('sde.pendidikan.status','=', 2)->distinct();
          

          $pelabuhanz =  DB::table('daerah_tr')
          ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
          ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
          ->join('sde.pelabuhan', 'daerah.id_daerah', '=', 'pelabuhan.id_daerah')
          ->join('sde.pelabuhan_tr', 'pelabuhan.id_pelabuhan', '=', 'pelabuhan_tr.id_pelabuhan')
          ->join('sde.lokasi', 'pelabuhan.id_lokasi', '=', 'lokasi.id_lokasi')
          ->select('sde.daerah.*','sde.daerah_tr.*','sde.pelabuhan.*','sde.pelabuhan_tr.*','sde.lokasi.*')
           ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
           ->where('sde.pelabuhan_tr.kode_bahasa', '=', 'id' )
           ->where('sde.daerah.id_parent', '=', $id )
           ->where('sde.pelabuhan.status','=', 2)->distinct();

           $rumahsakitz =  DB::table('daerah_tr')
           ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
           ->join('sde.rumah_sakit', 'daerah.id_daerah', '=', 'rumah_sakit.id_daerah')
           ->join('sde.rumah_sakit_tr', 'rumah_sakit.id_rumah_sakit', '=', 'rumah_sakit_tr.id_rumah_sakit')
           ->join('sde.lokasi', 'rumah_sakit.id_lokasi', '=', 'lokasi.id_lokasi')
           ->select('sde.daerah.*','sde.daerah_tr.*','sde.rumah_sakit.*','sde.rumah_sakit_tr.*','sde.lokasi.*')
            ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
            ->where('sde.rumah_sakit_tr.kode_bahasa', '=', 'id' )
            ->where('sde.daerah.id_parent', '=', $id )
            ->where('sde.rumah_sakit.status','=', 2)->distinct();

        
    
    $bentuk = $bentukz->get();
    $kota = $kotaz->get();
    $projecs = $queryz->paginate(6);
    $hotel = $hotelz->paginate(10);
    $pendidikan = $pendidikanz->paginate(6);
    $pelabuhan = $pelabuhanz->get();
    $rumahsakit = $rumahsakitz->paginate(6);
    $hotel2 = $hotelzz->get();
    $propertiesx = $daerah->get();
    $pages = 'infra';

  
    return Response::json($pelabuhan);
}
    
    public function allow() {
        $pages = 'master';
        return view('allow', compact('pages'));
    }

    public function super() {
        $pages = 'master';
        return view('super', compact('pages'));
    }
    
    public function getKotaByProvinsi($provinsiId)
	{
        $queryz = DB::table('daerah_tr')
        ->join('sde.daerah', 'daerah_tr.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
         ->where('sde.daerah_tr.kode_bahasa', '=', 'id' )
         ->where('sde.daerah.id_parent', '=', $provinsiId )
         ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
      
         ->select('sde.daerah.*','sde.daerah_tr.*','sde.bentuk_daerah_tr.bentuk_daerah')
         ->orderBy('daerah.id_parent','ASC')
         ->orderBy('daerah_tr.nama','ASC')->distinct()->get();


		return Response::json($queryz);
	} 
	
	/**
     * Gallery Page.
     *
     */
    public function gallery() {
        $categories =  DB::table('info_peluang_da')
        ->join('info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*')
        ->get();
        $keywords = "";
		
        if (!empty(Input::get("keywords"))) {
            $keywords = Input::get("keywords");
        }


        $type = "";
        if (!empty(Input::get("type"))) {
            $type = Input::get("type");
        }

        $category = "";
        if (!empty(Input::get("category"))) {
            $category = Input::get("category");
        }

        $bed = "";
        if (!empty(Input::get("bed"))) {
            $bed = Input::get("bed");
        }
        $bath = "";
        if (!empty(Input::get("bath"))) {
            $bath = Input::get("bath");
        }

        $min = 0;
        if (!empty(Input::get("min"))) {
            $min = Input::get("min");
        }
        $max = 0;
        if (!empty(Input::get("max"))) {
            $max = Input::get("max");
        }
        $output="";
       
        $query = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang', 'peluang.id_sektor_peluang', '=', 'sektor_peluang.id_sektor_peluang')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','lokasi.*','peluang_tr.*','peluang.status','peluang.*')
        ->where('peluang.status', '=', 2 )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )->distinct();
     
        if (!empty($keywords)) {
            $query->where('info_peluang_da_tr.judul', 'like', "%$keywords%");
            
          
        }
		
		

        if (!empty($min) and ! empty($max)) {
            $query->where('price', '>=', $min);
            $query->where('price', '<=', $max);
        }


        if (!empty($bed)) {
            $query->where('beds', $bed);
        }

        if (!empty($bath)) {
            $query->where('bath', $bath);
        }

        if (!empty($type)) {
            $query->where('type', $type);
        }
        
        if (!empty($category)) {
            $query->where('id_sektor_peluang', $category);
        }

        $forms = array(
            "keywords" => $keywords,
           "category" => $category,
            "type" => $type,
            "max" => $max,
            "min" => $min,
            "bath" => $bath,
            "bed" => $bed
        );



        $order = "new";
        $orderby = "id";
        $ordertype = "desc";
        if (!empty(Input::get("order"))) {
            $order = Input::get("order");

            if ($order == "priceh") {
                $orderby = "price";
                $ordertype = "desc";
            }

            if ($order == "pricel") {
                $orderby = "price";
                $ordertype = "asc";
            }
        }

        $propertiesz = $query->get();
        
        $propertiesx = $query->paginate(12);
        $categories = Category::get();
       
        return view('pages.gallery', ['propertiesx' => $propertiesx,'propertiesz' => $propertiesz,'categories' => $categories,"forms" => $forms]);
      

 
    }

    public function sendEmail(Request $request,$id)
    {
        
        try{
              
        $query = DB::table('sde.info_peluang_da')
       
        ->join('sde.info_peluang_da_tr', 'info_peluang_da.id_info_peluang_da', '=', 'info_peluang_da_tr.id_info_peluang_da')
        ->join('sde.lokasi', 'info_peluang_da.id_lokasi', '=', 'lokasi.id_lokasi')
        ->join('sde.peluang_tr', 'info_peluang_da.id_peluang', '=', 'peluang_tr.id_peluang')
        ->join('sde.peluang', 'info_peluang_da.id_peluang', '=', 'peluang.id_peluang')
        ->join('sde.sektor_peluang_tr', 'peluang.id_sektor_peluang', '=', 'sektor_peluang_tr.id_sektor_peluang')
        ->join('sde.daerah', 'info_peluang_da.id_daerah', '=', 'daerah.id_daerah')
        ->join('sde.daerah_tr', 'info_peluang_da.id_daerah', '=', 'daerah_tr.id_daerah')
        ->join('sde.bentuk_daerah_tr', 'daerah.id_bentuk_daerah', '=', 'bentuk_daerah_tr.id_bentuk_daerah')
        ->select('info_peluang_da_tr.*',  'info_peluang_da.*','sektor_peluang_tr.*','sde.lokasi.*','sde.daerah.*','sde.daerah_tr.*','sde.peluang.status','sde.bentuk_daerah_tr.*')
        ->where('info_peluang_da.status', '=', 2 )
        ->where('info_peluang_da.id_info_peluang_da', '=', $id )
        ->where('info_peluang_da_tr.kode_bahasa', '=', 'id' )
        ->where('peluang_tr.kode_bahasa', '=', 'id' )
        ->where('daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sde.bentuk_daerah_tr.kode_bahasa', '=', 'id' )
        ->where('sektor_peluang_tr.kode_bahasa', '=', 'id' )->distinct();
      
     

        $propertiesz = $query->get();
        

        $data_contact = array(
            "email" => $request->input("email"),
            "phone" => $request->input("phone"),
            "id_daerah" => $request->input("daerah")
        
           
        );

        
         DB::table("history_unduh")->insert($data_contact);
       
     
            Mail::send('email', ['nama' => $request->nama, 'pesan' => $request->pesan, 'propertiesz' => $propertiesz], function ($message) use ($request)
            {
                $message->subject('Investement Opportunity');
                $message->from('donotreply@kiddy.com', 'Regional Investement BKPM');
                $message->to($request->email);
            });
            return back()->with('alert-success','Berhasil Kirim Email');
        }
        catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }

      
    }
    public function send_request(Request $request) {
        if ($request->input("property_id")) {
            $property_id = $request->input("property_id");
        } else {
            $property_id = 0;
        }

		$setting = get_setting();
        $email = $request->Input("email");
        $name = $request->Input("name");
        $msg = $request->Input("message");
        $phone = $request->Input("phone");

		$to_email = $setting->email;
        $data_contact = array(
            "property_id" => $property_id,
            "name" => $request->input("name"),
            "phone" => $request->input("phone"),
            "email" => $request->input("email"),
            "message" => $request->input("message")
        );
        $insert_id = DB::table("customers_request")->insertGetId($data_contact);
        $data = array('phone' => $phone, 'title' => 'Contact Request', 'email' => $email, 'name' => $name, 'msg' => $msg);
        Mail::send(
                'email_template', $data, function ($header) use ($email, $name ,$to_email ) {
            $header->from($email, $name);
            $header->to($to_email)->subject("Contact Request");
        }
        );


        return $insert_id;
    }
	
	function newsletter_subscription(Request $request) { 
		$email = $request->input("email");
		DB::table("newsletter")->insert(array("email" => $email));
	}

}