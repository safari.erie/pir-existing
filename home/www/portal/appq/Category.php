<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'sektor_peluang_tr';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
