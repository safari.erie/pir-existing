<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Rating_post_dt extends Model
{
    protected $table="rating_post_dt";
	protected $primaryKey = "rating_post_dt_id";
	public $timestamps = false;

    protected $fillable = [
        'rating_post_id', 'rating_id', 'rate'
    ];
	
	public function rating_post() {
		return $this->belongsTo('\App\Rating_post', 'rating_post_id', 'rating_post_id');
	}
	
	public function rating() {
		return $this->belongsTo('\App\Rating', 'rating_id', 'rating_id');
	}
}
