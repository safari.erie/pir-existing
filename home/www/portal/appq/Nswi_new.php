<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Nswi_new extends Model
{
    protected $table="nswi_new";
	protected $primaryKey = "id_nswi";
	public $timestamps = false;

    protected $fillable = [
        'id_daerah', 'id_sektor', 'jenis', 'proyek', 'investasi', 'jenis_data', 'tahun', 'status', 'id_sumber'
    ];
	
}
